<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/**
 * 医生
 */
Route::group( [ 'prefix' => 'c4', 'namespace' => 'doctorApp' ], function () {
    /**
     * 登陆
     */
    Route::group( [ 'namespace' => 'User', 'prefix' => 'user' ], function () {
        Route::post( 'login', 'LoginController@login' );
    } );
    /**
     * 接口
     */
    Route::group( [ 'middleware' => [ 'auth:doctor' ] ], function () {
        Route::group( [ 'namespace' => 'User', 'prefix' => 'user' ], function () {
            Route::post( 'loginOut', 'LoginController@loginOut' );//退出登录
        } );
        Route::group( [ 'namespace' => 'User', 'prefix' => 'user' ], function () {
            Route::get( 'index', 'IndexController@index' );
            Route::post( 'index', 'IndexController@edit' );
        } );

        Route::group( [ 'prefix' => 'common', 'namespace' => 'Common' ], function () {
            Route::post( 'uploadFile', 'UploadController@upload' );    //上传
        } );
    } );
} );
