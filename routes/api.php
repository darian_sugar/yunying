<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


/**
 * 支付
 */
Route::group( [ 'namespace' => 'Pay', 'prefix' => 'pay' ], function () {
    Route::any( 'aliNotify', 'IndexController@aliNotify' );
    Route::any( 'wxNotify', 'IndexController@wxNotify' );
    Route::any( 'test', 'IndexController@test' );
} );

Route::group( [ 'namespace' => 'Callback' ], function () {
    /**
     * 云信回调
     */
    Route::group( [ 'prefix' => 'copy' ], function () {
        Route::any( 'copyToSys', 'MessageController@index' );
    } );
    /**
     * 全局配置
     */
    Route::group( [ 'prefix' => 'config' ], function () {
        Route::any( 'list', 'ConfigController@index' );
    } );
    /**
     * 直播回调
     */
    Route::group( [ 'prefix' => 'live' ], function () {
        Route::any( 'index', 'LiveController@index' );
    } );

} );


/**
 * 用户APP
 */
Route::group( [ 'prefix' => 'c1', 'namespace' => 'App' ], function () {

    Route::group( [ 'prefix' => 'common', 'namespace' => 'V1\Common' ], function () {
        Route::post( 'uploadFile', 'IndexController@upload' );    //上传
        Route::get( 'banner', 'IndexController@banner' );         //轮播图
        Route::get( 'areaList', 'IndexController@areaList' );     //地域列表
    } );

    Route::group( [ 'prefix' => 'mongo', 'namespace' => 'V1\Mongo' ], function () {
        Route::get( 'mongoUser', 'MongoController@mongoUser' );      //mongo用户列表
        Route::post( 'addDB', 'MongoController@addDB' );             //mongo用户DB添加
        Route::post( 'addModel', 'MongoController@addModel' );       //mongo用户Model添加
    } );

    /**
     * 登陆注册
     */
    Route::group( [ 'prefix' => 'user', 'namespace' => 'V1\User' ], function () {
        Route::post( 'register', 'LoginController@register' );
        Route::get( 'smsCode', 'LoginController@smsCode' );
        Route::get( 'login', 'LoginController@login' );
        Route::get( 'qqLogin', 'QqLoginController@qqLogin' );
        Route::get( 'wxLogin', 'WxLoginController@wxLogin' );
        Route::post( 'updatePwdCode', 'LoginController@updatePwdCode' );   //根据验证码修改密码
    } );


    /**
     * 不需要token百科
     */
    Route::group( [ 'namespace' => 'V1\Wiki', 'prefix' => 'wiki' ], function () {
        Route::get( 'file_detail', 'KnowledgeController@detail' );
    } );


    Route::group( [ 'middleware' => [ 'auth:api', 'check.login' ] ], function () {

        Route::group( [ 'prefix' => 'common', 'namespace' => 'V1\Common' ], function () {
            Route::post( 'uploadFile', 'IndexController@upload' );          //上传
            Route::post( 'uploadBigFile', 'IndexController@uploadBigFile' );//上传
        } );
        Route::group( [ 'namespace' => 'V1\User' ], function () {
            /**
             * 个人中心
             */
            Route::group( [ 'prefix' => 'user' ], function () {
                Route::post( 'add', 'IndexController@add' );                                 //测试添加
                Route::get( 'userInfo', 'IndexController@userInfo' );                        //用户信息
                Route::get( 'account', 'IndexController@account' );                          //三方帐号信息
                Route::post( 'bindAccount', 'IndexController@bindAccount' );                 //绑定帐号
                Route::post( 'unbindAccount', 'IndexController@unbindAccount' );             //解绑帐号
                Route::post( 'updateUserInfo', 'IndexController@updateUserInfo' );           //修改用户信息
                Route::post( 'updateUserMobile', 'IndexController@updateUserMobile' );       //修改手机号
                Route::post( 'updatePwd', 'IndexController@updatePwd' );                     //根据旧密码修改密码
                Route::post( 'suggest', 'IndexController@suggest' );                         //反馈意见
                Route::get( 'myMessage', 'IndexController@myMessage' );                      //我的消息
                Route::get( 'myScale', 'IndexController@myScale' );                          //我的评估
                Route::get( 'myScaleDetail', 'IndexController@myScaleDetail' );              //我的评估详情
            } );

            /**
             * 地址
             */
            Route::group( [ 'prefix' => 'address' ], function () {
                Route::get( 'index', 'AddressController@index' );                 //地址列表
                Route::post( 'add', 'AddressController@add' );                    //添加地址
                Route::post( 'changeDefault', 'AddressController@changeDefault' );//默认值
                Route::post( 'delete', 'AddressController@delete' );              //默认值
            } );

            /**
             * 医生
             */
            Route::group( [ 'prefix' => 'doctor' ], function () {
                Route::get( 'index', 'DoctorController@index' );                           //医生列表
                Route::get( 'detail', 'DoctorController@detail' );                         //医生详情
                Route::get( 'getSchedule', 'DoctorController@getSchedule' );               //医生排班
                Route::get( 'getUsefulDate', 'DoctorController@getUsefulDate' );           //医生可预约日期

            } );
        } );


        // 经期，辣妈，育儿
        Route::group( [ 'namespace' => 'V1\HomePage', 'prefix' => 'homepage' ], function () {

            Route::get( 'test', 'IndexController@test' );                                 //测试
            Route::get( 'lastScaleResult', 'IndexController@lastScaleResult' );           //量表评估结果
            Route::get( 'todayKnowledge', 'IndexController@todayKnowledge' );             //今日知识

            Route::post( 'periodSet', 'PeriodController@periodSet' );                    // 经期设置
            Route::get( 'periodInfo', 'PeriodController@periodInfo' );                   //经期预测信息

            Route::post( 'pregnancySet', 'PregnancyController@pregnancySet' );             // 怀孕设置
            Route::get( 'pregnancyDecipher', 'PregnancyController@pregnancyDecipher' );    // 预产期说明
            Route::get( 'pregnancyInfo', 'PregnancyController@pregnancyInfo' );            // 预产期说明

            Route::post( 'hotMomSet', 'HotMomController@hotMomSet' );                   // 辣妈设置
            Route::get( 'myBaby', 'HotMomController@myBaby' );                          // 我的宝宝
            Route::post( 'editBaby', 'HotMomController@editBaby' );                     // 编辑宝宝
            Route::post( 'defaultBaby', 'HotMomController@defaultBaby' );               // 修改默认宝宝
            Route::post( 'deleteBaby', 'HotMomController@deleteBaby' );                 // 删除宝宝

        } );


        // 量表问答
        Route::group( [ 'namespace' => 'V1\Scale', 'prefix' => 'scale' ], function () {
            Route::get( 'index', 'IndexController@index' );                               //问题列表
            Route::get( 'type', 'IndexController@type' );                                 //量表类型
            Route::get( 'hot', 'IndexController@hot' );                                   //热门量表
            Route::get( 'recommend', 'IndexController@recommend' );                       //推荐量表
            Route::post( 'basicInfoAdd', 'IndexController@basicInfoAdd' );                //基础数据添加
            Route::get( 'previousBasicInfo', 'IndexController@previousBasicInfo' );
            Route::get( 'allQuestion', 'IndexController@allQuestion' );                   //量表对应所有问题
            Route::post( 'submitAnswer', 'IndexController@submitAnswer' );                //提交问题
            Route::get( 'question', 'IndexController@question' );                         //获取单个问题
            Route::post( 'questionAdd', 'IndexController@questionAdd' );
            Route::post( 'answerAdd', 'IndexController@answerAdd' );
        } );

        Route::group( [ 'namespace' => 'V1\Tool', 'prefix' => 'tool' ], function () {
            /**
             * 问答
             */
            Route::group( [ 'prefix' => 'question' ], function () {
                Route::get( 'index', 'QuestionController@index' );
                Route::post( 'add', 'QuestionController@add' );
                Route::post( 'comment', 'QuestionController@comment' );
                Route::get( 'myQuestion', 'QuestionController@myQuestion' );
                Route::get( 'myReply', 'QuestionController@myReply' );
                Route::get( 'commentList', 'QuestionController@commentList' );
            } );
        } );

        /**
         * 百科
         */
        Route::group( [ 'namespace' => 'V1\Wiki', 'prefix' => 'wiki' ], function () {
            Route::get( 'tree', 'CateController@tree' );
            Route::get( 'file', 'KnowledgeController@index' );
            Route::get( 'fileDetail', 'KnowledgeController@detail' );
            Route::get( 'course', 'CourseController@index' );
            Route::get( 'courseDetail', 'CourseController@detail' );
        } );

        /**
         * 我的收藏
         */
        Route::group( [ 'namespace' => 'V1\Wiki', 'prefix' => 'collect' ], function () {
            Route::post( 'add', 'CollectController@add' );
            Route::get( 'file', 'CollectController@file' );
            Route::get( 'course', 'CollectController@course' );
        } );

        Route::group( [ 'namespace' => 'V1\Message' ], function () {
            /**
             * 在线咨询
             */
            Route::group( [ 'prefix' => 'message' ], function () {
                Route::post( 'inquiryOrder', 'InqueryOrderController@add' );
                Route::get( 'getOrderPay', 'InqueryOrderController@getOrderPay' );
                Route::post( 'finish', 'InqueryOrderController@finish' );
                Route::get( 'inquiryRecord', 'InqueryOrderController@inquiryRecord' );
                Route::post( 'saveMessage', 'InqueryOrderController@saveMessage' );
            } );

            /**
             * 线下预约
             */
            Route::group( [ 'prefix' => 'appoint' ], function () {
                Route::post( 'add', 'AppointmentController@add' );
                Route::get( 'getOrderPay', 'AppointmentController@getOrderPay' );
                Route::get( 'index', 'AppointmentController@index' );
                Route::get( 'detail', 'AppointmentController@detail' );
                Route::post( 'change', 'AppointmentController@change' );
            } );
        } );

        /**
         * 商城
         */
        Route::group( [ 'namespace' => 'V1\Shop' ], function () {
            Route::group( [ 'prefix' => 'good' ], function () {
                Route::get( 'getCate', 'GoodsController@getCate' );
                Route::get( 'index', 'GoodsController@index' );
                Route::get( 'showList', 'GoodsController@showList' );
                Route::get( 'detail', 'GoodsController@detail' );
                Route::post( 'goCart', 'GoodsController@goCart' );
                Route::post( 'deleteCart', 'GoodsController@deleteCart' );
                Route::get( 'cart', 'GoodsController@cart' );
            } );

            Route::group( [ 'prefix' => 'order' ], function () {
                Route::get( 'index', 'OrderController@index' );
                Route::get( 'detail', 'OrderController@detail' );
                Route::get( 'getOrderPay', 'OrderController@getOrderPay' );
                Route::get( 'pay', 'OrderController@pay' );
                Route::post( 'add', 'OrderController@add' );
                Route::post( 'afterSales', 'OrderController@afterSales' );
                Route::post( 'confirmOrder', 'OrderController@confirmOrder' );
                Route::post( 'cancelOrder', 'OrderController@cancelOrder' );
                Route::post( 'cancelAfterSales', 'OrderController@cancelAfterSales' );
                Route::get( 'getAfterSaleType', 'OrderController@getAfterSaleType' );
            } );
        } );

    } );

} );
