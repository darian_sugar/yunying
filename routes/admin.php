<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/**
 * 用户APP
 */
Route::group( [ 'prefix' => 'c3', 'namespace' => 'Admin' ], function () {
    /**
     * 登陆
     */
    Route::group( [ 'namespace' => 'User', 'prefix' => 'user' ], function () {
        Route::post( 'login', 'LoginController@login' );
    } );

    /**
     * 接口
     */
    Route::group( [ 'middleware' => [ 'auth:admin', 'check.admin.login' ] ], function () {

        Route::group( [ 'namespace' => 'Common', 'prefix' => 'common' ], function () {
            Route::post( 'uploadFile', 'UploadController@upload' );          //上传
            Route::post( 'uploadBigFile', 'UploadController@uploadBigFile' );//上传
            Route::get( 'log_list', 'LogController@logList' );               //日志列表
        } );

        Route::group( [ 'namespace' => 'User', 'prefix' => 'user' ], function () {
            Route::post( 'loginOut', 'LoginController@loginOut' );//退出登录
        } );
        /**
         * 管理员信息
         */
        Route::group( [ 'prefix' => 'home', 'namespace' => 'Home' ], function () {
            Route::get( 'index', 'IndexController@index' );
            Route::post( 'index', 'IndexController@edit' );
            Route::post( 'updatePwd', 'IndexController@updatePwd' );
            Route::get( 'access', 'IndexController@access' );
            Route::get( 'homePage', 'IndexController@homePage' );
            Route::get( 'homeTrading', 'IndexController@homeTrading' );

        } );

        /**
         * 权限
         */
        Route::group( [ 'prefix' => 'access', 'namespace' => 'Permission' ], function () {
            Route::get( 'roleIndex', 'RoleController@index' );                  //部门角色列表
            Route::post( 'roleAdd', 'RoleController@add' );                     //部门添加
            Route::post( 'changeRoleStatus', 'RoleController@changeStatus' );   //角色/成员 权限编辑
            Route::get( 'memberIndex', 'MemberController@index' );              //成员列表
            Route::post( 'memberAdd', 'MemberController@add' );                 //成员添加/编辑
            Route::post( 'changeAdminStatus', 'MemberController@changeStatus' );//角色/成员 权限编辑
            Route::post( 'changeCsStatus', 'MemberController@changeCsStatus' ); //是否是客服
            Route::get( 'priIndex', 'PrivilegeController@index' );              //权限列表
            Route::post( 'priAdd', 'PrivilegeController@add' );                 //角色/成员 权限编辑
        } );

        /**
         * 管理
         */
        Route::group( [ 'prefix' => 'manage', 'namespace' => 'Manage' ], function () {
            Route::group( [ 'prefix' => 'adv' ], function () {
                Route::get( 'list', 'AdvController@advList' );                             //广告列表
                Route::post( 'add', 'AdvController@advAdd' );                              //添加广告
                Route::post( 'changeStatus', 'AdvController@changeStatus' );               //修改状态
            } );

            Route::group( [ 'namespace' => 'Wiki', 'prefix' => 'wiki', ], function () {
                /**
                 * 频道
                 */
                Route::group( [ 'prefix' => 'channel' ], function () {
                    Route::get( 'index', 'KnowChannelController@index' );               //列表
                    Route::post( 'add', 'KnowChannelController@add' );                  //添加
                    Route::post( 'changeStatus', 'KnowChannelController@changeStatus' );//添加
                } );

                /**
                 * 分类
                 */
                Route::group( [ 'prefix' => 'cate' ], function () {
                    Route::get( 'index', 'KnowCateController@index' );               //列表
                    Route::post( 'add', 'KnowCateController@add' );                  //添加
                    Route::post( 'changeStatus', 'KnowCateController@changeStatus' );//添加
                } );
                /**
                 * 文章管理
                 */
                Route::group( [ 'prefix' => 'file' ], function () {
                    Route::get( 'index', 'KnowFileController@index' );                         //列表
                    Route::post( 'add', 'KnowFileController@add' );                            //添加
                    Route::post( 'changeStatus', 'KnowFileController@changeStatus' );          //添加
                    Route::get( 'detail', 'KnowFileController@detail' );                       //添加
                    Route::post( 'changeCheckStatus', 'KnowFileController@changeCheckStatus' );//添加
                } );

                /**
                 * 课程
                 */
                Route::group( [ 'prefix' => 'course' ], function () {
                    Route::get( 'price', 'KnowCourseController@price' );                         //视频价格列表
                    Route::get( 'index', 'KnowCourseController@index' );                         //列表
                    Route::post( 'add', 'KnowCourseController@add' );                            //添加
                    Route::post( 'changeStatus', 'KnowCourseController@changeStatus' );          //启用 停用 删除
                    Route::post( 'changeCheckStatus', 'KnowCourseController@changeCheckStatus' );//审核通过 拒绝
                    Route::get( 'detail', 'KnowCourseController@detail' );                       //添加
                } );
            } );
            /**
             * 学堂
             */
            Route::group( [ 'namespace' => 'School', 'prefix' => 'school', ], function () {
                /**
                 * 分类
                 */
                Route::group( [ 'prefix' => 'cate' ], function () {
                    Route::get( 'index', 'CateController@index' );               //列表
                    Route::post( 'add', 'CateController@add' );                  //添加
                    Route::post( 'changeStatus', 'CateController@changeStatus' );//添加
                } );
                /**
                 * 文章管理
                 */
                Route::group( [ 'prefix' => 'file' ], function () {
                    Route::get( 'index', 'FileController@index' );                         //列表
                    Route::post( 'add', 'FileController@add' );                            //添加
                    Route::post( 'changeStatus', 'FileController@changeStatus' );          //添加
                    Route::get( 'detail', 'FileController@detail' );                       //添加
                    Route::post( 'changeCheckStatus', 'FileController@changeCheckStatus' );//添加
                } );

                /**
                 * 课程
                 */
                Route::group( [ 'prefix' => 'course' ], function () {
                    Route::get( 'price', 'CourseController@price' );                         //视频价格列表
                    Route::get( 'index', 'CourseController@index' );                         //列表
                    Route::post( 'add', 'CourseController@add' );                            //添加
                    Route::post( 'changeStatus', 'CourseController@changeStatus' );          //启用 停用 删除
                    Route::post( 'changeCheckStatus', 'CourseController@changeCheckStatus' );//审核通过 拒绝
                    Route::get( 'detail', 'CourseController@detail' );                       //添加
                } );
            } );
        } );


        Route::group( [ 'namespace' => 'Doctor' ], function () {
            /**
             * 医生
             */
            Route::group( [ 'prefix' => 'doctor' ], function () {
                Route::get( 'index', 'IndexController@index' );                        //医生列表
                Route::get( 'detail', 'IndexController@detail' );                      //医生详情
                Route::post( 'add', 'IndexController@add' );                           //添加医生
                Route::post( 'edit', 'IndexController@edit' );                         //编辑医生
                Route::post( 'changeStatus', 'IndexController@changeStatus' );         //添加医生
                Route::get( 'inquiryList', 'IndexController@inquiryList' );            //问诊列表
                Route::post( 'checkPicStatus', 'IndexController@checkPicStatus' );     //资质审核
                Route::post( 'recommendDoctor', 'IndexController@recommendDoctor' );   //推荐医生
                Route::get( 'getSchedule', 'IndexController@getSchedule' );            //获取排班
                Route::post( 'setSchedule', 'IndexController@setSchedule' );           //设置排班
            } );

            Route::group( [ 'prefix' => 'appoint' ], function () {
                Route::get( 'index', 'AppointmentController@index' );
                Route::post( 'confirmAppoint', 'AppointmentController@confirmAppoint' );
            } );
        } );


        /**
         * 患者
         */
        Route::group( [ 'prefix' => 'patient', 'namespace' => 'User' ], function () {
            Route::get( 'index', 'IndexController@index' );                            //患者列表
            Route::get( 'list', 'IndexController@list' );                              //患者姓名ID列表
            Route::get( 'detail', 'IndexController@detail' );                          //患者基础信息
            Route::get( 'inquiryList', 'IndexController@inquiryList' );                //患者问诊信息
            Route::get( 'appointList', 'IndexController@appointList' );                //患者预约信息
            Route::post( 'changeStatus', 'IndexController@changeStatus' );             //患者状态修改
            Route::get( 'scaleList', 'IndexController@scaleList' );                    //患者量表列表
            Route::get( 'scaleDetail', 'IndexController@scaleDetail' );                //患者量表记录详情
        } );

        /**
         * 意见反馈
         */
        Route::group( [ 'prefix' => 'suggest', 'namespace' => 'Suggest' ], function () {
            Route::get( 'index', 'IndexController@list' );         //列表
            Route::get( 'detail', 'IndexController@detail' );      //详情
            Route::post( 'feedback', 'IndexController@feedback' ); //详情
        } );


        Route::group( [ 'prefix' => 'message', 'namespace' => 'Message' ], function () {
            Route::get( 'index', 'IndexController@index' );                       //消息列表
            Route::post( 'emailSend', 'IndexController@emailSend' );              //消息列表
            Route::post( 'sendMessage', 'IndexController@sendMessage' );          //发送消息
        } );


        Route::group( [ 'prefix' => 'scale', 'namespace' => 'Scale' ], function () {
            Route::get( 'index', 'IndexController@list' );                    //列表
            Route::get( 'detail', 'IndexController@detail' );                 //详情
            Route::post( 'enable', 'IndexController@enable' );                //是否启用
            Route::post( 'del', 'IndexController@del' );                      //删除
            Route::post( 'recommended', 'IndexController@recommended' );      //是否推荐
            Route::post( 'importScale', 'IndexController@importScale' );      //量表模版添加
            Route::get( 'scaleType', 'IndexController@scaleType' );           //量表类型
            Route::get( 'questionType', 'IndexController@questionType' );     //量表问题类型
            Route::post( 'add', 'IndexController@add' );                      //量表问题添加
            Route::post( 'addQuestion', 'IndexController@addQuestion' );      //量表问题单个添加
        } );

        /**
         * 报表
         */
        Route::group( [ 'namespace' => 'Report', 'prefix' => 'report' ], function () {
            Route::get( 'finance', 'FinanceController@index' );                          //财务列表
            Route::get( 'increaseUser', 'CountController@increaseUser' );                //用户增长人数统计
            Route::get( 'increaseDateUser', 'CountController@increaseDateUser' );        //用户增长人数统计
            Route::get( 'countUser', 'CountController@countUser' );                      //用户总数统计
            Route::get( 'tradeCount', 'CountController@tradeCount' );
            Route::get( 'tradePayCount', 'CountController@tradePayCount' );
        } );


        Route::group( [ 'prefix' => 'shop', 'namespace' => 'Shop' ], function () {
            /**
             * 商品
             */
            Route::group( [ 'prefix' => 'goods' ], function () {
                Route::get( 'index', 'GoodsController@index' );
                Route::post( 'add', 'GoodsController@add' );
                Route::post( 'changeStatus', 'GoodsController@changeStatus' );
                Route::get( 'detail', 'GoodsController@detail' );
            } );
            Route::group( [ 'prefix' => 'cate' ], function () {
                Route::get( 'index', 'CateController@index' );
                Route::get( 'tree', 'CateController@tree' );
                Route::post( 'add', 'CateController@add' );
                Route::post( 'changeStatus', 'CateController@changeStatus' );
            } );
            /**
             * 订单
             */
            Route::group( [ 'prefix' => 'order' ], function () {
                Route::get( 'index', 'OrderController@index' );
                Route::get( 'detail', 'OrderController@detail' );
                Route::post( 'getKuaidi', 'OrderController@getKuaidi' );                    //获取快递信息
                Route::post( 'cancelOrder', 'OrderController@cancelOrder' );
                Route::post( 'kuaidi', 'OrderController@kuaidi' );
                Route::post( 'confirmAfterSales', 'OrderController@confirmAfterSales' );
                Route::get( 'afterSales', 'OrderAfterSalesController@index' );
            } );
            Route::group( [ 'prefix' => 'kuaidi' ], function () {
                Route::get( 'index', 'KuaiDiController@index' );

            } );
        } );


    } );
} );
