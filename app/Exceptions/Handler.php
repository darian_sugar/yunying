<?php

namespace App\Exceptions;

use App\Http\Controllers\Controller;
use Exception;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Database\QueryException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\Exceptions\ThrottleRequestsException;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Log;
use Illuminate\Validation\ValidationException;
use Symfony\Component\Debug\Exception\FatalErrorException;
use Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * @param Exception $exception
     *
     * @return void
     */
    public function report( Exception $exception )
    {
        parent::report( $exception );
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param Request $request
     * @param Exception $exception
     *
     * @return Response
     */
    public function render( $request, Exception $exception )
    {
        $response = new Controller( $request );
        //echo  $exception->getMessage().$exception->getFile();die;
        if ( $exception instanceof HttpResponseException ) {
            return $response->failed( $exception->getMessage() );
        } elseif ( $exception instanceof AuthorizationException ) {
            return $response->unAuthenticated();
        } elseif ( $exception instanceof ValidationException ) {
            $errors = $exception->errors();
            if ( is_array( $errors ) && $errors )
                return $response->failed( reset( $errors )[0], $exception->status );
            else
                return $response->failed( $errors, $exception->status );
        } elseif ( $exception instanceof MethodNotAllowedHttpException ) {
            return $response->methodNotAllow( '请求方式错误' );
        } elseif ( $exception instanceof AuthenticationException ) {
            return $response->unAuthenticated( '身份验证失败' );
        } elseif ( $exception instanceof NotFoundHttpException ) {
            return $response->notFond();
        } elseif ( $exception instanceof FatalErrorException ) {
            return $response->failed( $exception->getMessage(), $exception->getCode() );
        } elseif ( $exception instanceof QueryException ) {
            if ( getenv( 'APP_ENV' ) == 'production' ) {
                logLog( $exception->getMessage(), 'sqlerror' );
                return $response->failed( '系统错误', $exception->getCode() );
            } else {
                return $response->failed( $exception->getMessage() );
            }

        } elseif ( $exception instanceof ModelNotFoundException ) {
            return $response->failed( '数据错误' );
        } elseif ( $exception instanceof ThrottleRequestsException ) {
            return $response->failed( '请求次数超限,请稍后再试' );

        } else {
            return $response->failed( $exception->getMessage() );
        }
        //        return parent::render($request, $exception);
    }
}
