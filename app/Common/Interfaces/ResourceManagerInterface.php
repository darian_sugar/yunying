<?php
/**
 * 资源管理接口
 */

namespace App\Common\Interfaces;


interface ResourceManagerInterface
{
    /**
     * 每个子类实现这个方法,用于返回具体Manager实例
     * @return mixed
     */
    public static function getInstance();
}
