<?php

namespace App\Common\Constants;

class ConstantQueueRedisKey{

    const SEND_SMS_EVENT_QUEUE = 'send_sms_event_queue';

    const USER_LOGIN_JOB_QUEUE = 'use_login_job_queue';
}
