<?php
namespace App\Common\Constants;

 class ConstantMysql{

     /**
      * scale 量表
      */
     //是否启用
     const IS_ENABLE = 1;      //启用
     const IS_NOT_ENABLE = 2;  //不启用
     //是否推荐
     const IS_RECOMMENDED = 1;      //推荐
     const IS_NOT_RECOMMENDED = 2;  //不推荐
 }
