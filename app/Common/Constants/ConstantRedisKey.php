<?php

namespace App\Common\Constants;

class ConstantRedisKey
{
    // test
    const TESTS = 'tests';

    // 用户操作超时时间
    const YH_USER_OVERTIME = 'YH:USER:OVERTIME';

    // 用户密码输入错误次数
    const YH_USER_PASSWORD_ERROR_COUNT = 'YH:USER:PASSWORD:ERROR:COUNT';

    // 用户密码错误超出限制对应锁定时间
    const YH_USER_PASSWORD_ERROR_LOCK_TIME = 'YH:USER:PASSWORD:ERROR:LOCK:TIME';

    // 盈海科室列表
    const YH_HIS_DEPARTMENT_LIST = 'YH:HIS:DEPARTMENT:LIST';

    // 地域列表
    const AREAS = 'AREAS';

}
