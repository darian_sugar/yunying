<?php

namespace App\Listeners;

use App\Common\Constants\ConstantQueueRedisKey;
use App\Events\SendSmsEvent;
use App\Helpers\Sms;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Log;

class SendSmsListener implements ShouldQueue
{
    use InteractsWithQueue;

    /**
     * 任务应该发送到的队列的连接的名称
     *
     * @var string|null
     */
    public $connection = 'redis';

    /**
     * 任务应该发送到的队列的名称
     *
     * @var string|null
     */
    public $queue = ConstantQueueRedisKey::SEND_SMS_EVENT_QUEUE;

    /**
     * 任务可以尝试的最大次数。
     *
     * @var int
     */
    public $tries = 1;

    /**
     * 超时时间。
     *
     * @var int
     */
    public $timeout = 300;


    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  SendSmsEvent  $event
     * @return void
     */
    public function handle(SendSmsEvent $event)
    {
        file_put_contents(storage_path() . '/logs/test.log', date('Y-m-d H:i:s') .$event->type . $event->mobile. '等待五秒钟' . PHP_EOL, FILE_APPEND);
        Sms::getInstance()->sendSing($event->type, $event->mobile);
        return true;
    }

    //有时候队列中的事件监听器可能会执行失败。如果队列中的监听器任务执行时超出了队列进程定义的最大尝试次数，
    //failed 方法接收事件实例和导致失败的异常
    public function failed(SendSmsEvent $event, $exception)
    {
        file_put_contents(storage_path() . '/logs/test.log', date('Y-m-d H:i:s') . $exception . PHP_EOL, FILE_APPEND);
    }

}
