<?php

namespace App\Services\Common;


use App\Services\Service;
use Illuminate\Support\Facades\DB;

class MongodbService extends Service
{
    public static function connectionMongodb()
    {
        $tables = env( 'MONGODB_DATABASE' ) . '-' . date( 'Y-m' );
        return $users = DB::connection( 'mongodb' )->collection( $tables );
    }
}
