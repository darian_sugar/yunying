<?php

namespace App\Services\Common;

use App\Common\Constants\ConstantRedisKey;
use App\Models\User;
use App\Models\UserThirdAccount;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redis;
use Zhuzhichao\IpLocationZh\Ip;

class LoginService
{

    public static function clearPwdErrorLockNumAndTime( $id, $table )
    {
        $redisKey = $table . '_' . $id;
        Redis::hdel(ConstantRedisKey::YH_USER_PASSWORD_ERROR_COUNT, $redisKey);
        Redis::hdel(ConstantRedisKey::YH_USER_PASSWORD_ERROR_LOCK_TIME, $redisKey);
    }




}
