<?php

namespace App\Services\Common;

use App\Common\Constants\ConstantRedisKey;
use App\Models\User;
use App\Services\Service;
use Illuminate\Support\Facades\Redis;

class CheckService extends Service {

    /**
     * @param $password
     * @throws \Exception
     */
    public static function checkPassword($password)
    {
        // 密码规范字母大写+字母小写+数字
        $checkPwd = preg_match('/(?=.*[A-Z])(?=.*[a-z])(?=.*[0-9])[A-Za-z0-9]{8,20}/', $password);
        if (!$checkPwd) {
            throw new \Exception('密码不符合规范，需包含大写字母，小写字母，以及数字', '400');
        }
    }


    /**
     * 验证用户操作是否超时
     *
     * @param $id
     * @param $table
     *
     * @return bool
     */
    public static function checkOvertime($id, $table)
    {
        $redisKey = $table . '_' . $id;
        $overTime = Redis::hget(ConstantRedisKey::YH_USER_OVERTIME, $redisKey) ?? time();
        $optionIntervalTime = number_format((time() - $overTime) / 60, 1);
        if ($optionIntervalTime >= 30) {
            // 超时，清空token,需重新登录
            // 1 是医生清空doctor表对应token , 2 是患者清空user表对应token
            switch ($table) {
                case 'users':
                    User::getInstance()->where(['id' => $id])->update(['api_token' => '']);
                    break;
                default:
                    break;
            }
            Redis::hdel(ConstantRedisKey::YH_USER_OVERTIME, $redisKey);
            return false;
        } else {
            Redis::hset(ConstantRedisKey::YH_USER_OVERTIME, $redisKey, time());
        }
        return true;
    }


    /**
     * 密码错误五次锁定十分钟
     *
     * @param $id
     * @param $table
     */
    public static function checkPasswordErrorCount($id, $table)
    {
        $redisKey = $table . '_' . $id;
        $num = Redis::hget(ConstantRedisKey::YH_USER_PASSWORD_ERROR_COUNT, $redisKey) ?? 0;
        $num++;
        if ($num >= 5) {
            Redis::hset(ConstantRedisKey::YH_USER_PASSWORD_ERROR_LOCK_TIME, $redisKey, time() + 600);
        }
        Redis::hset(ConstantRedisKey::YH_USER_PASSWORD_ERROR_COUNT, $redisKey, $num);

    }


    /**
     * 检测是否超过密码错误锁定时间
     *
     * @param $id
     * @param $table
     */
    public static function checkPasswordErrorLockTime($id, $table)
    {
        $redisKey = $table . '_' . $id;
        $lockTime = Redis::hget(ConstantRedisKey::YH_USER_PASSWORD_ERROR_LOCK_TIME, $redisKey) ?? 0;
        $time = time();
        if ($lockTime > $time) {
            Redis::hset(ConstantRedisKey::YH_USER_PASSWORD_ERROR_COUNT, $redisKey, 0);
            $remainingTime = ceil(($lockTime - $time) / 60);
            throw new \JsonException('账号锁定，请' . $remainingTime . '分钟后再登录', '400');
        }
    }


    /**
     * 清除密码错误锁定
     * @param $id
     * @param $table
     */
    public static function clearPwdErrorLockNumAndTime($id, $table)
    {
        $redisKey = $table . '_' . $id;
        Redis::hdel(ConstantRedisKey::YH_USER_PASSWORD_ERROR_COUNT, $redisKey);
        Redis::hdel(ConstantRedisKey::YH_USER_PASSWORD_ERROR_LOCK_TIME, $redisKey);
    }
}

