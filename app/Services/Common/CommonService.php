<?php

namespace App\Services\Common;

use App\Services\Service;

class CommonService extends Service{

    public static function getCurl($url)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);
        $res = curl_exec($ch);
        curl_close($ch);
        $json_obj = json_decode($res, true);
        return $json_obj;
    }


    /**
     * 递归
     * @param $nodes
     * @param int $pid
     * @return array
     */
    public static function getTree($nodes, $pid = 0)
    {
        $tree = [];
        foreach ($nodes as $k => $v) {
            if ($v->pid == $pid) {
                $son = self::getTree($nodes, $v->id);
                if (!empty($son)) $v['son'] = $son;
                $tree[] = $v;
            }
        }
        return $tree;
    }



    /**
     * 根据生日计算星座
     */
    public static function getConstellation($birthday)
    {
        //    传入$birthday格式如：2018-05-06

        $month = intval(substr($birthday, 5, 2));
        $day = intval(substr($birthday, 8, 2));
        if ($month < 1 || $month > 12 || $day < 1 || $day > 31)
        {
            return NULL;
        }
        $signs = array(
            array('20' => '水瓶座'),
            array('19' => '双鱼座'),
            array('21' => '白羊座'),
            array('20' => '金牛座'),
            array('21' => '双子座'),
            array('22' => '巨蟹座'),
            array('23' => '狮子座'),
            array('23' => '处女座'),
            array('23' => '天秤座'),
            array('24' => '天蝎座'),
            array('22' => '射手座'),
            array('22' => '摩羯座')
        );
        list($start, $name) = each($signs[$month - 1]);
        if ($day < $start)
        {
            list($start, $name) = each($signs[($month - 2 < 0) ? 11 : $month - 2]);
        }

        return $name;
    }
}
