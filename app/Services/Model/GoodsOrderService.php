<?php

namespace App\Services\Model;

use App\Common\Constants\ConstantRedisKey;
use App\Helpers\PayClass;
use App\Models\GoodsOrder;
use App\Models\SystemMsg;
use App\Models\User;
use App\Services\Service;
use App\Traits\Singleton;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redis;

class GoodsOrderService extends Service
{

    /**
     * @param $type 0待付款 1 待收货 2 已完成 3 待审核 4 已同意 5 已拒绝 6 全部订单
     * @return array[]
     */
    public function getSearchWhere( $type )
    {
        switch ( $type ) {
            //待收货(包含待发货)
            case 1:
                $where = [ [ 'order_status', 1 ], [ 'pay_status', '>', 0 ] ];
                $where[] = [ DB::raw( "trans_status in (1,2) and goods_orders.deleted_at" ) ];
                break;
            //待发货
            case 11:
                $where = [ [ 'order_status', 1 ], [ 'trans_status', 1 ], [ 'pay_status', '>', 0 ] ];
                break;
            //已发货
            case 12:
                $where = [ [ 'order_status', 1 ], [ 'trans_status', 2 ], [ 'pay_status', '>', 0 ] ];
                break;
            //已完成
            case 2:
                $where = [ [ 'order_status', '!=', 2 ], [ 'trans_status', 4 ], [ 'pay_status', '>', 0 ] ];
                break;
            //待审核
            case 3:
                $where = [ [ 'order_status', 3 ], [ 'trans_status', 4 ], [ 'pay_status', '>', 0 ] ];
                break;
            //已拒绝
            case 4:
                $where = [ [ 'order_status', 4 ], [ 'trans_status', 4 ], [ 'pay_status', '>', 0 ] ];

                break;
            //已同意
            case 5 :
                $where = [ [ 'order_status', 5 ], [ 'trans_status', 4 ], [ 'pay_status', '>', 0 ] ];
                break;
            //待付款
            case 6 :
                $where = [ [ 'order_status', 1 ], [ 'pay_status', 0 ] ];
                break;
            //售后
            case 7 :
                $where = [ [ 'order_status', 1 ], [ 'trans_status', 4 ], [ 'pay_status', '>', 0 ] ];
                $where[] = [ DB::raw( "order_status in(3,4,5) and goods_orders.deleted_at" ) ];
                break;

            default:
                //全部
                $where = [ [ 'order_status', '!=', 2 ], [ 'pay_status', '>', 0 ] ];
                break;
        }
        return $where;
    }


    public function hasStock( Model $order )
    {
        foreach ( $order->hasManyDetail as $k => $v ) {
            if ( !$v->belongsToGoods )
                throw new \JsonException( '商品不存在' );
            if ( !$v->belongsToGoodsSpec )
                throw new \JsonException( '商品规格不存在' );
            if ( $v->belongsToGoods->stock <= 0 )
                throw new \JsonException( '商品库存不足' );
            if ( $v->belongsToGoodsSpec->stock <= 0 )
                throw new \JsonException( '商品库存不足' );
        }
        return true;
    }

    /**
     * 取消订单
     * @param Model $orderModel
     * @param $from 1 管理后台 2用户
     * @return Model
     */
    public function cancelOrder( Model $orderModel, $from )
    {

        if ( $orderModel->pay_status != 0 )
            throw new \JsonException( '订单已支付' );
        if ( $orderModel->order_status == 2 )
            throw new \JsonException( '订单已取消，请勿重复操作' );
        if ( $orderModel->order_status != 1 )
            throw new \JsonException( '订单状态错误' );
        $orderModel->order_status = 2;
        $orderModel->save();
        if ( $orderModel->pay_status == 1 )
            PayClass::getInstance()->refund( $orderModel->order_number, $orderModel->pay_amount, $orderModel->pay_amount, $orderModel->method_pay );

        //发送通知
        $orderGoods = $orderModel->hasManyDetail()->get()->transform( function ( $item, $key ) {
            return $item['goods_spec'] = $item['goods_name'] . ' ' . $item['spec_name'];
        } );
        $string = '（' . $orderGoods[0] . '）';
        if ( count( $orderGoods ) > 1 )
            $string .= '等';
        if ( $from == 1 )
            $msg = '您购买的' . $string . '，订单号：' . $orderModel->order_number . '，已被取消。';
        else
            $msg = '您购买的' . $string . '，订单号：' . $orderModel->order_number . '，已取消。';

        SystemMsg::getInstance()->sendSysMsg( $msg, $orderModel->user_id, 1, '订单取消', 2 );
        return $orderModel;
    }
}

