<?php


namespace App\Services\Model;


use App\Models\AdminApi;
use App\Models\Privilege;
use App\Models\RolePriRelevance;
use App\Services\Service;
use Illuminate\Database\Eloquent\Model;

class AdminService extends Service
{

    public function adminAccess( Model $user )
    {
        $model = Privilege::getInstance()
            ->select( 'id', 'pid', 'icon', 'title', 'p_name', 'level' );

        if ( $user->is_admin != 1 ) {
            $roleIds = $user->hasManyRole()->distinct()->pluck( 'r_id' );
            $RolePriRelevance = RolePriRelevance::getInstance()->where( [ 'type' => 2, 'r_id' => $user->id, 'is_api' => 0 ] );
            if ( $roleIds ) {
                $RolePriRelevance->orWhere( function ( $query ) use ( $roleIds ) {
                    $query->whereIn( 'r_id', $roleIds )->where( 'type', 1 )->where( 'is_api', 0 );
                } );
            }
            $pages = $RolePriRelevance->pluck( 'p_id' )->flatten();

            $model->where( function ( $query ) use ( $pages ) {
                $query->whereIn( 'id', $pages )->where( [ 'status' => 1 ] );
            } )->orWhere( function ( $query ) {
                $query->where( [ 'status' => 1, 'type' => 2 ] );
            } );

        } else {
            $model->where( [ 'status' => 1 ] );
        }
        $data = $model->with( [
            'hasManyChildren' => function ( $query ) {
                $query->select( 'id', 'pid', 'icon', 'title', 'p_name', 'level' )->where( 'level', 2 );
            }
        ] )->orderByRaw( 'order_sort asc,id asc' )
            ->where( 'level', '<', 2 )->get()->toArray();
        $tree = [];

        listToTree( $data, 'id', 'pid', 'has_many_children', 0, $tree );
        return $tree;
    }

    public function apiAccess( Model $user )
    {
        $roleIds = $user->hasManyRole()->distinct()->pluck( 'r_id' );
        $RolePriRelevance = RolePriRelevance::getInstance()->where( [ 'type' => 2, 'r_id' => $user->id, 'is_api' => 1 ] );
        if ( $roleIds ) {
            $RolePriRelevance->orWhere( function ( $query ) use ( $roleIds ) {
                $query->whereIn( 'r_id', $roleIds )->where( 'type', 1 )->where( 'is_api', 1 );
            } );
        }
        $apisid = $RolePriRelevance->pluck( 'p_id' )->flatten();
        $common = AdminApi::getInstance()->where( [ 'status' => 1, 'type' => 2 ] )->get();
        if ( $apisid ) {
            $owenApis = AdminApi::getInstance()->whereIn( 'id', $apisid )->where( 'status', 1 )->select( 'id', 'method', 'url' )->get();
            return $common->merge( $owenApis );
        } else {
            return $common->all();
        }
    }
}
