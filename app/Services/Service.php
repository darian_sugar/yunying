<?php

/**
 * Description: 服务层 业务服务抽象扩展类
 */
namespace App\Services;

use Illuminate\Support\Facades\Redis;

abstract class Service
{
    /**
     * @var \Illuminate\Redis\Connections\Connection
     */
    protected $redis = null;


    public function __construct()
    {
        # 实例化Redis
        if (is_null($this->redis)) {
            $this->redis = Redis::connection();
        }
    }

    /**
     * @return \Illuminate\Redis\Connections\Connection
     */
    public function getRedis()
    {
        if (is_null($this->redis)) {
            $this->redis = Redis::connection();
        }
        return $this->redis;
    }

}
