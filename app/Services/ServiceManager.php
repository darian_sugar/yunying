<?php

namespace App\Services;

use App\Common\BaseClasses\ResourceManager;
use App\Services\App\HomePage\HotMomService;
use App\Services\App\HomePage\PeriodService;
use App\Services\App\HomePage\PregnancyService;
use App\Services\App\User\UserIndexService;
use App\Services\App\User\LoginService;
use App\Services\Common\CheckService;
use App\Services\Common\CommonService;
use App\Services\Model\AdminService;
use App\Services\Model\GoodsOrderService;


/**
 * Description:
 * Class ServiceManager
 * @package App\Services
 * 语法糖 方法申明 类名
 * @method CheckService CheckService( string $fullClassName )
 * @method CommonService CommonService( string $fullClassName )
 * @method LoginService LoginService( string $fullClassName )
 * @method PeriodService PeriodService( string $fullClassName )
 * @method PregnancyService PregnancyService( string $fullClassName )
 * @method HotMomService HotMomService( string $fullClassName )
 * @method GoodsOrderService GoodsOrderService( string $fullClassName )
 * @method AdminService AdminService( string $fullClassName )
 * @method UserIndexService UserIndexService( string $fullClassName )
 * @method MongodbService
 */
class ServiceManager extends ResourceManager
{

    /**
     * Description: 每个Manager类都只要实现以下这段即可,注意正确的返回值
     * @return ServiceManager
     * @throws \ReflectionException
     */
    public static function getInstance(): ServiceManager
    {
        return parent::_getInstance( ServiceManager::class );
    }
}

