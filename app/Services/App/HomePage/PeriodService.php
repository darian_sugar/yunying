<?php

namespace App\Services\App\HomePage;

use App\Services\Service;

class PeriodService extends Service{


    public function formatPeriodRecordInsert($input)
    {
        $cycle = $input['cycle'];
        $length = $input['length'];
        $period_predictive_date = $input['menstruation_date'];
        $period_predictive_end_date = date('Y-m-d', strtotime("+$length day $period_predictive_date"));
        return [
          'period_reality_date' => $period_predictive_date,
          'period_reality_end_date' => $period_predictive_end_date,
          'month' => date('Y-m', strtotime("$period_predictive_date")),
          'user_id' => $input['user_id'],
        ];
    }

    public function formatPeriodRecordNextInsert($input)
    {
        $cycle = $input['cycle'];
        $length = $input['length'];
        $menstruation_date = $input['menstruation_date'];
        $period_predictive_date = date('Y-m-d', strtotime("+$cycle day $menstruation_date"));
        $period_predictive_end_date = date('Y-m-d', strtotime("+$length day $period_predictive_date"));
        return [
            'period_predictive_date' => $period_predictive_date,
            'period_predictive_end_date' => $period_predictive_end_date,
            'month' => date('Y-m', strtotime("+1 month $menstruation_date")),
            'user_id' => $input['user_id'],
        ];
    }
}
