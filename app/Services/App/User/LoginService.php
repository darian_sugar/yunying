<?php

namespace App\Services\App\User;

use App\Helpers\WxOauth;
use App\Helpers\YunXin\YunXin;
use App\Models\User;
use App\Models\UserThirdAccount;
use App\Services\Common\CommonService;
use App\Services\Service;
use Illuminate\Support\Facades\DB;
use Zhuzhichao\IpLocationZh\Ip;

class LoginService extends Service{


    /**
     * QQ登录
     * @param $input
     * @return false
     * @throws \JsonException
     */
    public function qqLogin($input)
    {
        $accessToken = $input['access_token'];
        $openid      = $input['openid'];
        $thirdInfo   = $this->getQqThirdInfo($accessToken, $openid);
        //查询此次登陆用户是否首次登陆，不是首次登陆返回信息，是首次登陆注册新用户
        $useThirdInfo = UserThirdAccount::getInstance()
            ->where(['openid' => $openid, 'third_type' => 1])
            ->first();
        if (!empty($useThirdInfo)) {
            //非首次登陆，刷新token，返回用户信息
            return  User::getInstance()->find($useThirdInfo->user_id);
        }

        //获取登录地址
        $ip = request()->getClientIp();
        $areas = Ip::find($ip);
        try {
            DB::beginTransaction();
            $user = new User();
            $createUser = $this->qqCreateUser($user, $thirdInfo, $ip, $areas);
            $userId = $createUser->id;
            //将第三方QQ用户信息入库
            $insertThird = [
                'user_id'    => $userId,
                'third_key'  => $openid,
                'third_type' => 1,
                'openid'     => $openid,
                'nickname'   => $thirdInfo['nickname'],
                'sex'        => $thirdInfo['gender'],
                'headimgurl' => $thirdInfo['figureurl_qq_1'],
            ];
            $newThird    = UserThirdAccount::getInstance()->insert($insertThird);
            if (!$newThird) {
                DB::rollBack();
                return false;
            }
            DB::commit();
            return User::getInstance()->find($userId);
        } catch (\Exception $e) {
            DB::rollBack();
            return false;
        }
    }


    /**
     * 微信登录
     * @param $input
     * @return false
     * @throws \Exception
     */
    public function wxLogin($input)
    {
        $wxCode      = $input['wx_code'];
        // 移动端使用
        $WxOauth =new WxOauth();// 传入appid和appsecret
//公众号登录需要先获取code,下面方法会自动跳转到微信授权页面
//        $WxOauth->getCode();
// 通过移动端传来的code或者微信回调返回的code获取用户信息
        $user_info = $WxOauth->wxLogin($wxCode);

        if($user_info){
            return $user_info;
//获取到用户信息
        }else{
// 获取错误
            return   $WxOauth->error;
        }



        $wxInfo      = $this->getWxAccessToken($wxCode);
        $accessToken = $wxInfo['access_token'];
        $openid      = $wxInfo['openid'];
        //查询此次登陆用户是否首次登陆，不是首次登陆返回信息，是首次登陆注册新用户
        $useThirdInfo = UserThirdAccount::getInstance()
            ->where(['openid' => $openid, 'third_type' => 2])
            ->first();
        if (!empty($useThirdInfo)) {
            return User::getInstance()->find($useThirdInfo->user_id);
        }
        $thirdInfo   = $this->getWxThirdInfo($accessToken, $openid);
        //获取登录地址
        $ip    = request()->getClientIp();
        $areas = Ip::find($ip);
        try {
            DB::beginTransaction();
            $user = new User();
            // 无手机号直接注册新帐号
            $createUser = $this->wxCreateUser($user, $thirdInfo, $ip, $areas);
            $userId     = $createUser->id;
            //将第三方微信获取用户信息入库
            $insertThird = [
                'user_id'    => $userId,
                'third_key'  => $openid,
                'third_type' => 2,
                'openid'     => $openid,
                'nickname'   => $thirdInfo['nickname'],
                'sex'        => $thirdInfo['sex'],
                'province'   => $thirdInfo['province'],
                'city'       => $thirdInfo['city'],
                'country'    => $thirdInfo['country'],
                'headimgurl' => $thirdInfo['headimgurl'],
                'privilege'  => json_encode($thirdInfo['privilege']),
                'unionid'    => $thirdInfo['unionid'],
            ];
            $newThird    = UserThirdAccount::getInstance()->insert($insertThird);
            if (!$newThird) {
                DB::rollBack();
                return false;
            }
            DB::commit();
            return User::getInstance()->find($userId);
        } catch (\Exception $e) {
            DB::rollBack();
            return false;
        }
    }

    public function getQqThirdInfo($accessToken, $openid)
    {
        $appId = env('QQ_APP_ID');
        $url   = 'https://graph.qq.com/user/get_user_info?access_token=' . $accessToken . '&oauth_consumer_key=' . $appId . '&openid=' . $openid;
//        $client   = new Client();
//        $json_obj = $client->request('get', $url);
        $json_obj = CommonService::getCurl($url);
        if ($json_obj['ret'] != 0) throw new \JsonException('qq获取用户信息失败');

//        $json_obj     = '{
//        "ret":0,
//        "msg":"",
//        "nickname":"Peter",
//        "figureurl":"http://qzapp.qlogo.cn/qzapp/111111/942FEA70050EEAFBD4DCE2C1FC775E56/30",
//        "figureurl_1":"http://qzapp.qlogo.cn/qzapp/111111/942FEA70050EEAFBD4DCE2C1FC775E56/50",
//        "figureurl_2":"http://qzapp.qlogo.cn/qzapp/111111/942FEA70050EEAFBD4DCE2C1FC775E56/100",
//        "figureurl_qq_1":"http://q.qlogo.cn/qqapp/100312990/DE1931D5330620DBD07FB4A5422917B6/40",
//        "figureurl_qq_2":"http://q.qlogo.cn/qqapp/100312990/DE1931D5330620DBD07FB4A5422917B6/100",
//        "gender":"男",
//        "is_yellow_vip":"1",
//        "vip":"1",
//        "yellow_vip_level":"7",
//        "level":"7",
//        "is_yellow_year_vip":"1"
//        }';
//        $json_obj = json_decode($json_obj, true);

        foreach ($json_obj as $k => $v) {
            if ($json_obj['gender'] == '男') {
                $json_obj['gender'] = 1;
            } else {
                $json_obj['gender'] = 2;
            }
        }

        return $json_obj;
    }


    public function qqCreateUser($user, $thirdInfo, $ip, $areas)
    {
        //无手机号，创建新用户，关联微信账号，
        $user->head_pic = $thirdInfo['figureurl_qq_1'];
        $user->nickname = $thirdInfo['nickname'];
        $user->sex      = $thirdInfo['gender'];
        $user->save();
        //注册网易云信
        $result          = YunXin::getInstance()->user->create('user___' . $user->id . '_' . uniqid(), $user->nickname, $user->head_pic);
        $user->yx_token  = $result['token'];
        $user->accid     = $result['accid'];
        $user->ip        = $ip;
        $user->api_token = create_token(1);
        $user->area     = implode('-', $areas);
        $user->save();
        return $user;
    }


    /**
     * 由code获取openid,access_token
     * @return mixed
     */
    public function getWxAccessToken($code)
    {
//        $url      = 'https://api.weixin.qq.com/sns/oauth2/access_token?appid=wx4fe9d039da147d1b&secret=eebff446322d01ccb5a71024c30ce218&code=' . $code . '&grant_type=authorization_code';
        $url      = 'https://api.weixin.qq.com/sns/oauth2/access_token?appid=wx9971c415829fa661&secret=344d6b046b4b52f217eabc8ef11d321a&code=' . $code . '&grant_type=authorization_code';
        $json_obj = CommonService::getCurl($url);
        if (empty($json_obj['openid'])) throw new \JsonException('微信openid获取失败');
//      此时已获得微信的openid
//        $json_obj['openid'] = '123456';
//        $json_obj['access_token'] = '123456';
        return $json_obj;
    }


    /**
     * access_token和openid获取用户信息
     * @param $accessToken
     * @param $openid
     * @param $type
     * @return bool|mixed
     */
    public function getWxThirdInfo($accessToken, $openid)
    {
        $url = 'https://api.weixin.qq.com/sns/userinfo?access_token=' . $accessToken . '&openid=' . $openid;
        $json_obj = CommonService::getCurl($url);
        if (isset($json_obj['errcode'])) throw new \JsonException('获取微信用户信息失败');

//        $json_obj = '{
//        "openid":"OPENID",
//        "nickname":"NICKNAME",
//        "sex":1,
//        "province":"PROVINCE",
//        "city":"CITY",
//        "country":"COUNTRY",
//        "headimgurl": "http://wx.qlogo.cn/mmopen/g3MonUZtNHkdmzicIlibx6iaFqAc56vxLSUfpb6n5WKSYVY0ChQKkiaJSgQ1dZuTOgvLLrhJbERQQ4eMsv84eavHiaiceqxibJxCfHe/0",
//        "privilege":[
//        "PRIVILEGE1",
//        "PRIVILEGE2"
//        ],
//        "unionid": " o6_bmasdasdsad6_2sgVt7hMZOPfL"
//        }';
//        $json_obj = json_decode($json_obj, true);

        return $json_obj;
    }


    public function wxCreateUser($user, $thirdInfo, $ip, $areas)
    {
        //无手机号，创建新用户，关联微信账号，
        $user->head_pic = $thirdInfo['headimgurl'];
        $user->nickname = $thirdInfo['nickname'];
        $user->sex      = $thirdInfo['sex'];
        $user->save();
        //注册网易云信
        $result          = YunXin::getInstance()->user->create('user_' . $user->id . '_' . uniqid(), $user->nickname, $user->head_pic);
        $user->yx_token  = $result['token'];
        $user->accid     = $result['accid'];
        $user->ip        = $ip;
        $user->api_token = create_token(1);
        $user->area      = implode('-', $areas);
        $user->save();
        return $user;
    }
}
