<?php

namespace App\Services\App\User;

use App\Helpers\WxOauth;
use App\Http\Controllers\App\V1\User\LoginController;
use App\Models\User;
use App\Models\UserThirdAccount;
use App\Services\Service;
use Illuminate\Support\Facades\DB;
use Zhuzhichao\IpLocationZh\Ip;

class UserIndexService extends Service{

    /**
     * QQ绑定
     * @param $input
     * @return false
     * @throws \JsonException
     */
    public function qqBinding($input, $userId)
    {
        $accessToken = $input['access_token'];
        $openid      = $input['openid'];
        $thirdInfo   = LoginController::getService()->getQqThirdInfo($accessToken, $openid);
        //获取登录地址
        $ip = request()->getClientIp();
        $areas = Ip::find($ip);
        try {
            DB::beginTransaction();
            $info = UserThirdAccount::getInstance()->where(['user_id' => $userId, 'third_type' => 1])->first();
            if (empty($info)) {
                //将第三方QQ用户信息入库
                $third = [
                    'user_id'    => $userId,
                    'third_key'  => $openid,
                    'third_type' => 1,
                    'openid'     => $openid,
                    'nickname'   => $thirdInfo['nickname'],
                    'sex'        => $thirdInfo['gender'],
                    'headimgurl' => $thirdInfo['figureurl_qq_1'],
                ];
                $newThird    = UserThirdAccount::getInstance()->insert($third);
            } else {
                //将第三方QQ用户信息修改
                $third = [
                    'third_key'  => $openid,
                    'openid'     => $openid,
                    'nickname'   => $thirdInfo['nickname'],
                    'sex'        => $thirdInfo['gender'],
                    'headimgurl' => $thirdInfo['figureurl_qq_1'],
                ];
                $newThird    = UserThirdAccount::getInstance()->where(['id' => $info->id])->update($third);
            }
            if (!$newThird) {
                DB::rollBack();
                return false;
            }
            DB::commit();
            return User::getInstance()->find($userId);
        } catch (\Exception $e) {
            DB::rollBack();
            return false;
        }
    }


    /**
     * 微信绑定
     * @param $input
     * @return false
     * @throws \Exception
     */
    public function wxBinding($input, $userId)
    {
        $wxCode      = $input['wx_code'];
        // 移动端使用
        $WxOauth =new WxOauth();// 传入appid和appsecret
//公众号登录需要先获取code,下面方法会自动跳转到微信授权页面
//        $WxOauth->getCode();
// 通过移动端传来的code或者微信回调返回的code获取用户信息
        $user_info = $WxOauth->wxLogin($wxCode);

        if($user_info){
            return $user_info;
//获取到用户信息
        }else{
// 获取错误
         return   $WxOauth->error;
        }

        $wxCode      = $input['wx_code'];
        $wxInfo      = LoginController::getService()->getWxAccessToken($wxCode);
        $accessToken = $wxInfo['access_token'];
        $openid      = $wxInfo['openid'];
        $thirdInfo   = LoginController::getService()->getWxThirdInfo($accessToken, $openid);
        //获取登录地址
        $ip    = request()->getClientIp();
        $areas = Ip::find($ip);
        try {
            DB::beginTransaction();
            $info = UserThirdAccount::getInstance()->where(['user_id' => $userId, 'third_type' => 2])->first();
            //将第三方微信获取用户信息入库
            if (empty($info)) {
                $third = [
                    'user_id'    => $userId,
                    'third_key'  => $openid,
                    'third_type' => 2,
                    'openid'     => $openid,
                    'nickname'   => $thirdInfo['nickname'],
                    'sex'        => $thirdInfo['sex'],
                    'province'   => $thirdInfo['province'],
                    'city'       => $thirdInfo['city'],
                    'country'    => $thirdInfo['country'],
                    'headimgurl' => $thirdInfo['headimgurl'],
                    'privilege'  => json_encode($thirdInfo['privilege']),
                    'unionid'    => $thirdInfo['unionid'],
                ];
                $newThird    = UserThirdAccount::getInstance()->insert($third);
            } else {
                $third = [
                    'third_key'  => $openid,
                    'openid'     => $openid,
                    'nickname'   => $thirdInfo['nickname'],
                    'sex'        => $thirdInfo['sex'],
                    'province'   => $thirdInfo['province'],
                    'city'       => $thirdInfo['city'],
                    'country'    => $thirdInfo['country'],
                    'headimgurl' => $thirdInfo['headimgurl'],
                    'privilege'  => json_encode($thirdInfo['privilege']),
                    'unionid'    => $thirdInfo['unionid'],
                ];
                $newThird    = UserThirdAccount::getInstance()->where(['id' => $info->id])->update($third);
            }

            if (!$newThird) {
                DB::rollBack();
                return false;
            }
            DB::commit();
            return User::getInstance()->find($userId);
        } catch (\Exception $e) {
            DB::rollBack();
            return false;
        }
    }
}
