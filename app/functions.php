<?php


/**
 * 加密 AES，加密模式:AES-128-ECB，PKCS5Padding补码，16位密钥
 *  AES-256-CBC
 *  $zeroPack = pack('i*', 0);
 *  $iv = str_repeat($zeroPack, 4);
 *
 * @param $str  string
 *
 * @return string  返回加密后的数据
 */
function new_encrypt( $str, $token = '' )
{
    if ( !$token )
        $token = env( 'HISSECRET' );
    $str = openssl_encrypt( $str, 'aes-128-ecb', $token, OPENSSL_RAW_DATA );
    return base64_encode( $str );
}

/**
 * 解密 AES，加密模式:AES-128-ECB，PKCS5Padding补码，16位密钥
 *
 * @param $str  string
 *
 * @return string  返回解密后的数据
 */
function new_decrypt( $str, $token = '' )
{
    if ( !$token )
        $token = env( 'HISSECRET' );
    $encrypted = base64_decode( $str );
    return openssl_decrypt( $encrypted, 'aes-128-ecb', $token, OPENSSL_RAW_DATA );
}

/**
 * 获取指定日期段内每一天的日期
 *
 * @param Date $startdate 开始日期
 * @param Date $enddate 结束日期
 *
 * @return Array
 */
function getDateFromRange( $startdate, $enddate )
{

    $stimestamp = strtotime( $startdate );
    $etimestamp = strtotime( $enddate );

    // 计算日期段内有多少天
    $days = ( $etimestamp - $stimestamp ) / 86400 + 1;

    // 保存每天日期
    $date = array();

    for ( $i = 0; $i < $days; $i++ ) {
        $date[] = date( 'Y-m-d', $stimestamp + ( 86400 * $i ) );
    }

    return $date;
}

/**
 * 一维数组处理为二维
 */
function oneToTwo( $array )
{
    if ( !isset( $array[0] ) ) {
        $return[] = $array;
    } else {
        $return = $array;
    }
    return $return;
}


function logLog( $data, $filename = 'schedule' )
{
    $path = storage_path() . "/logs/" . $filename . "/";
    if ( !is_dir( $path ) )
        mkdir( $path, 0777, true );

    if ( !is_dir( $path ) )
        $path = storage_path() . "/logs/";


    $date = date( 'Y-m-d' );
    if ( is_array( $data ) )
        $data = json_encode( $data );
    file_put_contents( $path . $filename . "--" . $date . '.log', date( 'Y-m-d H:i:s' ) . ' -- data:' . $data . PHP_EOL, FILE_APPEND );
}

/**
 *  计算两组经纬度坐标 之间的距离
 *
 * @param     $lat1
 * @param     $lng1
 * @param     $lat2
 * @param     $lng2
 * @param int $len_type len_type （1:m or 2:km);
 * @param int $decimal 保留小数位
 *
 * @return float return m or km
 */
function get_distance_by_lat_lng( $lat1, $lng1, $lat2, $lng2, $len_type = 1, $decimal = 2 )
{
    $radLat1 = $lat1 * PI() / 180.0;   //PI()圆周率
    $radLat2 = $lat2 * PI() / 180.0;
    $a = $radLat1 - $radLat2;
    $b = ( $lng1 * PI() / 180.0 ) - ( $lng2 * PI() / 180.0 );
    $s = 2 * asin( sqrt( pow( sin( $a / 2 ), 2 ) + cos( $radLat1 ) * cos( $radLat2 ) * pow( sin( $b / 2 ), 2 ) ) );
    $s = $s * 6378.137;
    $s = round( $s * 1000 );
    if ( $len_type-- > 1 ) {
        $s /= 1000;
    }
    return round( $s, $decimal );
}

function get_distance_text( $deviation )
{
    if ( $deviation <= 1000 )
        return $deviation . 'm';
    else
        return number_format( $deviation / 1000, 2 ) . 'km';

}

function listToTree( $list, $primaryKey = 'id', $parentKey = 'pid', $childStr = 'children', $root = 0, array &$tree )
{
    if ( is_array( $list ) ) {

        //创建基于主键的数组引用
        $refer = array();

        foreach ( $list as $key => $data ) {
            $refer[ $data[ $primaryKey ] ] = &$list[ $key ];
        }

        foreach ( $list as $key => $data ) {

            //判断是否存在parent
            $parantId = $data[ $parentKey ];

            if ( $root == $parantId ) {


                $tree[] = &$list[ $key ];

            } else {

                if ( isset( $refer[ $parantId ] ) ) {
                    $parent = &$refer[ $parantId ];
                    $parent[ $childStr ][] = &$list[ $key ];
                }

            }
        }
    }

    return $tree;
}

/**
 * @param int $v 1 患者 2 医生app 3 管理后台 4 药店后台 5医院后台
 *
 * @return mixed
 */
function create_token( $v = 1 )
{
    $key = mt_rand();
    $hash = hash_hmac( "sha1", $v . mt_rand() . time(), $key, true );
    $token = $v . str_replace( '=', '', strtr( base64_encode( $hash ), '+/', '-_' ) );
    return $token;
}


function prDates( $start, $end )
{
    $res = [];
    $dt_start = strtotime( $start );
    $dt_end = strtotime( $end );
    while ( $dt_start <= $dt_end ) {
        $res[] = date( 'Y-m-d', $dt_start );
        $dt_start = strtotime( '+1 day', $dt_start );
    }
    return $res;
}

/**
 * 获取Request Rules
 *
 * @param $ins
 * @param $commons
 *
 * @return array
 */
function get_request_rules( $ins, $commons )
{
    $action = get_current_action()['method'];
    $tmpAction = $action . 'Rules';
    if ( method_exists( $ins, $tmpAction ) )
        return array_merge( $commons, (array)$ins->$tmpAction() );

    return $commons;
}

/**
 * 获取当前控制器与方法
 *
 * @return array
 */
function get_current_action()
{
    $action = request()->route()->getActionName();
    if ( $action == 'Closure' )
        list( $class, $method ) = explode( '.', request()->route()->getName() );
    else
        list( $class, $method ) = explode( '@', $action );

    return [ 'controller' => $class, 'method' => $method ];
}

/**
 * CURL发送HTTP请求
 * @param string $url 请求URL
 * @param array $params 请求参数
 * @param string $method 请求方法GET/POST
 * @param  $header 头信息
 * @param  $multi  是否支付附件
 * @param  $debug  是否输出错误
 * @param  $optsother 附件项
 * @return array  $data   响应数据
 */
function curlget( $url, $params = '', $method = 'GET', $header = array(), $multi = false, $debug = false, $optsother = '' )
{
    $opts = array( CURLOPT_TIMEOUT => 10, CURLOPT_RETURNTRANSFER => 1, CURLOPT_SSL_VERIFYPEER => false, CURLOPT_SSL_VERIFYHOST => false, CURLOPT_HTTPHEADER => $header );
    switch ( strtoupper( $method ) ) {/* 根据请求类型设置特定参数 */
        case 'GET':
            $opts[ CURLOPT_URL ] = $params ? $url . '?' . http_build_query( $params ) : $url;
            break;
        case 'POST':
            $params = $multi ? $params : http_build_query( $params );//判断是否传输文件
            $opts[ CURLOPT_URL ] = $url;
            $opts[ CURLOPT_POST ] = 1;
            $opts[ CURLOPT_POSTFIELDS ] = $params;
            break;
        default:
            if ( $debug )
                E( '不支持的请求方式！' );
            break;
    }
    $ch = curl_init();
    if ( $optsother && is_array( $optsother ) )
        $opts = $opts + $optsother;
    curl_setopt_array( $ch, $opts );
    $data = curl_exec( $ch );
    $error = curl_error( $ch );
    curl_close( $ch );/* 初始化并执行curl请求 */
    if ( $error && $debug ) {
        E( '请求发生错误:' . $error );
    }
    return $data;
}

/*
 * uuid 唯一序列码
 */

function uuid( $prefix = '' )
{
    $chars = md5( uniqid( mt_rand(), true ) );
    $uuid = substr( $chars, 0, 8 ) . '-';
    $uuid .= substr( $chars, 8, 4 ) . '-';
    $uuid .= substr( $chars, 12, 4 ) . '-';
    $uuid .= substr( $chars, 16, 4 ) . '-';
    $uuid .= substr( $chars, 20, 12 );
    return $prefix . $uuid;
}




