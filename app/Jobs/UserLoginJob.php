<?php

namespace App\Jobs;

use App\Common\Constants\ConstantQueueRedisKey;
use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Log;

class UserLoginJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;


    /**
     * 任务应该发送到的队列的连接的名称
     *
     * @var string|null
     */
    public $connection = 'redis';

    /**
     * 任务应该发送到的队列的名称
     *
     * @var string|null
     */
    public $queue = ConstantQueueRedisKey::USER_LOGIN_JOB_QUEUE;


    /**
     * 任务最大尝试次数。
     *
     * @var int
     */
    public $tries = 5;


    /**
     * 任务运行的超时时间。
     *
     * @var int
     */
    public $timeout = 300; //秒数

    /**
     * @var User
     */
    public $user;


    /**
     * Create a new job instance.
     * UserLoginJob constructor.
     * @param User $user
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * Execute the job.
     * @return bool
     */
    public function handle()
    {
        Log::debug('用户登录任务触发');
        return true;
    }
}
