<?php

namespace App\Observers;

use App\Models\User;

class UserObserver
{

    /**
     * 监听用户创建事件.
     *
     * @param  User  $user
     * @return void
     */
    public function created(User $user)
    {

    }

    /**
     * 监听用户删除正在事件.
     *
     * @param  User  $user
     * @return void
     */
    public function deleting(User $user)
    {

    }


    /**
     * 监听用户删除完成事件
     *
     * @param User $user
     * @return void
     */
    public function deleted(User $user)
    {

    }



    /**
     * 保存前调用
     * @param User $userModel
     */
    public function saving(User $user)
    {

    }


    /**
     * 保存后调用
     * @param User $user
     */
    public function saved(User $user)
    {

    }


}
