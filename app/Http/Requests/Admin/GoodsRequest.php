<?php

namespace App\Http\Requests\Admin;

use App\Rules\Numeric2Rule;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Arr;

class GoodsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */

    public function rules()
    {
        $common = [

        ];
        return get_request_rules( $this, $common );
    }

    public function addRules()
    {
        return [
            'cate_id'              => 'required',
            'name'                 => 'required',
            'code'                 => 'string|max:100',
            'intro'                => 'required',
            //            'brand_id'             => 'required',
            'status'               => 'required',
            'key_words'            => 'max:255',
            'photos'               => 'required|array',
            'photos.*.image'       => 'required',
            'photos.*.is_default'  => 'required',
            'extre'                => 'required|array',
            'extre.*.num'          => 'required',
            'extre.*.packing_unit' => 'required|string|max:10',
            'extre.*.weight_unit'  => 'required|string|max:10',
            'extre.*.weight_num'   => 'required|string|max:100',
            'extre.*.code'         => 'max:100',
            'extre.*.price'        => [
                'required',
                new Numeric2Rule( 2, Arr::get( $this->attributes(), 'extre.*.price' ) )
            ],
            'extre.*.stock'        => 'required|int|max:99999',
            'extre.*.stock_warn'   => 'required|int|max:999',

        ];
    }

    public function detailRules()
    {
        return [
            'id' => 'required',
        ];
    }

    public function attributes()
    {
        return [
            'cate_id'              => '所属分类',
            'name'                 => '商品名称',
            'code'                 => '商品货号',
            'intro'                => '商品信息',
            'brand_id'             => '所属品牌',
            'status'               => '是否上架',
            'key_word'             => '商品关键词',
            'extre'                => '商品属性',
            'extre.*.num'          => '商品数量',
            'extre.*.packing_unit' => '包装单位',
            'extre.*.weight_unit'  => '计量单位',
            'extre.*.weight_num'   => '计量数量',
            'extre.*.price'        => '销售价格',
            'extre.*.stock'        => '商品库存',
            'extre.*.stock_warn'   => '商品预警值',
            'extre.*.code'         => 'SKU编码'
        ];

    }
}
