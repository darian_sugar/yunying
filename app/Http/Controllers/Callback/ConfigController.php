<?php

namespace App\Http\Controllers\Callback;


use App\Http\Controllers\Controller;


class ConfigController extends Controller
{

    public function index()
    {
        return $this->success( [
            'user_verison'           => '1.0.0',
            'user_force_update'      => false,
            'doctor_verison'         => '1.0.0',
            'doctor_force_update'    => false,
            'doctor_pc_verison'      => '1.0.0',
            'doctor_pc_force_update' => false,
        ] );
    }
}
