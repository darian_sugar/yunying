<?php

namespace App\Http\Controllers\Callback;

use App\Helpers\YunXin\YunXin;
use App\Models\AvMessage;
use App\Models\AvMessageFile;
use App\Models\Doctor;
use App\Models\Message;
use App\Models\User;
use App\Models\UserDoctorRecord;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Log;

/**
 * 网易云消息抄送
 * Class MessageController
 * @package App\Http\Controllers\Copy
 */
class MessageController extends Controller
{
    //
    public function index()
    {
        $data = $this->request->all();
        $header = $this->request->header();
        $str = json_encode( $data );
        logLog( $str, 'copytosys' );
        Log::debug( "copy header", $header );
        try {

            YunXin::getInstance()->Base->checkSumRequest( $this->request );
            //$data = json_decode($this->request->getContent(), true);
            if ( $data['eventType'] == 1 ) {
                $type = strtoupper( $data['convType'] );
                if ( $type == 'PERSON' ) {
                    // 二人对话信息
                    //$this->getPersonMsg($arr['body']);
                }
            } elseif ( $data['eventType'] == 3 ) {
                //用户登出事件
                $this->changeAskStatus( $data );
            } elseif ( $data['eventType'] == 5 ) {
                //音视频 时长
                $this->saveAvDuration( $data );
            } elseif ( in_array( $data['eventType'], [ 6, 22 ] ) ) {
                //音视频 存储
                $this->saveAvDownload( $data );
            }
        } catch ( \Exception $e ) {
            logLog( $e->getMessage(), 'error_copytosys' );
            logLog( json_encode( $data ), 'error_copytosys' );
        }
        return $this->message( 'success' );

    }

    /**
     * 保存音视频 时长信息
     *
     * @param array $arr
     *
     * @return bool
     */
    public function saveAvDuration( array $arr )
    {
        if ( $arr['channelName'] ) {

            return true;
        } else {
            $this->callInquery( $arr );
        }

    }

    /**
     * 保存音视频 下载链接
     *
     * @param array $arr
     *
     * @return bool
     */
    protected function saveAvDownload( array $arr )
    {
        $arr['fileinfo'] = json_decode( $arr['fileinfo'], true );
        foreach ( $arr['fileinfo'] as $k => $v ) {
            $insert[ $k ]['caller'] = 0;
            if ( isset( $v['caller'] ) && $v['caller'] == true )
                $insert[ $k ]['caller'] = 1;
            $insert[ $k ]['user_id'] = $v['user'];
            $insert[ $k ]['channelId'] = $v['channelid'];
            $insert[ $k ]['filename'] = $v['filename'];
            $insert[ $k ]['size'] = $v['size'];
            $insert[ $k ]['type'] = $v['type'];
            $insert[ $k ]['vid'] = $v['vid'];
            $insert[ $k ]['url'] = $v['url'];
        }
        AvMessageFile::insert( $insert );
    }

    /**
     * 二人对话信息
     *
     * @param array $arr
     *
     * @return bool
     */
    protected function getPersonMsg( array $arr )
    {
        $msg = Message::getInstance()->where( [ 'msgidServer' => $arr['msgidServer'], 'msgidClient' => $arr['msgidClient'] ] )->first();
        if ( $msg )
            return true;
        $toFirstLetter = strtolower( substr( $arr['to'], 0, 1 ) );           //接受者
        $fromFirstLetter = strtolower( substr( $arr['fromAccount'], 0, 1 ) );//发送者
        if ( $toFirstLetter == 'u' && $fromFirstLetter == 'd' )//医生  病人
            $class = 1;
        elseif ( $toFirstLetter == 'd' && $fromFirstLetter == 'u' )//病人 医生
            $class = 2;
        elseif ( $toFirstLetter == 'u' && $fromFirstLetter == 'u' )
            $class = 4;

        elseif ( $toFirstLetter == 'd' && $fromFirstLetter == 'd' )
            $class = 3;
        else
            return true;

        if ( strtoupper( $arr['msgType'] ) == 'TEXT' )
            $msgType = 1;
        elseif ( strtoupper( $arr['msgType'] ) == 'PICTURE' )
            $msgType = 2;
        else
            return true;

        if ( $toFirstLetter == 'u' ) {
            $toInfo = User::getInstance()->getByAccid( $arr['to'] );
        } else {
            $toInfo = Doctor::getInstance()->getByAccid( $arr['to'] );
        }
        if ( $fromFirstLetter == 'd' ) {
            $fromInfo = Doctor::getInstance()->getByAccid( $arr['fromAccount'] );
        } else {
            $fromInfo = User::getInstance()->getByAccid( $arr['fromAccount'] );
        }
        $message = new Message();
        $message->user_id = $fromInfo->id;
        $message->to_id = $toInfo->id;
        $message->content = $arr['body'];
        $message->type = $msgType;
        $message->class = $class;
        $message->msgidServer = $arr['msgidServer'];
        $message->msgidClient = $arr['msgidClient'];
        $message->save();
    }

    /**
     * 登出时间回调
     *
     * @param $data
     *
     * @return bool
     */
    protected function changeAskStatus( $data )
    {
        $accid = $data['accid'];
        $timestamp = $data['timestamp'];
        $firstLetter = strtolower( substr( $accid, 0, 1 ) );//接受者
        if ( $firstLetter != 'd' )
            return true;
        $doctor = Doctor::getInstance()->getByAccid( $accid );
        if ( !$doctor->ask_change_time || $doctor->ask_change_time < $timestamp ) {
            $doctor->ask_status = 2;             //离线
            $doctor->yx_status = 2;              //离线
            $doctor->yx_change_time = $timestamp;//离线
            $doctor->save();
        } else {
            return true;
        }

    }


    public function callInquery( $arr )
    {

        if ( strtoupper( $arr['status'] ) == 'SUCCESS' ) {
            if ( !is_array( $arr['members'] ) )
                $arr['members'] = json_decode( $arr['members'], true );
        } else
            return true;
        $count = count( $arr['members'] );
        if ( $count > 2 )
            return false;
        foreach ( $arr['members'] as $k => $v ) {
            if ( isset( $v['caller'] ) && $v['caller'] == true ) {
                $from = $v['accid'];
                $duration = $v['duration'];
            } else {
                $to = $v['accid'];
                $to_duration = $v['duration'];
            }
        }
        $toFirstLetter = strtolower( substr( $to, 0, 1 ) );    //接受者
        $fromFirstLetter = strtolower( substr( $from, 0, 1 ) );//发送者
        if ( $toFirstLetter == 'u' && $fromFirstLetter == 'd' )//医生  病人
            $class = 1;
        elseif ( $toFirstLetter == 'd' && $fromFirstLetter == 'u' )//病人 医生
            $class = 2;
        elseif ( $toFirstLetter == 'u' && $fromFirstLetter == 'u' )
            $class = 4;
        elseif ( $toFirstLetter == 'd' && $fromFirstLetter == 'd' )
            $class = 3;
        else
            return true;
        if ( $toFirstLetter == 'u' ) {
            $toInfo = User::getInstance()->where( 'accid', $to )->first();
        } else {
            $toInfo = Doctor::getInstance()->where( 'accid', $to )->first();
        }
        if ( $fromFirstLetter == 'd' ) {
            $fromInfo = Doctor::getInstance()->getByAccid( $from );
        } else {
            $fromInfo = User::getInstance()->getByAccid( $from );
        }
        if ( strtoupper( $arr['type'] ) == 'AUDIO' ) {
            $type = 2;
            //音频
        } elseif ( strtoupper( $arr['type'] ) == 'VEDIO' ) {
            //视频
            $type = 3;
        } else {
            return false;
        }
        if ( $class == 1 ) {
            $where['user_id'] = $toInfo->id;
            $where['doctor_id'] = $fromInfo->id;
            $record = UserDoctorRecord::getInstance()->getRecord( $toInfo->id, $fromInfo->id, $type );
        } elseif ( $class == 2 ) {
            $where['user_id'] = $fromInfo->id;
            $where['doctor_id'] = $toInfo->id;
            $record = UserDoctorRecord::getInstance()->getRecord( $fromInfo->id, $toInfo->id, $type );
        } else {
            return false;
        }
        if ( !$record )
            throw new \JsonException( '记录未找到，' . json_encode( $arr ) );
        $av = AvMessage::getInstance()->where( [ 'channelId' => $arr['channelId'] ] )->first();
        if ( !$av )
            $av = new AvMessage();
        $av->record_id = $record->id;
        $av->channelId = $arr['channelId'];
        $av->user_id = $fromInfo->id;
        $av->to_id = $toInfo->id;
        $av->type = ( $type - 1 );
        $av->start_time = date( 'Y-m-d H:i:s', intval( $arr['createtime'] / 1000 ) );
        $av->duration = $duration;
        $av->to_duration = $to_duration;
        $av->class = $class;
        $av->save();
        //挂断直接清零
        //$time = $record->time - (ceil($user_duration/60));
        //if($time <= 0)
        //    $time = 0;
        $record->chat_at = $av->created_at;
        $record->time = 0;
        $record->save();
    }
}
