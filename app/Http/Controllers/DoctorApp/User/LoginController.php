<?php

namespace App\Http\Controllers\DoctorApp\User;

use App\Helpers\YunXin\YunXin;
use App\Http\Controllers\Controller;
use App\Helpers\Sms;
use App\Models\Doctor;
use App\Models\OperationLog;
use App\Models\Protocol;
use App\Models\User;
use App\Services\Common\CheckService;
use App\Services\Common\LoginService;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Redis;

class LoginController extends Controller
{
    /**
     * 密码登录
     * @return array
     * @throws JsonException
     * @throws \Illuminate\Validation\ValidationException
     */
    public function login()
    {
        $this->validate( $this->request, [
            "account"  => 'required|max:255|exists:doctors',
            'password' => 'required|string|min:6|max:16',
        ] );
        $input = $this->request->all();
        $user = Doctor::getInstance()->getByAccount( $input['account'] );
        if ( !$user )
            return $this->failed( '用户不存在' );
        $id = $user->id;
        $table = 'doctors';
        CheckService::checkPasswordErrorLockTime( $id, $table );
        if ( !Hash::check( $input['password'], $user->password ) ) {
            CheckService::checkPasswordErrorCount( $id, $table );
            return $this->failed( '密码错误' );
        }
        if ( $user->status != 1 )
            return $this->failed( '账户未启用' );
        $result = $user->saveToken();
        $user->makeVisible( 'api_token' );
        $user->makeVisible( 'accid' );
        $user->makeVisible( 'yx_token' );
        if ( $result === false )
            return $this->failed( '登陆失败' );
        else
            LoginService::clearPwdErrorLockNumAndTime( $id, $table );
        return $this->success( $user );
    }

    public function loginOut()
    {
        $user = Auth::user();
        $user->api_token = '';
        $user->ask_status = 2;
        $user->save();
        return $this->success( '退出成功' );
    }

}
