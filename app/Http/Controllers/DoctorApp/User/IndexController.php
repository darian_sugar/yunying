<?php

namespace App\Http\Controllers\DoctorApp\User;

use App\Helpers\PayClass;
use App\Helpers\Sms;
use App\Helpers\YunXin\YunXin;
use App\Helpers\ZtySms;
use App\Http\Controllers\Controller;
use App\Http\Controllers\DoctorApp\BaseController;
use App\Http\Service\CheckService;
use App\Models\Area;
use App\Models\CircleOfFriendComments;
use App\Models\Department;
use App\Models\Doctor;
use App\Models\DoctorBalance;
use App\Models\DoctorDetail;
use App\Models\Hospital;
use App\Models\HospitalDepartment;
use App\Models\IllnessOrder;
use App\Models\InquiryOrder;
use App\Models\KnowCate;
use App\Models\KnowCourse;
use App\Models\KnowFile;
use App\Models\Message;
use App\Models\Suggest;
use App\Models\SystemMsg;
use App\Models\UsefulDepartment;
use App\Models\User;
use App\Models\WithdrawOrder;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Redis;
use Illuminate\Validation\Rule;

class IndexController extends BaseController
{
    //

    public function index()
    {

        $user = Auth::user();
        $data = Doctor::getInstance()->where( 'id', $user->id )
            ->withCount( [
                //问诊消息
                'hasManyMsgStatus as has_many_ask_msg' => function ( $query ) {
                    $query->where( [ 'from' => 2, 'status' => 0 ] );
                },
                'hasManySystemMsgStatus',
            ] )
            ->first();
        return $this->success( $data );
    }

    public function edit()
    {
        $this->validate( $this->request, [
            'ask_status' => 'max:2|min:1',
            'head_pic'   => 'max:255',
            'name'       => 'max:255',
        ] );
        $this->request->only( [ 'name', 'head_pic', 'ask_status' ] );
        $user = Auth::user();
        DB::beginTransaction();
        $input = $this->request->all();
        try {
            $user->fill( $input )->save();
            //注册网易云信
            $sign = $user->job_title;
            YunXin::getInstance()->User->updateUserInfo( $user->accid, $user->name, $user->head_pic, $sign );
            DB::commit();
            return $this->message( '修改成功' );

        } catch ( \Exception $e ) {
            DB::rollBack();
            return $this->failed( $e->getMessage() . $e->getFile() );
        }
    }


    /**
     * 账户明细
     * @return mixed
     */
    public function banlanceList()
    {
        try {
            $user = Auth::user();
            $data = $user->UserHasMany( DoctorBalance::class )->orderByDesc( 'id' )
                ->paginate( $this->pageSize )->toArray();
            return $this->success( $data );
        } catch ( \Exception $e ) {

            return $this->failed( $e->getMessage() );
        }

    }


    /**
     * 问诊信息
     * @return mixed
     */
    public function visits()
    {
        $user = Auth::user();
        $data = InquiryOrder::getInstance()->where( [ 'doctor_id' => $user->id, 'order_status' => 1, 'pay_status' => 1 ] )->select( 'id', 'created_at' )->get();
        $today = date( 'Y-m-d' );
        $total_count = $data->count();
        $today_count = $data->where( 'created_at', '>=', $today )->count();
        return $this->success( [ 'total_count' => $total_count, 'today_count' => $today_count ] );
    }


    /**
     * 提现
     * @return mixed
     * @throws \Illuminate\Validation\ValidationException
     */
    public function transfer()
    {
        $this->validate( $this->request, [
            'amount' => 'required',
        ] );
        try {
            $user = Auth::user();
            $user->makeVisible( 'balance' );
            $apply = WithdrawOrder::getInstance()->hasTransfer( $user->id );
            if ( $apply )
                return $this->failed( '本月已申请过提现' );
            $input = $this->request->all();
            //$key = ZtySms::DOCTOR_WITHDRAW . $user->mobile;
            //ZtySms::getInstance()->checkCode($key, $input['code']);
            if ( $input['amount'] < 0.1 )
                return $this->failed( '提现金额最低为0.1元' );
            if ( $input['amount'] > $user->balance )
                return $this->failed( '账户余额不足' );
            if ( !$user->ali_account )
                return $this->failed( '请先绑定支付宝账户' );
            $input['account'] = $user->ali_account;
            $input['pay_amount'] = $input['amount'];
            $input['method_pay'] = 1;
            $input['order_number'] = PayClass::getInstance()->getOrderNumber( PayClass::OORDERTRANSFER, $user->id );
            $user->UserHasOne( WithdrawOrder::class )->create( $input );
            return $this->message( '申请成功,请耐心等待审核' );
        } catch ( \Exception $e ) {

            return $this->failed( $e->getMessage() );
        }
    }

    /**
     * 获取提现验证码
     *
     * @param Sms $sms
     *
     * @return array
     * @throws JsonException
     * @throws \Illuminate\Validation\ValidationException
     */
    public function withdrawSms()
    {
        try {
            $user = Auth::user();
            $mobile = $user->mobile;
            $code = ZtySms::getInstance()->sendSing( 'amount_withdraw', $mobile, ZtySms::DOCTOR_WITHDRAW );
            if ( $code->code == 200 )
                return $this->message( '发送成功' );
            else
                return $this->failed( '发送失败' );
        } catch ( \Exception $exception ) {
            return $this->failed( $exception->getMessage() );
        }
    }

    /**
     * 修改密码
     * @return array
     * @throws \Illuminate\Validation\ValidationException
     */
    public function updateMobile()
    {

        $this->validate( $this->request, [
            'mobile' => 'bail|required|max:11|min:11',
            'code'   => 'required|max:4|min:4|',
        ] );

        try {
            $input = $this->request->all();
            $key = ZtySms::DOCTOR_UPDATE_MOBILE . $input['mobile'];
            ZtySms::getInstance()->checkCode( $key, $input['code'] );
            $user = Doctor::getInstance()->getByMobile( $input['mobile'] );
            if ( $user )
                return $this->failed( '手机号已存在' );
            $user = Auth::user();
            $user->mobile = $input['mobile'];
            $res = $user->save();
            if ( $res )
                return $this->message( '修改成功' );
            else
                return $this->fail( '修改失败' );
        } catch ( \Exception $e ) {
            return $this->failed( $e->getMessage() );
        }

    }

    /**
     * 获取修改密码证码
     *
     * @param Sms $sms
     *
     * @return array
     * @throws JsonException
     * @throws \Illuminate\Validation\ValidationException
     */
    public function updateMobileSms()
    {
        $this->validate( $this->request, [
            'mobile' => 'bail|required|max:11|min:11',
        ] );
        try {
            $mobile = $this->request->get( 'mobile' );
            $user = Doctor::getInstance()->getByMobile( $mobile );
            if ( $user->id == Auth::user()->id )
                $this->failed( '不允许修改为当前手机号' );
            if ( $user )
                return $this->failed( '手机号已存在' );
            $code = ZtySms::getInstance()->sendSing( 'change_mobile', $mobile, ZtySms::DOCTOR_UPDATE_MOBILE );
            if ( $code->code == 200 )
                return $this->message( '发送成功' );
            else
                return $this->failed( '发送失败' );
        } catch ( \Exception $exception ) {
            return $this->failed( $exception->getMessage() );
        }
    }

    /**
     * 修改密码
     * @return array
     * @throws \Illuminate\Validation\ValidationException
     */
    public function updatePwd()
    {

        $this->validate( $this->request, [
            'old_password'          => 'required|string',
            'password'              => 'required|string|min:6|max:16|confirmed',
            'password_confirmation' => 'required|same:password',
        ] );

        try {
            $user = Auth::user()->makeVisible( 'password' );
            $input = $this->request->all();
            CheckService::checkPassword( $input['password'] );
            $input['mobile'] = $user->mobile;
            if ( !Hash::check( $input['old_password'], $user->password ) )
                return $this->failed( '密码错误' );
            $user->password = Hash::make( $input['password'] ) ?? '';
            $res = $res = $user->save();
            if ( $res )
                return $this->message( '修改成功' );
            else
                return $this->fail( '修改失败' );
        } catch ( \Exception $e ) {
            return $this->failed( $e->getMessage() );
        }

    }

    /**
     * 获取修改密码证码
     *
     * @param Sms $sms
     *
     * @return array
     * @throws JsonException
     * @throws \Illuminate\Validation\ValidationException
     */
    public function updatePwdSms()
    {
        $mobile = Auth::user()->mobile;
        $code = ZtySms::getInstance()->sendSing( 'change_pwd', $mobile, ZtySms::DOCTOR_UPDATE_PASSWORD );
        if ( $code->code == 200 )
            return $this->message( '发送成功' );
        else
            return $this->failed( '发送失败' );
    }

    /**
     * 反馈
     * @return mixed
     * @throws \Illuminate\Validation\ValidationException
     */
    public function suggest()
    {
        $this->validate( $this->request, [
            'type'    => 'required',
            'content' => 'required|max:500',
            'img'     => 'array|mix:3'
        ] );
        $user = Auth::user();
        $input = $this->request->all();
        $input['source'] = 2;
        $res = $user->hasManySuggest()->create( $input );
        if ( $res )
            return $this->message( '反馈成功' );
        else
            return $this->fail( '反馈失败' );

    }

    public function bindAli()
    {
        $this->validate( $this->request, [
            'account' => 'required',
        ] );
        $user = Auth::user();
        $user->ali_account = $this->request->get( 'account' );
        $user->save();
        return $this->message( '绑定成功' );


    }

    public function getDoctorDetai()
    {
        $this->validate( $this->request, [
            'doctor_id' => 'required'
        ] );
        $data = Doctor::getInstance()->select( 'id', 'name', 'head_pic', 'accid', 'yx_token' )->first();
        if ( !$data )
            return $this->failed( '医生信息不存在' );
        return $this->success( $data );
    }

    public function getUserDetail()
    {
        $name = $this->request->get( 'name' );
        $data = User::getInstance()->where( 'status', 1 );
        if ( !empty( $name ) ) {
            $data = $data->where( 'name', 'like', '%' . $name . '%' );
        };
        $data = $data->where( 'id_card', '!=', '' )
            ->select( 'id', 'name', 'id_card', 'sex', 'birthday', 'mobile' )->paginate( $this->pageSize );
        return $this->success( $data );
    }

    public function getDoctorList()
    {
        $this->validate( $this->request, [
            'h_id' => 'filled|int',
            'd_id' => 'filled|int',
            'name' => 'filled',
        ] );
        $input = $this->request->all();
        $user = Auth::user();
        $where = [];
        if ( $input['name'] ) {
            $where[] = [ 'name', 'like', '%' . $input['name'] . '%' ];
        }
        if ( $input['h_id'] )
            $where['h_id'] = $input['h_id'];
        if ( $input['d_id'] )
            $where['d_id'] = $input['d_id'];
        $model = Doctor::getInstance()->select( 'id', 'name', 'h_id', 'd_id', 't_id', 'head_pic', 'status', 'accid', 'good_at' )
            ->whereHas( 'belongsToHospital', function ( $querys ) use ( $input ) {
                if ( isset( $input['province_id'] ) )
                    $querys->where( 'province_id', $input['province_id'] );
                if ( isset( $input['city_id'] ) )
                    $querys->where( 'city_id', $input['city_id'] );
                if ( isset( $input['area_id'] ) )
                    $querys->where( 'area_id', $input['area_id'] );
            } )
            ->with( [ 'belongsToHospital:id,name',
                      'belongsToDepartment:id,name', ] );
        $data = $model->where( $where )->where( 'id', '!=', $user->id )->where( 'status', 1 )->paginate( $this->pageSize );
        return $this->success( $data );
    }

    public function hospitalList()
    {
        $input = $this->request->all();
        $model = Hospital::getInstance()->select( 'id', 'name', 'level_id', 'attr_id', 'popular_department', 'hospital_image' )->with( [
            'hasOneAttr:id,attr_name',
            'hasOneLevel:id,level_name',
        ] )->where( [ 'status' => 1 ] );

        if ( isset( $input['province_id'] ) )
            $model->where( 'province_id', $input['province_id'] );
        if ( isset( $input['city_id'] ) )
            $model->where( 'city_id', $input['city_id'] );
        if ( isset( $input['area_id'] ) )
            $model->where( 'area_id', $input['area_id'] );
        if ( isset( $input['key_word'] ) )
            $model->where( 'name', 'like', '%' . $input['key_word'] . '%' );
        $data = $model->paginate( $this->pageSize );
        return $this->success( $data );
    }

    public function departmentList()
    {
        $h_id = $this->request->get( 'h_id' );
        if ( $h_id ) {
            $depId = HospitalDepartment::getInstance()->whereIn( 'h_id', $h_id )->distinct()->pluck( 'd_id' );
        }
        $where['status'] = 1;
        $data = Department::getInstance()->when( $depId, function ( $where, $value ) {
            $where->whereIn( 'id', $value );
        } )->select( 'id', 'name', 'p_id', 'image' )->where( $where )->paginate( $this->pageSize );
        return $this->success( $data );
    }

    /**
     * 科室列表
     * @return mixed
     * @throws \Illuminate\Validation\ValidationException
     */
    public function departmentTree()
    {
        $input = $this->request->all();
        if ( $input['h_id'] ) {
            $result = HospitalDepartment::getInstance()
                ->with( [
                    'belongsToDepartment:id,name,image'
                ] )->where( 'h_id', $input['h_id'] )->get();
            $data = $result->transform( function ( $item ) {
                $newItem['id'] = $item->d_id;
                $newItem['p_id'] = $item->p_id;
                $newItem['name'] = $item->belongsToDepartment->name;
                $newItem['image'] = $item->belongsToDepartment->image;
                return $newItem;
            } )->toArray();
        } else {
            $data = Department::getInstance()->where( [ 'status' => 1 ] )
                ->select( 'id', 'p_id', 'name', 'image' )->get()->toArray();
        }
        $tree = [];
        listToTree( $data, 'id', 'p_id', 'children', 0, $tree );
        return $this->success( $tree );
    }

    public function usefulDepartmentList()
    {
        $where['status'] = 1;
        $data = UsefulDepartment::getInstance()->select( 'id', 'name' )->where( $where )->paginate( $this->pageSize );
        return $this->success( $data );
    }

    public function getAreaList()
    {
        $data = Area::getInstance()->selectRaw( 'id as value,name as label,pid' )->get()->toArray();
        $tree = [];
        listToTree( $data, 'value', 'pid', 'children', 0, $tree );
        if ( isset( $tree[0]['children'] ) )
            $tree = $tree[0]['children'];
        return $this->success( $tree );
    }

    public function getLineAreaList()
    {
        $pid = $this->request->get( 'pid' );
        if ( $pid )
            $where['pid'] = $pid;
        else
            $where[] = [ 'pid', 1 ];
        $data = Area::getInstance()->where( $where )->pluck( 'name', 'id' );
        return $this->success( $data );
    }


    /**
     * 我的上传
     * @return mixed
     * @throws \Illuminate\Validation\ValidationException
     */
    public function myUpload()
    {
        $this->validate( $this->request, [
            'type'    => 'required',
            'cate_id' => 'filled',
        ] );
        $type = $this->request->get( 'type', 1 );
        $input = $this->request->all();
        $user = Auth::user();
        $where['author_id'] = $user->id;
        $where['from'] = 2;
        if ( $input['cate_id'] )
            $where['cate_id'] = $input['cate_id'];
        if ( $type == 1 ) {
            $data = KnowFile::getInstance()->select( 'id', 'title', 'price', 'read' )->where( $where )->withCount( [
                'hasManyDown as is_down' => function ( $query ) use ( $user ) {
                    $query->where( [ 'u_id' => $user->id, 'from' => 2, 'pay_status' => 1 ] );
                },
                'hasManyDown'
            ] )->paginate( $this->pageSize );
        } else {
            $course = KnowCourse::getInstance()->selectRaw( 'id,title,price' )->withCount( [
                'hasManyDown as is_down' => function ( $query ) use ( $user ) {
                    $query->where( [ 'u_id' => $user->id, 'from' => 2, 'pay_status' => 1 ] );
                },
                'hasManyDown',
            ] )->where( $where );
            $data = $course->paginate( $this->pageSize );
        }

        return $this->success( $data );
    }


    /**
     * 开启关闭直播
     * @throws \Illuminate\Validation\ValidationException
     */
    public function avStatus()
    {
        $this->validate( $this->request, [
            'status' => 'required|int|between:1,2',
        ] );
        $user = Auth::user();
        $input = $this->request->all();
        $detail = DoctorDetail::getInstance()->where( 'doctor_id', $user->id )->firstOrFail();
        $detail->av_status = $input['status'];
        $detail->save();
        return $this->message( '操作成功' );
    }

    /**
     * 我的收藏
     * @return mixed
     * @throws \Illuminate\Validation\ValidationException
     */
    public function knowCollect()
    {
        $this->validate( $this->request, [
            'type' => 'required|min:1|max:2'
        ] );
        $input = $this->request->all();
        $user = Auth::user();
        $model = $user->hasManyCollect()
            ->select( 'id', 'c_id', 'type' )
            ->selectRaw( '1 as is_collect' )
            ->where( 'type', $input['type'] );

        if ( $input['type'] == 1 )
            $model->with( [
                'belongsToKnowFile',
                'belongsToKnowFile.belongsToCate:id,name',
                'belongsToKnowFile.hasManyDown' => function ( $query ) use ( $user ) {
                    $query->where( [ 'u_id' => $user->id, 'from' => 1, 'pay_status' => 1 ] )->take( 1 );
                }
            ] );
        else {
            $model->with( [
                'belongsToKnowCourse:id,cate_id,image,title,author_id,price,read,duration,from,content',
                'belongsToKnowCourse.belongsToCate:id,name',
                'belongsToKnowCourse.belongsToDoctor:id,name,head_pic,h_id,d_id,t_id',
                'belongsToKnowCourse.belongsToDoctor.belongsToDepartment:id,name',
                'belongsToKnowCourse.belongsToDoctor.belongsToHospital:id,name',
                'belongsToKnowCourse.hasManyDown' => function ( $query ) use ( $user ) {
                    $query->where( [ 'u_id' => $user->id, 'from' => 1, 'pay_status' => 1 ] )->take( 1 );
                }
            ] )->withCount( [
                'hasManyComment',
            ] );
        }
        $data = $model->paginate( $this->pageSize );
        return $this->success( $data );

    }

}
