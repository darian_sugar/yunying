<?php

namespace App\Http\Controllers\DoctorApp\Common;

use App\Helpers\Upload;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UploadController extends Controller
{
    //
    public function upload()
    {
        $this->validate( $this->request, [
            'type' => 'required|between:1,7',
            'file' => 'required',
        ] );
        try {
            $type = $this->request->get( 'type' );
            $file = $this->request->file( 'file' );
            $input['url'] = Upload::getInstance()->uploadFile( $file, $type );
            $input['full_url'] = Upload::getInstance()->getPicName( $input['url'], $type );
            $input['size'] = bcdiv( $file->getClientSize(), 1024, 1 );
            return $this->success( $input );
        } catch ( \Exception $e ) {
            return $this->failed( $e->getMessage() );

        }
    }
}
