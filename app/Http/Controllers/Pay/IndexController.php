<?php

namespace App\Http\Controllers\Pay;

use App\Helpers\DingDingRobotService;
use App\Helpers\PayClass;
use App\Models\Appointment;
use App\Models\Doctor;
use App\Models\GoodsOrder;
use App\Models\InquiryOrder;
use App\Models\PayNotify;
use App\Models\User;
use App\Models\UserCart;
use App\Models\UserDoctorRecord;
use App\Services\Model\GoodsOrderService;
use App\Services\ServiceManager;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Yansongda\Pay\Exceptions\InvalidSignException;
use Yansongda\Pay\Pay;

class IndexController extends Controller
{

    public $user_id, $type;

    /**
     * 支付宝异步通知
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \Exception
     */
    public function aliNotify()
    {
        $url = Request()->url();
        $request = \Symfony\Component\HttpFoundation\Request::createFromGlobals();
        $get = $request->request->count() > 0 ? $request->request->all() : $request->query->all();
        //日志
        $insert['content'] = $url . json_encode( $get );
        $log = PayNotify::getInstance()->create( $insert );
        //开始
        file_put_contents( storage_path( 'ali.txt' ), json_encode( $get ), FILE_APPEND );
        $pays = new PayClass();
        $alipay = Pay::alipay( $pays->getConfig( 1 ) );
        try {
            $data = $alipay->verify( $get ); // 是的，验签就这么简单！
            $log->verify = json_encode( $data->all() );
            $log->save();

            //验证trade_status
            if ( $data->get( 'trade_status' ) == 'TRADE_SUCCESS' || $data->get( 'trade_status' ) == 'TRADE_FINISHED' ) {
                if ( $data->get( 'app_id' ) != getenv( 'ALI_APP_ID' ) )
                    throw new \JsonException( '支付商户号不匹配' );
                DB::beginTransaction();
                $this->confirm( $data );
                DB::commit();
            }
        } catch ( \Exception $e ) {
            DB::rollBack();
            DingDingRobotService::getInstance()->send( [ '平台支付宝异步回调', $e->getMessage() ], 3 );
            $log->err_result = $e->getMessage();
            $log->save();
            DingDingRobotService::getInstance()->send( [ '支付宝异步回调', $e->getMessage() ], 3 );
            if ( $data ) {
                $order_no = $data->get( 'out_trade_no' );
                $total_amount = $data->get( 'total_amount' );
                $pays->aliRefund( $order_no, $total_amount );
            }
        }
        return $alipay->success();
    }


    /**
     * 微信异步通知
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws InvalidSignException
     * @throws \Yansongda\Pay\Exceptions\InvalidArgumentException
     */
    public function wxNotify()
    {
        file_put_contents( storage_path( 'wxpay.txt' ), Request::createFromGlobals()->getContent(), FILE_APPEND );
        //日志
        $url = Request()->url();
        $insert['content'] = $url . json_encode( Request::createFromGlobals()->getContent() );
        $log = PayNotify::getInstance()->create( $insert );
        $pays = new PayClass();
        $pay = Pay::wechat( $pays->getConfig( 2 ) );

        try {
            $data = $pay->verify(); // 是的，验签就这么简单！
            $log->verify = json_encode( $data->all() );
            $log->save();

            //验证trade_status
            if ( $data->get( 'return_code' ) == 'SUCCESS' ) {
                DB::beginTransaction();
                $price = bcdiv( $data->get( 'total_fee', 0 ), 100, 2 );
                $data->set( 'total_amount', $price );
                $attach = $data->get( 'attach' );
                $data->set( 'passback_params', $attach );
                $this->confirm( $data );
                DB::commit();
            }
        } catch ( \Exception $e ) {
            DB::rollBack();
            $log->err_result = $e->getMessage();
            $log->save();

            //退款
            if ( $data ) {
                $order_no = $data->get( 'out_trade_no' );
                $total_amount = $data->get( 'total_amount' );
                $total_amount = $total_amount * 100;
                $pays->wxRefund( $order_no, $total_amount, $total_amount, $this->type . '退款' );
            }
        }
        return $pay->success();
    }


    /**
     * @param $data
     *
     * @throws \JsonException
     */
    protected function confirm( $data )
    {

        //验证out_trade_no是否存在
        $order_no = $data->get( 'out_trade_no' );
        $type = Str::substr( $order_no, 0, 2 );
        if ( in_array( $type, [ PayClass::ORDERTEXT, PayClass::ORDERAUDICE, PayClass::ORDERVIDEO ] ) ) {
            //问诊
            $this->type = '线上咨询';
            $this->confirmInquiryOrder( $order_no, $data, $type );
        } elseif ( $type == PayClass::ORDERAPPOINT ) {
            $this->type = '线下预约';
            $this->confirmAppoint( $order_no, $data );
        } elseif ( $type == PayClass::ORDERGOODS ) {
            $this->type = '商品订单';
            $this->confirmGoodsOrder( $order_no, $data );
        }
    }


    /**
     * 线上问诊
     *
     * @param $order_no
     * @param $data
     *
     * @throws \JsonException
     */
    protected function confirmInquiryOrder( $order_no, $data, $type )
    {
        $order = InquiryOrder::getInstance()->with( [ 'belongsToDoctor:id,name' ] )->where( [ 'order_number' => $order_no ] )->firstOrFail();
        $this->user_id = $order->user_id;
        $doctor = Doctor::getInstance()->where( 'id', $order->doctor_id )->firstOrFail();
        $user = User::getInstance()->where( 'id', $order->user_id )->firstOrFail();
        if ( $order->pay_status == 1 )
            return true;
        if ( env( 'APP_ENV' ) == 'production' && $order->pay_amount != $data->get( 'total_amount' ) )
            throw new \JsonException( '支付金额不匹配' . $order->pay_amount . '!=' . $data->get( 'total_amount' ) );
        $order->pay_status = 1;
        $order->save();
        if ( $type == PayClass::ORDERTEXT ) {
            $type = 1;
        } elseif ( $type == PayClass::ORDERAUDICE ) {
            $type = 2;
        } else {
            $type = 3;
        }
        $record = UserDoctorRecord::getInstance()->getRecord( $order->user_id, $order->doctor_id, $type );
        if ( !$record )
            $record = new UserDoctorRecord();
        //图文设置过期时间 av挂断时设置当前时间 为 过期时间
        if ( $type == UserDoctorRecord::TEXT ) {
            //图文默认
            $over_at = Carbon::now()->addMinutes( $doctor->text_param )->timestamp;
            $record->time = $over_at;
        } elseif ( $type == UserDoctorRecord::AUDIO ) {
            $record->time += $doctor->audio_param;
        } else {
            $record->time += $doctor->video_param;
        }
        $record->type = $type;
        $record->has_pay = 1;
        $record->user_id = $order->user_id;
        $record->doctor_id = $order->doctor_id;
        $record->save();
        //        Doctor::getInstance()->updateBalance( $order->pay_amount, $doctor->id, $type + 1, $order );
        //网易云信 更新好友关系
        //YunXin::getInstance()->Friend->add($user->accid, $doctor->accid);
    }


    /**
     * 线下预约
     * @param $order_no
     * @param $data
     * @throws \JsonException
     */
    public function confirmAppoint( $order_no, $data )
    {
        $model = Appointment::getInstance()->where( [ 'order_number' => $order_no ] )->firstOrFail();
        $schdeule = $model->belongsToSchedule()->firstOrFail();
        if ( $model->pay_status != 1 ) {
            if ( env( 'APP_ENV' ) == 'production' && $model->pay_amount != $data->get( 'total_amount' ) )
                throw new \JsonException( '支付金额不匹配' . $model->pay_amount . '!=' . $data->get( 'total_amount' ) );
            if ( $schdeule->used_num >= $schdeule->total_num )
                throw new \JsonException( '剩余号源不足' );
            $model->pay_status = 1;
            $model->save();
            $schdeule->increment( 'used_num' );
        }
    }

    /**
     * 商品订单
     * @param $order_no
     * @param $data
     * @throws \JsonException
     */
    public function confirmGoodsOrder( $order_no, $data )
    {
        $model = GoodsOrder::getInstance()->where( [ 'order_number' => $order_no ] )->with( [
            'hasManyDetail.belongsToGoods',
            'hasManyDetail.belongsToGoodsSpec'
        ] )->firstOrFail();
        ServiceManager::getInstance()->GoodsOrderService( GoodsOrderService::class )->hasStock( $model );

        if ( $model->pay_status != 1 ) {
            if ( env( 'APP_ENV' ) == 'production' && $model->pay_amount != $data->get( 'total_amount' ) )
                throw new \JsonException( '支付金额不匹配' . $model->pay_amount . '!=' . $data->get( 'total_amount' ) );
            $model->pay_status = 1;
            $model->trans_status = 1;
            $model->pay_time = date( 'Y-m-d H:i:s' );
            $model->save();
            //减库存
            foreach ( $model->hasManyDetail as $k => $v ) {
                if ( $v->belongsToGoods )
                    $v->belongsToGoods->decrement( 'stock', $v->num );
                if ( $v->belongsToGoodsSpec )
                    $v->belongsToGoodsSpec->decrement( 'stock', $v->num );
            }
            //删除购物车
            if ( $model->order_from == 1 ) {
                $cart_id = $model->hasManyDetail->pluck( 'cart_id' );
                UserCart::getInstance()->whereIn( 'id', $cart_id )->update( [ 'status' => 2 ] );
            }

        }
    }

    public function test()
    {
        foreach ( $arr as $k => $v ) {
            $p_id = DB::table( 'privileges' )->insertGetId( [
                'icon'  => $v['icon'],
                'title' => $v['title'],
                'level' => 0,

            ] );
            if ( $v['subs'] ) {
                foreach ( $v['subs'] as $val ) {
                    $pri_id = DB::table( 'privileges' )->insertGetId( [
                        'pid'    => $p_id,
                        'title'  => $val['title'],
                        'p_name' => $val['index'],
                        'level'  => 1,
                    ] );
                    if ( $val['api'] ) {
                        foreach ( $val['api'] as $key1 => $val1 ) {

                            DB::table( 'admin_apis' )->insert( [
                                'method' => $val1['method'],
                                'url'    => $val1['url'],
                                'title'  => $key1,
                                'pri_id' => $pri_id,
                                'type'   => 1,
                            ] );
                        }
                    }
                    if ( $val['subs'] ) {
                        foreach ( $val['subs'] as $val1 ) {
                            $pris_id = DB::table( 'privileges' )->insertGetId( [
                                'pid'    => $pri_id,
                                'title'  => $val1['title'],
                                'p_name' => $val1['index'],
                                'level'  => 2,
                            ] );
                            if ( $val1['api'] ) {
                                foreach ( $val1['api'] as $key1 => $val2 ) {
                                    DB::table( 'admin_apis' )->insert( [
                                        'method' => $val2['method'],
                                        'url'    => $val2['url'],
                                        'title'  => $key1,
                                        'pri_id' => $pris_id,
                                        'type'   => 1,
                                    ] );
                                }
                            }

                        }
                    }

                }
            }
        }
        if ( env( 'APP_ENV' ) == 'local' )
            $this->confirmGoodsOrder( '23456787654', [] );
    }

}
