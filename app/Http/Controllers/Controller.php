<?php

namespace App\Http\Controllers;

use App\Traits\ApiResponse;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests, ApiResponse;

    public $request, $pageSize, $page, $user;

    public function __construct( Request $request )
    {
        error_reporting(E_ALL & ~E_NOTICE);
        $this->user = Auth::user();
        $this->request = $request;
        $this->pageSize = $request->get('page_size') ? $request->get('page_size') : 10;
        $this->page = $request->get('page') ? $request->get('page') : 1;
        if( getenv('APP_ENV') == 'local' )
            DB::connection()->enableQueryLog();
    }

    public function getQueryLog()
    {
        print_r(DB::getQueryLog());
        die;
    }
}
