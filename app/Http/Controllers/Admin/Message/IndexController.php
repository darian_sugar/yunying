<?php

namespace App\Http\Controllers\Admin\Message;

use App\Helpers\Push;
use App\Helpers\YunXin\YunXin;
use App\Http\Controllers\Controller;
use App\Models\PushMessage;
use App\Models\SystemMsg;
use App\Models\User;

class IndexController extends Controller
{
    /**
     * 消息列表
     * @return mixed
     * @throws \Illuminate\Validation\ValidationException
     */
    public function index()
    {
        $this->validate($this->request, [
            'type' => 'required|between:1,2'
        ]);
        $type  = $this->request->get('type');
        $model = new SystemMsg();
        if (!empty($type)) $model = $model->where(['type' => $type]);
        $data = $model->paginate($this->pageSize);
        return $this->success($data);
    }


    /**
     * 发送消息
     * @return mixed
     * @throws \Illuminate\Validation\ValidationException
     */
    public function sendMessage()
    {
        $this->validate($this->request, [
            'type'         => 'required|int',
            'title'        => 'required',
            'msg'          => 'required',
            'receive_type' => 'required|int|between:1,2,3,4',
        ]);
        $input        = $this->request->all();
        $receive_type = $input['receive_type'];
        $userIds      = $input['user_id'][0] ?? 0;
        switch ($receive_type) {
            case 1:
//                $userIds = User::getInstance()->whereBetween();
//                $userIds = array_column($userIds, 'id');
                  $userIds = 0;
                  $type = 0;
                break;
            case 2:
                $type = 1;
                break;
            case 4:
                $type = 2;
                break;
            case 3:
                $type = 3;
//                $userIds = Doctor::getInstance()->value('id');
                break;
        }
//        Push::getInstance()->sendMsg($input['title'], $input['msg'], $userIds);
        $messageInsert = [
            'msg_type' => $input['type'],
            'title'    => $input['title'],
            'content'  => $input['msg'],
            'source'    => 1,
            'type'     => $type,
            'to_id'    => $userIds,
        ];
        $res           = SystemMsg::getInstance()->create($messageInsert);
        if (!$res) return $this->failed('发送消息失败');
        return $this->success('发送成功');
    }


    /**
     * 审核
     * @return mixed
     * @throws \Illuminate\Validation\ValidationException
     */
    public function audit()
    {
        $this->validate($this->request, [
            'id'     => 'required',
            'status' => 'required',
        ]);
        $input = $this->request->all();
        $res   = PushMessage::getInstance()->where(['id' => $input['id']])
            ->update(['status' => $input['status'], 'why' => $input['why'] ?? '']);
        if (!$res) return $this->failed('审核失败');
        return $this->success();
    }


    public function emailSend()
    {
        $this->validate($this->request, [
            'email' => 'required'
        ]);
        $email = $this->request->post('email');
        try {
            $data = [
                'email' => $email,
                'name'  => '',
            ];
            \Mail::send('emails.old-mail',
                [
                    'patientName' => '姓名',
                    'view'        => 'https://www.baidu.com',
                ],
                function ($message) use ($data) {
                    foreach ($data['email'] as $email) {
                        $message->to($email, $data['name'])->subject('邮件测试报告');
                    }
                });
            return $this->success();
        } catch (\Exception $e) {
            return $this->failed($e->getMessage());
        }
    }
}
