<?php

namespace App\Http\Controllers\Admin\Suggest;

use App\Http\Controllers\Controller;
use App\Models\Suggest;
use App\Models\User;

class IndexController extends Controller
{
    public function list()
    {
        $input = $this->request->all();
        $where['flag'] = 1;
        $data = Suggest::getInstance()->select( 'id', 'user_id', 'type', 'created_at' )
            ->when( $input['type'], function ( $where, $value ) {
                $where->where( 'type', $value );
            } )
            ->when( $input['type'], function ( $where, $value ) {
                $where->whereHas( 'belongsToUser', function ( $query ) use ( $value ) {
                    $query->where( [ 'identity' => $value ] );
                } );
            } )
            ->with( [
                'belongsToUser:id,nickname,identity'
            ] )
            ->where( $where )->latest()->paginate( $this->pageSize )->toArray();

        $data['type_arr'] = Suggest::getInstance()->typeArrDesc;
        $data['identity_arr'] = User::getInstance()->identityArrDesc;
        return $this->success( $data );
    }

    /**
     * 详情
     * @return mixed
     * @throws \Illuminate\Validation\ValidationException
     */
    public function detail()
    {
        $this->validate( $this->request, [
            'id' => 'required',
        ] );
        $input = $this->request->all();

        $data = Suggest::getInstance()->find( $input['id'] );
        return $this->success( $data );
    }
}
