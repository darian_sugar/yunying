<?php

namespace App\Http\Controllers\Admin\Common;

use App\Helpers\Upload;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UploadController extends Controller
{
    /**
     * 小于10M文件上传
     * @return mixed
     * @throws \Illuminate\Validation\ValidationException
     * @throws \JsonException
     */
    public function upload()
    {
        $this->validate( $this->request, [
            'type' => 'required|between:1,7',
            'file' => 'required',
        ] );
        $type = $this->request->get( 'type' );
        $file = $this->request->file( 'file' );

        $input['url'] = Upload::getInstance()->uploadFile( $file, $type );
        $input['full_url'] = Upload::getInstance()->getPicName( $input['url'], $type );
        $input['size'] = bcdiv( $file->getClientSize(), 1024, 1 );
        return $this->success( $input );
    }

    /**
     * 大文件上传
     * @return mixed
     * @throws \Illuminate\Validation\ValidationException
     * @throws \JsonException
     */
    public function uploadBigFile()
    {
        $this->validate( $this->request, [
            'file'         => 'required|file',
            'blobNum'      => 'required|int',
            'totalBlobNum' => 'required|int',
            'md5FileName'  => 'required',
            'type'         => 'required',
            'size'         => 'required',
        ], [], [
            'file'         => '上传文件',
            'blobNum'      => '当前切片数',
            'totalBlobNum' => '总切片数',
            'md5FileName'  => '文件名称',
            'type'         => '文件类型',
            'size'         => '每片文件大小',
        ] );

        $file = $this->request->file( 'file' );
        $blobNum = $this->request->get( 'blobNum' );
        $totalBlobNum = $this->request->get( 'totalBlobNum' );
        $md5FileName = $this->request->get( 'md5FileName' );
        $type = $this->request->get( 'type' );
        $size = $this->request->get( 'size' );
        $result = Upload::getInstance()->uploadBigFile( $file, $blobNum, $totalBlobNum, $md5FileName, $type );

        if ( $result ) {
            $data['url'] = $result;
            $data['full_url'] = Upload::getInstance()->getPicName( $result, $type );
            $data['size'] = bcdiv( ( $file->getClientSize() + bcmul( $blobNum - 1, $size ) ), 1024, 1 ) . ' kb';
        }
        return $this->success( $data );
    }
}
