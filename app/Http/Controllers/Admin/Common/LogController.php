<?php

namespace App\Http\Controllers\Admin\Common;

use App\Http\Controllers\Admin\BaseController;
use App\Models\OperationLog;

class LogController extends BaseController
{
    //
    public function logList()
    {
        $data = OperationLog::getInstance()->orderByDesc('id')->paginate($this->pageSize);
        return $this->success($data);
    }

}
