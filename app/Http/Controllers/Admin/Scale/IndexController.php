<?php

namespace App\Http\Controllers\Admin\Scale;

use App\Http\Controllers\Controller;
use App\Models\Scale;
use App\Models\ScaleAnswer;
use App\Models\ScaleQuestion;
use App\Models\ScaleQuestionType;
use App\Models\ScaleType;
use App\Models\Suggest;
use Illuminate\Support\Facades\DB;
use PhpOffice\PhpSpreadsheet\IOFactory;

class IndexController extends Controller
{
    /**
     * 列表
     * @return mixed
     */
    public function list()
    {
        $scaleName = $this->request->get('scale_name');
        $model     = Scale::getInstance()->withCount('hasManyScaleRecord');
        if (!empty($model)) $model = $model->where('scale_name', 'like', "%$scaleName%");
        $data = $model->orderByDesc('id')->paginate($this->pageSize);
        return $this->success($data);
    }

    /**
     * 详情
     * @return mixed
     * @throws \Illuminate\Validation\ValidationException
     */
    public function detail()
    {
        $this->validate($this->request, [
            'id' => 'required',
        ]);
        $id                = $this->request->get('id');
        $where['scale_id'] = $id;
        try {
            $data = Scale::getInstance()
                ->with(
                    [
                        'hasManyQuestion:id,scale_id,question,prompt,risk_level,sort',
                        'hasManyQuestion.hasManyAnswer:id,q_id,options,content,grade'
                    ]
                )->where('id', $id)
                ->get();
            return $this->success($data);
        } catch (\Exception $e) {
            return $this->failed($e->getMessage());
        }
    }


    /**
     * 删除
     * @return mixed
     */
    public function del()
    {
        $id = $this->request->post('id');
        $res = Scale::getInstance()->where(['id' => $id])->delete();
        if (!$res) return $this->failed('删除失败');
        return $this->success();
    }


    /**
     * 是否启用
     * @return mixed
     * @throws \Illuminate\Validation\ValidationException
     */
    public function enable()
    {
        $this->validate($this->request, [
            'is_enable' => 'required|between:1,2'
        ]);
        $id = $this->request->post('id');
        $is_enable = $this->request->post('is_enable');
        $res = Scale::getInstance()->where(['id' => $id])->update(['is_enable' => $is_enable]);
        if (!$res) return $this->failed('操作失败');
        return $this->success();
    }


    /**
     * 是否推荐
     * @return mixed
     * @throws \Illuminate\Validation\ValidationException
     */
    public function recommended()
    {
        $this->validate($this->request, [
            'is_recommended' => 'required|between:1,2'
        ]);
        $id = $this->request->post('id');
        $is_recommended = $this->request->post('is_recommended');
        $res = Scale::getInstance()->where(['id' => $id])->update(['is_recommended' => $is_recommended]);
        if (!$res) return $this->failed('操作失败');
        return $this->success();
    }




    /**
     * 导入表格
     * @return mixed
     * @throws \Illuminate\Validation\ValidationException
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Reader\Exception
     */
    public function importScale()
    {
        $this->validate($this->request, [
            'scale_name' => 'required',
            'type'       => 'required',
            'introduce'  => 'required',
            'file'       => 'required',
        ]);
        $input = $this->request->all();
        $file  = $this->request->file('file');
        # 创建读操作
        $reader = IOFactory::createReader('Xlsx');
        # 打开文件、载入excel表格
        $spreadsheet = $reader->load($file);
        # 获取活动工作薄
        $sheet = $spreadsheet->getActiveSheet();
        # 获取总列数
        $highestColumn = $sheet->getHighestColumn();
        # 获取总行数
        $highestRow = $sheet->getHighestRow();
        # 列数 改为数字显示
        $highestColumnIndex = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::columnIndexFromString($highestColumn);
        $data               = [];
        $options            = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H'];
        $sort               = 1;
        $scoreInsert        = [
            'scale_name' => $input['scale_name'],
            'type'       => $input['type'],
            'introduce'  => $input['introduce'],
        ];
        try {
            DB::beginTransaction();
            $scaleRes    = Scale::getInstance()->create($scoreInsert);
            $questionRes = false;
            $answerRes   = false;
            $scaleId     = $scaleRes->id;
            for ($i = 0; $i < $highestRow; $i++) {
                $a      = $i + 3;
                $answer = [];
                $q_type = $sheet->getCellByColumnAndRow(1, $a)->getValue();
                if (empty($q_type)) continue;
                $question             = $sheet->getCellByColumnAndRow(2, $a)->getValue();
                $answer1              = $sheet->getCellByColumnAndRow(3, $a)->getValue();
                $answer2              = $sheet->getCellByColumnAndRow(4, $a)->getValue();
                $answer3              = $sheet->getCellByColumnAndRow(5, $a)->getValue();
                $answer4              = $sheet->getCellByColumnAndRow(6, $a)->getValue();
                $answer5              = $sheet->getCellByColumnAndRow(7, $a)->getValue();
                $answer6              = $sheet->getCellByColumnAndRow(8, $a)->getValue();
                $answer7              = $sheet->getCellByColumnAndRow(9, $a)->getValue();
                $answer8              = $sheet->getCellByColumnAndRow(10, $a)->getValue();
                $data[$i]['q_type']   = $q_type;
                $data[$i]['question'] = $question;
                $data[$i]['answer']   = array_filter([$answer1, $answer2, $answer3, $answer4, $answer5, $answer6, $answer7, $answer8]);
                $answer               = array_filter([$answer1, $answer2, $answer3, $answer4, $answer5, $answer6, $answer7, $answer8]);

                $questionInsert = [
                    'question' => $question,
                    'sort'     => $sort++,
                    'scale_id' => $scaleId
                ];
                $questionRes    = ScaleQuestion::getInstance()->create($questionInsert);
                $questionId     = $questionRes->id;
                if (empty($answer)) continue;
                $answerInsert = [];
                foreach ($answer as $k1 => $v1) {
                    $answerInsert[$k1]['q_id']    = $questionId;
                    $answerInsert[$k1]['content'] = $v1;
                    $answerInsert[$k1]['grade']   = 0;
                    $answerInsert[$k1]['options'] = $options[$k1];
                }
                $answerRes = ScaleAnswer::getInstance()->insert($answerInsert);
            }
            if ($scaleRes && $questionRes && $answerRes) {
                DB::commit();
                return $this->success();
            }
            DB::rollBack();
            return $this->failed('导入失败');
        } catch (\Exception $e) {
            DB::rollBack();
            return $this->failed($e->getMessage());
        }
    }


    /**
     * 问题类型
     */
    public function questionType()
    {
        $data = ScaleQuestionType::getInstance()->get();
        return $this->success($data);
    }


    /**
     * 量表类型
     * @return mixed
     */
    public function scaleType()
    {
        $data = ScaleType::getInstance()->get(['id', 'type_name']);
        return $this->success($data);
    }


    /**
     * 量表选项添加（只有选择题）
     * @throws \Illuminate\Validation\ValidationException
     */
    public function addSelect()
    {
        $this->validate($this->request, [
            'scale_name'       => 'required|unique:scales',
            'type'             => 'required',
            'introduce'        => 'required',
            'question_structs' => 'required',
        ]);
        $input            = $this->request->all();
        $question_structs = json_decode($input['question_structs'], true);
        try {
            DB::beginTransaction();
            $scaleInsert = [
                'scale_name' => $input['scale_name'],
                'type'       => $input['type'],
                'introduce'  => $input['introduce'],
            ];
            $scaleRes    = Scale::getInstance()->create($scaleInsert);
            $scaleId = $scaleRes->id;
            $sort = 1;
            $options = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L'];
            foreach ($question_structs as $k => $v) {
                $questionInsert = [
                    'q_mark_id' => $k,
                    'question' => $v['q_title'],
                    'q_type' => $v['question_type'],
                    'scale_id' => $scaleId,
                    'jump_id' => $v['jump_id'],
                    'sort' => $sort++,
                ];
                $questionRes = ScaleQuestion::getInstance()->create($questionInsert);
                $questionId = $questionRes->id;
                $answer = $v['option_list'];
                $answerInsert = [];
                foreach ($answer as $k1 => $v1) {
                    $answerInsert[] = [
                        'content' => $v1['a_title'],
                        'prompt' => $v1['prompt'],
                        'attr_option' => json_encode($v1['attr_option'], JSON_UNESCAPED_UNICODE),
                        'options' => $options[$k1],
                    ];
                }
                $answerRes = ScaleAnswer::getInstance()->insert($answerInsert);
            }
            if (!$answerRes) {
                DB::rollBack();
                return $this->failed('添加失败');
            }
            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            return $this->failed($e->getMessage());
        }
        return $this->success();
    }


    /**
     * 问题添加
     * @return mixed
     * @throws \Illuminate\Validation\ValidationException
     */
    public function add()
    {
        $this->validate($this->request, [
            'scale_name'       => 'required|unique:scales',
            'type'             => 'required',
            'introduce'        => 'required',
            'question_structs' => 'required',
        ]);
        $input            = $this->request->all();

        $question_structs = json_decode($input['question_structs'], true);
        try {
            DB::beginTransaction();
            $scaleInsert = [
                'scale_name' => $input['scale_name'],
                'type'       => $input['type'],
                'introduce'  => $input['introduce'],
            ];
            $scaleRes    = Scale::getInstance()->create($scaleInsert);
            $scaleId = $scaleRes->id;
            $sort = 1;
            $options = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L'];
            foreach ($question_structs as $k => $v) {
                $type = substr($v['q_type'], 0, 1);
                $questionInsert = [
                    'q_mark_id' => $k,
                    'question' => $v['question'],
                    'q_type' => $v['q_type'],
                    'scale_id' => $scaleId,
                    'jump_id' => $v['jump_id'] ?? -1,
                    'sort' => $sort++,
                ];
                $questionRes = ScaleQuestion::getInstance()->create($questionInsert);
                $questionId = $questionRes->id;
                $answer = $v['option_list'];
                $answerInsert = [];
                foreach ($answer as $k1 => $v1) {
                    switch ($type) {
                        case 1:
                            $answerInsert[] = [
                                'q_id' => $questionId,
                                'content' => $v1['content'],
                                'prompt' => $v1['prompt'],
                                'direction' => $v1['direction'] ?? 0,
                                'is_fill' => $v1['is_fill'] ?? 0,
                                'attr_option' => json_encode($v1['attr_option'], JSON_UNESCAPED_UNICODE),
                            ];
                            break;
                        case 2:
                            $answerInsert[] = [
                                'q_id' => $questionId,
                                'content' => $v1['content'],
                                'prompt' => $v1['prompt'],
                                'direction' => $v1['direction'] ?? 0,
                                'is_fill' => $v1['is_fill'] ?? 0,
                                'attr_option' => json_encode($v1['attr_option'], JSON_UNESCAPED_UNICODE),
                                'options' => $options[$k1],
                            ];
                            break;
                        default:
                            DB::rollBack();
                            return $this->failed('问题类型错误' . $v['question_type']);
                            break;
                    }

                }
                $answerRes = ScaleAnswer::getInstance()->insert($answerInsert);
            }
            if (!$answerRes) {
                DB::rollBack();
                return $this->failed('添加失败');
            }
            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            return $this->failed($e->getMessage());
        }
        return $this->success();
    }

    /**
     * 问题添加
     * @return mixed
     * @throws \Illuminate\Validation\ValidationException
     */
    public function addQuestion()
    {
        $this->validate($this->request, [
            'scale_id'       => 'required',
            'question_structs' => 'required',
        ]);
        $input            = $this->request->all();

        $question_structs = json_decode($input['question_structs'], true);
        try {
            DB::beginTransaction();
            $scaleId = $input['scale_id'];
            $lastQuestion = ScaleQuestion::getInstance()->where(['scale_id'], $input['scale_id'])->orderByDesc('sort')->clunm('sort');
            var_dump($lastQuestion);exit;
            $sort = 1;
            $options = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L'];
            foreach ($question_structs as $k => $v) {
                $type = substr($v['q_type'], 0, 1);
                $questionInsert = [
                    'q_mark_id' => $k,
                    'question' => $v['question'],
                    'q_type' => $v['q_type'],
                    'scale_id' => $scaleId,
                    'jump_id' => $v['jump_id'] ?? -1,
                    'sort' => $sort++,
                ];
                $questionRes = ScaleQuestion::getInstance()->create($questionInsert);
                $questionId = $questionRes->id;
                $answer = $v['option_list'];
                $answerInsert = [];
                foreach ($answer as $k1 => $v1) {
                    switch ($type) {
                        case 1:
                            $answerInsert[] = [
                                'q_id' => $questionId,
                                'content' => $v1['content'],
                                'prompt' => $v1['prompt'],
                                'direction' => $v1['direction'] ?? 0,
                                'is_fill' => $v1['is_fill'] ?? 0,
                                'attr_option' => json_encode($v1['attr_option'], JSON_UNESCAPED_UNICODE),
                            ];
                            break;
                        case 2:
                            $answerInsert[] = [
                                'q_id' => $questionId,
                                'content' => $v1['content'],
                                'prompt' => $v1['prompt'],
                                'direction' => $v1['direction'] ?? 0,
                                'is_fill' => $v1['is_fill'] ?? 0,
                                'attr_option' => json_encode($v1['attr_option'], JSON_UNESCAPED_UNICODE),
                                'options' => $options[$k1],
                            ];
                            break;
                        default:
                            DB::rollBack();
                            return $this->failed('问题类型错误' . $v['question_type']);
                            break;
                    }

                }
                $answerRes = ScaleAnswer::getInstance()->insert($answerInsert);
            }
            if (!$answerRes) {
                DB::rollBack();
                return $this->failed('添加失败');
            }
            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            return $this->failed($e->getMessage());
        }
        return $this->success();
    }
}
