<?php

namespace App\Http\Controllers\Admin\Permission;

use App\Models\Admin;
use App\Models\Privilege;
use App\Models\Role;
use App\Models\RolePriRelevance;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Admin\BaseController;

class PrivilegeController extends BaseController
{
    public function index()
    {

        $this->validate($this->request, [
            'id'   => 'required',
            'type' => 'required|int|max:2',
        ]);
        $input = $this->request->all();

        if ($input['type'] == 2) {
            $admin = Admin::getInstance()->findOrFail($input['id']);
            $roleIds = $admin->hasManyRole()->distinct()->pluck('r_id');

        }
        $model = RolePriRelevance::getInstance()->where(['type' => $input['type'], 'r_id' => $input['id']]);

        if ($roleIds) {
            $model->orWhere(function ($query) use ($roleIds) {
                $query->whereIn('r_id', $roleIds)->where('type', 1);
            });
        }
        $pri = $model->distinct()->pluck('p_id', 'is_api')->toArray();
        $apis = isset($pri[1]) ? $pri[1] : [];
        $pages = isset($pri[0]) ? $pri[0] : [];
        $data = Privilege::getInstance()
            ->select('id', 'pid', 'icon', 'title', 'p_name', 'level')
            ->where(['status' => 1, 'type' => 1])
            ->with([
                'hasManyApis' => function ($query) use ($apis) {
                    $query->select( 'id', 'title', 'pri_id' );

                    if ( $apis )
                        $query->selectRaw( '(case when id in (' . implode( ',', $apis ) . ') then 1 else 2 end) as is_checked ' )->where( [ 'status' => 1, 'type' => 1 ] );
                    else
                        $query->selectRaw( '2 as is_checked ' )->where( [ 'status' => 1, 'type' => 1 ] );


                }
            ] )
            ->orderByRaw( 'order_sort asc,id asc' )->get()->toArray();
        $tree = [];
        foreach ($data as &$v) {
            if (in_array($v['id'], $pages))
                $v['is_checked'] = 1;
            else
                $v['is_checked'] = 2;
            if ($v['level'] == 2)
                $v['is_checked'] = 1;
        }
        listToTree($data, 'id', 'pid', 'has_many_children', 0, $tree);
        return $this->success($tree);
    }

    public function add()
    {
        $this->validate($this->request, [
            'id'   => 'required',
            'type' => 'required|int|max:2',
            'p_id' => 'required|array',
            'apis' => 'required|array',
        ]);
        $input = $this->request->all();
        if (!in_array(1, $input['p_id']) || !in_array(2, $input['p_id']))
            return $this->failed('系统首页必选');
        $insert = [
            ['p_id' => $input['p_id'], 'type' => $input['type'], 'is_api' => 0],
            ['p_id' => $input['apis'], 'type' => $input['type'], 'is_api' => 1],
        ];
        if ($input['type'] == 1) {
            $model = Role::getInstance()->findOrFail($input['id']);
            $model->hasManyPri()->delete();
            $model->hasManyPri()->createMany($insert);
        } else {
            $model = Admin::getInstance()->findOrFail($input['id']);
            $model->hasManyPrie()->delete();
            $model->hasManyPrie()->createMany($insert);
        }

        return $this->message();
    }


}
