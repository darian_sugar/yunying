<?php

namespace App\Http\Controllers\Admin\Permission;

use App\Models\Admin;
use App\Services\Common\CheckService;
use Exception;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rule;
use Illuminate\Validation\ValidationException;
use App\Http\Controllers\Admin\BaseController;

class MemberController extends BaseController
{
    /**
     * 成员列表
     *
     * @return mixed
     * @throws ValidationException
     */
    public function index()
    {
        $input = $this->request->all();
        $model = Admin::getInstance()->select( 'id', 'account', 'name', 'mobile', 'created_at', 'status', 'last_login_time', 'is_cs' )
            ->with( [
                'hasManyRole:id,a_id,r_id',
                'hasManyRole.belongsToRole:id,r_name',
            ] )
            ->where( 'is_admin', '!=', 1 );
        if ( $input['name'] )
            $model->where( 'name', 'like', '%' . $input['name'] . '%' );

        if ( $input['role_type_id'] ) {
            $model->whereHas( 'hasManyRole', function ( $query ) use ( $input ) {
                $query->where( [ 'r_id' => $input['role_type_id'] ] );
            } );
        }

        $data = $model->orderByDesc( 'id' )->paginate( $this->pageSize );
        return $this->success( $data );
    }


    public function add()
    {
        $this->validate( $this->request, [
            'r_id'     => 'array',
            'account'  => [
                'filled',
                Rule::unique( 'admins' )->whereNull( 'deleted_at' )->ignore( $this->request->get( 'id' ), 'id' ),
                'min:1',
                'max:50',
            ],
            'password' => 'min:6|max:16',
            'name'     => 'required',
            'head_pic' => 'filled',
            'mobile'   => 'required',
            'memo'     => 'filled',
            'id'       => 'filled|int',
        ] );
        $input = $this->request->all();
        CheckService::checkPassword( $input['password'] );
        if ( $input['id'] ) {
            $model = Admin::getInstance()->findOrFail( $input['id'] );
            $model->hasManyRole()->delete();
        } else {
            $model = new Admin();
        }
        $input['head_pic'] = $input['head_pic'] ?? 'default_head.jpg';
        DB::beginTransaction();
        try {
            $model->fill( $input )->save();

            //if( !$input['id'] ){
            //    //注册网易云信
            //    $result = YunXin::getInstance()->user->create(YunXin::ADMIN . '_' . $model->id . '_' . uniqid(), $model->name, $model->head_pic);
            //    $model->yx_token = $result['token'];
            //    $model->accid = $result['accid'];
            //}else{
            //    //网易云信修改信息
            //    YunXin::getInstance()->User->updateUserInfo($model->accid, $model->name, $model->head_pic);
            //}
            $model->password = Hash::make( $input['password'] ) ?? '';
            $model->save();
            if ( $input['r_id'] ) {
                array_walk( $input['r_id'], function ( &$value ) {
                    $value = [ 'r_id' => $value ];
                } );
                $model->hasManyRole()->createMany( $input['r_id'] );
            }

            DB::commit();
            return $this->message();
        } catch ( Exception $exception ) {
            DB::rollBack();
            return $this->failed( $exception->getMessage() );
        }
    }


    /**
     * 启用 停用 删除
     *
     * @return mixed
     * @throws ValidationException
     */
    public function changeStatus()
    {
        $this->validate( $this->request, [
            'type' => 'required|int|between:1,3',
            'id'   => 'required',
        ] );
        try {

            $input = $this->request->all();
            if ( is_array( $input['id'] ) ) {
                $model = Admin::getInstance()->whereIn( 'id', $input['id'] );
            } else {
                $model = Admin::getInstance()->where( 'id', $input['id'] );
            }
            if ( $input['type'] == 3 ) {
                $model->delete();
            } elseif ( $input['type'] == 2 ) {
                $model->update( [ 'status' => 2 ] );
            } else {
                $model->update( [ 'status' => 1 ] );
            }
            return $this->message( '修改成功' );
        } catch ( Exception $e ) {

            return $this->failed( $e->getMessage() );
        }
    }

    /**
     * 客服 1是 2 否
     *
     * @return mixed
     * @throws ValidationException
     */
    public function changeCsStatus()
    {
        $this->validate( $this->request, [
            'type' => 'required|int|between:1,2',
            'id'   => 'required',
        ] );
        try {

            $input = $this->request->all();
            if ( is_array( $input['id'] ) ) {
                $model = Admin::getInstance()->whereIn( 'id', $input['id'] );
            } else {
                $model = Admin::getInstance()->where( 'id', $input['id'] );
            }
            if ( $input['type'] == 3 ) {
                $model->delete();
            } elseif ( $input['type'] == 2 ) {
                $model->update( [ 'is_cs' => 2 ] );
            } else {
                $model->update( [ 'is_cs' => 1 ] );
            }
            return $this->message( '修改成功' );
        } catch ( Exception $e ) {

            return $this->failed( $e->getMessage() );
        }
    }
}
