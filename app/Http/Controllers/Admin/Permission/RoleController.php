<?php

namespace App\Http\Controllers\Admin\Permission;

use App\Models\Role;
use App\Http\Controllers\Controller;
use Exception;
use Illuminate\Validation\Rule;
use Illuminate\Validation\ValidationException;
use App\Http\Controllers\Admin\BaseController;

class RoleController extends BaseController
{
    /**
     * 角色列表
     *
     * @return mixed
     */
    public function index()
    {
        $this->validate($this->request, [
            'status' => 'filled|int|between:1,2',
        ]);
        $input = $this->request->all();
        if( $input['status'] )
            $where['status'] = $input['status'];
        $where['r_type'] = Role::$admin;
        $data = Role::getInstance()
            ->where($where)
            ->select('id', 'r_name', 'memo')
            ->withCount('hasManyAdmin')->paginate($this->pageSize);
        return $this->success($data);
    }

    public function add()
    {
        $this->validate($this->request, [
            'r_name' => [
                'required',
                Rule::unique('roles')->whereNull('deleted_at')->ignore($this->request->get('id'), 'id'),
            ],
            'memo'   => 'filled',
            'id'     => 'filled',
        ]);
        $input = $this->request->all();
        if( $input['id'] )
            $model = Role::getInstance()->findOrFail($input['id']);
        else
            $model = new Role();
        $input['r_type'] = Role::$admin;
        $model->fill($input)->save();
        return $this->message();
    }

    /**
     * 启用 停用 删除
     *
     * @return mixed
     * @throws ValidationException
     */
    public function changeStatus()
    {
        $this->validate($this->request, [
            'type' => 'required|int|between:1,3',
            'id'   => 'required',
        ]);
        try{

            $input = $this->request->all();
            if( is_array($input['id']) ){
                $model = Role::getInstance()->whereIn('id', $input['id']);
            }else{
                $model = Role::getInstance()->where('id', $input['id']);
            }
            if( $input['type'] == 3 ){
                $model->delete();
            }elseif( $input['type'] == 2 ){
                $model->update([ 'status' => 2 ]);
            }else{
                $model->update([ 'status' => 1 ]);
            }
            return $this->message('修改成功');
        }catch( Exception $e ){

            return $this->failed($e->getMessage());
        }
    }
}
