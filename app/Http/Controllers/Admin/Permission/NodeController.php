<?php

namespace App\Http\Controllers\Admin\Permission;

use App\Models\Admin;
use App\Models\AdminApi;
use App\Models\Privilege;
use App\Models\Role;
use App\Models\RolePriRelevance;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Admin\BaseController;

class NodeController extends BaseController
{
    public function index()
    {


    }

    public function add()
    {
        $this->validate($this->request, [
            'method' => 'required',
            'url'    => 'required',
            'title'  => 'required',
            'type'   => 'required',
            'pri_id' => 'required_if:type,1',
        ], [], [
            'method' => '请求方式',
            'url'    => '请求接口地址',
            'title'  => '请求名称',
            'type'   => '请求类型',
            'pri_id' => '所属页面ID',
        ]);

        $model = new AdminApi();


    }


}
