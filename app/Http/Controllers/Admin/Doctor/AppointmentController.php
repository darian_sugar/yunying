<?php


namespace App\Http\Controllers\Admin\Doctor;


use App\Helpers\PayClass;
use App\Http\Controllers\Admin\BaseController;
use App\Models\Appointment;
use App\Models\Scale;
use App\Models\SystemMsg;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class AppointmentController extends BaseController
{
    public function index()
    {
        $input = $this->request->all();
        $result = Appointment::getInstance()
            ->select( 'id', 'user_id', 'doctor_id', 'cate_desc', 'start_time', 'check_status' )
            ->with( [
                'belongsToDoctor:id,name',
                'belongsToUser:id,nickname,identity',
            ] )
            ->where( [ 'order_status' => 1, 'check_status' => 0, 'pay_status' => 1 ] )
            ->when( $input['key'], function ( $where, $value ) {
                $where->whereHas( 'belongsToUser', function ( $where ) use ( $value ) {
                    $where->where( 'nick', 'like', '%' . $value . '%' );
                    $where->orWhere( 'mobile', 'like', '%' . $value . '%' );
                    $where->orWhere( 'id', 'like', '%' . $value . '%' );
                } );
            } )
            ->when( $input['cate_id'], function ( $where, $value ) {
                $where->where( 'cate_id', $value );
            } )
            ->paginate( $this->pageSize )->toArray();
        $cate = Scale::getInstance()->where( [ 'is_enable' => 1 ] )->select( 'scale_name', 'id' )->get();
        $result['cate_arr'] = $cate;
        return $this->success( $result );
    }


    /**
     * 审核
     * @return mixed
     * @throws \Illuminate\Validation\ValidationException
     */
    public function confirmAppoint()
    {
        $this->validate( $this->request, [
            'id'    => 'required',
            'type'  => 'required|int',
            'reply' => 'required_if:type,2',

        ], [], [
            'reply' => '回复内容',
            'type'  => '审核类型',
        ] );
        $input = $this->request->all();
        $user = Auth::user();
        $order = Appointment::getInstance()
            ->where( [ 'order_status' => 1, 'check_status' => 0, 'pay_status' => 1 ] )
            ->with( [
                'belongsToDoctor:id,name',
            ] )
            ->findOrFail( $input['id'] );

        if ( $order->check_status != 0 )
            return $this->failed( '预约已审核' );

        DB::beginTransaction();
        try {
            $order->check_uid = $user->id;
            $order->check_status = $input['type'];
            $order->reply = $input['reply'];
            if ( $input['type'] == 2 )
                $order->order_status = 3;
            $order->save();
            if ( $input['type'] == 2 ) {
                $type_desc = '拒绝，款项会原路退回，请注意查收。';
                PayClass::getInstance()->refund( $order->order_number, $order->pay_amount, $order->pay_amount, $order->method_pay );
            } else {
                $type_desc = '通过';
            }

            $msg = '您线下预约' . $order->belongsToDoctor->name . '医生的' . $order->cate_desc . '已审核' . $type_desc;
            SystemMsg::getInstance()->sendSysMsg( $msg, $order->user_id, 1, '线下预约审核', 2 );
            DB::commit();
            return $this->message();
        } catch ( \Exception $exception ) {
            DB::rollBack();
            return $this->failed( $exception->getMessage() );
        }
    }


}
