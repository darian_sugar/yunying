<?php

namespace App\Http\Controllers\Admin\Doctor;

use App\Helpers\YunXin\YunXin;
use App\Http\Controllers\Admin\BaseController;
use App\Models\Doctor;
use App\Models\InquiryOrder;
use App\Models\SystemConfig;
use App\Models\SystemMsg;
use App\Rules\Numeric2Rule;
use App\Services\Common\CheckService;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rule;

class IndexController extends BaseController
{
    //

    public function index()
    {
        $input = $this->request->all();
        $data = Doctor::getInstance()->select( 'id', 'name', 't_id', 'type', 'status', 'is_top' )
            ->when( $input['t_id'], function ( $where, $value ) {
                $where->where( 't_id', $value );
            } )
            ->when( $input['type'], function ( $where, $value ) {
                $where->where( 'type', $value );
            } )
            ->when( $input['keyword'], function ( $where, $value ) {
                $where->where( function ( $query ) use ( $value ) {
                    $query->orWhere( 'name', 'like', '%' . $value . '%' );
                } );
            } )->latest( 'id' )
            ->paginate( $this->pageSize )->toArray();
        $data['title_arr'] = Doctor::getInstance()->title;
        return $this->success( $data );
    }


    /**
     * 专家添加
     * @return mixed
     * @throws \Illuminate\Validation\ValidationException
     */
    public function add()
    {
        $this->validate( $this->request, [
            'name'          => 'required',
            't_id'          => 'required',
            'password'      => 'required:min:6|max:16',
            'intro'         => 'max:255',
            'account'       => [
                'required',
                'max:200',
                'unique:doctors'
            ],
            'text_price'    => [
                'required_if:type,1,3',
                new Numeric2Rule( 2, '图文问诊价格' ),
            ],
            'video_price'   => [
                'required_if:type,1,3',
                new Numeric2Rule( 2, '视频问诊价格' ),
            ],
            'audio_price'   => [
                'required_if:type,1,3',
                new Numeric2Rule( 2, '音频问诊价格' ),
            ],
            'appoint_price' => [
                'required_if:type,2,3',
                new Numeric2Rule( 2, '线下问诊价格' ),
            ],
        ], [], [
            'text_price'    => '图文问诊价格',
            'video_price'   => '视频问诊价格',
            'audio_price'   => '音频问诊价格',
            'appoint_price' => '线下问诊价格',
            'account'       => '账户名称',
            'name'          => '专家姓名',
            't_id'          => '专家职称',
        ] );
        $input = $this->request->all();
        CheckService::checkPassword( $input['password'] );

        DB::beginTransaction();
        $info = new Doctor();
        try {
            $input['head_pic'] = $input['head_pic'] ?? 'default_head.jpg';
            $info->fill( $input )->save();
            $info->password = Hash::make( $input['password'] );
            $info->text_param = SystemConfig::getValue( 'doctor_text', 15 );
            $info->audio_param = SystemConfig::getValue( 'doctor_audio', 15 );
            $info->video_param = SystemConfig::getValue( 'doctor_video', 15 );
            $info->save();
            $data = Doctor::getInstance()->where( 'id', $info->id )->first();
            //注册网易云信
            $sign = $data->job_title;
            $result = YunXin::getInstance()->User->create( YunXin::DOCTOR . $info->id . '_' . uniqid(), $info->name, $info->head_pic, $sign );
            $info->yx_token = $result['token'];
            $info->accid = $result['accid'];
            $info->save();
            DB::commit();
            return $this->message( '添加成功' );
        } catch ( \Exception $e ) {
            DB::rollBack();
            return $this->failed( $e->getMessage() );
        }

    }


    /**
     * 专家详情
     * @return mixed
     * @throws \Illuminate\Validation\ValidationException
     */
    public function detail()
    {
        $this->validate( $this->request, [
            'id' => 'required|exists:doctors'
        ] );
        $input = $this->request->all();
        $info = Doctor::getInstance()->findOrFail( $input['id'] );
        $info->title_arr = $info->title;
        return $this->success( $info );
    }

    /**
     * 专家详情
     * @return mixed都
     * @throws \Illuminate\Validation\ValidationException
     */
    public function edit()
    {

        $this->validate( $this->request, [
            'id'            => 'required|exists:doctors',
            'type'          => 'required',
            'text_price'    => [
                'required_if:type,1,3',
                new Numeric2Rule( 2, '图文问诊价格' ),
            ],
            'video_price'   => [
                'required_if:type,1,3',
                new Numeric2Rule( 2, '视频问诊价格' ),
            ],
            'audio_price'   => [
                'required_if:type,1,3',
                new Numeric2Rule( 2, '音频问诊价格' ),
            ],
            'appoint_price' => [
                'required_if:type,2,3',
                new Numeric2Rule( 2, '线下问诊价格' ),
            ],
            'account'       => [
                'filled',
                Rule::unique( 'doctors' )->whereNull( 'deleted_at' )->ignore( $this->request->get( 'id' ), 'id' )
            ],
            'name'          => 'filled',
        ], [], [
            'text_price'    => '图文问诊价格',
            'video_price'   => '视频问诊价格',
            'audio_price'   => '音频问诊价格',
            'appoint_price' => '线下问诊价格',
            'account'       => '账户名称',
            'name'          => '专家姓名',
        ] );
        $this->request->only( [ 'text_price', 'video_price', 'audio_price', 'appoint_price', 'type', 'account', 'name', 'head_pic' ] );
        $input = $this->request->all();
        DB::beginTransaction();
        try {
            $info = Doctor::getInstance()->where( [ 'id' => $input['id'] ] )->whereNull( 'deleted_at' )->first();
            if ( !$info )
                return $this->failed( '专家信息不存在' );
            $res = $info->fill( $input )->save();
            $data = Doctor::getInstance()->where( 'id', $info->id )->first();
            //注册网易云信
            $sign = $data->job_title . ' ' . $data->belongsToDepartment->name . ' ' . $data->belongsToHospital->name;
            YunXin::getInstance()->User->updateUserInfo( $info->accid, $info->name, $info->head_pic, $sign );
            DB::commit();
            return $this->message();
        } catch ( \Exception $e ) {
            DB::rollBack();
            return $this->failed( $e->getMessage(), $e->getFile() );
        }
    }

    /**
     * 资质审核
     * @return mixed
     * @throws \Illuminate\Validation\ValidationException
     */
    public function checkPicStatus()
    {
        $this->validate( $this->request, [
            'doctor_id' => 'required',
            'status'    => 'required|between:1,2',
        ] );
        try {
            $input = $this->request->all();
            $doctor = Doctor::getInstance()->where( 'id', $input['doctor_id'] )->first();
            if ( !$doctor )
                return $this->failed( '专家信息不存在' );
            $details = $doctor->hasOneDetail()->first();

            if ( !$details || $details->license_pic_status != 0 )
                return $this->failed( '资质审核状态错误' );
            $details->license_pic_status = $input['status'];
            $details->save();
            $msg = '您的资质证明已被审核' . ( $input['status'] == 1 ? '通过，届时会显示在大医123的专家详情界面' : '拒绝，需要您重新上传' );
            $title = '您有一条资质审核消息通知';
            $type = 2;
            $data = [ 'code' => '10001' ];
            SystemMsg::getInstance()->sendSysMsg( $msg, $input['doctor_id'], $type, $title );
            $push = new Push( $type );
            $push->sendMsg( $title, $msg, [ $input['doctor_id'] ], $data );
            return $this->message( '操作成功' );

        } catch ( \Exception $e ) {

            return $this->failed( $e->getMessage() );
        }
    }

    /**
     * 问诊列表
     * @return mixed
     * @throws \Illuminate\Validation\ValidationException
     */
    public function inquiryList()
    {
        $this->validate( $this->request, [
            'id' => 'required|exists:doctors'
        ] );
        $input = $this->request->all();
        $doctor = Doctor::getInstance()->where( [ 'id' => $input['id'] ] )->first();
        if ( !$doctor )
            return $this->failed( '专家信息不存在' );
        $where['pay_status'] = 1;
        $where['order_status'] = 1;
        $data = $doctor->UserHasMany( InquiryOrder::class )->select( 'id', 'doctor_id', 'user_id', 'type', 'pay_amount', 'created_at' )->with( [
            'belongsToUser:id,nickname'
        ] )->where( $where )->paginate( $this->pageSize );
        return $this->success( $data );
    }


    /**
     * 启用 停用 删除
     * @return mixed
     * @throws \Illuminate\Validation\ValidationException
     */
    public function changeStatus()
    {
        $this->validate( $this->request, [
            'type' => 'required|int|between:1,7',
            'id'   => 'required',
        ] );
        try {

            $input = $this->request->all();
            $user = Auth::user();
            if ( is_array( $input['id'] ) ) {
                $model = Doctor::getInstance()->whereIn( 'id', $input['id'] );
            } else {
                $model = Doctor::getInstance()->where( 'id', $input['id'] );
            }
            if ( $input['type'] == 3 ) {
                $model->delete();
            } elseif ( $input['type'] == 2 ) {
                $update = [ 'status' => 2 ];
            } else {
                $update = [ 'status' => 1 ];
            }
            $update['change_uid'] = $user->id;
            $update['change_time'] = date( 'Y-m-d H:i:s' );
            $model->update( $update );
            return $this->message( '修改成功' );
        } catch ( \Exception $e ) {

            return $this->failed( $e->getMessage() );
        }
    }

    /**
     * 专家推荐
     * @return mixed
     * @throws \Illuminate\Validation\ValidationException
     */
    public function recommendDoctor()
    {
        $this->validate( $this->request, [
            'type' => 'required|int|between:1,2',
            'id'   => 'required',
        ] );
        try {

            $input = $this->request->all();
            $doctor = Doctor::getInstance()->find( $input['id'] );
            if ( !$doctor )
                return $this->failed( '专家信息不存在' );
            $doctor->is_top = $input['type'];
            $doctor->save();
            return $this->message( '修改成功' );
        } catch ( \Exception $e ) {

            return $this->failed( $e->getMessage() );
        }
    }


    /**
     * 获取专家排班
     *
     * @return mixed
     * @throws \Illuminate\Validation\ValidationException
     */
    public function getSchedule()
    {
        $this->validate( $this->request, [
            'id' => 'required',
        ] );
        $input = $this->request->all();
        $doctor = Doctor::getInstance()->findOrFail( $input['id'] );
        $data = $doctor->hasManySchedule()->get();
        return $this->success( $data );
    }


    /**
     * 设置排班
     * @return mixed
     * @throws \Illuminate\Validation\ValidationException
     */
    public function setSchedule()
    {
        $this->validate( $this->request, [
            'doctor_id'         => 'required',
            'data'              => 'array',
            'data.*.date'       => 'required|distinct',
            'data.*.start_time' => 'required|distinct',
            'data.*.end_time'   => 'required|distinct',
            'data.*.total_num'  => 'required',
        ], [], [
            'data'              => '排班数据',
            'data.*.date'       => '日期',
            'data.*.start_time' => '开始时间',
            'data.*.end_time'   => '结束时间',
            'data.*.total_num'  => '号源数',
        ] );
        $input = $this->request->all();
        $user = Doctor::getInstance()->findOrFail( $input['doctor_id'] );
        $user->hasManySchedule()->createMany( $input['data'] );
        return $this->message();

    }

}
