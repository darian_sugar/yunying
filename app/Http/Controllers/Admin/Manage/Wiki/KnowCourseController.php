<?php

namespace App\Http\Controllers\Admin\Manage\Wiki;

use App\Models\Doctor;
use App\Models\KnowCate;
use App\Models\KnowCourse;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Admin\BaseController;
use Illuminate\Validation\ValidationException;

class KnowCourseController extends BaseController
{
    /**
     *
     * 视频列表
     * @return array
     */
    public function index()
    {
        $data = $this->request->all();
        $model = KnowCourse::getInstance()->select( 'id', 'cate_id', 'title', 'author_id', 'created_at', 'status', 'check_status', 'duration' );
        if ( isset( $data['title'] ) )
            $model->where( 'title', 'like', '%' . $data['title'] . '%' );
        if ( isset( $data['cate_id'] ) )
            $model->where( 'cate_id', $data['cate_id'] );
        if ( isset( $data['created_at'] ) )
            $model->whereDate( 'created_at', $data['created_at'] );
        if ( isset( $data['check_status'] ) )
            $model->where( 'check_status', $data['check_status'] );
        if ( isset( $data['author'] ) ) {
            $doctorId = Doctor::getInstance()->where( 'name', 'like', '%' . $data['author'] . '%' )->pluck( 'id' );
            $model->whereIn( 'author_id', $doctorId );
        }
        $list = $model->with( [
            'belongsToAuthor:id,name,t_id',
            'belongsToCate:id,name,channel_id',
            'belongsToCate.belongsToChannel:id,p_id,name',
        ] )->where( 'type', 1 )
            ->orderByRaw( 'order_sort desc,id desc' )->paginate( $this->pageSize )->toArray();
        $data = KnowCate::getInstance()
            ->where( [ 'type' => 1, 'channel_id' => 0 ] )->with( [ 'hasManyChildren:id,channel_id,name' ] )->where( 'p_id', '>', 0 )->select( 'id', 'name' )->latest()->get();
        $list['channel_arr'] = $data;
        return $this->success( $list );
    }

    /**
     * 添加视频
     * @return array
     */
    public function add()
    {
        $this->validate( $this->request, [
            'cate_id'   => 'required|exists:know_cates,id',
            'title'     => 'required|between:1,30',
            'content'   => 'required',
            'image'     => 'required',
            'author_id' => 'required|exists:doctors,id',
            'status'    => 'required|int|between:1,2',
            'duration'  => 'required|int',
        ], [], [
            'cate_id'   => '分类ID',
            'title'     => '标题',
            'content'   => 'url路径',
            'image'     => '封面',
            'price'     => '价格',
            'author_id' => '作者ID',
            'status'    => '启用状态',
            'duration'  => '时长',
        ] );
        $data = $this->request->all();
        if ( $data['type'] == 1 && !is_array( $data['image'] ) )
            return $this->failed( '接受的图片字段必须是数组类型' );
        if ( $data['id'] ) {
            $model = KnowCourse::getInstance()->where( 'id', $data['id'] )->where( 'type', 1 )->first();
            if ( !$model )
                return $this->failed( '未找到该条数据' );
            if ( $model->from == 2 )
                return $this->failed( '该文章不允许编辑' );
        } else {
            $model = new KnowCourse();
        }
        $user = Auth::user();

        $priceList = KnowCourse::$price;
        foreach ( $priceList as $k => $v ) {
            if ( $data['price'] == $v['price'] ) {
                $data['product_id'] = $v['product_id'];
            }
        }
        $data['admin_id'] = $user->id;
        $data['type'] = 1;
        $model->fill( $data )->save();
        return $this->message( '操作成功' );
    }

    /**
     * 修改状态
     * @return array
     * @throws ValidationException
     */
    public function changeStatus()
    {
        $this->validate( $this->request, [
            'type' => 'required|int|between:1,5',
            'id'   => 'required'
        ] );
        $status = $this->request->get( 'type' );
        $id = $this->request->get( 'id' );
        if ( !$id )
            return $this->failed( '参数错误' );
        $model = KnowCourse::getInstance()->where( 'type', 1 );
        if ( is_array( $id ) )
            $model = $model->whereIn( 'id', $id );
        else
            $model = $model->where( 'id', $id );
        if ( $status == 3 ) {
            $res = $model->delete();
        } else {
            $res = $model->update( [ 'status' => $status ] );
        }
        if ( $res === false )
            return $this->failed( '更新失败' );
        else
            return $this->message( '更新成功' );
    }

    public function detail()
    {
        $this->validate( $this->request, [
            'id' => 'required',
        ] );
        $input = $this->request->all();
        $data = KnowCourse::getInstance()->where( 'id', $input['id'] )->where( 'type', 1 )
            ->with( [
                'belongsToAuthor:id,name,t_id',
                'belongsToCate:id,name,channel_id',
                'belongsToCate.belongsToChannel:id,p_id,name',
            ] )->first();
        return $this->success( $data );
    }

    /**
     * 审核
     * @return mixed
     * @throws ValidationException
     */
    public function changeCheckStatus()
    {
        $this->validate( $this->request, [
            'type' => 'required|int|between:1,2',
            'id'   => 'required',
        ] );

        $input = $this->request->all();
        $user = Auth::user();
        $model = KnowCourse::getInstance()->where( 'id', $input['id'] )->where( 'type', 1 )->firstOrFail();
        $model->check_status = $input['type'];
        $model->check_uid = $user->id;
        $model->check_time = date( 'Y-m-d H:i:s' );
        $model->save();
        return $this->message();
    }


    /**
     * 视频价格列表
     * @return mixed
     */
    public function price()
    {
        $data = KnowCourse::$price;
        return $this->success( $data );
    }
}
