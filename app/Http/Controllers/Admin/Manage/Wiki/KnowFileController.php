<?php

namespace App\Http\Controllers\Admin\Manage\Wiki;

use App\Models\Doctor;
use App\Models\KnowCate;
use App\Models\KnowFile;
use App\Models\KnowLive;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use App\Http\Controllers\Admin\BaseController;

class KnowFileController extends BaseController
{
    /**
     *
     * 文章列表
     * @return array
     */
    public function index()
    {
        $data = $this->request->all();

        $model = KnowFile::getInstance()->select( 'id', 'cate_id', 'title', 'author_id', 'created_at', 'status', 'check_status' );
        if ( isset( $data['title'] ) )
            $model->where( 'title', 'like', '%' . $data['title'] . '%' );
        if ( isset( $data['cate_id'] ) )
            $model->where( 'cate_id', $data['cate_id'] );
        if ( isset( $data['created_at'] ) )
            $model->whereDate( 'created_at', $data['created_at'] );
        if ( isset( $data['check_status'] ) )
            $model->where( 'check_status', $data['check_status'] );
        if ( isset( $data['author'] ) ) {
            $doctorId = Doctor::getInstance()->where( 'name', 'like', '%' . $data['author'] . '%' )->pluck( 'id' );
            $model->whereIn( 'author_id', $doctorId );
        }
        $list = $model->with( [
            'belongsToAuthor:id,name,t_id',
            'belongsToCate:id,name,channel_id',
            'belongsToCate.belongsToChannel:id,p_id,name',

        ] )->where( 'type', 1 )
            ->orderByRaw( 'order_sort desc,id desc' )->paginate( $this->pageSize )->toArray();
        $data = KnowCate::getInstance()
            ->where( [ 'type' => 1, 'channel_id' => 0 ] )->with( [ 'hasManyChildren:id,channel_id,name' ] )->where( 'p_id', '>', 0 )->select( 'id', 'name' )->latest()->get();
        $list['channel_arr'] = $data;
        return $this->success( $list );
    }

    /**
     * 添加文章
     * @return array
     */
    public function add()
    {
        $this->validate( $this->request, [
            'title'      => 'required|between:1,30',
            'content'    => 'required',
            'image'      => 'required',
            //'author_id'  => 'required|exists:doctors,id',
            'order_sort' => 'int',
            'status'     => 'required|int|between:1,2',
            'cate_id'    => 'required',
        ] );
        $data = $this->request->all();
        if ( $data['id'] ) {
            $model = KnowFile::getInstance()->where( 'id', $data['id'] )->where( 'type', 1 )->first();
            if ( !$model )
                return $this->failed( '未找到该条数据' );
            if ( $model->from == 2 )
                return $this->failed( '该文章不允许编辑' );
        } else {
            $model = new KnowFile();
        }
        $user = Auth::user();
        $data['admin_id'] = $user->id;
        $data['type'] = 1;
        $model->fill( $data )->save();
        return $this->message( '操作成功' );
    }

    /**
     * 修改状态
     * @return array
     * @throws \Illuminate\Validation\ValidationException
     */
    public function changeStatus()
    {
        $this->validate( $this->request, [
            'type' => 'required|int|between:1,7',
            'id'   => 'required'
        ] );
        $status = $this->request->get( 'type' );
        $id = $this->request->get( 'id' );

        if ( !$id )
            return $this->failed( '参数错误' );

        $model = KnowFile::getInstance()->where( 'type', 1 );
        if ( is_array( $id ) )
            $model = $model->whereIn( 'id', $id );
        else
            $model = $model->where( 'id', $id );
        if ( $status == 3 ) {
            $res = $model->delete();
        } elseif ( $status == 6 ) {
            $model->update( [ 'is_top' => 2 ] );
        } elseif ( $status == 7 ) {
            $model->update( [ 'is_top' => 1 ] );
        } else {
            $res = $model->update( [ 'status' => $status ] );
        }

        if ( $res === false )
            return $this->failed( '更新失败' );
        else
            return $this->message( '更新成功' );
    }

    public function detail()
    {
        $this->validate( $this->request, [
            'id' => 'required',
        ] );
        $input = $this->request->all();
        $data = KnowFile::getInstance()->where( 'id', $input['id'] )->where( 'type', 1 )
            ->with( [
                'belongsToAuthor:id,name,t_id',
                'belongsToCate:id,name,channel_id',
                'belongsToCate.belongsToChannel:id,p_id,name',
            ] )->first();
        return $this->success( $data );
    }

    /**
     * 审核
     * @return mixed
     * @throws \Illuminate\Validation\ValidationException
     */
    public function changeCheckStatus()
    {
        $this->validate( $this->request, [
            'type' => 'required|int|between:1,2',
            'id'   => 'required',
        ] );

        $input = $this->request->all();
        $user = Auth::user();
        $model = KnowFile::getInstance()->where( 'id', $input['id'] )->where( 'type', 1 )->firstOrFail();
        $model->check_status = $input['type'];
        $model->check_uid = $user->id;
        $model->check_time = date( 'Y-m-d H:i:s' );
        $model->save();
        return $this->message();
    }
}
