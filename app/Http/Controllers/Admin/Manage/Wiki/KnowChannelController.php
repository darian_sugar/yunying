<?php

namespace App\Http\Controllers\Admin\Manage\Wiki;

use App\Models\KnowCate;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;
use App\Http\Controllers\Admin\BaseController;

class KnowChannelController extends BaseController
{
    /**
     *
     * 列表
     * @return array
     */
    public function index()
    {
        $data = KnowCate::getInstance()
            ->where( [ 'type' => 1, 'channel_id' => 0 ] )
            ->where( 'p_id', '>', 0 )
            ->latest()->paginate( $this->pageSize )->toArray();
        $data['base_cate_arr'] = KnowCate::$base_cate_arr;
        return $this->success( $data );
    }

    /**
     * 添加
     * @return array
     */
    public function add()
    {
        $this->validate( $this->request, [
            'name'   => [
                'required',
                'between:1,64',
                Rule::unique( 'know_cates' )->where( 'type', 1 )->whereNull( 'deleted_at' )->ignore( $this->request->get( 'id' ), 'id' )
            ],
            'p_id'   => 'required',
            'status' => 'required',
        ], [], [
            'name'   => '分类名称',
            'p_id'   => '所属类别',
            'status' => '是否显示',
        ] );
        $this->request->only( [ 'name', 'p_id', 'status' ] );
        $data = $this->request->all();
        if ( $data['id'] ) {
            $model = KnowCate::getInstance()->where( 'id', $data['id'] )->firstOrFail();
        } else {
            $model = new KnowCate();
        }
        $user = Auth::user();
        $data['admin_id'] = $user->id;
        $data['type'] = 1;
        $model->fill( $data )->save();
        return $this->message();
    }

    /**
     * 修改状态
     * @return array
     * @throws \Illuminate\Validation\ValidationException
     */
    public function changeStatus()
    {
        $this->validate( $this->request, [
            'type' => 'required|int|between:1,3',
            'id'   => 'required'
        ] );
        $input = $this->request->all();
        $model = KnowCate::getInstance()
            ->where( [ 'type' => 1, 'channel_id' => 0 ] )
            ->where( 'p_id', '>', 0 );
        if ( is_array( $input['id'] ) )
            $model->whereIn( 'id', $input['id'] );
        else
            $model->where( 'id', $input['id'] );

        if ( $input['type'] == 3 )
            $res = $model->delete();
        else
            $res = $model->update( [ 'status' => $input['type'] ] );

        if ( $res === false )
            return $this->failed( '更新失败' );
        else
            return $this->message( '更新成功' );
    }

}
