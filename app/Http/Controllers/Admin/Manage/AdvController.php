<?php

namespace App\Http\Controllers\Admin\Manage;

use App\Models\Admin;
use App\Models\Adv;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class AdvController extends Controller
{
    /**
     *
     * 广告列表
     * @return array
     */
    public function advList()
    {
        $list = Adv::getInstance()->latest( 'id' )->paginate( $this->pageSize );
        return $this->success( $list );
    }

    /**
     * 添加广告
     * @return array
     */
    public function advAdd()
    {
        $this->validate( $this->request, [
            'adv_name'   => 'required',
            'start_time' => 'required',
            'end_time'   => 'required',
            'image'      => 'required',
        ] );
        $data = $this->request->all();
        if ( $data['id'] ) {
            $model = Adv::getInstance()->where( 'id', $data['id'] )->firstOrFail();
        } else {
            $model = new Adv();
        }
        $user = Auth::user();
        $data['admin_id'] = $user->id;
        $model->fill( $data )->save();
        return $this->message( '添加成功' );
    }

    /**
     * 修改状态
     * @return array
     * @throws \Illuminate\Validation\ValidationException
     */
    public function changeStatus()
    {
        $this->validate( $this->request, [
            'type' => 'required',
            'id'   => 'required'
        ] );
        $status = $this->request->get( 'type' );
        $id = $this->request->get( 'id' );

        if ( !$id )
            return $this->failed( '参数错误' );
        if ( !in_array( $status, [ 1, 2, 3 ] ) )
            return $this->failed( '状态参数错误' );

        $model = Adv::getInstance();
        if ( is_array( $id ) )
            $model = $model->whereIn( 'id', $id );
        else
            $model = $model->where( 'id', $id );
        if ( $status == 3 )
            $res = $model->delete();
        else {
            $res = $model->update( [ 'status' => $status ] );
        }
        if (!$res) return $this->failed('操作失败');
        return $this->success();
    }
}
