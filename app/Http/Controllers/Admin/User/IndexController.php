<?php

namespace App\Http\Controllers\Admin\User;

use App\Http\Controllers\Admin\BaseController;
use App\Models\Admin;
use App\Models\Appointment;
use App\Models\Doctor;
use App\Models\InquiryOrder;
use App\Models\ScaleQuestion;
use App\Models\ScaleRecord;
use App\Models\User;
use App\Models\UserDoctorRecord;
use Illuminate\Support\Facades\Auth;

class IndexController extends BaseController
{

    /**
     * 用户列表
     * @return mixed
     */
    public function index()
    {
        $input = $this->request->input();
        $data = User::getInstance()
            ->select( 'id', 'nickname', 'identity' )
            ->with( [
                'latestInquiry:id,user_id,created_at',
                'latestAppoint:id,user_id,created_at',
            ] )
            ->when( $input['key_word'], function ( $where, $key_word ) {
                $where->where( function ( $query ) use ( $key_word ) {
                    $query->where( 'nickname', 'like', "%$key_word%" );
                } );
            } )
            ->when( $input['identity'], function ( $where, $value ) {
                $where->where( 'identity', $value );
            } )
            ->latest( 'id' )->paginate( $this->pageSize );
        foreach ( $data->items() as $k => $v ) {
            $v->latest = null;
            if ( $v->latestInquiry && $v->latestInquiry->created_at > $v->latestAppoint->created_at ) {
                $v->latest = $v->latestInquiry;
                $v->latest->type_text = '在线咨询';
            }
            if ( $v->latestAppoint && $v->latestInquiry->created_at < $v->latestAppoint->created_at ) {
                $v->latest = $v->latestAppoint;
                $v->latest->type_text = '线下预约';

            }
            $v->makeHidden( [ 'latestInquiry', 'latestAppoint' ] );
        }
        return $this->success( $data );
    }


    /**
     * 患者姓名ID列表
     * @return mixed
     */
    public function list()
    {
        $data = User::getInstance()
            ->select( 'id', 'nickname')
            ->get();
        return $this->success( $data );
    }

    /**
     * 病人基础信息
     * @return mixed
     */
    public function detail()
    {
        $this->validate( $this->request, [
            'id' => "required"
        ] );
        $input = $this->request->all();
        $data = User::getInstance()->select( 'id', 'nickname', 'mobile', 'sex', 'identity', 'head_pic', 'age' )->findOrFail( $input['id'] );
        return $this->success( $data );
    }


    /**
     * 问诊信息
     * @return mixed
     * @throws \Illuminate\Validation\ValidationException
     */
    public function inquiryList()
    {
        $this->validate( $this->request, [
            'id' => 'required|exists:users',
        ] );
        $input = $this->request->all();
        $user = User::getInstance()->where( 'id', $input['id'] )->firstOrFail();
        $data = $user->UserHasMany( InquiryOrder::class )
            ->select( 'id', 'doctor_id', 'user_id', 'type', 'created_at' )
            ->with( [
                'belongsToDoctor:id,name',
            ] )->latest( 'id' )->paginate( $this->pageSize );
        return $this->success( $data );
    }


    /**
     * 预约列表
     * @return mixed
     * @throws \Illuminate\Validation\ValidationException
     */
    public function appointList()
    {
        $this->validate( $this->request, [
            'id' => 'required|exists:users'
        ] );
        $input = $this->request->all();
        $doctor = User::getInstance()->where( [ 'id' => $input['id'] ] )->first();
        if ( !$doctor )
            return $this->failed( '医生信息不存在' );
        $where['pay_status'] = 1;
        $data = $doctor->hasManyAppoint()
            ->select( 'id', 'doctor_id', 'date', 'check_status', 'order_status', 'cate_desc' )
            ->with( [
                'belongsToDoctor:id,name'
            ] )
            ->where( $where )
            ->whereIn( 'order_status', [ 1, 3 ] )->paginate( $this->pageSize );
        return $this->success( $data );
    }

    /**
     * 启用 停用 删除
     * @return mixed
     * @throws \Illuminate\Validation\ValidationException
     */
    public function changeStatus()
    {
        $this->validate( $this->request, [
            'type' => 'required|int|between:1,3',
            'id'   => 'required',
        ] );

        $input = $this->request->all();
        if ( is_array( $input['id'] ) ) {
            $model = User::getInstance()->whereIn( 'id', $input['id'] );
        } else {
            $model = User::getInstance()->where( 'id', $input['id'] );
        }
        if ( $input['type'] == 3 ) {
            $model->delete();
        } elseif ( $input['type'] == 2 ) {
            $model->update( [ 'status' => 2 ] );
        } else {
            $model->update( [ 'status' => 1 ] );
        }
        return $this->message( '修改成功' );
    }

    /**
     * 获取聊天记录
     * @return mixed
     * @throws \Illuminate\Validation\ValidationException
     */
    public function getChatMessage()
    {
        $this->validate( $this->request, [
            'id' => 'required',
        ] );
        $input = $this->request->all();
        $record = UserDoctorRecord::getInstance()->where( 'id', $input['id'] )->first();
        if ( !$record )
            return $this->failed( '记录不存在' );
        if ( $record->type == 1 ) {
            $model = $record->hasManyMessage()->where( 'source', 1 );
        } elseif ( $record->type == 2 ) {
            $model = $record->hasManyAvMsg()->where( 'type', 1 );
        } else {
            $model = $record->hasManyAvMsg()->where( 'type', 2 );
        }
        $data = $model->paginate( $this->pageSize );
        return $this->success( $data );
    }


    /**
     * 用户量表列表
     * @return mixed
     * @throws \Illuminate\Validation\ValidationException
     */
    public function scaleList()
    {
        $this->validate( $this->request, [
            'id' => 'required|exists:users',
        ] );
        $input = $this->request->all();
        $user = User::getInstance()->where( 'id', $input['id'] )->firstOrFail();
        $data = $user->UserHasMany( ScaleRecord::class )
            ->select( 'id', 'user_id', 'scale_id', 'scale_name', 'result', 'type', 'created_at' )
            ->orderByDesc( 'id' )
            ->paginate( $this->pageSize );
        return $this->success( $data );
    }


    /**
     * 量表详情
     * @return mixed
     * @throws \Illuminate\Validation\ValidationException
     */
    public function scaleDetail()
    {
        $this->validate( $this->request, [
            'id' => 'required|exists:scale_records',
        ] );
        $input = $this->request->all();
        $data = ScaleRecord::getInstance()
            ->with( [
                'hasOneBasic:id,baby_name,baby_month,baby_sex,blood_type',
                'hasOneUser:id,nickname,identity,mobile,head_pic',
                'hasManyRecordDetail:id,scale_record_id,q_id,a_id',
            ] )->where( [ 'id' => $input['id'] ] )
            ->first();
        $questionRecord = $data->hasManyRecordDetail;
        if ( empty( $questionRecord ) )
            return $this->success( $questionRecord );
        $questionRecord = $questionRecord->toArray();
        $questionRecordIds = array_column( $questionRecord, 'q_id' );
        $questionRecordKeyIds = array_column( $questionRecord, null, 'q_id' );
        $questionAnswer = ScaleQuestion::getInstance()->with( [
            'hasManyAnswer:id,q_id,options,content'
        ] )->select( [ 'id', 'question' ] )
            ->whereIn( 'id', $questionRecordIds )
            ->get()->toArray();
        foreach ( $questionAnswer as $k => $v ) {
            $qId = $v['id'];
            $aId = $questionRecordKeyIds[ $qId ]['a_id'];
            $has_many_answer = $v['has_many_answer'];
            foreach ( $has_many_answer as $k1 => $v1 ) {
                $questionAnswer[ $k ]['has_many_answer'][ $k1 ]['is_select'] = 0;
                if ( $v1['id'] == $aId ) {
                    $questionAnswer[ $k ]['has_many_answer'][ $k1 ]['is_select'] = 1;
                }
            }
        }
        $data = $data->toArray();
        unset( $data['has_many_record_detail'] );
        $data['has_many_detail'] = $questionAnswer;
        return $this->success( $data );
    }

}
