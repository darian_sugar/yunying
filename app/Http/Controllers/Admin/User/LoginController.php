<?php

namespace App\Http\Controllers\Admin\User;

use App\Http\Controllers\Controller;
use App\Helpers\Sms;
use App\Services\Common\CheckService;
use App\Services\Common\LoginService;
use App\Models\Admin;
use App\Models\Doctor;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\Admin\BaseController;

class LoginController extends BaseController
{
    /**
     * 密码登录
     * @return array
     * @throws JsonException
     * @throws \Illuminate\Validation\ValidationException
     */
    public function login()
    {
        $this->validate($this->request, [
            "account"  => 'required|exists:admins,account',
            'password' => 'required|min:6|max:16',
        ]);
        $input = $this->request->all();
        $user = Admin::getInstance()->getByAccount($input['account']);
        if( !$user )
            return $this->failed('用户不存在');
        $id = $user->id;
        $table = 'admins';
        CheckService::checkPasswordErrorLockTime($id, $table);
        if( !Hash::check($input['password'], $user->password) ){
            CheckService::checkPasswordErrorCount($id, $table);
            return $this->failed('密码错误');
        }
        $result = $user->saveToken();
        if( $result === false )
            return $this->failed('fail');
        else
            LoginService::clearPwdErrorLockNumAndTime($id, $table);
        $user->makeVisible([ 'api_token' ]);
        return $this->success($user);

    }


    public function loginOut()
    {
        try{
            $user = Auth::user();
            $user->api_token = '';
            $user->save();
            return $this->success('退出成功');
        }catch( \Exception $e ){

            return $this->failed($e->getMessage());
        }
    }

}
