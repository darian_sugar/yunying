<?php

namespace App\Http\Controllers\Admin\Report;

use App\Models\Appointment;
use App\Models\Doctor;
use App\Models\InquiryOrder;
use App\Models\RegisteredHospitals;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\Rules\In;
use PhpParser\Comment\Doc;

class CountController extends Controller
{


    /**
     * 用户增长统计
     * @return mixed
     * @throws \Illuminate\Validation\ValidationException
     */
    public function increaseUser()
    {

        $this->validate( $this->request, [
            'type' => 'min:1|max:2|required_without:start_time',
        ] );
        $input = $this->request->all();
        $model = User::getInstance()->groupBy( 'date', 'identity' );

        if ( $input['type'] == 1 ) {
            $format = "date(created_at)";
            $str = $format . " as date";
            $startTime = Carbon::today()->startOfMonth()->toDateString();
            $endTime = Carbon::yesterday()->toDateString();
            $dateRange = getDateFromRange( $startTime, $endTime );
            $where[] = [ DB::raw( 'date(created_at)' ), '>=', $startTime ];
            $where[] = [ DB::raw( "date(created_at)" ), '<=', $endTime ];
        } elseif ( $input['type'] == 2 ) {
            $format = "DATE_FORMAT(created_at,'%m')";
            $str = $format . " as date";
            $startTime = 1;
            $endTime = Carbon::today()->month;
            $dateRange = range( $startTime, $endTime, 1 );
            $where[] = [ DB::raw( $format ), '>=', $startTime ];
            $where[] = [ DB::raw( $format ), '<=', $endTime ];

        } else {
            $format = "date(created_at)";
            $str = $format . " as date";
            $dateRange = getDateFromRange( $input['start_time'], $input['end_time'] );
            $where[] = [ DB::raw( $format ), '>=', $input['start_time'] ];
            $where[] = [ DB::raw( $format ), '<=', $input['end_time'] ];
        }
        $dateRange = array_map( function () {
            return 0;
        }, array_flip( $dateRange ) );
        $data = $model->where( $where )
            ->when( !$input['type'], function ( $where ) {
                $dateYaer = date( 'Y' );
                $where->whereYear( 'created_at', '>=', $dateYaer );
            } )
            ->get( [
                DB::raw( $str ),
                DB::raw( 'COUNT(*) as total,identity' )
            ] )->toArray();
        $identityData_1 = $dateRange;
        $identityData_2 = $dateRange;
        $identityData_3 = $dateRange;
        $identityData_4 = $dateRange;
        foreach ( $data as $v ) {
            if ( $input['type'] == 2 )
                $date = (int)$v['date'];
            else
                $date = $v['date'];

            $dateRange[ $date ] += $v['total'];
            $identityData_1[ $date ] += ( $v['identity'] == 1 ? $v['total'] : 0 );
            $identityData_2[ $date ] += ( $v['identity'] == 2 ? $v['total'] : 0 );
            $identityData_3[ $date ] += ( $v['identity'] == 3 ? $v['total'] : 0 );
            $identityData_4[ $date ] += ( $v['identity'] == 4 ? $v['total'] : 0 );
        }
//        $result['x'] = array_values( $dateRange );
        $result['x'] = array_keys( $dateRange );
        $result['y_identityData_1'] = array_values( $identityData_1 );
        $result['y_identityData_2'] = array_values( $identityData_2 );
        $result['y_identityData_3'] = array_values( $identityData_3 );
        $result['y_identityData_4'] = array_values( $identityData_4 );
        $result['total'] = User::getInstance()->count();
        return $this->success( $result );
    }

    public function increaseDateUser()
    {
        $this->validate( $this->request, [
            'type' => 'min:1|max:2|required_without:start_time',
        ] );
        $input = $this->request->all();
        $model = User::getInstance()->groupBy( 'date' );

        if ( $input['type'] == 1 ) {
            $format = "date(created_at)";
            $str = $format . " as date";
            $startTime = Carbon::today()->startOfMonth()->toDateString();
            $endTime = Carbon::yesterday()->toDateString();
            $dateRange = getDateFromRange( $startTime, $endTime );
            $where[] = [ DB::raw( 'date(created_at)' ), '>=', $startTime ];
            $where[] = [ DB::raw( "date(created_at)" ), '<=', $endTime ];
        } elseif ( $input['type'] == 2 ) {
            $format = "DATE_FORMAT(created_at,'%m')";
            $str = $format . " as date";
            $startTime = 1;
            $endTime = Carbon::today()->month;
            $dateRange = range( $startTime, $endTime, 1 );
            $where[] = [ DB::raw( $format ), '>=', $startTime ];
            $where[] = [ DB::raw( $format ), '<=', $endTime ];

        } else {
            $format = "date(created_at)";
            $str = $format . " as date";
            $dateRange = getDateFromRange( $input['start_time'], $input['end_time'] );
            $where[] = [ DB::raw( $format ), '>=', $input['start_time'] ];
            $where[] = [ DB::raw( $format ), '<=', $input['end_time'] ];
        }
        $dateRange = array_map( function () {
            return 0;
        }, array_flip( $dateRange ) );
        $data = $model->where( $where )
            ->when( !$input['type'], function ( $where ) {
                $dateYaer = date( 'Y' );
                $where->whereYear( 'created_at', '>=', $dateYaer );
            } )
            ->get( [
                DB::raw( $str ),
                DB::raw( 'COUNT(*) as total' )
            ] )->toArray();
        foreach ( $data as $v ) {
            if ( $input['type'] == 2 )
                $date = (int)$v['date'];
            else
                $date = $v['date'];

            $dateRange[ $date ] += $v['total'];
        }
        $result['x'] = array_keys( $dateRange );
        $result['y'] = array_values( $dateRange );
        return $this->success( $result );
    }

    /**
     * 交易统计
     * @return mixed
     * @throws \Illuminate\Validation\ValidationException
     */
    public function tradeCount()
    {
        $this->validate( $this->request, [
            'start_time' => 'nullable|date|required_without:type',
            'end_time'   => 'nullable|date|required_with:start_time|after_or_equal:start_time',
            'type'       => 'min:1|max:3|required_without:start_time',
        ] );
        $input = $this->request->all();
        $modelInquery = InquiryOrder::getInstance()->where( [ 'pay_status' => 1, 'order_status' => 1 ] )->groupBy( [ 'date' ] );
        $modelAppoint = Appointment::getInstance()->whereIn( 'order_status', [ 1, 3 ] )->where( [ 'pay_status' => 1 ] )->groupBy( [ 'date' ] );

        if ( $input['type'] == 1 ) {
            $startTime = Carbon::yesterday()->toDateString();
            $endTime = $startTime;
            $dateRange = getDateFromRange( $startTime, $endTime );
        } elseif ( $input['type'] == 2 ) {
            $startTime = Carbon::yesterday()->subDays( 7 );
            $endTime = Carbon::yesterday()->toDateString();
            $dateRange = getDateFromRange( $startTime, $endTime );

        } elseif ( $input['type'] == 3 ) {
            $startTime = Carbon::yesterday()->subDays( 30 );
            $endTime = Carbon::yesterday()->toDateString();
            $dateRange = getDateFromRange( $startTime, $endTime );

        } else {
            $startTime = $input['start_time'];
            $endTime = $input['end_time'];
            $dateRange = getDateFromRange( $startTime, $endTime );
        }

        $dateRange = array_map( function () {
            return 0;
        }, array_flip( $dateRange ) );
        list( $online, $offline ) = [ $dateRange, $dateRange ];

        $data = $modelInquery->whereDate( 'created_at', '>=', $startTime )->whereDate( 'created_at', '<=', $endTime )->get( [
            DB::raw( "date(created_at) as date" ),
            DB::raw( 'sum(pay_amount) as total' )
        ] )->toArray();
        foreach ( $data as $v ) {
            $v['total'] = bcdiv( $v['total'], 100, 2 );
            $online[ $v['date'] ] = $v['total'];
        }
        $data = $modelAppoint->whereDate( 'created_at', '>=', $startTime )->whereDate( 'created_at', '<=', $endTime )->get( [
            DB::raw( "date(created_at) as date" ),
            DB::raw( 'sum(pay_amount) as total' )
        ] )->toArray();
        foreach ( $data as $v ) {
            $v['total'] = bcdiv( $v['total'], 100, 2 );
            $offline[ $v['date'] ] = $v['total'];
        }
        $result['x'] = array_keys( $dateRange );
        $result['y_online'] = array_values( $online );
        $result['y_offine'] = array_values( $offline );
        return $this->success( $result );
    }

    /**
     * 支付方式统计
     * @return mixed
     * @throws \Illuminate\Validation\ValidationException
     */
    public function tradePayCount()
    {
        $this->validate( $this->request, [
            'start_time' => 'nullable|date|required_without:type',
            'end_time'   => 'nullable|date|required_with:start_time|after_or_equal:start_time',
            'type'       => 'min:1|max:3|required_without:start_time',
        ] );
        $input = $this->request->all();
        $modelInquery = InquiryOrder::getInstance()->selectRaw( 'method_pay,sum(pay_amount) as pay_amount' )->where( [ 'pay_status' => 1, 'order_status' => 1 ] );
        $modelAppoint = Appointment::getInstance()->selectRaw( 'method_pay,sum(pay_amount) as pay_amount' )->whereIn( 'order_status', [ 1, 3 ] )->where( [ 'pay_status' => 1 ] );
        if ( $input['type'] == 1 ) {
            $startTime = Carbon::yesterday()->toDateString();
            $endTime = $startTime;
        } elseif ( $input['type'] == 2 ) {
            $startTime = Carbon::yesterday()->subDays( 7 );
            $endTime = Carbon::yesterday()->toDateString();

        } elseif ( $input['type'] == 3 ) {
            $startTime = Carbon::yesterday()->subDays( 30 );
            $endTime = Carbon::yesterday()->toDateString();

        } else {
            $startTime = $input['start_time'];
            $endTime = $input['end_time'];
        }
        $data1 = $modelInquery->whereDate( 'created_at', '>=', $startTime )->whereDate( 'created_at', '<=', $endTime )->groupBy( [ 'method_pay' ] )->select( 'pay_amount', 'method_pay' )->get();
        $data2 = $modelAppoint->whereDate( 'created_at', '>=', $startTime )->whereDate( 'created_at', '<=', $endTime )->groupBy( [ 'method_pay' ] )->select( 'pay_amount', 'method_pay' )->get();

        $result['method_1'] = $data1->where( 'method_pay', 1 )->sum( 'pay_amount' ) + $data2->where( 'method_pay', 1 )->sum( 'pay_amount' );
        $result['method_2'] = $data1->where( 'method_pay', 2 )->sum( 'pay_amount' ) + $data2->where( 'method_pay', 2 )->sum( 'pay_amount' );
        $result['online'] = $data1;
        $result['offline'] = $data2;
        $result['method'] = [ 1 => '支付宝', 2 => '微信' ];
        $result['line'] = [ '线上咨询', '线下预约' ];
        return $this->success( $result );
    }

}
