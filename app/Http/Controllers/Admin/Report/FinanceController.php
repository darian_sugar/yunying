<?php

namespace App\Http\Controllers\Admin\Report;

use App\Helpers\PayClass;
use App\Helpers\Push;
use App\Http\Controllers\Admin\BaseController;
use App\Models\Appointment;
use App\Models\Doctor;
use App\Models\DoctorBalance;
use App\Models\FeeOrder;
use App\Models\GoodsOrder;
use App\Models\InquiryOrder;
use App\Models\QuickAskDoctor;
use App\Models\QuickAskOrder;
use App\Models\QuickAskOrderDetail;
use App\Models\RegisteredHospitals;
use App\Models\SeparateMoney;
use App\Models\SystemMsg;
use App\Models\WithdrawOrder;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use PhpParser\Comment\Doc;

class FinanceController extends BaseController
{
    /**
     * 财务列表
     * @return mixed
     * @throws \Illuminate\Validation\ValidationException
     */
    public function index()
    {
        $this->validate( $this->request, [
            'type'       => 'filled',
            'start_time' => 'filled',
            'end_time'   => 'required_with:start_time',
            'method_pay' => 'min:1|max:2',
        ] );

        $input = $this->request->all();
        //问诊
        $unionInquery = InquiryOrder::getInstance()->selectRaw( 'id,user_id, ' . PayClass::ORDERTEXT . ' as type,pay_amount,created_at,method_pay' )->where( [ 'pay_status' => 1, 'order_status' => 1 ] );
        //appoint
        $appoint = Appointment::getInstance()->selectRaw( 'id,user_id,' . PayClass::ORDERAPPOINT . ' as type,pay_amount,created_at,method_pay' )->where( [ 'pay_status' => 1, 'order_status' => 1 ] );

        //商品
        $goods = GoodsOrder::getInstance()->selectRaw( 'id,user_id,' . PayClass::ORDERGOODS . ' as type,pay_amount-total_refund_amount as pay_amount,created_at,method_pay' )->whereIn( 'pay_status', [ 1, 2 ] )->where( 'order_status', '!=', 2 );
        switch ( $input['type'] ) {
            case PayClass::ORDERGOODS:
                $result = $goods;
                break;
            case PayClass::ORDERAPPOINT:
                $result = $appoint;
                break;
            case PayClass::ORDERTEXT:
                $result = $unionInquery;
                break;
            default:
                $result = $unionInquery->unionAll( $appoint )->unionAll( $goods );
                $total = $result;
                break;
        }
        $table = $result->toSql();
        $model = DB::table( DB::raw( "($table) as t" ) )
            ->when( $input['start_time'], function ( $where, $value ) {
                $where->whereDate( 't.created_at', '>=', $value );
            } )
            ->when( $input['end_time'], function ( $where, $value ) {
                $where->whereDate( 't.created_at', '<=', $value );
            } )
            ->when( $input['method_pay'], function ( $where, $value ) {
                $where->where( 't.method_pay', $value );
            } );;
        $result = $model
            ->selectRaw( 't.*,u.nickname as name' )
            ->leftJoin( 'users as u', 'u.id', '=', 't.user_id' )
            ->setBindings( array_merge( $result->getBindings(), $model->getBindings() ) )
            ->orderByDesc( 't.created_at' )
            ->paginate( $this->pageSize )->toArray();
        array_walk( $result['data'], function ( $v ) {
            return $v->pay_amount = $v->pay_amount / 100;
        } );

        if ( !$total )
            $total = $unionInquery->unionAll( $appoint )->unionAll( $goods );

        $total = $total->sum( 'pay_amount' );
        return $this->success( [ 'total' => $total / 100, 'data' => $result ] );
    }


    /**
     * 提现列表
     * @return mixed
     * @throws \Illuminate\Validation\ValidationException
     */
    public function list()
    {
        $this->validate( $this->request, [
            'status' => 'filled'
        ] );
        try {
            $input = $this->request->all();
            $where[] = [ 'pay_status', '>', 0 ];
            $where['order_status'] = 1;
            if ( $input['status'] )
                $where['pay_status'] = $input['status'];
            $data = WithdrawOrder::getInstance()
                ->withCount( [ 'hasManySelf as has_many_self_sum' => function ( $query ) {
                    $query->select( DB::raw( "ifnull(sum(pay_amount),0) as relationsum" ) )->where( 'pay_status', 2 )->where( 'order_status', 1 );
                } ] )
                ->with( [ 'belongsToDoctor' => function ( $query ) {
                    $query->select( 'id', 'name' )->withTrashed();
                } ] )
                ->where( $where )->paginate( $this->pageSize )->toArray();
            foreach ( $data['data'] as $k => &$v ) {
                $v['has_many_self_sum'] = bcdiv( $v['has_many_self_sum'], 100, 2 );
            }
            $where['pay_status'] = 2;
            $total = WithdrawOrder::getInstance()->where( $where )->sum( 'pay_amount' );
            return $this->success( [ 'total' => bcdiv( $total, 100, 2 ), 'data' => $data ] );
        } catch ( \Exception $e ) {
            return $this->failed( $e->getMessage() );
        }
    }

    /**
     * 提现详情
     * @return mixed
     * @throws \Illuminate\Validation\ValidationException
     */
    public function detail()
    {
        $this->validate( $this->request, [
            'id' => 'required'
        ] );
        try {
            $input = $this->request->all();
            $info = WithdrawOrder::getInstance()->with( [ 'belongsToDoctor' => function ( $query ) {
                $query->select( 'id', 'name', 'mobile', 'balance' )->withTrashed();
            } ] )->where( 'id', $input['id'] )->first();
            if ( !$info )
                return $this->failed( '数据不存在' );
            $info->belongsToDoctor->makeVisible( 'balance' );
            return $this->success( $info );
        } catch ( \Exception $e ) {
            return $this->failed( $e->getMessage() );
        }
    }


    public function handle()
    {
        $this->validate( $this->request, [
            'id'   => 'required|int',
            'type' => 'required|int|max:2|min:1'
        ] );
        try {
            $input = $this->request->all();
            $order = WithdrawOrder::getInstance()->find( $input['id'] );
            if ( !$order )
                return $this->failed( '订单不存在' );
            if ( $order->pay_status == 2 )
                return $this->failed( '申请已同意' );
            if ( $order->pay_status == 3 )
                return $this->failed( '申请已拒绝' );
            $user = Doctor::getInstance()->find( $order->doctor_id );
            if ( !$user )
                return $this->failed( '医生信息不存在' );

            if ( $input['type'] == 1 ) {
                //同意
                DB::beginTransaction();
                $pay = new PayClass();
                $result = $pay->transfer( 1, $order->order_number, $order->account, $order->pay_amount );
                if ( $result['code'] != '10000' )
                    return $this->failed( '提现失败' );
                $order->pay_status = 2;
                $user->balance -= $input['amount'];
                $user->save();
                $input['result'] = json_encode( $result );
                $input['amount'] = $order->pay_amount;
                $input['type'] = 5;
                $user->UserHasMany( DoctorBalance::class )->create( $input );
                $order->save();
                //Doctor::getInstance()->updateBalance($order->pay_amount, $order->doctor_id, 5);
                DB::commit();
                //发送消息给用户
                $msg = '您在' . $order->created_at . '创建的提现申请已被通过';
                SystemMsg::getInstance()->sendSysMsg( $msg, $order->doctor_id, 2 );
                //推送
                $data['code'] = 10001;
                $push = new Push( 2 );
                $push->sendMsg( '提现申请已通过', $msg, [ $order->doctor_id ], $data );
                return $this->message( '提现成功' );
            } else {
                $order->pay_status = 3;
                $order->save();
                DB::commit();
                //发送消息给用户
                $msg = '您在' . $order->created_at . '创建的提现申请已被拒绝';
                SystemMsg::getInstance()->sendSysMsg( $msg, $order->doctor_id, 2 );
                //推送
                $data['code'] = 10001;
                $push = new Push( 2 );
                $push->sendMsg( '提现申请已被拒绝', $msg, [ $order->doctor_id ], $data );
                return $this->message( '审核已拒绝' );
            }
        } catch ( \Exception $e ) {
            DB::rollBack();
            return $this->failed( $e->getMessage() );
        }
    }

    public function getFinanceType()
    {
        $type = $this->request->get( 'type', 0 );
        if ( $type == 0 ) {

            $data = [
                [ 'type' => PayClass::ORDERTEXT, 'name' => '问诊' ],
                [ 'type' => PayClass::ORDERQUICK, 'name' => '问答' ],
                [ 'type' => PayClass::OORDERREG, 'name' => '挂号' ],
                [ 'type' => PayClass::ORDERFEE, 'name' => '门诊缴费' ],
            ];
        } else {
            $data = [
                [ 'type' => 1, 'name' => '快速问诊' ],
                [ 'type' => 2, 'name' => '图文问诊' ],
                [ 'type' => 3, 'name' => '音频问诊' ],
                [ 'type' => 4, 'name' => '视频问诊' ],
            ];
        }


        return $this->success( $data );
    }

    public function separate()
    {
        $this->validate( $this->request, [
            'type'       => 'max:4',
            'start_time' => 'date',
            'end_time'   => 'date|required_with:start_time|after_or_equal:start_time',
            'h_id'       => 'filled',
            'doctor_id'  => 'filled',
        ] );
        try {

            $input = $this->request->all();
            $model = SeparateMoney::getInstance()->with( [ 'belongsToUser:id,name' ] );

            if ( $input['h_id'] || $input['doctor_id'] ) {
                $doctor = Doctor::getInstance()->withTrashed();
                $input['h_id'] && $doctor->where( 'h_id', $input['h_id'] );
                $input['doctor_id'] && $doctor->where( 'id', $input['doctor_id'] );
                $doctorIds = $doctor->select( 'id' )->pluck( 'id' );
                $ask = QuickAskDoctor::getInstance()->whereIn( 'doctor_id', $doctorIds )->where( 'order_status', 1 )->distinct()->pluck( 'order_id' );
                $inquery = InquiryOrder::getInstance()->whereIn( 'doctor_id', $doctorIds )->pluck( 'id' );
                if ( $inquery || $ask ) {
                    $model->where( function ( $query ) use ( $ask, $inquery ) {
                        $query->where( function ( $query ) use ( $inquery ) {
                            $query->where( 'type', '>', 1 )->whereIn( 'order_id', $inquery );
                        } )->orWhere( function ( $query ) use ( $ask ) {
                            $query->where( 'type', 1 )->whereIn( 'order_id', $ask );
                        } );
                    } );
                }
            }

            if ( $input['type'] )
                $model->where( 'type', $input['type'] );
            if ( $input['start_time'] )
                $model->whereDate( 'created_at', '>=', $input['start_time'] )->whereDate( 'created_at', '<=', $input['end_time'] );
            if ( $input['i_type'] )
                $model->where( 'type', $input['i_type'] );
            $data = $model->orderByDesc( 'id' )->paginate( $this->pageSize )->toArray();
            $total = SeparateMoney::getInstance()->selectRaw( 'id,amount*scale as total' )->pluck( 'total' )->toArray();
            $h_total = SeparateMoney::getInstance()->selectRaw( 'id,amount*h_scale as total' )->pluck( 'total' )->toArray();
            $a_total = SeparateMoney::getInstance()->selectRaw( 'id,amount*a_scale as total' )->pluck( 'total' )->toArray();
            $total = bcdiv( array_sum( $total ), 10000, 2 );
            $h_total = bcdiv( array_sum( $h_total ), 10000, 2 );
            $a_total = bcdiv( array_sum( $a_total ), 10000, 2 );
            return $this->success( [ 'data' => $data, 'total' => $total, 'h_total' => $h_total, 'a_total' => $a_total ] );
        } catch ( \Exception $e ) {

            return $this->failed( $e->getMessage() );
        }
    }


}
