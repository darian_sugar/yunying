<?php

namespace App\Http\Controllers\Admin\Shop;

use App\Helpers\Kuaidi100;
use App\Helpers\PayClass;
use App\Helpers\Push;
use App\Http\Controllers\Admin\BaseController;
use App\Models\Address;
use App\Models\GoodsOrder;
use App\Models\SystemMsg;
use App\Rules\Numeric2Rule;
use App\Services\Model\GoodsOrderService;
use App\Services\ServiceManager;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\Rule;

class OrderController extends BaseController
{
    public function index()
    {
        $input = $this->request->all();
        $where = ServiceManager::getInstance()->GoodsOrderService( GoodsOrderService::class )->getSearchWhere( $input['type'] );

        $result = GoodsOrder::getInstance()->select( 'id', 'user_id', 'order_number', 'method_pay', 'pay_amount', 'pay_time', 'trans_status', 'addr_id' )
            ->where( $where )
            ->when( $input['key'], function ( $where, $value ) {
                $where->where( 'order_number', 'like', '%' . $value . '%' )
                    ->orWhereHas( 'hasManyDetail', function ( $query ) use ( $value ) {
                        $query->where( 'goods_code', 'like', '%' . $value . '%' );
                    } );
            } )
            ->when( $input['ukey'], function ( $where, $value ) {
                $where->whereHas( 'hasOneSubsidiary', function ( $query ) use ( $value ) {
                    $query->where( 'name', 'like', '%' . $value . '%' )
                        ->orWhere( 'mobile', 'like', '%' . $value . '%' );
                } );
            } )
            ->when( $input['date'], function ( $where, $value ) {
                $where->whereDate( 'created_at', $value );
            } )
            ->with( [
                'belongsToUser:id,mobile,nickname',
            ] )
            ->paginate( $this->pageSize );
        return $this->success( $result );
    }

    /**
     * 订单详情
     * @return mixed
     * @throws \Illuminate\Validation\ValidationException
     * @throws \JsonException
     */
    public function detail()
    {
        $this->validate( $this->request, [
            'id' => 'required'
        ] );

        $input = $this->request->all();
        $result = GoodsOrder::getInstance()
            ->with( [
                'hasManyDetail.belongsToGoods:id,code',
                'hasOneSubsidiary:order_id,mobile,name,province_id,area_id,city_id,addr',
                'hasOneSubsidiary.belongsToArea:id,name',
                'hasOneSubsidiary.belongsToCity:id,name',
                'hasOneSubsidiary.belongsToProvince:id,name',
                'hasOneAfterSales'
            ] )
            ->findOrFail( $input['id'] );
        $kuaidi = Kuaidi100::getInstance()->search( [ 'sn' => $result->trans_number, 'code' => $result->trans_company_code ] );
        $result->kuaidi = $kuaidi;
        return $this->success( $result );
    }


    /**
     * 获取物流
     * @return mixed
     * @throws \Illuminate\Validation\ValidationException
     * @throws \JsonException
     */
    public function getKuaidi()
    {
        $this->validate( $this->request, [
            'sn'   => 'required',
            'code' => 'required',
        ] );
        $input = $this->request->all();
        $result = Kuaidi100::getInstance()->search( $input );
        return $this->success( $result );
    }

    /**
     * 发货
     * @return mixed
     * @throws \Illuminate\Validation\ValidationException
     */
    public function kuaidi()
    {
        $this->validate( $this->request, [
            'id'            => 'required',
            'sn'            => 'required',
            'code'          => 'required',
            'trans_company' => 'required',
        ] );
        $input = $this->request->all();
        $order = GoodsOrder::getInstance()->findOrFail( $input['id'] );
        if ( $order->trans_status > 1 )
            return $this->failed( '订单已发货' );
        $order->trans_number = $input['sn'];
        $order->trans_company_code = $input['code'];
        $order->trans_company = $input['trans_company'];
        $order->trans_status = 2;
        $order->save();
        //发送通知
        $orderGoods = $order->hasManyDetail()->get()->transform( function ( $item, $key ) {
            return $item['goods_spec'] = $item['goods_name'] . ' ' . $item['spec_name'];
        } );
        $string = '（' . $orderGoods[0] . '）';
        if ( count( $orderGoods ) > 1 )
            $string .= '等';
        $msg = '您购买的' . $string . '，订单号：' . $order->order_number . '已发货，由' . $input['trans_company'] . '承运，运单号：' . $input['sn'] . '，您可以在官网查询物流进程。';
        SystemMsg::getInstance()->sendSysMsg( $msg, $order->user_id, 1, '商品已发货', 2 );
        return $this->message();
    }


    /**
     * 取消订单
     * @return mixed
     * @throws \Illuminate\Validation\ValidationException
     */
    public function cancelOrder()
    {
        $this->validate( $this->request, [
            'id' => 'required',
        ] );
        $input = $this->request->all();
        $order = GoodsOrder::getInstance()->findOrFail( $input['id'] );
        //取消订单
        ServiceManager::getInstance()->GoodsOrderService( GoodsOrderService::class )->cancelOrder( $order, 1 );
        return $this->message();
    }

    /**
     * 售后审核
     * @return mixed
     * @throws \Illuminate\Validation\ValidationException
     */
    public function confirmAfterSales()
    {
        $this->validate( $this->request, [
            'id'              => 'required',
            'reply'           => 'required_if:type,1',
            'type'            => 'required|int',
            'data'            => 'required_if:type,2|array',
            'data.*.goods_id' => 'required|int',
            'data.*.num'      => 'required|int',
            'data.*.price'    => [
                'required',
                new Numeric2Rule( '2', '商品退款金额' )
            ],
        ], [], [
            'reply'           => '回复内容',
            'type'            => '审核类型',
            'data'            => '退货商品信息',
            'data.*.goods_id' => '退货商品',
            'data.*.num'      => '退货商品数量',
            'data.*.price'    => '退货商品价格',
        ] );
        $input = $this->request->all();
        $user = Auth::user();
        $order = GoodsOrder::getInstance()
            ->with( [
                'hasOneAfterSales',
                'hasManyDetail:order_id,goods_id,goods_name,num,total_price',
            ] )
            ->where( [ 'order_status' => 3, 'pay_status' => 1 ] )->findOrFail( $input['id'] );
        $afterSales = $order->hasOneAfterSales;

        if ( $afterSales->check_status != 0 )
            return $this->failed( '订单售后已处理' );
        $goodsDetail = $order->hasManyDetail->keyBy( 'goods_id' )->all();
        $refundAmount = collect( $input['data'] )->sum( 'price' );
        foreach ( $input['data'] as $k => $v ) {
            if ( !$goodsDetail[ $v['goods_id'] ] )
                return $this->failed( '退货商品不存在' );
            if ( $v['num'] > $goodsDetail[ $v['goods_id'] ]['num'] )
                return $this->failed( $goodsDetail[ $v['goods_id'] ]['goods_name'] . ' 退货数量大于订单商品数量' );
            if ( $v['price'] > $goodsDetail[ $v['goods_id'] ]['total_price'] )
                return $this->failed( $goodsDetail[ $v['goods_id'] ]['goods_name'] . ' 退款金额大于订单金额' );
        }

        if ( $refundAmount > $order->total_amount )
            return $this->failed( '退款金额大于订单金额' );

        DB::beginTransaction();
        try {
            $afterSales->admin_id = $user->id;
            $afterSales->check_status = $input['type'];
            $afterSales->reply = $input['reply'];
            $afterSales->refund_amount = $refundAmount;
            $afterSales->save();
            $order->order_status = $input['type'] == 1 ? 4 : 5;
            if ( $order->total_amount == $refundAmount )
                $order->pay_status = 3;
            else
                $order->pay_status = 2;
            $order->total_refund_amount += $refundAmount;
            $order->save();
            $afterSales->hasManyGoods()->create( $input['data'] );
            //todo
//            PayClass::getInstance()->refund( $order->order_number, $order->pay_amount, $refundAmount, $order->method_pay );
            //发送通知
            $orderGoods = $order->hasManyDetail()->get()->transform( function ( $item, $key ) {
                return $item['goods_spec'] = $item['goods_name'] . ' ' . $item['spec_name'];
            } );
            $string = '（' . $orderGoods[0] . '）';
            if ( count( $orderGoods ) > 1 )
                $string .= '等';
            $msg = '您购买的' . $string . '，订单号：' . $order->order_number . '，售后已审核' . ( $input['type'] == 1 ? '拒绝' : '通过，退款金额：' . $refundAmount . '，款项将原路退回，请您注意查收。' );
            SystemMsg::getInstance()->sendSysMsg( $msg, $order->user_id, 1, '商品售后审核', 2 );
            DB::commit();
            return $this->message();
        } catch ( \Exception $exception ) {
            DB::rollBack();
            return $this->failed( $exception->getMessage() );
        }
    }


}
