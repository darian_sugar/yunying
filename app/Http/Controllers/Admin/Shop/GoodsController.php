<?php

namespace App\Http\Controllers\Admin\Shop;

use App\Http\Requests\Admin\GoodsRequest;
use App\Models\Goods;
use App\Http\Controllers\Controller;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;

class GoodsController extends Controller
{
    //

    public function index()
    {
        $input = $this->request->all();
        $result = Goods::getInstance()
            ->select( 'id', 'name', 'code', 'stock', 'status', 'is_show' )
            ->when( $input['key'], function ( $where, $value ) {
                $where->where( 'name', 'like', '%' . $value . '%' )
                    ->orWhere( 'code', 'like', '%' . $value . '%' );
                //                    ->orWhereRaw( "FIND_IN_SET(?,key_words)", [ $value ] );
            } )
            ->when( $input['cate_id'], function ( $where, $value ) {
                $where->where( 'cate_id', $value );
            } )
            ->with( [
                'hasMainPhoto:id,goods_id,image',
                'hasMinPrice:id,goods_id,price'
            ] )
            ->withCount( [
                'hasManyOrder as goods_sum' => function ( $query ) {
                    $query->select( DB::raw( "ifnull(sum(goods_order_details.num),0)" ) )
                        ->where( 'goods_orders.pay_status', 1 )
                        ->where( 'goods_orders.order_status', 1 );
                }
            ] )->paginate( $this->pageSize );
        return $this->success( $result );
    }

    public function add( GoodsRequest $request )
    {
        $input = $request->all();
        if ( $input['id'] )
            $orderModel = Goods::getInstance()->findOrFail( $input['id'] );
        else
            $orderModel = new Goods();
        $input['stock'] = collect( $input['extre'] )->sum( 'stock' );
        DB::beginTransaction();
        try {
            $orderModel->fill( $input )->save();
            $this->getQueryLog();
            if ( $input['extre'] ) {
                if ( $input['id'] ) {
                    $orderModel->hasManySpec()->delete();
                }
                $orderModel->hasManySpec()->createMany( $input['extre'] );
            }
            if ( $input['photos'] ) {
                if ( $input['id'] ) {
                    $orderModel->hasManyPhoto()->delete();
                }
                $orderModel->hasManyPhoto()->createMany( $input['photos'] );
            }
            DB::commit();
            return $this->message();
        } catch ( \Exception $exception ) {
            DB::rollBack();
            return $this->failed( $exception->getMessage() );
        }
    }

    public function detail( GoodsRequest $request )
    {
        $id = $request->get( 'id' );
        $goods = Goods::getInstance()
            ->with( [
                'hasManySpec',
                'hasManyPhoto',
            ] )
            ->findOrFail( $id );
        return $this->success( $goods );
    }

    /**
     * 修改状态
     * @return array
     * @throws \Illuminate\Validation\ValidationException
     */
    public function changeStatus()
    {
        $this->validate( $this->request, [
            'type' => 'required|int|between:1,5',
            'id'   => 'required'
        ] );
        $input = $this->request->all();
        $model = Goods::getInstance();

        if ( is_array( $input['id'] ) )
            $model = $model->whereIn( 'id', $input['id'] );
        else
            $model = $model->where( 'id', $input['id'] );

        if ( $input['type'] == 3 )
            $res = $model->delete();
        elseif ( $input['type'] == 4 )
            $res = $model->update( [ 'is_show' => 1 ] );
        elseif ( $input['type'] == 5 )
            $res = $model->update( [ 'is_show' => 2 ] );
        else
            $res = $model->update( [ 'status' => $input['type'] ] );

        $this->getQueryLog();
        if ( $res === false )
            return $this->failed( '更新失败' );
        else
            return $this->message( '更新成功' );
    }
}
