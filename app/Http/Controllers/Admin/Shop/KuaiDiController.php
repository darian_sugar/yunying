<?php

namespace App\Http\Controllers\Admin\Shop;

use App\Helpers\Kuaidi100;
use App\Helpers\PayClass;
use App\Helpers\Push;
use App\Http\Controllers\Admin\BaseController;
use App\Models\Address;
use App\Models\GoodsOrder;
use App\Models\SystemMsg;
use App\Rules\Numeric2Rule;
use App\Services\Model\GoodsOrderService;
use App\Services\ServiceManager;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\Rule;

class KuaiDiController extends BaseController
{
    public function index()
    {
        $result = DB::table( 'kuaidi_codes' )->where( 'status', 1 )->select( 'code', 'title' )->get();
        return $this->success( $result );
    }


}
