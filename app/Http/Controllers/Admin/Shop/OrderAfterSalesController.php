<?php

namespace App\Http\Controllers\Admin\Shop;

use App\Helpers\Kuaidi100;
use App\Helpers\PayClass;
use App\Helpers\Push;
use App\Http\Controllers\Admin\BaseController;
use App\Models\Address;
use App\Models\GoodsOrder;
use App\Models\SystemMsg;
use App\Rules\Numeric2Rule;
use App\Services\Model\GoodsOrderService;
use App\Services\ServiceManager;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\Rule;

class OrderAfterSalesController extends BaseController
{
    public function index()
    {
        $input = $this->request->all();

        $result = GoodsOrder::getInstance()->select( 'id', 'user_id', 'order_number', 'method_pay', 'pay_amount', 'order_status', 'addr_id' )
            ->whereIn( 'order_status', [ 3, 4, 5 ] )
            ->when( $input['key'], function ( $where, $value ) {
                $where->where( 'order_nuamber', 'like', '%' . $value . '%' )
                    ->orWhereHas( 'hasManyDetail', function ( $query ) use ( $value ) {
                        $query->where( 'goods_code', 'like', '%' . $value . '%' );
                    } );
            } )
            ->when( $input['ukey'], function ( $where, $value ) {
                $where->whereHas( 'hasOneSubsidiary', function ( $query ) use ( $value ) {
                    $query->where( 'name', 'like', '%' . $value . '%' )
                        ->orWhere( 'mobile', 'like', '%' . $value . '%' );
                } );
            } )
            ->when( $input['date'], function ( $where, $value ) {
                $where->whereHas( 'hasOneAfterSales', function ( $where ) use ( $value ) {
                    $where->whereDate( 'created_at', $value );
                } );
            } )
            ->with( [
                'hasOneAfterSales:order_id,created_at',
                'belongsToUser:id,mobile,nickname',
            ] )
            ->paginate( $this->pageSize );
        return $this->success( $result );
    }
}
