<?php

namespace App\Http\Controllers\Admin\Shop;

use App\Http\Controllers\Admin\BaseController;
use App\Models\Goods;
use App\Models\GoodsCate;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;

class CateController extends BaseController
{
    /**
     *
     * 列表
     * @return array
     */
    public function index()
    {
        if ( $this->request->get( 'type' ) == 1 ) {
            $result = GoodsCate::getInstance()
                ->select( 'id', 'name', 'status', 'level', 'p_id' )
                ->where( 'status', 1 )->get()->toArray();
            $data = [];
            listToTree( $result, 'id', 'p_id', 'children', 0, $data );
        } else {
            $data = GoodsCate::getInstance()
                ->select( 'id', 'name', 'status', 'level' )
                ->withCount( [
                    'hasManyGoods as goods_count'
                ] )
                ->latest( 'id' )
                ->paginate( $this->pageSize );
        }
        return $this->success( $data );
    }

    /**
     * 分类树
     * @return array
     */
    public function tree()
    {

    }

    /**
     * 添加
     * @return array
     */
    public function add()
    {
        $this->validate( $this->request, [
            'name'   => [
                'required',
                'between:1,64',
                Rule::unique( 'goods_cates' )->whereNull( 'deleted_at' )->ignore( $this->request->get( 'id' ), 'id' )
            ],
            'status' => 'required',
            //            'p_id'   => '',
            'icon'   => 'required',
            'desc'   => 'required|max:200',
            'level'  => 'required|int',
        ], [], [
            'name'   => '分类名称',
            'status' => '是否显示',
            'p_id'   => '父分类',
            'icon'   => '分类图标',
            'desc'   => '分类描述',
            'level'  => '分类级别',
        ] );
        $data = $this->request->all();
        if ( $data['id'] ) {
            $model = GoodsCate::getInstance()->where( 'id', $data['id'] )->firstOrFail();
        } else {
            $model = new GoodsCate();
        }
        if ( $data['p_id'] ) {
            $pCate = GoodsCate::getInstance()->find( $data['p_id'] );
            if ( !$pCate )
                return $this->failed( '父分类不存在' );
            $parnets = $pCate->parnets;
        }
        $user = Auth::user();
        $data['admin_id'] = $user->id;
        $data['parnets'] = $parnets ? $parnets . ',' . $data['p_id'] : ( $data['p_id'] ? $data['p_id'] : 0 );
        $model->fill( $data )->save();
        return $this->message();
    }

    /**
     * 修改状态
     * @return array
     * @throws \Illuminate\Validation\ValidationException
     */
    public function changeStatus()
    {
        $this->validate( $this->request, [
            'type' => 'required|int|between:1,3',
            'id'   => 'required'
        ] );
        $input = $this->request->all();
        $model = GoodsCate::getInstance();

        if ( is_array( $input['id'] ) )
            $model = $model->whereIn( 'id', $input['id'] );
        else
            $model = $model->where( 'id', $input['id'] );
        if ( $input['type'] == 3 )
            $model->delete();
        else
            $model->update( [ 'status' => $input['type'] ] );
        return $this->message();
    }
}
