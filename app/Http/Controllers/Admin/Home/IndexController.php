<?php

namespace App\Http\Controllers\Admin\Home;

use App\Models\Admin;
use App\Models\Appointment;
use App\Models\InquiryOrder;
use App\Models\User;
use App\Services\Common\CheckService;
use App\Services\Model\AdminService;
use App\Services\Model\GoodsOrderService;
use App\Services\ServiceManager;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class IndexController extends Controller
{
    //
    public function index()
    {
        $user = Auth::user();
        return $this->success( $user );
    }

    public function edit()
    {

        $input = $this->request->all();
        $user = Auth::user();
        $user->fill( $input )->save();
        return $this->message();
    }

    /**
     * 修改密码
     * @return array
     * @throws \Illuminate\Validation\ValidationException
     */
    public function updatePwd()
    {

        $this->validate( $this->request, [
            'old_password'          => 'required|string|min:6|max:16',
            'password'              => 'required|string|min:6|max:16|confirmed',
            'password_confirmation' => 'required|same:password',
        ] );

        $input = $this->request->all();
        CheckService::checkPassword( $input['password'] );
        $user = Auth::user();
        if ( !Hash::check( $input['old_password'], $user->password ) )
            return $this->failed( '密码错误' );
        $user->password = Hash::make( $input['password'] ) ?? '';
        $user->save();
        return $this->message( '修改成功' );
    }

    public function access()
    {
        $user = Auth::user();
        $data = ServiceManager::getInstance()->AdminService( AdminService::class )->adminAccess( $user );
        return $this->success( $data );

    }


    /**
     * 首页数据
     * @return mixed
     */
    public function homePage()
    {
        $date = date( 'Y-m-d' );
        $start = $this->request->get( 'start', $date );
        $end = $this->request->get( 'end', $date );
        $where = " created_at BETWEEN '$start' AND '$end' ";

        $userNumber = User::getInstance()
            ->where( 'status', 1 )
            ->whereRaw( $where )
            ->count();
        $inqueryCount = InquiryOrder::getInstance()
            ->where( [ 'pay_status' => 1, 'order_status' => 1 ] )
            ->whereRaw( $where )
            ->count();
        $appointCount = Appointment::getInstance()
            ->where( [ 'pay_status' => 1 ] )
            ->whereRaw( $where )
            ->whereIn( 'order_status', [ 1, 3 ] )
            ->whereIn( 'check_status', [ 0, 1 ] )->count();

        return $this->success(
            [
                'user_number'  => $userNumber,
                'inqueryCount' => $inqueryCount,
                'appointCount' => $appointCount,
            ]
        );

    }


    /**
     * 交易统计金额
     * @return mixed
     */
    public function homeTrading()
    {
        $now = date( 'Y-m-d' );
        $start = $this->request->get( 'start' ) ?? $now;
        $end = $this->request->get( 'end' ) ?? $now;

        $where = "created_at BETWEEN '$start' AND '$end' ";
        $appoint = Appointment::getInstance()->whereRaw( $where )->where( 'pay_status', 1 )->sum( 'total_amount' );
        $inquiry = InquiryOrder::getInstance()->whereRaw( $where )->where( 'pay_status', 1 )->sum( 'total_amount' );
        return $this->success( [ [ 'name' => '预约', 'value' => $appoint ], [ 'name' => '问诊', 'value' => $inquiry ] ] );
    }
}
