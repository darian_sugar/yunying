<?php

namespace App\Http\Controllers\App\V1\Shop;

use App\Http\Controllers\App\BaseController;
use App\Models\Adv;
use App\Models\Goods;
use App\Models\GoodsCate;
use App\Models\UserCart;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class GoodsController extends BaseController
{
    //

    public function getCate()
    {
        $result = GoodsCate::getInstance()->select( 'id', 'name', 'icon' )
            ->where( [ 'status' => 1, 'p_id' => 0 ] )
            ->get();
        return $this->success( $result );
    }

    public function index()
    {
        $input = $this->request->all();
        $cate_id = $this->request->get( 'cate_id' );
        $cateId = GoodsCate::getInstance()
            ->when( $cate_id, function ( $where, $value ) {
                $where->whereRaw( "FIND_IN_SET(?,parnets)", [ $value ] )->orWhere( 'id', $value );
            } )
            ->where( [ 'status' => 1 ] )
            ->pluck( 'id' )->toArray();
        $result = Goods::getInstance()
            ->select( 'id', 'name', 'stock' )
            ->where( [ 'status' => 1 ] )
            ->where( 'stock', '>', 0 )
            ->whereIn( 'cate_id', $cateId )
            ->when( $input['key'], function ( $where, $value ) {
                $where->where( 'name', 'like', '%' . $value . '%' )->orWhere( 'key_words', 'like', '%' . $value . '%' );
            } )
            ->with( [
                'hasMainPhoto:id,goods_id,image',
                'hasMinPrice:id,goods_id,price,num,packing_unit,weight_unit,weight_num',
            ] )
            ->latest( 'id' )
            ->paginate( $this->pageSize );
        return $this->success( $result );
    }

    /**
     * 推荐商品列表
     * @return mixed
     */
    public function showList()
    {
        $result = Goods::getInstance()
            ->select( 'id', 'name', 'stock' )
            ->where( [ 'status' => 1, 'is_show' => 1 ] )
            ->where( 'stock', '>', 0 )
            ->with( [
                'hasMainPhoto:id,goods_id,image',
                'hasMinPrice:id,goods_id,price,num,packing_unit,weight_unit,weight_num',

            ] )
            ->latest( 'id' )
            ->paginate( $this->pageSize );
        return $this->success( $result );
    }

    /**
     * 商品详情
     * @return mixed
     * @throws \Illuminate\Validation\ValidationException
     */
    public function detail()
    {

        $this->validate( $this->request, [
            'id' => 'required',
        ] );
        $id = $this->request->get( 'id' );
        $advId = $this->request->get( 'adv_id', 0 );
        $result = Goods::getInstance()
            ->where( [ 'status' => 1 ] )
            ->where( 'stock', '>', 0 )
            ->with( [
                'belongsToCate:id,name,icon',
                'hasManyPhoto:id,image,goods_id',
                'hasManySpec:id,goods_id,num,packing_unit,weight_unit,weight_num,price',
                'belongsToBrand:id,name,icon',
                'hasMinPrice:id,goods_id,price,num,packing_unit,weight_unit,weight_num',
                'hasMainPhoto:id,goods_id,image',
            ] )
            ->latest( 'id' )
            ->findOrFail( $id );
        try {
            if ( $advId )
                Adv::getInstance()->where( 'id', $advId )->increment( 'click_num' );
        } catch ( \Exception $exception ) {
            logLog( $exception->getMessage(), 'error' );
        }

        return $this->success( $result );
    }

    /**
     * 添加购物车
     * @return mixed
     * @throws \Illuminate\Validation\ValidationException
     */
    public function goCart()
    {
        $user = Auth::user();
        $this->request->only( [ 'goods_id', 'num', 'spec' ] );
        $this->validate( $this->request, [
            'goods_id' => 'required',
            'num'      => 'required',
            'spec_id'  => 'required',
            'type'     => 'required',
        ], [], [
            'goods_id' => '商品',
            'spec_id'  => '规格',
            'num'      => '商品',
            'type'     => '商品增加类型'
        ] );
        $input = $this->request->all();
        $good = Goods::getInstance()->where( 'status', 1 )->findOrFail( $input['goods_id'] );
        if ( $good->stock < $input['num'] )
            return $this->failed( '库存不足' );
        $spec = $good->hasManySpec()->findOrFail( $input['spec_id'] );
        if ( $spec->stock < $input['num'] )
            return $this->failed( '库存不足' );
        $record = $user->hasManyCartGoods()->where( [ 'goods_id' => $input['goods_id'], 'spec_id' => $input['spec_id'] ] )->first();
        if ( $record ) {
            if ( $record->spec_id == $input['spec_id'] ) {
                if ( $input['type'] != 2 ) {
                    $record->update( [ 'num' => $input['num'] ] );
                } else {
                    $record->increment( 'num', $input['num'] );
                }
            } else
                $record->fill( $input )->save();
        } else {
            $user->hasManyCartGoods()->create( $input );
        }
        return $this->message();
    }

    /**
     * 购物车列表
     * @return mixed
     */
    public function cart()
    {
        $user = Auth::user();
        $data = $user->hasManyCartGoods()->with( [
            'belongsToGoods:id,name',
            'belongsToGoods.hasMainPhoto:id,image,goods_id',
            'belongsToGoods.hasManySpec:id,num,packing_unit,weight_unit,weight_num,goods_id,price',
        ] )->latest( 'updated_at' )->paginate( $this->pageSize );
        return $this->success( $data );
    }

    /**
     * 删除购物车
     * @return mixed
     * @throws \Illuminate\Validation\ValidationException
     */
    public function deleteCart()
    {
        $this->validate( $this->request, [
            'cart_id' => 'required|array|distinct'
        ] );
        $user = Auth::user();
        $input = $this->request->all();
        $user->hasManyCartGoods()->whereIn( 'id', $input['cart_id'] )->delete();
        return $this->message();
    }

}
