<?php

namespace App\Http\Controllers\App\V1\Shop;

use App\Helpers\Kuaidi100;
use App\Helpers\PayClass;
use App\Http\Controllers\App\BaseController;
use App\Models\Address;
use App\Models\Goods;
use App\Models\GoodsAfterSales;
use App\Models\GoodsOrder;
use App\Services\Model\GoodsOrderService;
use App\Services\ServiceManager;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class OrderController extends BaseController
{
    public function index()
    {
        $this->validate( $this->request, [
            'type' => 'required|int'
        ], [], [
            'type' => '订单类型'
        ] );
        $type = $this->request->get( 'type' );
        $user = Auth::user();
        $where = ServiceManager::getInstance()->GoodsOrderService( GoodsOrderService::class )->getSearchWhere( $type );
        $where['user_id'] = $user->id;
        $result = GoodsOrder::getInstance()
            ->select( 'id', 'order_number', 'pay_amount', 'pay_time', 'created_at', 'order_status' )
            ->with( [
                'hasManyDetail:id,order_id,goods_name,image,price,num,total_price,spec_name',
            ] )
            ->where( $where )
            ->latest( 'id' )
            ->paginate( $this->pageSize );
        foreach ( $result->items() as $k => $v ) {
            $v->goods_total = $v->hasManyDetail->sum( 'num' );
            $v->type = (int)$type;
        }
        return $this->success( $result );
    }

    public function detail()
    {
        $this->validate( $this->request, [
            'id' => 'required'
        ] );

        $input = $this->request->all();
        $user = Auth::user();
        $result = GoodsOrder::getInstance()
            ->select( 'id', 'order_number', 'pay_amount', 'trans_amount', 'total_amount', 'pay_time', 'created_at', 'method_pay', 'trans_number', 'trans_company_code', 'addr_id', 'trans_company', 'order_status' )
            ->with( [
                'hasOneSubsidiary:order_id,mobile,name,province_id,area_id,city_id,addr',
                'hasOneSubsidiary.belongsToArea:id,name',
                'hasOneSubsidiary.belongsToCity:id,name',
                'hasOneSubsidiary.belongsToProvince:id,name',
                'hasManyDetail:id,order_id,goods_name,image,price,num,total_price,spec_name',
                'hasOneAfterSales'
            ] )
            ->where( [ 'user_id' => $user->id ] )->findOrFail( $input['id'] );
        $result->goods_total = $result->hasManyDetail->sum( 'num' );

        if ( $result->order_status == 4 )
            $result->with( [ 'hasOneAfterSales' ] )->first();
        if ( $result->trans_number && $result->trans_company_code )
            $kuaidi = Kuaidi100::getInstance()->search( [ 'sn' => $result->trans_number, 'code' => $result->trans_company_code ] );
        $result->kuaidi = $kuaidi;
        return $this->success( $result );
    }


    /**
     * 获取物流信息
     * @return mixed
     * @throws \Illuminate\Validation\ValidationException
     * @throws \JsonException
     */
    public function getKuaidi()
    {
        $this->validate( $this->request, [
            'sn'   => 'required',
            'code' => 'required',
        ] );
        $input = $this->request->all();
        $result = Kuaidi100::getInstance()->search( $input );
        return $this->success( $result );
    }

    /**
     * 新增订单
     * @return mixed
     * @throws \Illuminate\Validation\ValidationException
     */
    public function add()
    {
        $this->validate( $this->request, [
            'type'       => 'int',
            'cart_id'    => 'array|required_if:type,1',
            'goods_id'   => 'required_if:type,2',
            'spec_id'    => 'required_if:type,2',
            'num'        => 'required_if:type,2',
            'method_pay' => 'required',
            'addr_id'    => 'required',
        ] );
        $input = $this->request->all();
        $user = Auth::user();
        //获取地址
        $addr = Address::getInstance()->where( [ 'user_id' => $user->id, 'id' => $input['addr_id'] ] )->first();
        if ( !$addr )
            return $this->failed( '地址不存在' );

        $orderData = [];
        $total_amount = 0;
        if ( $input['type'] == 1 ) {
            $data = $user->hasManyCartGoods()
                ->whereIn( 'id', $input['cart_id'] )
                ->with( [
                    'belongsToGoods:id,name',
                    'belongsToSpec:id,num,packing_unit,weight_unit,weight_num,stock,price',
                ] )
                ->get();
            foreach ( $data as $k => $v ) {
                if ( !$v->belongsToGoods )
                    return $this->failed( '商品信息不存在' );
                if ( !$v->belongsToSpec )
                    return $this->failed( '商品规格不存在' );
                if ( $v->belongsToSpec->stock < $v->num )
                    return $this->failed( $v->belongsToGoods->name . '商品库存不足' );
                $goods_total_price = round( bcmul( $v->belongsToSpec->price, $v->num, 3 ), 2 );
                $goods[ $k ]['cart_id'] = $v->id;
                $goods[ $k ]['goods_id'] = $v->goods_id;
                $goods[ $k ]['goods_name'] = $v->belongsToGoods->name;
                $goods[ $k ]['spec_id'] = $v->spec_id;
                $goods[ $k ]['spec_name'] = $v->belongsToSpec->spec;
                $goods[ $k ]['price'] = $v->belongsToSpec->price;
                $goods[ $k ]['num'] = $v->num;
                $goods[ $k ]['total_price'] = $goods_total_price;
                $total_amount += $goods_total_price;
                $body = $v->belongsToGoods->name;
            }
        } else {
            $goodsModel = Goods::getInstance()->where( 'status', 1 )->findOrFail( $input['goods_id'] );
            $spec = $goodsModel->hasManySpec()->where( [ 'id' => $input['spec_id'] ] )->first();
            if ( !$spec )
                return $this->failed( '商品规格不存在' );
            $total_amount = round( bcmul( $spec->price, $input['num'], 3 ), 2 );
            $goods[] = [
                'goods_id'    => $goodsModel->id,
                'goods_name'  => $goodsModel->name,
                'spec_id'     => $spec->id,
                'spec_name'   => $spec->spec,
                'price'       => $spec->price,
                'num'         => $input['num'],
                'total_price' => $total_amount
            ];
        }
        if ( !$goods )
            return $this->failed( '购买商品不存在' );
        $trans_amount = 0;//运费 todo
        //添加订单
        $order = new GoodsOrder();
        DB::beginTransaction();
        try {
            $order->user_id = $user->id;
            $order->order_from = $input['type'];
            $order->method_pay = $input['method_pay'];
            $order->addr_id = $input['addr_id'];
            $order->total_amount = $total_amount;
            $order->trans_amount = $trans_amount;
            $order->pay_amount = $total_amount + $trans_amount;
            $order->order_number = PayClass::getInstance()->getOrderNumber( PayClass::ORDERGOODS, $user->id );
            $order->save();
            if ( $goods )
                $order->hasManyDetail()->createMany( $goods );
            $addr->addr = $addr->address;
            $order->hasOneSubsidiary()->create( $addr->toArray() );
            DB::commit();;
            return $this->success( $order );
        } catch ( \Exception $exception ) {
            DB::rollBack();
            return $this->failed( $exception->getMessage() );
        }

    }

    /**
     * 订单支付
     * @return mixed
     * @throws \Illuminate\Validation\ValidationException
     */
    public function pay()
    {
        $this->validate( $this->request, [
            'id' => 'required',
        ] );
        $input = $this->request->all();
        $user = Auth::user();
        $order = GoodsOrder::getInstance()->with( [
            'hasManyDetail.belongsToGoods',
            'hasManyDetail.belongsToGoodsSpec',
        ] )->where( [ 'user_id' => $user->id, 'id' => $input['id'] ] )->firstOrFail();
        if ( $order->pay_status == 1 )
            return $this->failed( '订单已支付' );
        if ( $order->pay_status > 1 )
            return $this->failed( '订单支付状态错误' );

        ServiceManager::getInstance()->GoodsOrderService( GoodsOrderService::class )->hasStock( $order );

        $pay = new PayClass();
        $title = '订单编号' . $order->order_number;
        $body = $user->nickname . '订单';
        $ch = $pay->pay( $order->order_number, $order->pay_amount, $title, $body, $order->method_pay );
        return $this->success( $ch );
    }

    /**
     * 查询支付状态
     * @return mixed
     * @throws \Illuminate\Validation\ValidationException
     */
    public function getOrderPay()
    {
        $this->validate( $this->request, [
            'order_number' => 'required'
        ] );
        $input = $this->request->all();
        $user = Auth::user();
        $result = GoodsOrder::getInstance()
            ->select( 'id', 'order_number', 'pay_status' )
            ->where( [ 'order_number' => $input['order_number'], 'user_id' => $user->id ] )
            ->firstOrFail();
        return $this->success( $result );
    }

    /**
     * 申请售后
     * @return mixed
     * @throws \Illuminate\Validation\ValidationException
     */
    public function afterSales()
    {
        $this->validate( $this->request, [
            'order_id' => 'required|int',
            'content'  => 'required|string|max:255',
            'image'    => 'required|array|max:9',
            'reson_id' => 'required|int',
        ] );

        $input = $this->request->all();
        $user = Auth::user();
        $orderModel = GoodsOrder::getInstance()->where( [ 'user_id' => $user->id, 'id' => $input['order_id'] ] )->firstOrFail();
        if ( $orderModel->pay_status != 1 )
            return $this->failed( '订单支付状态错误' );
        if ( $orderModel->order_status == 2 )
            return $this->failed( '订单已取消' );
        if ( $orderModel->trans_status != 4 )
            return $this->failed( '请先确认收货' );
        if ( in_array( $orderModel->order_status, [ 3, 4, 5 ] ) )
            return $this->failed( '此订单已提交过售后，请勿重复提交' );
        $orderModel->order_status = 3;
        $orderModel->save();
        $orderModel->hasOneAfterSales()->create( $input );
        return $this->message();
    }

    /**
     * 取消售后申请
     * @return mixed
     * @throws \Illuminate\Validation\ValidationException
     */
    public function cancelAfterSales()
    {
        $this->validate( $this->request, [
            'id' => 'required'
        ] );
        $input = $this->request->all();
        $user = Auth::user();
        $orderModel = GoodsOrder::getInstance()
            ->where( [ 'user_id' => $user->id, 'id' => $input['id'] ] )
            ->with( [
                'hasOneAfterSales'
            ] )
            ->firstOrFail();
        if ( $orderModel->order_status != 3 )
            return $this->failed( '订单状态错误' );
        if ( $orderModel->hasOneAfterSales->check_status > 0 )
            return $this->failed( '订单售后已处理' );
        $orderModel->order_status = 1;
        $orderModel->save();
        if ( $orderModel->hasOneAfterSales )
            $orderModel->hasOneAfterSales->delete();
        return $this->message();
    }


    /**
     * 确认收货
     * @throws \Illuminate\Validation\ValidationException
     */
    public function confirmOrder()
    {
        $this->validate( $this->request, [
            'id' => 'required'
        ] );
        $input = $this->request->all();
        $user = Auth::user();
        $orderModel = GoodsOrder::getInstance()->where( [ 'user_id' => $user->id, 'id' => $input['id'] ] )->firstOrFail();
        if ( $orderModel->pay_status != 1 )
            return $this->failed( '订单支付状态错误' );
        if ( $orderModel->order_status == 2 )
            return $this->failed( '订单已取消' );
        if ( $orderModel->trans_status == 4 )
            return $this->failed( '订单已收货 请勿重复操作' );
        if ( $orderModel->order_status > 4 )
            return $this->failed( '订单状态错误' );
        $orderModel->trans_status = 4;
        $orderModel->save();
        return $this->message();
    }


    /**
     * 未支付取消订单
     * @return mixed
     * @throws \Illuminate\Validation\ValidationException
     */
    public function cancelOrder()
    {
        $this->validate( $this->request, [
            'id' => 'required'
        ] );
        $input = $this->request->all();
        $user = Auth::user();
        $orderModel = GoodsOrder::getInstance()->where( [ 'user_id' => $user->id, 'id' => $input['id'] ] )->firstOrFail();
        //取消订单
        ServiceManager::getInstance()->GoodsOrderService( GoodsOrderService::class )->cancelOrder( $orderModel, 2 );
        return $this->message();
    }


    /**
     * 售后类型
     * @return mixed
     */
    public function getAfterSaleType()
    {
        $data = GoodsAfterSales::getInstance()->resonDesc;
        return $this->success( $data );
    }

}
