<?php

namespace App\Http\Controllers\App\V1\Message;

use App\Helpers\PayClass;
use App\Http\Controllers\App\BaseController;
use App\Models\Appointment;
use App\Models\Doctor;
use App\Models\DoctorComment;
use App\Models\DoctorSchedule;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class AppointmentController extends BaseController
{

    /**
     * 已预约列表
     * @return mixed
     * @throws \Illuminate\Validation\ValidationException
     */
    public function index()
    {
        $this->validate( $this->request, [
            'status' => 'required'
        ], [], [
            'status' => '状态'
        ] );
        $user = Auth::user();
        $input = $this->request->all();

        $where['order_status'] = $input['status'] ? 3 : 1;
        $where['pay_status'] = 1;
        $data = $user->hasManyAppoint()
            ->select( 'id', 'schedule_id', 'cate_desc', 'date', 'pay_amount', 'total_amount', 'start_time', 'end_time' )
            ->where( $where )
            ->paginate( $this->pageSize );
        return $this->success( $data );

    }

    /**
     * 添加预约
     * @return mixed
     * @throws \Illuminate\Validation\ValidationException
     */
    public function add()
    {
        $this->validate( $this->request, [
            'doctor_id'   => 'required',
            'schedule_id' => 'required',
            'method_pay'  => 'required',
            'cate_id'     => 'required',
            'cate_desc'   => 'required',
        ] );
        $input = $this->request->all();
        $doctor = Doctor::getInstance()->findOrFail( $input['doctor_id'] );
        if ( $doctor->type == 1 )
            return $this->failed( '改医生不支持线下预约' );
        $schedule = DoctorSchedule::getInstance()->findOrFail( $input['schedule_id'] );
        if ( $schedule->doctor_id != $input['doctor_id'] )
            return $this->failed( '数据不存在' );
        if ( $schedule->num <= 0 )
            return $this->failed( '暂无剩余号源' );

        $input['date'] = $schedule->date;
        $input['start_time'] = $schedule->start_time;
        $input['end_time'] = $schedule->end_time;
        $user = Auth::user();
        $result = $user->hasManyAppoint()->where( 'schedule_id', $input['schedule_id'] )->first();
        if ( $result )
            return $this->failed( '您已预约此号源' );

        $input['total_amount'] = $doctor->appoint_price;
        $input['order_number'] = PayClass::getInstance()->getOrderNumber( PayClass::ORDERAPPOINT, $user->id );

        DB::beginTransaction();
        try {
            $result = $user->hasManyAppoint()->create( $input );
            $schedule->increment( 'used_num' );
            //@todo
            $result->total_amount = 0.01;
            if ( $result ) {
                //调用支付流程
                $pays = new PayClass();
                $ch = $pays->pay( $result->order_number, $result->total_amount, '在线预约--' . $doctor->name, $doctor->name, $input['method_pay'] );
            }
            DB::commit();
            return $this->success( [ 'pay' => $ch, 'order_number' => $input['order_number'] ] );

        } catch ( \Exception $exception ) {
            DB::rollBack();
            return $this->failed( $exception->getMessage() );
        }
    }

    /**
     * 查询支付状态
     * @return mixed
     * @throws \Illuminate\Validation\ValidationException
     */
    public function getOrderPay()
    {
        $this->validate( $this->request, [
            'order_number' => 'required'
        ] );
        $input = $this->request->all();
        $user = Auth::user();
        $result = Appointment::getInstance()
            ->select( 'id', 'order_number', 'pay_status' )
            ->where( [ 'order_number' => $input['order_number'], 'user_id' => $user->id, 'order_status' => 1 ] )
            ->firstOrFail();
        return $this->success( $result );
    }


    /**
     * 预约详情
     * @return mixed
     * @throws \Illuminate\Validation\ValidationException
     */
    public function detail()
    {
        $this->validate( $this->request, [
            'id' => 'required'
        ] );
        $input = $this->request->all();
        $user = Auth::user();
        $result = Appointment::getInstance()
            ->select( 'id', 'schedule_id', 'cate_desc', 'doctor_id', 'total_amount', 'date', 'start_time', 'end_time' )
            ->with( [
                'belongsToDoctor:id,name',
            ] )
            ->where( [ 'id' => $input['id'], 'user_id' => $user->id, 'pay_status' => 1 ] )
            ->firstOrFail();
        return $this->success( $result );
    }

    /**
     * 完成/取消预约
     * @return mixed
     * @throws \Illuminate\Validation\ValidationException
     */
    public function change()
    {
        $this->validate( $this->request, [
            'id'     => 'required',
            'type'   => 'required|int|max:2',
            'source' => 'required_if:type,1|int',
        ] );
        $input = $this->request->all();
        $user = Auth::user();
        $result = Appointment::getInstance()
            ->where( [ 'id' => $input['id'], 'user_id' => $user->id, 'pay_status' => 1 ] )
            ->firstOrFail();
        if ( $result->order_status == 3 )
            return $this->failed( '该预约已完成，请勿重复提交' );
        if ( $result->order_status == 2 )
            return $this->failed( '该预约已取消，请勿重复提交' );
        DB::beginTransaction();
        try {
            if ( $input['type'] == 2 ) {
                //取消
                if ( $result->can_cancel != true )
                    return $this->failed( '您至少在预约时间前一天的16时之前取消，现在已超时无法取消' );
                $schedule = DoctorSchedule::getInstance()->findOrFail( $result->schedule_id );
                PayClass::getInstance()->refund( $result->order_number, $result->total_amount, $result->total_amount, $result->method_pay );
                $result->order_status = 2;
                $schedule->decrement( 'used_num' );
            } else {
                $result->order_status = 3;
                //医生评论
                DoctorComment::getInstance()->fill( [ 'doctor_id' => $result->doctor_id, 'user_id' => $user->id, 'score' => $input['source'] ] )->save();
            }
            $result->save();
            DB::commit();
            return $this->message();
        } catch ( \Exception $exception ) {
            DB::rollBack();
            return $this->failed( $exception->getMessage() );
        }
    }

}
