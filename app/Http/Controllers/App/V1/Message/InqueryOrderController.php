<?php

namespace App\Http\Controllers\App\V1\Message;

use App\Helpers\PayClass;
use App\Http\Controllers\App\BaseController;
use App\Models\Doctor;
use App\Models\InquiryOrder;
use App\Models\Message;
use App\Models\Suggest;
use App\Models\User;
use App\Models\UserDoctorRecord;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class InqueryOrderController extends BaseController
{


    /**
     * 问诊订单添加
     * @return mixed
     * @throws \Illuminate\Validation\ValidationException
     */
    public function add()
    {
        $this->validate( $this->request, [
            'type'       => 'required|int',
            'method_pay' => 'required|int|between:1,2',
            'doctor_id'  => 'required|int|exists:doctors,id',
        ] );
        $user = Auth::user();
        $input = $this->request->all();
        $doctor = Doctor::getInstance()->findOrFail( $input['doctor_id'] );
        if ( $doctor->ask_status != 1 )
            return $this->failed( '医生不在线，请刷新医生目录' );
        if ( $doctor->av_status == 1 )
            return $this->failed( '医生忙线中,请您稍后再付费拨打' );

        if ( $input['type'] == UserDoctorRecord::TEXT ) {
            $input['order_number'] = PayClass::getInstance()->getOrderNumber( PayClass::ORDERTEXT, $user->id );
            $price = $doctor->text_price;
        } elseif ( $input['type'] == UserDoctorRecord::AUDIO ) {
            $input['order_number'] = PayClass::getInstance()->getOrderNumber( PayClass::ORDERAUDICE, $user->id );
            $price = $doctor->audio_price;
        } elseif ( $input['type'] == UserDoctorRecord::VIDEO ) {
            $input['order_number'] = PayClass::getInstance()->getOrderNumber( PayClass::ORDERVIDEO, $user->id );
            $price = $doctor->video_price;
        } else
            return $this->failed( '参数不合法' );
        $price = 0.01;
        $input['pay_amount'] = $price;
        $input['total_amount'] = $price;
        $res = UserDoctorRecord::getInstance()->getRecord( $user->id, $input['doctor_id'], $input['type'] );
        if ( $res && $res->time > 0 )
            return $this->failed( '该医生您有尚未使用的问诊服务，请到"个人中心_我的问诊_进行中"查看使用' );
        try {
            DB::beginTransaction();
            if ( !$res )
                $res = $user->UserHasOne( UserDoctorRecord::class )->create( $input );
            $input['record_id'] = $res->id;
            $result = $user->hasManyInquiry()->create( $input );
            $result->pay_amount = 0.01;
            if ( $result && $input['total_amount'] != 0 ) {
                //调用支付流程
                $pays = new PayClass();
                $ch = $pays->pay( $result->order_number, $result->total_amount, '在线咨询--' . $doctor->name, $doctor->name, $input['method_pay'] );

            }
            DB::commit();
            return $this->success( [ 'pay' => $ch, 'order_number' => $input['order_number'] ] );

        } catch ( \Exception $e ) {
            DB::rollBack();
            return $this->failed( $e->getMessage() );
        }
    }

    /**
     * 查询支付状态
     * @return mixed
     * @throws \Illuminate\Validation\ValidationException
     */
    public function getOrderPay()
    {
        $this->validate( $this->request, [
            'order_number' => 'required'
        ] );
        $input = $this->request->all();
        $user = Auth::user();
        $result = InquiryOrder::getInstance()
            ->select( 'id', 'order_number', 'pay_status' )
            ->where( [ 'order_number' => $input['order_number'], 'user_id' => $user->id, 'order_status' => 1 ] )
            ->firstOrFail();
        return $this->success( $result );


    }

    /**
     *
     * 问诊记录
     * @return mixed
     * @throws \Illuminate\Validation\ValidationException
     */
    public function inquiryRecord()
    {
        $this->validate( $this->request, [
            'status' => 'required'
        ] );
        $user = Auth::user();
        $now = Carbon::now()->timestamp;
        $user_id = $user->id;
        $type = $this->request->get( 'status' );
        $where['user_id'] = $user_id;
        $where['has_pay'] = 1;
        $model = UserDoctorRecord::getInstance()->where( $where );
        if ( $type == 1 ) {
            $model->where( function ( $query ) use ( $now ) {
                $query->whereRaw( '(type = ? and time  >= ?)', [ 1, $now ] )
                    ->orWhereRaw( '((type = ? or type = ?) and time  > ?)', [ 2, 3, 0 ] );
            } );
        } else {
            $model->where( function ( $query ) use ( $now ) {
                $query->whereRaw( '(type = ? and time  < ?)', [ 1, $now ] )
                    ->orWhereRaw( '((type = ? or type = ?) and time  <= ?)', [ 2, 3, 0 ] );
            } );
        }
        $data = $model->with( [
            'belongsToDoctor'  => function ( $query ) {
                $query->select( 'id', 'name', 'head_pic', 'accid', 'yx_token', 'status', 'ask_status', 'deleted_at' )->withTrashed();
            },
            'hasLatestMessage' => function ( $query ) {
                $query->select( 'id', 'record_id', 'content', 'type' )->selectRaw( 'created_at as updated_at' );
            }
        ] )->withCount( [
            'hasManyMessage as unread_count' => function ( $query ) use ( $user ) {
                $query->where( [ 'from' => 1, 'status' => 0, 'to_id' => $user->id ] );
            } ] )->orderByDesc( 'chat_at' )->paginate( $this->pageSize );
        return $this->success( $data );
    }


    /**
     * 保存发送信息
     * @return mixed
     * @throws \Illuminate\Validation\ValidationException
     */
    public function saveMessage()
    {
        $this->validate( $this->request, [
            'content' => 'required',
            'to_id'   => 'required',
            'type'    => 'required',
        ] );
        $input = $this->request->all();
        $user = Auth::user();
        $record = UserDoctorRecord::getInstance()->getRecord( $user->id, $input['to_id'] );
        if ( !$record )
            return $this->failed( '记录不存在' );
        if ( $record->time <= 0 )
            return $this->failed( '问诊已过期' );
        if ( $record->type != 1 )
            return $this->failed( '仅支持图文问诊' );
        $doctor = Doctor::getInstance()->find( $record->doctor_id );
        if ( $doctor->status != 1 )
            return $this->failed( '医生账户未启用' );
        if ( $doctor->ask_status != 1 )
            return $this->failed( '医生当前未在线' );
        $input['user_id'] = $user->id;
        $input['from'] = 2;
        DB::beginTransaction();
        try {
            $res = $record->hasManyMessage()->create( $input );
            $record->chat_at = $res->created_at;
            $record->save();
            DB::commit();
            return $this->message();
        } catch ( \Exception $e ) {
            DB::rollBack();
            return $this->failed( $e->getMessage() );
        }
    }

    /**
     * 聊天记录
     * @return mixed
     * @throws \Illuminate\Validation\ValidationException
     */
    public function msgDetail()
    {
        $this->validate( $this->request, [
            'record_id' => 'required'
        ] );
        $input = $this->request->all();
        $user = Auth::user();
        $record = UserDoctorRecord::getInstance()->where( [ 'id' => $input['record_id'], 'user_id' => $user->id ] )->first();
        if ( !$record )
            return $this->failed( '数据不存在' );
        $msg = $record;
        $msg->hasManyMessage()->where( [ 'to_id' => $user->id, 'status' => 0, 'class' => 1 ] )->update( [ 'status' => 1 ] );
        $data = $record->hasManyMessage()->where( [ 'record_id' => $input['record_id'] ] )->paginate( $this->pageSize )->toArray();

        $user = User::getInstance()->select( 'id', 'name', 'head_pic' )->find( $record->user_id );
        $doctor = Doctor::getInstance()->select( 'id', 'name', 'head_pic' )->find( $record->doctor_id );
        foreach ( $data['data'] as $k => &$v ) {
            if ( $v['class'] == 1 )
                $v['belongs_to_send'] = $doctor;
            else
                $v['belongs_to_send'] = $user;
        }

        return $this->success( $data );
    }

    /**
     * 反馈意见
     * @return mixed
     * @throws \Illuminate\Validation\ValidationException
     */
    public function suggest()
    {
        $this->validate( $this->request, [
            'mobile'  => 'required_if:flag,2|max:20',
            'content' => 'required|max:255',
            'type'    => 'required_if:flag,1|max:500',
            'image'   => 'array|filled|max:6',
            'flag'    => 'required|max:255',
        ] );
        $input = $this->request->all();
        $user = Auth::user();
        $res = $user->UserHasMany( Suggest::class )->create( $input );
        if ( $res )
            return $this->message( '提交成功' );
        else
            return $this->failed( '提交失败' );
    }

    /**
     * 获取有效时长
     * @return mixed
     * @throws \Illuminate\Validation\ValidationException
     */
    public function getValidTime()
    {
        $this->validate( $this->request, [
            'id' => 'required',
        ] );
        $input = $this->request->all();
        $user = Auth::user();
        $where['user_id'] = $user->id;
        $where['doctor_id'] = $input['id'];
        $data = UserDoctorRecord::getInstance()->where( $where )->first();
        return $this->success( $data );
    }

    public function sendToDoctor()
    {
        $this->validate( $this->request, [
            'doctor_id' => 'required',
        ] );
        $input = $this->request->all();
        $tile = '您有一个音视频通话';
        $user = Auth::user();
        //$push = new Push(2);
        //$push->sendMsg($tile,'患者'.$user->name.'正在呼叫您......',[$input['doctor_id']]);
        return $this->message( '发送成功' );
    }

    public function finish()
    {
        $this->validate( $this->request, [
            'doctor_id' => 'required',
            'type'      => 'required',
            'source'    => 'required',
        ] );
        $input = $this->request->all();
        $user = Auth::user();
        $user->hasManyComment()->create( $input );
        $record = UserDoctorRecord::getInstance()->where( [ 'user_id' => $user->id, 'type' => $input['type'], 'doctor_id' => $input['doctor_id'] ] )->first();
        if ( $record ) {
            $record->time = 0;
            $record->save();
        }
        return $this->message();
    }
}
