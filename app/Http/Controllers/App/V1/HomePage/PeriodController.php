<?php

namespace App\Http\Controllers\App\V1\HomePage;

use App\Http\Controllers\App\BaseController;
use App\Models\KnowFile;
use App\Models\PeriodRecord;
use App\Models\PeriodSet;
use App\Models\User;
use App\Services\App\HomePage\PeriodService;
use App\Services\ServiceManager;
use Illuminate\Support\Facades\Auth;

class PeriodController extends BaseController{


    /**
     * @return PeriodService
     * @throws \ReflectionException
     */
    public static function getService(): PeriodService
    {
        return ServiceManager::getInstance()->PeriodService(PeriodService::class);
    }


    /**
     * 经期设置
     * @return mixed
     * @throws \Illuminate\Validation\ValidationException
     * @throws \ReflectionException
     */
    public function periodSet()
    {
        $this->validate($this->request,[
           'length' => 'required',
           'cycle' => 'required',
           'menstruation_date' => 'required',
           'is_predictive' => 'required',
        ]);
        $input = $this->request->all();
        $user = Auth::user();
        $input['user_id'] = $user->id;
        $res = PeriodSet::getInstance()->addOrUpdate($input);
        if ($input['is_predictive'] == 1) {
            $periodRecordInsert = self::getService()->formatPeriodRecordInsert($input);
            $periodRecordNextInsert = self::getService()->formatPeriodRecordNextInsert($input);
            PeriodRecord::getInstance()->addOrUpdate($periodRecordInsert, $periodRecordNextInsert);
        }
        User::getInstance()->where(['id' => $user->id])->update(['identity' => 1]);
        if (!$res) return $this->failed('设置失败');
        return $this->success();
    }


    /**
     * 经期预测详情
     * @return mixed
     * @throws \Illuminate\Validation\ValidationException
     */
    public function periodInfo()
    {
        $this->validate($this->request,[
            'date' => 'required',
        ]);
        $user = Auth::user();
        $userId = $user->id;
        $set = PeriodSet::getInstance()->where(['user_id' => $userId, 'is_predictive' => 1])->first();
        $result = ['ovulation_rate' => '', 'day' => '', 'type' => 0];
        if (empty($set)) return $this->success($result);
        // 当天在本月经期之前查询本月，当天在本月经期之后查询下个月，当天在本月经期中，提示经期中
        $date = $this->request->get('date') ?? date('Y-m-d');
        $month = date('Y-m');
        $record = PeriodRecord::getInstance()->where(['user_id' => $userId, 'month' => $month])->first();
        $periodStartDate = $record['period_reality_date'] ?? $record['period_predictive_date'];
        $periodEndDate = $record['period_reality_end_date'] ?? $record['period_predictive_end_date'];

        $dateTime = strtotime($date);
        $periodStartDateTime = strtotime($periodStartDate);
        $periodEndDateTime = strtotime($periodEndDate);

        if ($dateTime < $periodStartDateTime) {
            // 本月未到经期
            $result['type'] = 1;
            $result['day'] = ($periodStartDateTime - $dateTime) / 86400;
        } elseif ($dateTime > $periodEndDateTime) {
            // 本月经期已过,查询下月还有多少天
            $result['type'] = 2;
            $month = date('Y-m', strtotime('+1 month'));
            $record = PeriodRecord::getInstance()->where(['user_id' => $userId, 'month' => $month])->first();
            $periodStartDate = $record['period_reality_date'] ?? $record['period_predictive_date'];
            $periodStartDateTime = strtotime($periodStartDate);
            $result['day'] = ($periodStartDateTime - $dateTime) / 86400;
        } else {
           // 经期中
           $result['type'] = 3;
           $result['day'] = 0;
        }

        return $this->success($result);

    }
}
