<?php

namespace App\Http\Controllers\App\V1\HomePage;

use App\Http\Controllers\App\BaseController;
use App\Models\FetusInfo;
use App\Models\HotMomSet;
use App\Models\KnowFile;
use App\Models\ScaleRecord;
use Illuminate\Support\Facades\Auth;

class IndexController extends BaseController{


    /**
     * 今日知识
     * @return mixed
     * @throws \Illuminate\Validation\ValidationException
     */
    public function todayKnowledge()
    {
        $this->validate($this->request,[
            'type' => 'required',
        ]);
        $type = $this->request->get('type');
        $data = KnowFile::getInstance()->where('cate_id', $type)->paginate($this->pageSize);
        return $this->success($data);
    }


    /**
     * 最后一次量表结果
     * @return mixed
     * @throws \Illuminate\Validation\ValidationException
     */
    public function lastScaleResult()
    {
        $this->validate($this->request,[
            'type' => 'required',
        ]);
        $type = $this->request->get('type');
        $user = Auth::user();
        $data = ScaleRecord::getInstance()
            ->where(['user_id' => $user->id, 'type' => $type])
            ->orderBy('created_at', 'DESC')
            ->first(['id', 'scale_name', 'result', 'created_at', 'scale_id']);
        if (empty($data)) return $this->successJson();
        $time = round((time() - strtotime($data['created_at'])) / 3600);
        $data['last_time'] = $time;
        return $this->success($data);
    }


    public function test()
    {
        $res1 = FetusInfo::getInstance()->getTable();
        $res2 = HotMomSet::getInstance()->getTable();
        return $this->success([$res1, $res2]);
    }
}
