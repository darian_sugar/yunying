<?php

namespace App\Http\Controllers\App\V1\HomePage;

use App\Http\Controllers\App\BaseController;
use App\Models\HotMomSet;
use App\Models\MyBaby;
use App\Models\PeriodRecord;
use App\Models\PeriodSet;
use App\Models\User;
use App\Services\App\HomePage\HotMomService;
use App\Services\ServiceManager;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class HotMomController extends BaseController
{

    public static function getService(): HotMomService
    {
        return ServiceManager::getInstance()->HotMomService(HotMomService::class);
    }


    /**
     * 辣妈设置
     * @return mixed
     * @throws \Illuminate\Validation\ValidationException
     */
    public function hotMomSet()
    {
        $this->validate($this->request, [
            'bb_sex'            => 'required',
            'bb_birthday'       => 'required',
            'length'            => 'required',
            'cycle'             => 'required',
            'menstruation_date' => 'required',
            'is_predictive'     => 'required',
        ]);
        $input            = $this->request->all();
        $user             = Auth::user();
        $input['user_id'] = $user->id;
        try {
            DB::beginTransaction();
            HotMomSet::getInstance()->addAndUpdate($input);
            MyBaby::getInstance()->setAdd($input);
            PeriodSet::getInstance()->addOrUpdate($input);
            if ($input['is_predictive'] == 1) {
                $periodRecordInsert     = PeriodController::getService()->formatPeriodRecordInsert($input);
                $periodRecordNextInsert = PeriodController::getService()->formatPeriodRecordNextInsert($input);
                PeriodRecord::getInstance()->addOrUpdate($periodRecordInsert, $periodRecordNextInsert);
            }
            User::getInstance()->where(['id' => $user->id])->update(['identity' => 3]);
            DB::commit();
            return $this->success([]);
        } catch (\Exception $exception) {
            DB::rollBack();
            return $this->failed($exception->getMessage());
        }
    }


    /**
     * 我的宝宝列表
     * @return mixed
     */
    public function myBaby()
    {
        $user = Auth::user();
        $data = MyBaby::getInstance()->where(['user_id' => $user->id])->get();
        return $this->success($data);
    }


    /**
     * 编辑宝宝
     * @return mixed
     * @throws \Illuminate\Validation\ValidationException
     */
    public function editBaby()
    {
        $this->validate($this->request, [
            'bb_pic'      => 'required',
            'bb_sex'      => 'required',
            'bb_birthday' => 'required',
            'bb_nickname' => 'required',
            'bb_height'   => 'required',
            'bb_weight'   => 'required',
            'relation'    => 'required',
        ]);
        $input            = $this->request->all();
        $user             = Auth::user();
        $input['user_id'] = $user->id;
        $res              = MyBaby::getInstance()->addAndUpdate($input);
        if (!$res) $this->failed('编辑失败');
        return $this->success();
    }


    /**
     * 修改默认宝宝
     * @return mixed
     * @throws \Illuminate\Validation\ValidationException
     */
    public function defaultBaby()
    {
        $this->validate($this->request, [
            'bb_id' => 'required',
        ]);
        $bbId        = $this->request->post('bb_id');
        $user        = Auth::user();
        $userId      = $user->id;
        $myBaby = DB::table('my_baby')->where(['user_id' => $userId])
            ->get();
        if (empty($myBaby)) return $this->success();
        $myBaby = $myBaby->toArray();
        $babyArr     = array_column($myBaby, 'id', 'bb_default');
        $defaultBaby = $babyArr[1];
        if ($bbId == $defaultBaby) return $this->success();
        $multipleData = [
            ['id' => (int)$bbId, 'bb_default' => 1],
            ['id' => $defaultBaby, 'bb_default' => 2]
        ];
        $myBabyModel  = new MyBaby();
        $res          = $myBabyModel->updateBatch($multipleData);
        if (!$res) return $this->failed('失败');
        return $this->success();
    }


    /**
     * 删除宝宝
     * @return mixed
     * @throws \Illuminate\Validation\ValidationException
     */
    public function deleteBaby()
    {
        $this->validate($this->request, [
            'bb_id' => 'required',
        ]);
        $bbId        = $this->request->post('bb_id');
        $user        = Auth::user();
        $userId      = $user->id;
        $myBaby      = MyBaby::getInstance()->where(['user_id' => $userId, 'id' => $bbId])->first();
        if (empty($myBaby)) return $this->failed('删除宝宝失败，此宝宝不是你的');
        $res = MyBaby::getInstance()->where(['user_id' => $userId, 'id' => $bbId])->delete();
        if (!$res) return $this->failed('失败');
        return $this->success();
    }

}
