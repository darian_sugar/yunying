<?php

namespace App\Http\Controllers\App\V1\HomePage;

use App\Http\Controllers\App\BaseController;
use App\Models\FetusInfo;
use App\Models\KnowFile;
use App\Models\PeriodRecord;
use App\Models\PeriodSet;
use App\Models\PregnancySet;
use App\Models\User;
use App\Services\App\HomePage\PeriodService;
use App\Services\App\HomePage\PregnancyService;
use App\Services\ServiceManager;
use Illuminate\Support\Facades\Auth;

class PregnancyController extends BaseController{


    /**
     * @return PregnancyService
     * @throws \ReflectionException
     */
    public static function getService(): PregnancyService
    {
        return ServiceManager::getInstance()->PregnancyService(PregnancyService::class);
    }


    /**
     * 预产期设置
     * @return mixed
     * @throws \Illuminate\Validation\ValidationException
     * @throws \ReflectionException
     */
    public function pregnancySet()
    {
        $this->validate($this->request,[
           'due_date' => 'required',
           'last_period' => 'required',
        ]);
        $input = $this->request->all();
        $user = Auth::user();
        $input['user_id'] = $user->id;
        $last_period = $input['last_period'];
        $month = date('Y-m', strtotime("+9 month $last_period"));
        $input['predictive_due_date'] = $month;
        $res = PregnancySet::getInstance()->addOrUpdate($input);
        User::getInstance()->where(['id' => $user->id])->update(['identity' => 2]);
        if (!$res) return $this->failed('设置失败');
        return $this->success();
    }



    /**
     * 经期预测详情
     * @return mixed
     * @throws \Illuminate\Validation\ValidationException
     */
    public function pregnancyInfo()
    {
        $this->validate($this->request,[
            'date' => 'required',
        ]);
        $date = $this->request->get('date');
        $user = Auth::user();
        $userId = $user->id;
        $set = PregnancySet::getInstance()->where(['user_id' => $userId])->first();
        $result = ['pregnancy_date' => '', 'bb_height' => '', 'bb_weight' => ''];
        if (empty($set)) return $this->success($result);

        // 根据最后一次月经，计算怀孕时间，根据怀孕时间计算胎儿长度和重量
        $lastPeriod = $set['last_period'];
        $day = (strtotime($date) - strtotime($lastPeriod)) / 86400;
        $week = floor($day / 7);
        $day = $day % 7;
        $result['pregnancy_date'] = $week . '周' . $day . '天';
        $babyInfo = FetusInfo::getInstance()->where(['week' => $week])->first();
        $result['bb_height'] = $babyInfo['height'] ?? '';
        $result['bb_weight'] = $babyInfo['weight'] ?? '';
        return $this->success($result);
    }


    /**
     * 预产期说明
     * @return mixed
     */
    public function pregnancyDecipher()
    {
        $data = '预产期说明';
        return $this->success(['decipher' => $data]);
    }

}
