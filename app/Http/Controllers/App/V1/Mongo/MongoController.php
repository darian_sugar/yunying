<?php

namespace App\Http\Controllers\App\V1\Mongo;

use App\Http\Controllers\Controller;
use App\MongoModels\ExampleModel;
use App\MongoModels\Mongodb;
use App\MongoModels\Users;
use Illuminate\Support\Facades\DB;

class MongoController extends Controller
{

    /**
     * DB操作
     * @return mixed
     */
    public function mongoDB()
    {
        /**
         * 增
         */
        $connection = Mongodb::connectionMongodb('user');
        $res        = $connection->insert(['id' => 1]);
        return $this->success($res);

        /**
         * 删
         */
        $connection = Mongodb::connectionMongodb('xxx表名');
        $result     = $connection->where('id', $id)->delete();

        //改
        $connection = Mongodb::connectionMongodb('xxx表名');
        $result     = $connection->where('id', $id)->update($data);

        /**
         * 查
         */
        $connection = Mongodb::connectionMongodb('xxx表名');

        //不分页
        $result = $connection->get();
        //如果用了软删除，此处要加上
        $result = $connection->whereNull('deleted_at')->get();


        //分页
        //可以默认分页数，或者根据传入data的指定页数进行分页
        if (isset($data['pageSize'])) {
            $pageSize = $data['pageSize'];
        } else {
            $pageSize = 10;
        }
        $results = $connection->paginate($pageSize);

        $connection = Mongodb::connectionMongodb('xxx表名');
        $result     = $connection->where('id', $id)->get();
    }


    /**
     * Model操作
     * @return mixed
     */
    public function mongoModel()
    {
        //增
        $result = ExampleModel::create(['title' => '标题']);
        return $this->success($result);

        //删
        $result = ExampleModel::where('id', $id)->delete();

        //改
        $result = ExampleModel::where('id', $id)->update($data);

        //查
        $result = ExampleModel::all();
        $result = ExampleModel::where('id', $id)->get();
    }


    /**
     * DB添加
     * @return mixed
     */
    public function addDB()
    {
        DB::connection('mongodb')
            ->collection('users')               //选择使用users集合
            ->insert([                          //插入数据
                'name' => 'tom',
                'age'  => 18
            ]);
        $res = DB::connection('mongodb')->collection('users')->get();   //查询所有数据
        return $this->success($res);
    }


    /**
     * 列表
     * @return mixed
     */
    public function mongoUser()
    {
        $users = Users::all();
        return $this->success($users);
    }


    /**
     * 模型添加
     */
    public function addModel()
    {
        Users::create([                      //插入数据
                'id'    => 1,
                'sex'   => 1,
                'name'  => 'tom',
                'phone' => 110]
        );
        return $this->success(Users::all());       //查询并打印数据
    }


}
