<?php

namespace App\Http\Controllers\App\V1\Tool;

use App\Http\Controllers\App\BaseController;
use App\Models\Question;
use App\Http\Controllers\Controller;
use App\Models\QuestionComment;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\ValidationException;

class QuestionController extends BaseController
{
    /**
     * 问答列表
     *
     * @return mixed
     * @throws ValidationException
     */
    public function index()
    {
        $this->validate( $this->request, [
        ], [], [
            'p_id'     => '所属类别',
            'key_word' => '搜索关键字'
        ] );
        $input = $this->request->all();
        $result = Question::getInstance()
            ->when( $input['p_id'], function ( $where, $value ) {
                $where->where( 'p_id', $value );
            } )
            ->when( $input['key_word'], function ( $where, $value ) {
                $where->where( 'content', 'like', '%' . $value . '%' );
            } )
            ->with( [
                'belongsToUser:id,nickname,head_pic,identity',
                'latestComment.belongsToUser:id,nickname,head_pic',
                'latestCommentUser.belongsToUser:id,nickname,head_pic',
            ] )
            ->withCount( [
                'hasManyComments'
            ] )
            ->latest()
            ->paginate( $this->pageSize );
        return $this->success( $result );
    }

    /**
     * 发表问答
     *
     * @return mixed
     * @throws ValidationException
     */
    public function add()
    {
        $this->validate( $this->request, [
            'p_id'    => 'required|int',
            'content' => 'required|max:200'
        ], [], [
            'p_id'    => '所属类别',
            'content' => '发布内容',
        ] );
        $input = $this->request->all();
        $user = Auth::user();
        $input['user_id'] = $user->id;
        Question::getInstance()->fill( $input )->save();
        return $this->message();
    }

    /**
     * 发表问答
     *
     * @return mixed
     * @throws ValidationException
     */
    public function comment()
    {
        $this->validate( $this->request, [
            'q_id'    => 'required|int',
            'comment' => 'required|max:200'
        ], [], [
            'q_id'    => '问答ID',
            'comment' => '评论内容',
        ] );
        $input = $this->request->all();
        $user = Auth::user();
        $input['user_id'] = $user->id;
        $model = Question::getInstance()->findOrFail( $input['q_id'] );
        $model->hasManyComments()->create( $input );
        return $this->message();
    }

    public function commentList()
    {
        $this->validate( $this->request, [
            'q_id' => 'required'
        ] );
        $input = $this->request->all();
        $model = Question::getInstance()->findOrFail( $input['q_id'] );
        $result = $model->hasManyComments()->with( [
            'belongsToUser:id,nickname,head_pic,identity',
        ] )
            ->latest()
            ->paginate( $this->pageSize );
        return $this->success( $result );
    }

    public function myQuestion()
    {
        $user = Auth::user();
        $result = Question::getInstance()
            ->where( 'user_id', $user->id )
            ->with( [
                'latestComment.belongsToUser:id,nickname,head_pic,identity',
            ] )
            ->withCount( [
                'hasManyComments'
            ] )
            ->latest()
            ->paginate( $this->pageSize );
        return $this->success( $result );
    }


    public function myReply()
    {
        $user = Auth::user();
        $question = Question::getInstance()
            ->where( 'user_id', $user->id )
            ->pluck( 'id' );
        $result = QuestionComment::getInstance()
            ->whereIn( 'q_id', $question )
            ->with( [
                'belongsToUser:id,nickname,head_pic,identity',
                'belongsToQuestion:id,content',
            ] )
            ->latest()
            ->paginate( $this->pageSize );
        return $this->success( $result );
    }
}
