<?php

namespace App\Http\Controllers\App\V1\Wiki;

use App\Helpers\PayClass;
use App\Http\Controllers\App\BaseController;
use App\Models\KnowCate;
use App\Models\KnowFile;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class CateController extends BaseController
{
    public function tree()
    {
        $type = $this->request->get( 'type', 1 );
        $result = KnowCate::getInstance()->select( 'id', 'p_id', 'channel_id', 'name' )->where( [ 'status' => 1, 'type' => $type ] )->get()->toArray();
        $tree = [];
        listToTree( $result, 'id', 'channel_id', 'children', 0, $tree );
        $tree = collect( $tree )->groupBy( 'p_id' )->toArray();
        $data = collect( KnowCate::BASE_CATE )->transform( function ( $item, $key ) use ( $tree ) {
            $newItem = [
                'id'       => $key,
                'name'     => $item,
                'children' => $tree[ $key ] ?? [],
            ];
            return $newItem;
        })->values();
        return $this->success($data);
    }

}
