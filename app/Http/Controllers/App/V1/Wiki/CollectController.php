<?php

namespace App\Http\Controllers\App\V1\Wiki;

use App\Http\Controllers\App\BaseController;
use Illuminate\Support\Facades\Auth;

class CollectController extends BaseController
{

    public function file()
    {
        $user = Auth::user();
        $data = $user->hasManyCollect()
            ->select( 'id', 'c_id', 'type' )
            ->where( 'type', 1 )
            ->with( [
                'belongsToKnowFile:id,title,image,author_id,read,updated_at',
                'belongsToKnowFile.belongsToCate:id,name,p_id,channel_id',
                'belongsToKnowFile.belongsToAuthor:id,name,t_id',
            ] )
            ->latest( 'id' )
            ->paginate( $this->pageSize )->toArray();
        $data['data'] = collect( $data['data'] )->pluck( 'belongs_to_know_file' )->values();
        return $this->success( $data );
    }

    public function course()
    {
        $user = Auth::user();
        $data = $user->hasManyCollect()
            ->select( 'id', 'c_id', 'type' )
            ->where( 'type', 2 )
            ->with( [
                'belongsToKnowCourse:id,cate_id,image,title,author_id,price,read,duration,price',
                'belongsToKnowCourse.belongsToCate:id,name,p_id,channel_id',
            ] )
            ->latest( 'id' )
            ->paginate( $this->pageSize )->toArray();
        $data['data'] = collect( $data['data'] )->pluck( 'belongs_to_know_course' )->values();

        return $this->success( $data );
    }

    public function add()
    {
        $this->validate( $this->request, [
            'id'   => 'required',
            'type' => 'required|min:1|max:2',
        ] );
        $input = $this->request->all();
        $user = Auth::user();
        $data['c_id'] = $input['id'];
        $data['type'] = $input['type'];
        $data['from'] = 1;
        $res = $user->hasManyCollect()->where( $data )->first();
        if ( $res ) {
            $res->delete();
            return $this->message( '取消收藏成功' );
        } else {
            $user->hasManyCollect()->create( $data );
            return $this->message( '收藏成功' );
        }
    }

}
