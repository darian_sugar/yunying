<?php

namespace App\Http\Controllers\App\V1\Wiki;

use App\Helpers\PayClass;
use App\Http\Controllers\App\BaseController;
use App\Models\KnowCate;
use App\Models\KnowFile;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class KnowledgeController extends BaseController
{
    public function index()
    {
        $this->validate( $this->request, [
            'type' => 'required',
        ], [], [
            'type' => '文章类型'
        ] );
        $input = $this->request->all();
        $where = [ 'status' => 1, 'check_status' => 1 ];
        $where['type'] = $input['type'];
        $user = Auth::user();
        $model = KnowFile::getInstance()
            ->select( 'id', 'title', 'image', 'price', 'read', 'cate_id', 'author_id', 'created_at' )
            ->with( [
                'belongsToCate:id,name,p_id,channel_id',
                'belongsToAuthor:id,name,t_id,head_pic',
            ] )
            ->when( $input['key_word'], function ( $where, $value ) {
                $where->where( 'title', 'like', '%' . $value . '%' );
            } )
            ->when( $input['cate_id'], function ( $where, $value ) {
                $where->where( 'cate_id', $value );
            } )
            ->when( $input['is_top'], function ( $where, $value ) {
                $where->where( 'is_top', $value );
            } )
            ->withCount( [
                'hasOneCollect as is_collect' => function ( $query ) use ( $user ) {
                    $query->where( 'u_id', $user->id );
                },
                'hasManyDown',
                'hasManyDown as is_down'      => function ( $query ) use ( $user ) {
                    $query->where( [ 'u_id' => $user->id, 'from' => 1, 'pay_status' => 1 ] );
                },
            ] )
            ->where( $where );

        $data = $model->latest( 'id' )->paginate( $this->pageSize );

        return $this->success( $data );

    }

    public function detail()
    {
        $this->validate( $this->request, [
            'id' => 'required|exists:know_files'
        ] );
        $id = $this->request->get( 'id' );
        $user = Auth::user();
        $data = KnowFile::getInstance()
            ->select( 'id', 'content', 'read', 'author_id', 'cate_id' )
            ->with( [
                'belongsToAuthor:id,name,t_id,head_pic',
                'belongsToCate:id,p_id,channel_id,name'
            ] )
            ->withCount( [
                'hasOneCollect as is_collect' => function ( $query ) use ( $user ) {
                    $query->where( 'u_id', $user->id );
                },
            ] )
            ->findOrFail( $id );
        $data->increment( 'read' );
        return $this->success( $data );

    }

    /**
     * 收藏
     * @return mixed
     * @throws \Illuminate\Validation\ValidationException
     */
    public function collect()
    {
        $this->validate( $this->request, [
            'id'   => 'required',
            'type' => 'required|min:1|max:2',
        ] );
        $input = $this->request->all();
        $user = Auth::user();
        $data['c_id'] = $input['id'];
        $data['type'] = $input['type'];
        $data['from'] = 1;
        $res = $user->hasManyCollect()->where( $data )->first();
        if ( $res ) {
            $res->delete();
            return $this->message( '取消收藏成功' );
        } else {
            $user->hasManyCollect()->create( $data );
            return $this->message( '收藏成功' );
        }

    }

    public function getCate()
    {
        $data = KnowCate::getInstance()->select( 'id', 'name' )->where( [ 'status' => 1 ] )->paginate( $this->pageSize );
        return $this->success( $data );

    }

    /**
     * 查询
     * @throws \Illuminate\Validation\ValidationException
     */
    public function search()
    {
        $this->validate( $this->request, [
            'keyword' => 'required',
            'type'    => 'required|int|between:1,2',
        ] );
        $key = $this->request->get( 'keyword' );
        $type = $this->request->get( 'type' );
        $keys = explode( ' ', $key );
        $data = KnowFile::getInstance()->mergeSearch( $keys, $type )->paginate( $this->pageSize );
        return $this->success( $data );
    }

    /**
     * 热门文章
     */
    public function hotList()
    {

        $data = KnowFile::getInstance()->select( 'id', 'image', 'from' )->where( [ 'status' => 1, 'is_top' => 2, 'check_status' => 1 ] )->get();
        return $this->success( $data );
    }

    /**
     * 下载
     * @throws \Illuminate\Validation\ValidationException
     */
    public function downFile()
    {
        $this->validate( $this->request, [
            'id'         => 'required',
            'method_pay' => 'filled',
        ] );
        $id = $this->request->get( 'id' );
        $method_pay = $this->request->get( 'method_pay', 0 );

        $user = Auth::user();
        $data = KnowFile::getInstance()->findOrFail( $id );
        $payDown = $data->hasManyDown()->where( [ 'u_id' => $user->id, 'from' => 1, 'pay_status' => 1 ] )->first();
        if ( $payDown )
            return $this->message();
        $pays = new PayClass();
        $input['method_pay'] = $method_pay;
        $input['u_id'] = $user->id;
        $input['from'] = 2;
        $input['type'] = 1;
        $input['order_number'] = $pays->getOrderNumber( $pays::ORDERKNOW, $user->id );
        $result = $data->hasManyDown()->create( $input );
        if ( $data->price > 0 ) {
            $result->pay_amount = $data->price;
            $result->save();
            if ( $result->method_pay == 1 ) {
                $ch = $pays->pay( $result->order_number, $input['total_amount'], '购买视频' . $data->title, '购买视频', 1 );
            } elseif ( $result->method_pay == 2 ) {
                //微信以分为单位
                $total = $input['total_amount'] * 100;
                $ch = $pays->pay( $result->order_number, $total, '购买视频' . $data->title, '购买视频', 2 );
            }
            return $this->success( $ch );

        } else {
            $result->pay_status = 1;
            $result->save();
            return $this->message();
        }
    }


    public function downSource()
    {
        $this->validate( $this->request, [
            'id'   => 'required',
            'type' => 'required|int',
        ] );
        $id = $this->request->get( 'id' );
        $type = $this->request->get( 'type' );
        $user = Auth::user();
        $input['type'] = $type;
        $input['course_id'] = $id;
        $input['from'] = 1;
        $input['pay_amount'] = 0;
        $input['order_number'] = PayClass::getInstance()->getOrderNumber( PayClass::ORDERKNOW, $user->id );
        $res = $user->hasManyDown()->create( $input );
        $res->pay_status = 1;
        $res->save();
        return $this->message();
    }

}
