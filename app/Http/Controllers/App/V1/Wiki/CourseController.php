<?php

namespace App\Http\Controllers\App\V1\Wiki;

use App\Helpers\PayClass;
use App\Http\Controllers\App\BaseController;
use App\Models\KnowCourse;
use Illuminate\Support\Facades\Auth;

class CourseController extends BaseController
{
    //

    /**
     * 列表
     * @return mixed
     * @throws \Illuminate\Validation\ValidationException
     */
    public function index()
    {
        $this->validate( $this->request, [
            'type' => 'required',
        ], [], [
            'type' => '文章类型'
        ] );
        $input = $this->request->all();
        $user = Auth::user();
        $where['status'] = 1;
        $where['check_status'] = 1;
        $where['type'] = $input['type'];
        $data = KnowCourse::getInstance()
            ->select( 'id', 'cate_id', 'title', 'image', 'content', 'author_id', 'read', 'duration', 'created_at' )
            ->with( [
                'belongsToAuthor:id,t_id,name,head_pic',
                'belongsToCate:id,name,p_id,channel_id',
            ] )
            ->when( $input['key_word'], function ( $where, $value ) {
                $where->where( 'title', 'like', '%' . $value . '%' );
            } )
            ->when( $input['cate_id'], function ( $where, $value ) {
                $where->where( 'cate_id', $value );
            } )
            ->withCount( [
                'hasOneCollect as is_collect' => function ( $query ) use ( $user ) {
                    $query->where( 'u_id', $user->id );
                } ] )->where( $where )->orderByDesc( 'id' )->paginate( $this->pageSize )->toArray();
        //foreach( $data['data'] as $k => &$v ){
        //    $v['avg_rating'] && $v['grade'] = $v['avg_rating'][0]['aggregate'];
        //    unset($data['data'][$k]['avg_rating']);
        //}
        return $this->success( $data );

    }

    /**
     * 课程详情
     * @return mixed
     * @throws \Illuminate\Validation\ValidationException
     */
    public function detail()
    {
        $this->validate( $this->request, [
            'id' => 'required|int',
        ] );
        $where = $this->request->all();
        $user = Auth::user();
        $data = KnowCourse::getInstance()->select( 'id', 'image', 'title', 'content' )->where( 'id', $where['id'] )
            ->first();

        if ( !$data )
            return $this->failed( '课程不存在' );
        $data->increment( 'read' );
        $data = $data->toArray();
        return $this->success( $data );
    }


    /**
     * 下载历史
     * @throws \Illuminate\Validation\ValidationException
     */
    public function downList()
    {
        $user = Auth::user();
        $data = KnowCourse::getInstance()
            ->with( [
                'belongsToDoctor:id,d_id,t_id,h_id,name,head_pic',
                'belongsToDoctor.belongsToDepartment:id,name',
                'belongsToDoctor.belongsToHospital:id,name'
            ] )->withCount( [
                'hasManyDown as is_down'      => function ( $query ) use ( $user ) {
                    $query->where( [ 'u_id' => $user->id, 'from' => 1, 'pay_status' => 1 ] );
                },
                'hasManyComment',
                'hasOneCollect as is_collect' => function ( $query ) use ( $user ) {
                    $query->where( 'u_id', $user->id );
                } ] )
            ->whereHas( 'hasManyDown', function ( $query ) use ( $user ) {
                $query->where( [ 'u_id' => $user->id, 'from' => 1, 'pay_status' => 1 ] );
            } )
            ->paginate( $this->pageSize );
        return $this->success( $data );
    }

    /**
     * 下载
     * @throws \Illuminate\Validation\ValidationException
     */
    public function down()
    {
        $this->validate( $this->request, [
            'id'         => 'required',
            'method_pay' => 'filled',
        ] );
        $id = $this->request->get( 'id' );
        $method_pay = $this->request->get( 'method_pay', 0 );
        $user = Auth::user();
        $data = KnowCourse::getInstance()->findOrFail( $id );
        $product_id = $data->product_id;
        $payDown = $data->hasManyDown()->where( [ 'u_id' => $user->id, 'from' => 1, 'pay_status' => 1 ] )->first();
        $pays = new PayClass();
        $input['method_pay'] = $method_pay;
        $input['u_id'] = $user->id;
        $input['from'] = 1;
        $input['type'] = 2;
        $input['order_number'] = $pays->getOrderNumber( $pays::ORDERKNOW, $user->id );

        if ( isset( $input['product_id'] ) ) {
            unset( $input['product_id'] );
        }
        $result = $data->hasManyDown()->create( $input );
        if ( !$payDown && $data->price > 0 ) {
            $result->pay_amount = $data->price;
            $result->save();
            if ( $result->method_pay == 1 ) {
                $ch = $pays->pay( $result->order_number, $data->price, '购买视频' . $data->title, '购买视频', 1 );
            } elseif ( $result->method_pay == 2 ) {
                //微信以分为单位
                $total = $data->price * 100;
                $ch = $pays->pay( $result->order_number, $total, '购买视频' . $data->title, '购买视频', 2 );
            } elseif ( $result->method_pay == 3 ) {
                $ch = [
                    'produce_id'   => $product_id,
                    'order_number' => $input['order_number'],
                    'price'        => $data->price * 100
                ];
            }
            return $this->success( $ch );

        } else {
            $result->pay_status = 1;
            $result->save();
            return $this->message();
        }

    }

}
