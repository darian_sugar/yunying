<?php

namespace App\Http\Controllers\App\V1\User;

use App\Helpers\Sms;
use App\Helpers\YunXin\YunXin;
use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\UserThirdAccount;
use App\Services\Common\CommonService;
use Illuminate\Support\Facades\DB;
use Zhuzhichao\IpLocationZh\Ip;

class WxLoginController extends Controller
{

    /**
     * 由code获取openid,access_token
     * @return mixed
     */
    public function getAccessToken($code)
    {
        $url      = 'https://api.weixin.qq.com/sns/oauth2/access_token?appid=wx4fe9d039da147d1b&secret=eebff446322d01ccb5a71024c30ce218&code=' . $code . '&grant_type=authorization_code';
//        $json_obj = CommonService::getCurl($url);
//      此时已获得微信的openid
        $json_obj['openid'] = '123456';
        $json_obj['access_token'] = '123456';
        return $json_obj;
    }


    /**
     * access_token和openid获取用户信息
     * @param $accessToken
     * @param $openid
     * @param $type
     * @return bool|mixed
     */
    public function getThirdInfo($accessToken, $openid)
    {
        $url = 'https://api.weixin.qq.com/sns/userinfo?access_token=' . $accessToken . '&openid=' . $openid;
//        $json_obj = CommonService::getCurl($url);
//        if (!isset($json_obj['errcode'])) return $json_obj;
        $test = '{
        "openid":"OPENID",
        "nickname":"NICKNAME",
        "sex":1,
        "province":"PROVINCE",
        "city":"CITY",
        "country":"COUNTRY",
        "headimgurl": "http://wx.qlogo.cn/mmopen/g3MonUZtNHkdmzicIlibx6iaFqAc56vxLSUfpb6n5WKSYVY0ChQKkiaJSgQ1dZuTOgvLLrhJbERQQ4eMsv84eavHiaiceqxibJxCfHe/0",
        "privilege":[
        "PRIVILEGE1",
        "PRIVILEGE2"
        ],
        "unionid": " o6_bmasdasdsad6_2sgVt7hMZOPfL"
        }';
        $test = json_decode($test, true);
        return $test;
        return $this->errorJson([], '获取微信用户信息失败');
    }


    /**
     * 微信登陆
     * @return mixed
     * @throws \Illuminate\Validation\ValidationException
     */
    public function wxLogin()
    {
        $this->validate($this->request, [
            'wx_code' => 'required'
        ]);
        $wxCode      = $this->request->get('wx_code');
        $wxInfo      = $this->getAccessToken($wxCode);
        $accessToken = $wxInfo['access_token'];
        $openid      = $wxInfo['openid'];
        $thirdInfo   = $this->getThirdInfo($accessToken, $openid);

        //查询此次登陆用户是否首次登陆，不是首次登陆返回信息，是首次登陆注册新用户
        $useThirdInfo = UserThirdAccount::getInstance()
            ->where(['openid' => $openid, 'third_type' => 2])
            ->first();
        if (!empty($useThirdInfo)) {
            //非首次登陆，刷新token，返回用户信息
            $userInfo = User::getInstance()->find($useThirdInfo->user_id);
            $res      = $userInfo->saveToken();
            $userInfo->makeVisible('api_token');
            if (!$res) return $this->failed('登录失败');
            return $this->success($userInfo);
        }
        //获取登录地址
        $ip    = request()->getClientIp();
        $areas = Ip::find($ip);
        try {
            DB::beginTransaction();
            $user = new User();
            // 无手机号直接注册新帐号
            $createUser = $this->createUser($user, $thirdInfo, $ip, $areas);
            $userId     = $createUser->id;
            $userInfo   = User::getInstance()->find($userId);

            //将第三方微信获取用户信息入库
            $insertThird = [
                'user_id'    => $userId,
                'third_key'  => $openid,
                'third_type' => 2,
                'openid'     => $openid,
                'nickname'   => $thirdInfo['nickname'],
                'sex'        => $thirdInfo['sex'],
                'province'   => $thirdInfo['province'],
                'city'       => $thirdInfo['city'],
                'country'    => $thirdInfo['country'],
                'headimgurl' => $thirdInfo['headimgurl'],
                'privilege'  => json_encode($thirdInfo['privilege']),
                'unionid'    => $thirdInfo['unionid'],
            ];
            $newThird    = UserThirdAccount::getInstance()->insert($insertThird);
            if (!$newThird) {
                DB::rollBack();
                return $this->failed('登录失败');
            }
            DB::commit();
            return $this->success($userInfo);
        } catch (\Exception $e) {
            DB::rollBack();
            return $this->failed($e->getMessage());
        }
    }


    public function createUser($user, $qqInfo, $ip, $areas)
    {
        //无手机号，创建新用户，关联微信账号，
        $user->head_pic = $qqInfo['headimgurl'];
        $user->nickname = $qqInfo['nickname'];
        $user->sex      = $qqInfo['sex'];
        $user->save();
        //注册网易云信
        $result          = YunXin::getInstance()->user->create('user_' . $user->id . '_' . uniqid(), $user->nickname, $user->head_pic);
        $user->yx_token  = $result['token'];
        $user->accid     = $result['accid'];
        $user->ip        = $ip;
        $user->api_token = create_token(1);
        $user->area      = implode('-', $areas);
        $user->save();
        return $user;
    }


    public function wxLoginBackUp()
    {
        $this->validate($this->request, [
            'access_token' => 'required',
            'openid'       => 'required'
        ]);
        $accessToken = $this->request->get('access_token');
        $openid      = $this->request->get('openid');
        $mobile      = $this->request->get('mobile');
        $code        = $this->request->get('code');
        $thirdInfo   = $this->getThirdInfo($accessToken, $openid);

        if (!empty($mobile)) Sms::getInstance()->checkCode(Sms::REDIS_LOGIN . $mobile, $code);


        //查询此次登陆用户是否首次登陆，不是首次登陆返回信息，是首次登陆注册新用户
        $userInfo = UserThirdAccount::where('openid', $openid)
            ->leftJoin('users', 'users.id', '=', 'user_third_accounts.user_id')
            ->where('third_type', 2)->first();
        if (!empty($userInfo)) {
            //非首次登陆，刷新token和用户在线状态1，返回用户信息
            $res = $userInfo->changeTokenByUserId($userInfo->user_id);
            if (!$res) return $this->failed('登录失败');
            return $this->success($userInfo);
        }
        $ip = request()->getClientIp();
        //获取登录地址
        $areas = Ip::find($ip);


        try {
            DB::beginTransaction();
            $user = new User();
            // 无手机号直接注册新帐号
            if (empty($mobile)) {
                $userInfo = $this->createUser($user, $thirdInfo, $ip, $areas);
            }

            //有手机号检查手机号如果有对应用户直接绑定，如果无对应用户注册新用户并绑定
            $mobileUserInfo = $user->getByMobile($mobile);
            if (empty($mobileUserInfo)) {
                $userInfo = $this->createUser($user, $thirdInfo, $ip, $areas);
            }

            //手机号有对应用户,先查看手机是否绑定第三方QQ号，绑定则拒绝绑定，未绑定则绑定
            $userId       = $mobileUserInfo->id;
            $thirdAccount = UserThirdAccount::getInstance()->where(['user_id' => $userId, 'third_type' => 2])->first();
            if (!empty($thirdAccount)) {
                DB::rollBack();
                return $this->failed('手机号已绑定QQ账号');
            }

            //将第三方微信获取用户信息入库
            $insertThird = [
                'uid'        => $userId,
                'third_key'  => $thirdInfo['openid'],
                'third_type' => 2,
                'openid'     => $thirdInfo['openid'],
                'nickname'   => $thirdInfo['nickname'],
                'sex'        => $thirdInfo['sex'],
                'province'   => $thirdInfo['province'],
                'city'       => $thirdInfo['city'],
                'country'    => $thirdInfo['country'],
                'headimgurl' => $thirdInfo['headimgurl'],
                'privilege'  => json_encode($thirdInfo['privilege']),
                'unionid'    => $thirdInfo['unionid'],
            ];
            $newThird    = UserThirdAccount::getInstance()->insert($insertThird);
            if (!$newThird) {
                DB::rollBack();
                return $this->failed('登录失败');
            }
            DB::commit();
            return $this->success($userInfo);
        } catch (\Exception $e) {
            DB::rollBack();
            return $this->failed($e->getMessage());
        }
    }

}
