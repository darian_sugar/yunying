<?php

namespace App\Http\Controllers\App\V1\User;

use App\Helpers\Sms;
use App\Helpers\YunXin\YunXin;
use App\Http\Controllers\App\BaseController;
use App\Http\Controllers\Controller;
use App\Models\PushMessage;
use App\Models\Scale;
use App\Models\ScaleAnswer;
use App\Models\ScaleQuestion;
use App\Models\ScaleRecord;
use App\Models\ScaleRecordDetail;
use App\Models\Suggest;
use App\Models\SystemMsg;
use App\Models\User;
use App\Models\UserThirdAccount;
use App\Services\App\User\UserIndexService;
use App\Services\App\User\LoginService;
use App\Services\Common\CheckService;
use App\Services\ServiceManager;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use phpDocumentor\Reflection\Types\Self_;

class IndexController extends BaseController
{

    public function getService(): UserIndexService
    {
        return ServiceManager::getInstance()->UserIndexService(UserIndexService::class);
    }

    public function add()
    {
        $input = $this->request->all();
        $res   = User::getInstance()->insert($input);
        if (!$res) return $this->failed('添加用户失败');
        return $this->success([]);
    }

    /**
     * 个人资料
     * @return mixed
     */
    public function userInfo()
    {
        $user = Auth::user();
        $info = $user->getUserInfo($user->id);
        if (empty($info)) return $this->failed('获取信息失败');
        return $this->success($info);

    }

    /**
     * 退出登录
     * @return mixed
     */
    public function loginOut()
    {
        try {
            $user            = Auth::user();
            $user->api_token = '';
            $user->save();
            return $this->success('退出成功');
        } catch (\Exception $e) {
            return $this->failed($e->getMessage());
        }
    }


    /**
     * 修改个人信息
     * @return mixed
     */
    public function updateUserInfo()
    {
        $this->validate($this->request, [
            'sex' => 'between:1,2',
        ]);
        try {
            $data = $this->request->all();
            foreach ($data as $k => $v) {
                if ($v === null) {
                    unset($data[$k]);
                }
            }
            $user = Auth::user();
            DB::beginTransaction();
            $res = $user->fill($data)->save();
            //网易云信修改信息
            $yunxin = YunXin::getInstance()->User->updateUserInfo($user->accid, $user->nicename, $user->head_pic);
            DB::commit();
            return $this->message('修改成功');
        } catch (\Exception $e) {
            DB::rollBack();
            return $this->failed($e->getMessage());
        }
    }


    /**
     * 根据就密码修改密码
     * @return mixed
     * @throws \Illuminate\Validation\ValidationException
     */
    public function updatePwd()
    {
        $input = $this->request->all();
        $this->validate($this->request, [
            'old_password'          => 'required',
            'password'              => 'required|string|min:6|max:16|confirmed',
            'password_confirmation' => 'required|same:password',
        ]);
        $user   = Auth::user();
        $userId = $user->id;
        $pwd    = $input['password'];
        CheckService::checkPassword($pwd);
        $oldPassword = $input['old_password'];
        $newPwd      = Hash::make($pwd) ?? '';
        $oldPwd      = DB::table('users')->where(['id' => $userId])->first(['password']);
        $oldPwd      = $oldPwd->password;
        $check       = Hash::check($oldPassword, $oldPwd);
        if (!$check) return $this->failed('旧密码不正确');
        $userModel = new User();
        $res       = $userModel->where(['id' => $userId])->update(['password' => $newPwd]);
        if ($res) return $this->success();
        return $this->failed('修改密码失败');
    }


    /**
     * 修改手机号
     * @return mixed
     */
    public function updateUserMobile()
    {
        $this->validate($this->request, [
            'mobile' => 'required',
            'code'   => 'required',
        ]);
        try {
            $input = $this->request->all();
            $key   = Sms::REDIS_UPDATE_MOBILE . $input['mobile'];
            Sms::getInstance()->checkCode($key, $input['code']);
            $userInfo   = Auth::user();
            $userId     = $userInfo->id;
            $user       = new User();
            $mobileInfo = $user->getByMobile($input['mobile']);
            if (isset($mobileInfo->id)) return $this->failed('此号码已绑定用户');
            $res = $user->updateMobile($userId, $input['mobile']);
            if ($res) return $this->success();
            return $this->failed('修改失败');
        } catch (\Exception $e) {
            return $this->failed($e->getMessage());
        }
    }


    /**
     * 三方帐号信息
     * @return mixed
     */
    public function account()
    {
        $user         = Auth::user();
        $thirdAccount = UserThirdAccount::getInstance()
            ->where('user_id', $user->id)
            ->get();
        $qqAccount    = [];
        $wxAccount    = [];
        foreach ($thirdAccount as $k => $v) {
            switch ($v['third_type']) {
                case 1:
                    $qqAccount = $v;
                    break;
                case 2:
                    $wxAccount = $v;
                    break;

            }
        }
        $user->qq_account = $qqAccount ? $qqAccount : (object)[];
        $user->wx_account = $wxAccount ? $wxAccount : (object)[];
        return $this->success($user);
    }


    /**
     * 绑定帐号
     * type 1QQ，2微信
     * @return mixed
     * @throws \Illuminate\Validation\ValidationException
     */
    public function bindAccount()
    {
        $input  = $this->request->all();
        $user   = Auth::user();
        $userId = $user->id;
        $type   = $input['type'];
        switch ($type) {
            case 1:
                $this->validate($this->request, [
                    'access_token' => 'required',
                    'openid'       => 'required',
                ]);
                $user = self::getService()->qqBinding($input, $userId);
                break;

            case 2:
                $this->validate($this->request, [
                    'wx_code' => 'required',
                ]);
                $user = Self::getService()->wxBinding($input, $userId);
                break;
            default:
                throw new \JsonException('type类型错误');
        }
        return $this->success($user);
        $thirdAccount = UserThirdAccount::getInstance()
            ->where('user_id', $userId)
            ->get();
        $qqAccount    = [];
        $wxAccount    = [];
        foreach ($thirdAccount as $k => $v) {
            switch ($v['third_type']) {
                case 1:
                    $qqAccount = $v;
                    break;
                case 2:
                    $wxAccount = $v;
                    break;

            }
        }
        $user->qq_account = $qqAccount ? $qqAccount : (object)[];
        $user->wx_account = $wxAccount ? $wxAccount : (object)[];

        if (!$user) return $this->failed('绑定失败');
        return $this->success($user);
    }


    /**
     * 解绑帐号
     * @return mixed
     * @throws \Illuminate\Validation\ValidationException
     */
    public function unbindAccount()
    {
        $this->validate($this->request, [
            'type' => 'required',
        ]);
        $type   = $this->request->post('type');
        $user   = Auth::user();
        $userId = $user->id;
        switch ($type) {
            case 1:
            case 2:
                $res = UserThirdAccount::getInstance()->where(['user_id' => $userId, 'third_type' => $type])->delete();
                break;
            default:
                return $this->failed('类型错误，解绑失败');
        }
        if (!$res) return $this->failed('解绑失败');
        return $this->success();
    }


    /**
     * 反馈意见
     * @return mixed
     * @throws \Illuminate\Validation\ValidationException
     */
    public function suggest()
    {
        $this->validate($this->request, [
            'content' => 'required|max:255',
            'type'    => 'required_if:flag,1|max:500',
            'image'   => 'array|filled|max:6',
            'flag'    => 'required|max:255',
        ]);
        $input = $this->request->all();
        $user  = Auth::user();
        $user->UserHasMany(Suggest::class)->create($input);
        return $this->message('提交成功');
    }


    /**
     * 我的消息
     */
    public function myMessage()
    {
        $this->validate($this->request, [
            'type' => 'required|between:1,2',
        ]);
        $type   = $this->request->input('type');
        $userId = Auth::user()->id;
        if ($type == 1) {
            $data = SystemMsg::getInstance()
                ->where(['msg_type' => 1, 'type' => 0])
                ->orWhere(['msg_type' => 1, 'to_id' => $userId, 'type' => 1])
                ->paginate($this->pageSize, ['id', 'title', 'content', 'created_at']);
        } else {
            $data = SystemMsg::getInstance()
                ->where(['msg_type' => 2, 'type' => 0])
                ->orWhere(['msg_type' => 2, 'to_id' => $userId, 'type' => 1])
                ->paginate($this->pageSize, ['id', 'title', 'content', 'created_at']);
        }
        return $this->success($data);
    }


    /**
     * 我的评估
     * @return mixed
     */
    public function myScale()
    {
        $user   = Auth::user();
        $userId = $user->id;
        $data   = ScaleRecord::getInstance()
            ->where(['user_id' => $userId])
            ->orderBy('id', 'DESC')
            ->paginate($this->pageSize, ['scale_name', 'scale_id', 'id as scale_record_id', DB::raw('DATE_FORMAT(created_at, "%Y-%m-%d %H:%i") as time')]);
        return $this->success($data);

    }


    public function myScaleDetail()
    {
        $scale_record_id = $this->request->get('scale_record_id');
        $scaleDetail     = ScaleRecordDetail::getInstance()
            ->where(['scale_record_id' => $scale_record_id])
            ->get()
            ->toArray();
        $questions       = array_column($scaleDetail, 'q_id');
        $answers         = array_column($scaleDetail, 'a_id');
        $questionsInfo   = ScaleQuestion::getInstance()->whereIn('id', $questions)
            ->get(['id', 'question', 'prompt', 'risk_level', 'risk_warning'])->toArray();
        $answersInfo     = ScaleAnswer::getInstance()->whereIn('id', $answers)
            ->get(['id', 'q_id', 'content', 'options'])->toArray();
        $answersInfo     = array_column($answersInfo, null, 'q_id');
        foreach ($questionsInfo as $k => $v) {
            $questionsInfo[$k]['has_many_answer'] = $answersInfo[$v['id']] ?? [];
        }
        return $this->success($questionsInfo);
    }

}
