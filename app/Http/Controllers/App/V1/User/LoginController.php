<?php

namespace App\Http\Controllers\App\V1\User;

use App\Events\SendSmsEvent;
use App\Helpers\Sms;
use App\Helpers\YunXin\YunXin;
use App\Http\Controllers\Controller;
use App\Models\Protocol;
use App\Models\User;
use App\Models\UserThirdAccount;
use App\MongoModels\Users;
use App\Services\App\User\LoginService;
use App\Services\Common\CheckService;
use App\Services\ServiceManager;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Zhuzhichao\IpLocationZh\Ip;

class LoginController extends Controller
{

    /**
     * @return LoginService
     * @throws \ReflectionException
     */
    public static function getService(): LoginService
    {
        return ServiceManager::getInstance()->LoginService(LoginService::class);
    }

    /**
     * 获取验证码
     * @return mixed
     * @throws \Illuminate\Validation\ValidationException
     */
    public function smsCode()
    {
        $this->validate($this->request, [
            'mobile' => 'required|min:11|max:11',
            'type'   => 'required|between:1,2,3,4',
        ]);
        try {
            $mobile = $this->request->get('mobile');
            $type   = $this->request->get('type');
//          $res    = Sms::getInstance()->sendSing($type, $mobile);
            file_put_contents(storage_path() . '/logs/test.log', date('Y-m-d H:i:s') . '开始' . PHP_EOL, FILE_APPEND);
            event(new SendSmsEvent($type, $mobile));
            return $this->success('发送成功');
        } catch (\Exception $exception) {
            return $this->failed($exception->getMessage());
        }
    }


    public function loginBackup()
    {
        $this->validate($this->request, [
            'mobile' => 'required|min:11|max:11|exists:users',
            'type'   => 'required|between:1,2,3,4',
        ]);

        $type   = $this->request->get('type');
        $mobile = $this->request->get('mobile');
        try {
            if ($type == 1) {
                $code = $this->request->get('code');
                if (empty($code)) return $this->failed('验证码必填');
                $user = User::getInstance()->getByMobile($mobile);
                if (empty($user)) return $this->failed('此用户不存在请先注册');
                if ($user->status != 1) return $this->failed('账户未启用');
//                Sms::getInstance()->checkCode(Sms::REDIS_LOGIN . $mobile, $code);
            } elseif ($type == 2) {
                $password = $this->request->get('password');
                if (empty($password)) return $this->failed('密码必填');
                $user = User::getInstance()->makeVisible('password')->getByMobile($mobile);
                if (!Hash::check($password, $user->password)) return $this->failed('密码错误');
                if ($user->status != 1) return $this->failed('账户未启用');
            } else {

            }
            $result = $user->saveToken();
            $user->makeVisible('api_token');
            if ($result === false) return $this->failed('登陆失败');
            return $this->success($user);
        } catch (\Exception $e) {
            return $this->failed($e->getMessage());
        }
    }


    /**
     * 登录
     * type 1验证码登录，2密码登录，3QQ登录，4微信登录
     * @return mixed
     * @throws \Illuminate\Validation\ValidationException
     */
    public function login()
    {
        $input = $this->request->all();
        $type  = $input['type'];
        switch ($type) {
            case 1:
                $this->validate($this->request, [
                    'mobile' => 'required|min:11|max:11|exists:users',
                    'code'   => 'required',
                ]);
                $user = $this->mobileLogin($input);
                break;
            case 2:
                $this->validate($this->request, [
                    'mobile'   => 'required|min:11|max:11|exists:users',
                    'password' => 'required',
                ]);
                $user = $this->passwordLogin($input);
                break;
            case 3:
                $this->validate($this->request, [
                    'access_token' => 'required',
                    'openid'       => 'required',
                ]);
                $user = self::getService()->qqLogin($input);
                break;
            case 4:
                $this->validate($this->request, [
                    'wx_code' => 'required',
                ]);
                $user = self::getService()->wxLogin($input);
                return $this->success($user);
                break;
            default:
                throw new \JsonException('type类型错误');
        }

        if (!$user) return $this->failed('登录失败');
        try {
            $result = $user->saveToken();
            $user->makeVisible('api_token');
            if ($result === false) return $this->failed('登陆失败');
            return $this->success($user);
        } catch (\Exception $e) {
            return $this->failed($e->getMessage());
        }
    }

    public function mobileLogin($input)
    {
        $mobile = $input['mobile'];
        $code   = $input['code'];
        if (empty($code)) throw new \JsonException('验证码必填');
        $user = User::getInstance()->getByMobile($mobile);
        if (empty($user))  throw new \JsonException('此用户不存在请先注册');
        if ($user->status != 1)  throw new \JsonException('账户未启用');
        Sms::getInstance()->checkCode(Sms::REDIS_LOGIN . $mobile, $code);
        return $user;
    }


    public function passwordLogin($input)
    {
        $mobile   = $input['mobile'];
        $password = $input['password'];
        if (empty($password)) throw new \JsonException('密码必填');
        $user = User::getInstance()->makeVisible('password')->getByMobile($mobile);
        if (!Hash::check($password, $user->password)) throw new \JsonException('密码错误');
        if ($user->status != 1) throw new \JsonException('账户未启用');
        return $user;
    }


    /**
     * 用户注册
     * @return mixed
     * @throws \Illuminate\Validation\ValidationException
     */
    public function register()
    {
        $this->validate($this->request, [
            'mobile'                => 'bail|required|max:11|min:11|min:11|unique:users',
            'password'              => 'required|string|between:6,16|confirmed',
            'password_confirmation' => 'required|same:password',
            'code'                  => 'required|max:4|min:4|',
//            'protocol_id' => 'int',
        ]);
        try {
            $input = $this->request->all();
            CheckService::checkPassword($input['password']);
//            Sms::getInstance()->checkCode(Sms::REDIS_REGISTER . $input['mobile'], $input['code']);
            $ip = request()->getClientIp();
            //获取登录地址
            $areas = Ip::find($ip);
            DB::beginTransaction();
            $user           = new User();
            $user->mobile   = $input['mobile'];
            $user->head_pic = 'default_head.jpg';
            //$user->name = '用户' . uniqid();
            $user->nickname = '用户' . uniqid();
            $user->password = Hash::make($input['password']) ?? '';
            $user->save();
            //注册网易云信
            $result         = YunXin::getInstance()->user->create('user_' . $user->id . '_' . uniqid(), $user->nickname, $user->head_pic);
            $user->yx_token = $result['token'] ?? 1;
            $user->accid    = $result['accid'] ?? 2;
            $user->ip       = $ip;
            $user->area     = implode('-', $areas);
            $user->save();

            if (isset($input['protocol_id'])) {
                $protocol = Protocol::getInstance()->find($input['protocol_id']);
                if ($protocol) {
                    $insert['u_id'] = $user->id;
                    $result         = $protocol->hasOneRead()->where($insert)->first();
                    if (!$result)
                        $protocol->hasOneRead()->create($insert);
                }
            }
            DB::commit();
            return $this->success($user);
        } catch (\Exception $e) {
            DB::rollBack();
            return $this->failed($e->getMessage());
        }
    }


    /**
     * 更新密码
     * @return mixed
     * @throws \Illuminate\Validation\ValidationException
     */
    public function updatePwd()
    {

        $this->validate($this->request, [
            'mobile'                => 'bail|required|max:11|min:11|exists:users|min:4',
            'password'              => 'required|string|min:6|max:16|confirmed',
            'password_confirmation' => 'required|same:password',
            'code'                  => 'required|max:4|min:4|',
        ]);

        try {
            $input = $this->request->all();
            CheckService::checkPassword($input['password']);
            $key = Sms::REDIS_UPDATE_PASSWORD . $input['mobile'];
            Sms::getInstance()->checkCode($key, $input['code']);
            $user           = User::getInstance()->getByMobile($input['mobile']);
            $user->password = Hash::make($input['password']) ?? '';
            $user->save();
            return $this->success($user->id);
        } catch (\Exception $e) {
            return $this->failed($e->getMessage());
        }
    }


    /**
     * 修改手机号
     * @return mixed
     */
    public function updateUserMobile()
    {
        try {
            $input = $this->request->all();
            $key   = Sms::REDIS_UPDATE_MOBILE . $input['mobile'];
            Sms::getInstance()->checkCode($key, $input['code']);
            $user     = Auth::user();
            $id       = $user->id;
            $mobileId = DB::table('users')->where(['mobile' => $input['mobile']])->first(['id']);
            if (isset($mobileId->id)) return $this->failed('此号码已绑定用户');
            $res = User::getInstance()->where('id', $id)->update(['mobile' => $input['mobile']]);
            if ($res) return $this->success($id);
            return $this->failed('修改失败');
        } catch (\Exception $e) {
            return $this->failed($e->getMessage());
        }
    }


    /**
     * 根据验证码修改密码
     * @return mixed
     */
    public function updatePwdCode()
    {
        $input = $this->request->all();
        $this->validate($this->request, [
            'mobile'          => 'required',
            'code'            => 'required',
            'password'              => 'required|string|min:6|max:16|confirmed',
            'password_confirmation' => 'required|same:password',
        ]);
//        $user = Auth::user();
//        $userId = $user->id;
        $pwd = $input['password'];
        $key   = Sms::REDIS_UPDATE_PASSWORD . $input['mobile'];
        Sms::getInstance()->checkCode($key, $input['code']);
        $userModel = new User();
        $mobileInfo = $userModel->getByMobile($input['mobile']);
//        if ($mobileInfo->id != $userId) return $this->failed('手机号不对应此用户');
        if (empty($mobileInfo)) return $this->failed('用户不存在');
        $newPwd = Hash::make($pwd) ?? '';
        $res = User::getInstance()->where([ 'id' => $mobileInfo->id ])->update([ 'password' => $newPwd ]);
        if ($res) return $this->success();
        return $this->failed('修改失败');
    }

}
