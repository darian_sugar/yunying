<?php

namespace App\Http\Controllers\App\V1\User;

use App\Http\Controllers\App\BaseController;
use App\Models\Doctor;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class DoctorController extends BaseController
{
    /**
     * 医生列表
     *
     * @return mixed
     */
    public function index()
    {
        $this->validate( $this->request, [], [
            'type'     => 'required|int|max:2',
            'key_word' => 'string',
        ] );
        $input = $this->request->all();
        $where['ask_status'] = 1;
        $where['status'] = 1;
        $data = Doctor::getInstance()
            ->select( 'id', 'name', 't_id', 'head_pic', 'text_price', 'video_price', 'audio_price', 'type', 'appoint_price' )
            ->where( $where )
            ->when( $input['key_word'], function ( $where, $value ) {
                $where->where( 'name', 'like', '%' . $value . '%' );
            } )
            ->when( $input['type'], function ( $where, $value ) {
                $where->where( function ( $query ) use ( $value ) {
                    $query->where( 'type', $value )->orWhere( 'type', 3 );
                } );
            } )
            ->withCount( [
                'hasManyInquiryOrder'          => function ( $query ) {
                    $query->where( [ 'order_status' => 1, 'pay_status' => 1 ] );
                },
                'hasManyAppointment'           => function ( $query ) {
                    $query->where( [ 'order_status' => 1, 'pay_status' => 1 ] );

                },
                'hasManyComment as avg_source' => function ( $query ) {
                    $query->select( DB::raw( "round(ifnull(avg(score),0),2)" ) );
                }
            ] )
            ->latest( 'id' )
            ->when( $input['orderby'], function ( $query, $value ) {
                if ( $value == 1 )
                    $query->orderByDesc( 'avg_source' );
                elseif ( $value == 2 )
                    $query->orderByDesc( 'has_many_appointment_count' );
            } )
            ->paginate( $this->pageSize );
        return $this->success( $data );
    }


    /**
     * 医生详情
     *
     * @return mixed
     * @throws \Illuminate\Validation\ValidationException
     */
    public function detail()
    {
        $this->validate( $this->request, [
            'id' => 'required|int|exists:doctors'
        ] );
        $user = Auth::user();
        $where = $this->request->all();

        $data = Doctor::getInstance()
            ->select( 'id', 'name', 't_id', 'head_pic', 'text_price', 'video_price', 'audio_price', 'type', 'intro', 'accid', 'yx_token', 'appoint_price' )
            ->with( [
                'hasManyRecord' => function ( $query ) use ( $user ) {
                    $query->select( 'id', 'doctor_id', 'type', 'time' )->where( 'user_id', $user->id )->where( 'has_pay', 1 );
                }
            ] )
            ->where( $where )
            ->withCount( [
                'hasManyInquiryOrder'          => function ( $query ) {
                    $query->where( 'order_status', 1 )->where( 'pay_status', 1 );
                },
                'hasManyAppointment'           => function ( $query ) {
                    $query->where( [ 'order_status' => 1, 'pay_status' => 1 ] );

                },
                'hasManyComment as avg_source' => function ( $query ) {
                    $query->select( DB::raw( "round(ifnull(avg(score),0),2)" ) );
                }
            ] )->first();
        if ( $data )
            $data->makeVisible( [ 'accid', 'yx_token' ] );
        else
            return $this->failed( '医生信息不存在' );
        return $this->success( $data );
    }

    /**
     * 获取医生排班
     *
     * @return mixed
     * @throws \Illuminate\Validation\ValidationException
     */
    public function getSchedule()
    {
        $this->validate( $this->request, [
            'doctor_id' => 'required',
            'date'      => 'required',
        ] );
        $input = $this->request->all();
        $doctor = Doctor::getInstance()->findOrFail( $input['doctor_id'] );
        if ( $doctor->type == 1 )
            return $this->failed( '该专家暂不支持线下预约' );
        $data = $doctor->hasManySchedule()->whereDate( 'date', $input['date'] )->get();
        return $this->success( $data );
    }


    /**
     * 获取医生可预约日期
     * @return mixed
     * @throws \Illuminate\Validation\ValidationException
     */
    public function getUsefulDate()
    {
        $this->validate( $this->request, [
            'doctor_id'  => 'required',
            'start_date' => 'required|date',
            'end_date'   => 'required|date',
        ] );
        $input = $this->request->all();
        $doctor = Doctor::getInstance()->findOrFail( $input['doctor_id'] );
        if ( $doctor->type == 1 )
            return $this->failed( '该专家暂不支持线下预约' );
        $data = $doctor->hasManySchedule()
            ->whereDate( 'date', '>=', $input['start_date'] )
            ->whereDate( 'date', '<=', $input['end_date'] )
            ->selectRaw( 'total_num - used_num as a,date' )
            ->get();
        $result = $data->where( 'a', '>', 0 )->pluck( 'date' )->unique()->values();
        return $this->success( $result );
    }


}
