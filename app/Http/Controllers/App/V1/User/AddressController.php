<?php

namespace App\Http\Controllers\App\V1\User;

use App\Models\Address;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class AddressController extends Controller
{
    //
    public function index()
    {
        $user = Auth::user();
        $data = $user->UserHasMany(Address::class)->with([
            'belongsToArea:id,name',
            'belongsToCity:id,name',
            'belongsToProvince:id,name',
        ])->paginate($this->pageSize);
        return $this->success($data);
    }

    public function add()
    {
        $this->validate($this->request, [
            'address'     => 'required|max:100',
            'province_id' => 'required',
            'city_id'     => 'required',
            'area_id'     => 'required',
            'mobile'      => 'required|max:20',
            'name'        => 'required|max:10',
        ]);
        $input = $this->request->all();
        $fill = [ 'address', 'province_id', 'city_id', 'area_id', 'mobile', 'name', 'id', 'is_default' ];
        foreach( $input as $k => $v ){
            if( !in_array($k, $fill) )
                unset($input[$k]);
        }
        $user = Auth::user();
        $model = $user->UserHasMany(Address::class);
        if( $input['is_default'] == 1 )
            $model->update([ 'is_default' => 2 ]);
        if( $input['id'] ){
            $model->where([ 'id' => $input['id'] ])->delete();
            $input['user_id'] = $user->id;
            $input['from_id'] = $input['id'];
            $res = Address::getInstance()->fill($input)->save();
        }else{
            $res = $model->create($input);
        }

        if( $res )
            return $this->message('操作成功');
        else
            return $this->failed('操作失败');
    }

    /**
     * 修改默认地址
     * @return mixed
     * @throws \Illuminate\Validation\ValidationException
     */
    public function changeDefault()
    {
        $this->validate($this->request, [
            'id' => 'required'
        ]);
        $user = Auth::user();
        $input = $this->request->all();
        $address = $user->UserHasMany(Address::class)->where('id', $input['id'])->first();
        if( !$address )
            return $this->failed('地址不存在');
        $address = Address::getInstance()->findOrFail($input['id']);
        $user->UserHasMany(Address::class)->update([ 'is_default' => 2 ]);
        $address->is_default = 1;
        $address->save();
        return $this->message('操作成功');
    }

    public function delete()
    {
        $this->validate($this->request, [
            'id' => 'required',
        ]);
        $user = Auth::user();
        $id = $this->request->get('id');
        $data = $user->UserHasMany(Address::class)->findOrFail($id);
        $data->delete();
        return $this->message('删除成功');
    }
}
