<?php

namespace App\Http\Controllers\App\V1\User;

use App\Helpers\Sms;
use App\Helpers\YunXin\YunXin;
use App\Http\Controllers\App\BaseController;
use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\UserThirdAccount;
use App\Services\Common\CommonService;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Zhuzhichao\IpLocationZh\Ip;

class QqLoginController extends BaseController
{


    /**
     * 由accessToken,openId获取QQ用户信息
     *
     * @param $accessToken
     * @param $openid
     * @param $appId
     *
     * @return bool
     */
    public function getThirdInfo($accessToken, $openid)
    {
        $appId = env('QQ_APP_ID');
        $url   = 'https://graph.qq.com/user/get_user_info?access_token=' . $accessToken . '&oauth_consumer_key=' . $appId . '&openid=' . $openid;
//        $client   = new Client();
//        $json_obj = $client->request('get', $url);


//        $json_obj = CommonService::getCurl($url);
//        return $json_obj;
//        if ($json_obj['ret'] != 0) return $this->errorJson([],'qq获取用户信息失败');
        $test     = '{
        "ret":0,
        "msg":"",
        "nickname":"Peter",
        "figureurl":"http://qzapp.qlogo.cn/qzapp/111111/942FEA70050EEAFBD4DCE2C1FC775E56/30",
        "figureurl_1":"http://qzapp.qlogo.cn/qzapp/111111/942FEA70050EEAFBD4DCE2C1FC775E56/50",
        "figureurl_2":"http://qzapp.qlogo.cn/qzapp/111111/942FEA70050EEAFBD4DCE2C1FC775E56/100",
        "figureurl_qq_1":"http://q.qlogo.cn/qqapp/100312990/DE1931D5330620DBD07FB4A5422917B6/40",
        "figureurl_qq_2":"http://q.qlogo.cn/qqapp/100312990/DE1931D5330620DBD07FB4A5422917B6/100",
        "gender":"男",
        "is_yellow_vip":"1",
        "vip":"1",
        "yellow_vip_level":"7",
        "level":"7",
        "is_yellow_year_vip":"1"
        }';
        $json_obj = json_decode($test, true);

        foreach ($json_obj as $k => $v) {
            if ($json_obj['gender'] == '男') {
                $json_obj['gender'] = 1;
            } else {
                $json_obj['gender'] = 2;
            }
        }
        return $json_obj;
    }


    public function qqLogin()
    {
        $this->validate($this->request, [
            'access_token' => 'required',
            'openid'       => 'required'
        ]);
        $accessToken = $this->request->get('access_token');
        $openid      = $this->request->get('openid');
        $thirdInfo      = $this->getThirdInfo($accessToken, $openid);
        //查询此次登陆用户是否首次登陆，不是首次登陆返回信息，是首次登陆注册新用户
        $useThirdInfo = UserThirdAccount::getInstance()
            ->where(['openid' => $openid, 'third_type' => 1])
            ->first();
        if (!empty($useThirdInfo)) {
            //非首次登陆，刷新token，返回用户信息
            $userInfo = User::getInstance()->find($useThirdInfo->user_id);
            $res = $userInfo->saveToken();
            $userInfo->makeVisible('api_token');
            if (!$res) return $this->failed('登录失败');
            return $this->success($userInfo);
        }
        //获取登录地址
        $ip = request()->getClientIp();
        $areas = Ip::find($ip);
        try {
            DB::beginTransaction();
            $user = new User();
            // 无手机号直接注册新帐号
            $createUser = $this->createUser($user, $thirdInfo, $ip, $areas);
            $userId = $createUser->id;
            $userInfo = User::getInstance()->find($userId);

            //将第三方QQ用户信息入库
            $insertThird = [
                'user_id'    => $userId,
                'third_key'  => $openid,
                'third_type' => 1,
                'openid'     => $openid,
                'nickname'   => $thirdInfo['nickname'],
                'sex'        => $thirdInfo['gender'],
                'headimgurl' => $thirdInfo['figureurl_qq_1'],
            ];
            $newThird    = UserThirdAccount::getInstance()->insert($insertThird);
            if (!$newThird) {
               DB::rollBack();
                return $this->failed('登录失败');
            }
            DB::commit();
            return $this->success($userInfo);
        } catch (\Exception $e) {
            DB::rollBack();
            return $this->failed($e->getMessage());
        }

    }


    public function createUser($user, $thirdInfo, $ip, $areas)
    {
        //无手机号，创建新用户，关联微信账号，
        $user->head_pic = $thirdInfo['figureurl_qq_1'];
        $user->nickname = $thirdInfo['nickname'];
        $user->sex      = $thirdInfo['gender'];
        $user->save();
        //注册网易云信
        $result          = YunXin::getInstance()->user->create('user___' . $user->id . '_' . uniqid(), $user->nickname, $user->head_pic);
        $user->yx_token  = $result['token'];
        $user->accid     = $result['accid'];
        $user->ip        = $ip;
        $user->api_token = create_token(1);
        $user->area     = implode('-', $areas);
        $user->save();
        return $user;
    }

}
