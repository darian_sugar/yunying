<?php

namespace App\Http\Controllers\App\V1\Common;

use App\Common\Constants\ConstantRedisKey;
use App\Helpers\DingDingRobotService;
use App\Helpers\Upload;
use App\Http\Controllers\App\BaseController;
use App\Http\Controllers\Controller;
use App\Models\Adv;
use App\Models\Area;
use App\Services\Common\CommonService;
use Illuminate\Support\Facades\Redis;

class IndexController extends BaseController
{

    public function areaList()
    {
        $data = Redis::get(ConstantRedisKey::AREAS);
        if (!empty($data)) return $this->success(json_decode($data));
        $data = Area::getInstance()
            ->whereNotNull('pid')
            ->get(['id', 'name', 'pid']);
        $data = CommonService::getTree($data, 1);
        Redis::set(ConstantRedisKey::AREAS, json_encode($data, JSON_UNESCAPED_UNICODE));
        return $this->success($data);
    }


    /**
     * 上传接口
     * @return mixed
     * @throws \Illuminate\Validation\ValidationException
     */
    public function upload()
    {
        $this->validate($this->request, [
            'type' => 'required|between:1,7',
            'file' => 'required',
        ]);
        try {
            $type       = $this->request->get('type');
            $file       = $this->request->file('file');
            $fileDetail = $_FILES['file'];
            DingDingRobotService::getInstance()->send($fileDetail, 5);
            $input['url']      = Upload::getInstance()->uploadFile($file, $type);
            $input['full_url'] = Upload::getInstance()->getPicName($input['url'], $type);
            $input['size']     = bcdiv($file->getClientSize(), 1024, 1);
            return $this->success($input);
        } catch (\Exception $e) {
            $error = [
                'error' => $e->getMessage(),
                'type'  => $type,
                'file'  => $fileDetail,
            ];
            return $this->failed($error);
        }
    }

    /**
     * 大文件上传 (大于10M)
     * @return mixed
     * @throws \Illuminate\Validation\ValidationException
     * @throws \JsonException
     */
    public function uploadBigFile()
    {
        $this->validate($this->request, [
            'file'         => 'required|file',
            'blobNum'      => 'required|int',
            'totalBlobNum' => 'required|int',
            'md5FileName'  => 'required',
            'type'         => 'required',
            'size'         => 'required',
        ], [], [
            'file'         => '上传文件',
            'blobNum'      => '当前切片数',
            'totalBlobNum' => '总切片数',
            'md5FileName'  => '文件名称',
            'type'         => '文件类型',
            'size'         => '每片文件大小',
        ]);

        $file         = $this->request->file('file');
        $blobNum      = $this->request->get('blobNum');
        $totalBlobNum = $this->request->get('totalBlobNum');
        $md5FileName  = $this->request->get('md5FileName');
        $type         = $this->request->get('type');
        $size         = $this->request->get('size');
        $result       = Upload::getInstance()->uploadBigFile($file, $blobNum, $totalBlobNum, $md5FileName, $type);

        if ($result) {
            $data['url']      = $result;
            $data['full_url'] = Upload::getInstance()->getPicName($result, $type);
            $data['size']     = bcdiv(($file->getClientSize() + bcmul($blobNum - 1, $size)), 1024, 1) . ' kb';
        }
        return $this->success($data);
    }


    /**
     * 轮播图
     * @return mixed
     * @throws \Illuminate\Validation\ValidationException
     */
    public function banner()
    {
        $this->validate($this->request, [
            'adv_place' => 'required|between:1,7',
        ]);
        $now  = date('Y-m-d H:i:s');
        $data = Adv::getInstance()->select('id', 'image', 'link_url', 'start_time', 'end_time')
            ->where('start_time', '<=', $now)
            ->where('end_time', '>=', $now)->get();
        return $this->success($data);
    }


}
