<?php

namespace App\Http\Controllers\App\V1\Scale;

use App\Common\Constants\ConstantMysql;
use App\Http\Controllers\App\BaseController;
use App\Http\Controllers\Controller;
use App\Models\Scale;
use App\Models\ScaleAnswer;
use App\Models\ScaleBasic;
use App\Models\ScaleQuestion;
use App\Models\ScaleRecord;
use App\Models\ScaleRecordDetail;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use test\Mockery\ReturnTypeUnionTypeHint;

class IndexController extends BaseController
{

    /**
     * 量表列表
     * @return mixed
     */
    public function index()
    {
        $data = Scale::getInstance()->where(['is_enable' => ConstantMysql::IS_ENABLE])
            ->get(['id', 'scale_name', 'introduce']);
        return $this->success($data);
    }


    /**
     * 热门量表
     * @return mixed
     */
    public function hot()
    {
        $data     = ScaleRecord::getInstance()->join('scales as s', 's.id', '=', 'scale_records.scale_id')
            ->select(DB::raw('count(scale_id) as scale_num, scale_id, s.scale_name, s.introduce'))
            ->where(['is_enable' => ConstantMysql::IS_ENABLE])
            ->groupBy('scale_id')->orderBy('scale_num', 'desc')->first();
        $scale_id = $data->scale_id ?? 0;
        return $this->success($data);
    }


    /**
     * 推荐列表
     * @return mixed
     */
    public function recommend()
    {
        $data = ScaleRecord::getInstance()->join('scales as s', 's.id', '=', 'scale_records.scale_id')
            ->select(DB::raw('count(scale_id) as scale_num, scale_id, s.scale_name, s.introduce'))
            ->where(['s.is_recommended' => ConstantMysql::IS_RECOMMENDED, 's.is_enable' => ConstantMysql::IS_ENABLE])
            ->groupBy('scale_id')->orderBy('scale_num', 'desc')->get();
//        $scale_id = $data->scale_id ?? 0;
        return $this->success($data);
    }


    /**
     * 获取一个问题
     * @return mixed
     * @throws \Illuminate\Validation\ValidationException
     */
    public function question()
    {
        $this->validate($this->request,
            [
                'scale_id' => 'required',
                'type'     => 'required',
            ]
        );
        $input       = $this->request->all();
        $scale_id    = $input['scale_id'] ?? '';
        $question_id = $input['question_id'] ?? '';
        $type        = $input['type'] ?? '';
        try {
            $where = ['scale_id' => $scale_id];
            if (!empty($question_id)) {
                $where['id']   = $question_id;
                $oldQuestion   = ScaleQuestion::getInstance()
                    ->where($where)->orderBy('sort', 'asc')
                    ->select(['id', 'question', 'prompt', 'risk_level', 'scale_id', 'sort', 'q_type'])
                    ->first();
                $question_sort = $oldQuestion->sort;
                if ($question_sort == 1) return $this->failed('已是第一题');
                if ($type == 1) {
                    $where['sort'] = ++$question_sort;
                } else {
                    $where['sort'] = --$question_sort;
                }
                unset($where['id']);
            }
            $question = ScaleQuestion::getInstance()
                ->where($where)->orderBy('sort', 'asc')
                ->select(['id', 'question', 'prompt', 'risk_level', 'scale_id', 'sort', 'q_type'])
                ->first();

            $answer   = ScaleAnswer::getInstance()
                ->where(['q_id' => $question->id])
                ->get(['id', 'q_id', 'prompt', 'options', 'content', 'direction', 'is_fill', 'jump_id', 'grade']);
            $question['has_many_answer'] = $answer;
            return $this->success($question);
        } catch (\Exception $e) {
            return $this->failed($e->getMessage());
        }
    }


    /**
     * 获取所有问题
     * @return mixed
     * @throws \Illuminate\Validation\ValidationException
     */
    public function allQuestion()
    {
        $this->validate($this->request,
            [
                'scale_id' => 'required'
            ]
        );
        $input             = $this->request->all();
        $where['scale_id'] = $input['scale_id'];
        try {
            $data = ScaleQuestion::getInstance()
                ->with(['hasManyAnswer:id,q_id,prompt,options,content,direction,is_fill,jump_id,grade'])
                ->where($where)->orderBy('sort', 'asc')
                ->select(['id', 'question', 'prompt', 'risk_level', 'scale_id', 'sort', 'q_type'])
                ->get();
            return $this->success($data);
        } catch (\Exception $e) {
            return $this->failed($e->getMessage());
        }
    }


    /**
     * 批量添加答案
     * @return mixed
     * @throws \Illuminate\Validation\ValidationException
     */
    public function submitAnswer()
    {
        $this->validate($this->request,
            [
                'result'   => 'required',
                'basic_id' => 'required',
                'scale_id' => 'required',
                'is_stop'  => 'required|between:1,2',
            ]
        );
        $input          = $this->request->all();
        $result         = $input['result'];
//        $result = '{
//    "result": [
//        [
//            143,
//            363,
//            "爱剪辑"
//        ],
//        [
//            144,
//            364,
//            null
//        ],
//        [
//            145,
//            366,
//            2008
//        ],
//        [
//            145,
//            367,
//            11
//        ],
//        [
//            145,
//            368,
//            20
//        ],
//        [
//            146,
//            369,
//            2020
//        ],
//        [
//            146,
//            370,
//            11
//        ],
//        [
//            146,
//            371,
//            20
//        ],
//        [
//            147,
//            372,
//            "06:00"
//        ],
//        [
//            147,
//            373,
//            "18:00"
//        ],
//        [
//            148,
//            374,
//            "21:00"
//        ],
//        [
//            148,
//            375,
//            "05:11"
//        ],
//        [
//            149,
//            376,
//            "06:00"
//        ],
//        [
//            150,
//            378,
//            null
//        ],
//        [
//            151,
//            382,
//            null
//        ],
//        [
//            151,
//            383,
//            null
//        ],
//        [
//            152,
//            387,
//            null
//        ],
//        [
//            153,
//            389,
//            "1"
//        ],
//        [
//            154,
//            390,
//            null
//        ],
//        [
//            155,
//            393,
//            "1"
//        ],
//        [
//            156,
//            394,
//            "14"
//        ],
//        [
//            157,
//            397,
//            null
//        ],
//        [
//            158,
//            402,
//            12
//        ],
//        [
//            158,
//            403,
//            4
//        ]
//    ]
//}';
//        $result = json_decode($result,true)['result'];
//       var_dump($result);
        $basic_id       = $input['basic_id'];
        $scaleId        = $input['scale_id'];
        $isStop         = $input['is_stop'];
        $lastQuestionId = $result[count($result) - 1][0];
//        var_dump($lastQuestionId);exit;

        $questionInfo         = ScaleQuestion::getInstance()->where('id', $lastQuestionId)->first();
        $userId               = Auth::user()->id;
        $scaleBasic           = ScaleBasic::getInstance()->where('user_id', $userId)->orderByDesc('id')->first();
        $scaleInfo            = Scale::getInstance()->where('id', $scaleId)->orderByDesc('id')->first();
        if (empty($scaleInfo)) return $this->failed('量表不存在');
        $insert['user_id']    = $userId;
        $insert['scale_id']   = $scaleId;
        $insert['scale_name'] = $scaleInfo['scale_name'];
        $insert['type']       = $scaleInfo['type'];
        $insert['is_stop']    = $isStop;
        $insert['stop_id']    = $questionInfo->id;
        $insert['basic_id']   = $basic_id;
        try {
            DB::beginTransaction();
            $recordRes = ScaleRecord::getInstance()->create($insert);
            if (!$recordRes) $this->failed('添加答案失败');
            $recordId     = $recordRes->id;
            $detailInsert = [];
            foreach ($result as $k => $v) {
                $detailInsert[$k]['scale_record_id'] = $recordId;
                $detailInsert[$k]['q_id']            = $v[0];
                $detailInsert[$k]['a_id']            = $v[1];
                $detailInsert[$k]['a_content']            = $v[2] ?? '';
            }
            $res = ScaleRecordDetail::getInstance()->insert($detailInsert);
            if (!$res) {
                DB::rollBack();
                $this->failed('添加答案失败');
            }
            DB::commit();
            if ($isStop == 2) {
                return $this->success();
            }

            $questions     = array_column($result, 0);
            $answers       = array_column($result, 1);
            $questionsInfo = ScaleQuestion::getInstance()->whereIn('id', $questions)
                ->get(['id', 'question', 'prompt', 'risk_level', 'risk_warning', 'q_type'])->toArray();
            $answersInfo   = ScaleAnswer::getInstance()->join('scale_record_details as sd', 'sd.a_id', '=', 'scale_answers.id')
                ->whereIn('scale_answers.id', $answers)
                ->where('scale_record_id', $recordId)
                ->get(['scale_answers.id', 'scale_answers.q_id', 'scale_answers.direction',
                    'scale_answers.jump_id','scale_answers.is_fill','scale_answers.grade',
                    'scale_answers.content', 'scale_answers.options','sd.a_content'])
                ->toArray();
//            return $this->success($answersInfo);
//            $answersInfo   = array_column($answersInfo, null, 'q_id');
            $newAnswersInfo = [];
                foreach ($answersInfo as $k => $v) {
                    $newAnswersInfo[$v['q_id']][] = $v;
                }
            foreach ($questionsInfo as $k => $v) {
                $questionsInfo[$k]['has_many_answer'] = $newAnswersInfo[$v['id']] ?? [];
            }
            $result = ScaleRecord::getInstance()->where(['id' => $recordId])->first();
            return $this->success($questionsInfo);
        } catch (\Exception $e) {
            DB::rollBack();
            return $this->failed($e->getMessage());
        }
    }


    /**
     * 基础信息添加
     * @return mixed
     * @throws \Illuminate\Validation\ValidationException
     */
    public function basicInfoAdd()
    {
        $this->validate($this->request,
            [
                'baby_name'    => 'required',
                'baby_month'   => 'required',
                'baby_sex'     => 'required',
                'native_place' => 'required',
                'province_id'  => 'required',
                'city_id'      => 'required',
                'area_id'      => 'required',
                'blood_type'   => 'required',
                'mother_age'   => 'required',
            ]
        );
        $input            = $this->request->all();
        $userId           = Auth::user()->id;
        $input['user_id'] = $userId;
        $res              = ScaleBasic::getInstance()->create($input);
        if (!$res) return $this->failed('添加基本信息失败');
        return $this->success($res);
    }


    public function previousBasicInfo()
    {
        $user   = Auth::user();
        $userId = $user->id;
        $data   = ScaleBasic::getInstance()->where(['user_id' => $userId])->orderBy('id', 'DESC')->first();
        return $this->success($data);
    }


    /**
     * 在线评估问题添加
     * @return mixed
     */
    public function questionAdd()
    {
        $options = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L'];
        $input   = $this->request->post('input');
        $s_id    = $this->request->post('s_id');
        $input   = json_decode($input, true);
        $result  = [];
        $i       = 1;
        foreach ($input as $k => $v) {
            $questionInsert         = [
                'question'     => $v['question'],
                'risk_level'   => $v['risk_level'],
                'risk_warning' => '',
                'prompt'       => $v['prompt'] ?? '',
                's_id'         => $s_id,
                'sort'         => $i++,
            ];
            $res                    = ScaleQuestion::getInstance()->insert($questionInsert);
            $id                     = DB::getPdo()->lastInsertId();
            $result[$k]['question'] = [$id, $res];
            $answer                 = $v['answer'];
            $answer                 = array_values(array_filter(explode('□', $answer)));
            $answerInsert           = [];
            foreach ($answer as $k1 => $v1) {
                $answerInsert[$k1]['q_id']    = $id;
                $answerInsert[$k1]['content'] = $v1;
                $answerInsert[$k1]['grade']   = 0;
                $answerInsert[$k1]['options'] = $options[$k1];
            }
            $res                  = ScaleAnswer::getInstance()->insert($answerInsert);
            $result[$k]['answer'] = $res;
        }
        return $this->success($result);

    }


    /**
     * 答案添加
     * @return mixed
     */
    public function answerAdd()
    {
        $input   = $this->request->all();
        $q_id    = $input['q_id'];
        $options = $input['options'];
        $options = explode(',', $options);
        $content = $input['content'];
        $content = explode(',', $content);
        $result  = $input['result'];
        $insert  = [];
        foreach ($options as $k => $v) {
            $insert[$k]['q_id']    = $q_id;
            $insert[$k]['grade']   = 0;
            $insert[$k]['options'] = $v;
            $insert[$k]['content'] = $content[$k];
        }
        $res = ScaleAnswer::getInstance()->insert($insert);
        return $this->success($res);
    }
}
