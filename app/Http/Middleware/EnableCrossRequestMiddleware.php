<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class EnableCrossRequestMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param Request $request
     * @param Closure $next
     *
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
//		$origin = $request->header('ORIGIN', '*');
        $origin = Request()->server('HTTP_ORIGIN') ? Request()->server('HTTP_ORIGIN') : '';
        $request->headers->set('request_id', uniqid());
        header("Access-Control-Allow-Origin: $origin");
        header("Access-Control-Allow-Credentials: true");
        header('Access-Control-Allow-Methods: POST, GET, OPTIONS, PUT, DELETE');
        header('Access-Control-Allow-Headers: Origin, Access-Control-Request-Headers, SERVER_NAME, Access-Control-Allow-Headers, cache-control, X-Requested-With, Content-Type, Accept, Connection, User-Agent, Cookie, Authorization');
        return $next($request);
    }
}
