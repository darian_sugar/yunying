<?php

namespace App\Http\Middleware;

use App\Services\Common\CheckService;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

class CheckLogin
{
    /**
     * Handle an incoming request.
     *
     * @param Request $request
     * @param Closure $next
     *
     * @return mixed
     */
    public function handle( $request, Closure $next, $guard = null )
    {
        Log::debug( "*********request_id:{$request->header('request_id')}:" . $request->path(), array_merge( [ 'Authorization' => $request->header( 'Authorization' ) ], $request->all() ) );
        $user = Auth::guard( $guard )->user();
        $table = Auth::guard( $guard )->user()->getTable();
        $method = $request->route()->getActionMethod();
        $white_list = [ 'login', 'loginOut' ];
        $checkOverTime = true;
        if ( !in_array( $method, $white_list ) && env( 'APP_ENV' ) == 'production' ) {
            $checkOverTime = CheckService::checkOvertime( $user->id, $table );
        }
        if ( !$checkOverTime ) {
            return response()->json( [ 'status' => 'error', 'code' => 401, 'message' => '登录过期，请重新登录' ] );
        }
        if ( $user->status != 1 ) {
            return response()->json( [ 'status' => 'error', 'code' => 401, 'message' => '账户未启用' ] );
        }
        return $next( $request );
    }
}
