<?php

namespace App\Http\Middleware;


use App\Helpers\DingDingRobotService;
use App\Models\Admin;
use App\Models\Doctor;
use App\Models\User;
use App\MongoModels\RequestLog;
use App\Services\Common\CommonService;
use Closure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Cache;

class ApiLog
{
    /**
     * 路由全局中间件
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        $response   = $next($request);
        try {
            $token = $request->header()['authorization'][0] ?? '';
            if (!empty($token)) {
                $apiToken = explode(' ', $token)[1];
                $useType  = $apiToken[0] ?? 0;
                switch ($useType) {
                    case 1:
                        $userId = User::getInstance()->where('api_token', $apiToken)->value('id');
                        break;
                    case 2:
                        $userId = Doctor::getInstance()->where('api_token', $apiToken)->value('id');;
                        break;
                    case 3:
                        $userId = Admin::getInstance()->where('api_token', $apiToken)->value('id');;
                        break;
                    default:
                        $userId = 0;
                        break;
                }
            }
            $start_time = microtime(true);
            $rq_time    = microtime(true) - $start_time;
            //插入请求日志
            $request_url = $request->getRequestUri();
            $route       = explode('?', $request_url)[0] ?? '';
            // 不写入日志的接口列表
            $filterRoute = [
                '/api/c1/common/areaList',
            ];
            if (in_array($route, $filterRoute)) return $response;
            $insert     = [
                'user_id'    => $userId,
                'use_type'   => $useType,
                'header'     => json_encode($request->header()['authorization']),
                'ip_address' => $request->ip(),
                'method'     => $request->method(),
                'url'        => $request->fullUrl(),
                'param'      => json_encode($request->all()),
                'rq_time'    => sprintf("%.2f", $rq_time),
                'total_time' => '',
                'response'   => $response->getContent(),
                'created_at' => date('Y-m-d H:i:s'),
            ];
            $res        = RequestLog::getInstance()->create($insert);
            $total_time = microtime(true) - $start_time;
            RequestLog::getInstance()->where(['_id' => $res['_id']])->update(['total_time' => sprintf("%.4f", $total_time)]);
        } catch (\Exception $e) {
            file_put_contents(storage_path() . '/logs/apiLog.log', 'apiLog错误：' . $e->getMessage() . PHP_EOL, FILE_APPEND);
        } finally {
            return $response;
        }

    }
}
