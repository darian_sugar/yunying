<?php

namespace App\Http\Middleware;

use App\Models\Admin;
use App\Models\Privilege;
use App\Services\Model\AdminService;
use App\Services\ServiceManager;
use Closure;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

class CheckAdminLogin
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle( $request, Closure $next, $guard = null )
    {
        Log::debug( "*********request_id:{$request->header('request_id')}:" . $request->path(), array_merge( [ 'Authorization' => $request->header( 'Authorization' ) ], $request->all() ) );
        $user = Auth::guard( $guard )->user();
        if ( $user->status != 1 ) {
            return response()->json( [ 'status' => 'error', 'code' => 401, 'message' => '账户未启用' ] );
        }

        if ( env( 'APP_ENV' ) != 'local' && $user->is_admin == 2 ) {
            //验证节点权限
            $apis = ServiceManager::getInstance()->AdminService( AdminService::class )->apiAccess( $user );
            $url = $request->getPathInfo();
            $method = $request->method();
            $count = $apis->whereStrict( 'url', trim( $url, '/' ) )->whereStrict( 'method', strtolower( $method ) )->count();
            if ( $count <= 0 )
                return response()->json( [ 'status' => 'error', 'code' => 999, 'message' => '暂无权限' ] );
        }
        return $next( $request );
    }
}
