<?php

namespace App\MongoModels;

use Illuminate\Database\Eloquent\Model;
use Jenssegers\Mongodb\Eloquent\Model as MongoModel;
use Jenssegers\Mongodb\Eloquent\SoftDeletes;

class Users extends MongoModel
{

    use SoftDeletes;

    protected $connection = 'mongodb';  //库名
//    protected $collection = '';    //文档名
    protected $collection = 'users';     //文档名
    protected $primaryKey = '_id';    //设置id
    protected $fillable = ['id', 'name', 'phone'];  //设置字段白名单
    protected $dates = ['deleted_at']; //需要被转换成日期的属性。
}
