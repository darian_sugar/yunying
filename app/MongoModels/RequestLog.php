<?php

namespace App\MongoModels;

use App\Traits\Singleton;
use Illuminate\Database\Eloquent\Model;
use Jenssegers\Mongodb\Eloquent\Model as MongoModel;
use Jenssegers\Mongodb\Eloquent\SoftDeletes;

class RequestLog extends MongoModel
{

    use Singleton, SoftDeletes;

    protected $connection = 'mongodb';  //库名
//    protected $collection = '';    //文档名
    protected $collection = 'request_log';     //文档名
    protected $primaryKey = '_id';    //设置id
    protected $fillable = [
        'user_id',
        'user_type',
        'header',
        'ip_address',
        'method',
        'url',
        'param',
        'rq_time',
        'total_time',
        'response',
        ];  //设置字段白名单
    protected $dates = ['deleted_at']; //需要被转换成日期的属性。
}
