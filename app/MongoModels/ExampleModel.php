<?php

namespace App\MongoModels;

use Jenssegers\Mongodb\Eloquent\Model as MongoModel;
use Jenssegers\Mongodb\Eloquent\SoftDeletes;


class ExampleModel extends MongoModel
{

    /**
     * Indicates if the IDs are auto-incrementing.
     *
     * @var bool
     */
    public $incrementing = false;

    use SoftDeletes;

    protected $connection = 'mongodb';
    protected $collection = 'xxx表名';     //表名
    protected $primaryKey = 'id';    //设置主键
    protected $fillable = ['id', 'title', 'type', 'xx'];  //设置字段白名单


    /**
     * 需要被转换成日期的属性。
     *
     * @var array
     */

    protected $dates = ['deleted_at'];

}
