<?php

namespace App\Models;

use App\Helpers\Upload;
use App\Traits\Singleton;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserCart extends Model
{
    //
    use Singleton, SoftDeletes;

    protected $fillable = [
        'user_id',
        'goods_id',
        'num',
        'spec_id',
        'status',
    ];
    protected $hidden = [ 'created_at', 'deleted_at' ];


    public function belongsToGoods()
    {
        return $this->belongsTo( Goods::class, 'goods_id' );
    }

    public function belongsToSpec()
    {
        return $this->belongsTo( GoodsSpec::class, 'spec_id' );
    }
}
