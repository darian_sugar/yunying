<?php

namespace App\Models;

use App\Traits\Singleton;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Str;

class Question extends Model
{
    use Singleton, SoftDeletes;

    protected $fillable = [
        'p_id',
        'user_id',
        'content',
    ];
    protected $hidden = [ 'created_at', 'deleted_at' ];

    public function hasManyComments( $limit = 0 )
    {
        $model = $this->hasMany( QuestionComment::class, 'q_id' )->latest();
        if ( $limit )
            $model->limit( $limit );
        return $model;
    }

    public function latestComment()
    {
        return $this->hasOne( QuestionComment::class, 'q_id' )->latest( 'id' );
    }

    public function latestCommentUser()
    {
        return $this->hasManyComments()->select( 'q_id', 'user_id' )->groupBy( 'q_id', 'user_id' )->latest()->limit( 3 );
    }

    public function belongsToUser()
    {
        return $this->belongsTo( User::class, 'user_id' )->withTrashed();
    }


}
