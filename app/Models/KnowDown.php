<?php

namespace App\Models;

use App\Traits\Singleton;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class KnowDown extends Model
{
    //
    use Singleton, SoftDeletes;

    protected $fillable = [
        'course_id',
        'u_id',
        'from',
        'order_number',
        'pay_amount',
        'method_pay',
        'type',
    ];

    public function setPayAmountAttribute( $value )
    {
        $this->attributes['pay_amount'] = bcmul($value, 100);
    }

    public function getPayAmountAttribute( $value )
    {
        return $value ? floatval(bcdiv($value, 100, 2)) : 0;

    }

    public function belongsToCourse()
    {
        return $this->belongsTo( KnowCourse::class, 'course_id' )->withTrashed();
    }
}
