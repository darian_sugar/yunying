<?php

namespace App\Models;

use App\Helpers\Upload;
use App\Traits\Singleton;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class KnowCourse extends Model
{
    //
    use Singleton, SoftDeletes;

    protected $fillable = [
        'cate_id',// '分类ID',
        'image',// '图片',
        'title',// '标题',
        'content',// '内容',
        'author_id',// '讲师ID',
        'price',// '价格',
        'product_id',// 'ios对应product_id',
        'read',// '观看数',
        //        'status',// '1 启用 2 禁用',
        //        'check_status',// '0 待审核 1 审核通过 2 审核拒绝',
        //        'check_uid',// '审核人ID',
        //        'check_time',// '审核时间',
        'is_top',// '1 普通 2 热门',
        'from',// '1 admin 2 医生',
        'type',// '1 百科 2 学堂',
        'order_sort',// '排序',
        'duration',// '秒',
        'admin_id',// 'admin添加人 id',
    ];

    protected $hidden = [
        'deleted_at',
    ];
    public static $price = [
        [ 'price' => 0, 'product_id' => '0' ],
        [ 'price' => 1, 'product_id' => 'video1' ],
        [ 'price' => 3, 'product_id' => 'video3' ],
        [ 'price' => 6, 'product_id' => 'video6' ],
        [ 'price' => 12, 'product_id' => 'video12' ],
        [ 'price' => 18, 'product_id' => 'video18' ],
        [ 'price' => 25, 'product_id' => 'video25' ],
    ];
    public $checkStatusDesc = [
        0 => "待审核",
        1 => "已拒绝",
        2 => "已同意",
    ];

    public function getCateIdAttribute( $value )
    {
        if ( $this->getOriginal( 'type' ) == 2 )
            $this->append( 'cate_desc' );
        return $value;
    }

    public function getCateDescAttribute()
    {
        $value = $this->getOriginal( 'cate_id' );
        return KnowCate::BASE_CATE[ $value ];
    }

    public function getCheckStatusAttribute( $value )
    {
        $this->append( 'check_status_desc' );
        return $value;
    }

    public function getCheckStatusDescAttribute()
    {
        $value = $this->getOriginal( 'check_status' );
        return $this->checkStatusDesc[ $value ] ?? '未知';
    }

    /**
     * 课程->作者
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function belongsToCate()
    {
        return $this->belongsTo( KnowCate::class, 'cate_id' )->withTrashed();
    }

    public function getImageAttribute( $value )
    {
        $this->append( 'original_image' );
        return Upload::getInstance()->getPicName( $value, Upload::IMAGE );
    }

    public function getOriginalImageAttribute()
    {
        return $this->getOriginal( 'image' );
    }

    public function getContentAttribute( $value )
    {
        $this->append( 'full_content' );
        return $value;
    }

    public function getFullContentAttribute()
    {
        $value = $this->getAttribute( 'content' );
        return Upload::getInstance()->getPicName( $value, Upload::VIDEO );
    }

    public function setPriceAttribute( $value )
    {
        $this->attributes['price'] = bcmul( $value, 100 );
    }

    public function getPriceAttribute( $value )
    {
        return $value ? floatval( bcdiv( $value, 100, 2 ) ) : 0;
    }

    public function hasOneCollect()
    {
        return $this->hasOne( UserCollect::class, 'c_id' )->where( 'type', 2 );
    }

    public function belongsToAuthor()
    {
        return $this->belongsTo( Doctor::class, 'author_id' )->withTrashed();
    }

    public function getDurationAttribute( $value )
    {
        $this->append( 'duration_desc' );
        return $value;
    }

    public function getDurationDescAttribute()
    {
        $value = $this->getOriginal( 'duration', 0 );
        if ( $value >= 3600 )
            return gmstrftime( '%H:%M:%S', $value );
        else
            return gmstrftime( '%M:%S', $value );
    }

    public function hasManyDown()
    {
        return $this->hasMany( KnowDown::class, 'course_id' )->where( 'type', 2 );
    }


}
