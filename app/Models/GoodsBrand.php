<?php

namespace App\Models;

use App\Helpers\Upload;
use App\Traits\Singleton;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class GoodsBrand extends Model
{
    //
    use Singleton, SoftDeletes;

    protected $hidden = [ 'created_at', 'deleted_at' ];

}
