<?php

namespace App\Models;

use App\Traits\Singleton;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Str;

class Goods extends Model
{
    //
    use Singleton, SoftDeletes;

    protected $fillable = [
        'cate_id',// '分类 id',
        'name',// '名称',
        'code',// '货号',
        'intro',// '商品信息',
        'brand_id',// '品牌ID',
        'status',
        'key_words',
        'stock',
    ];
    protected $hidden = [ 'created_at', 'deleted_at' ];

    /**
     * Attribute start
     */

    public function setCodeAttribute( $value )
    {
        if ( !$value )
            $this->attributes['code'] = uuid();
        else
            $this->attributes['code'] = $value;
    }

    /**
     * Eloquent start
     */
    public function belongsToCate()
    {
        return $this->belongsTo( GoodsCate::class, 'cate_id' );
    }

    public function belongsToBrand()
    {
        return $this->belongsTo( GoodsBrand::class, 'brand_id' );
    }

    public function hasManyPhoto()
    {
        return $this->hasMany( GoodsPhoto::class );
    }

    public function hasMainPhoto()
    {
        return $this->hasOne( GoodsPhoto::class )->where( 'is_default', 1 );
    }

    public function hasManySpec()
    {
        return $this->hasMany( GoodsSpec::class );
    }

    public function hasMinPrice()
    {
        return $this->hasOne( GoodsSpec::class )->orderBy( 'price' );
    }

    public function hasManyOrder()
    {
        return $this->hasManyThrough(
            GoodsOrder::class,
            GoodsOrderDetail::class,
            'goods_id',       //中间表对主表的关联字段
            'id',             //远程表对中间表的关联字段
            'id',             //主表对中间表的关联字段
            'order_id'        //中间表对远程的关联字段
        );
    }
}
