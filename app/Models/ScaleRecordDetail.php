<?php

namespace App\Models;

use App\Traits\Singleton;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ScaleRecordDetail extends Model{
    use Singleton;
    protected $fillable = [
      'scale_record_id',
      'q_id',
      'a_id',
    ];
}
