<?php

namespace App\Models;

use App\Helpers\PayClass;
use App\Traits\Singleton;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

class GoodsOrder extends Model
{
    //
    use Singleton, SoftDeletes;

    protected $fillable = [];

    protected $hidden = [ 'deleted_at' ];

    static public $transDesc = [
        1 => '待发货',
        2 => '已发货',
        3 => '已签收',
        4 => '已完成',
    ];

    static public $orderDesc = [
        1 => '正常',
        2 => '取消订单',
        3 => '待审核',
        4 => '审核拒绝',
        5 => '审核完成'
    ];

    static public $payDesc = [
        0 => '待支付',
        1 => '已支付',
        2 => '半退款',
        3 => '全退款'
    ];

    /**
     * Attribut start
     */
    public function setTotalAmountAttribute( $value )
    {
        $this->attributes['total_amount'] = bcmul( $value, 100 );
    }

    public function getTotalAmountAttribute( $value )
    {
        return $value ? floatval( bcdiv( $value, 100, 2 ) ) : 0;
    }

    public function setPayAmountAttribute( $value )
    {
        $this->attributes['pay_amount'] = bcmul( $value, 100 );
    }

    public function getPayAmountAttribute( $value )
    {
        return $value ? floatval( bcdiv( $value, 100, 2 ) ) : 0;
    }

    public function setTransAmountAttribute( $value )
    {
        $this->attributes['trans_amount'] = bcmul( $value, 100 );
    }

    public function getTotalRefundAmountAttribute( $value )
    {
        return $value ? floatval( bcdiv( $value, 100, 2 ) ) : 0;
    }

    public function setTotalRefundAmountAttribute( $value )
    {
        $this->attributes['total_refund_amount'] = bcmul( $value, 100 );
    }

    public function getTransAmountAttribute( $value )
    {
        return $value ? floatval( bcdiv( $value, 100, 2 ) ) : 0;
    }

    public function getTransStatusAttribute( $value )
    {
        $this->append( 'trans_status_desc' );
        return $value;
    }

    public function getTransStatusDescAttribute()
    {
        $value = $this->getOriginal( 'trans_status' );
        return self::$transDesc[ $value ];
    }

    public function getOrderStatusAttribute( $value )
    {
        $this->append( 'order_status_desc' );
        return $value;
    }

    public function getOrderStatusDescAttribute()
    {
        $value = $this->getOriginal( 'order_status' );
        return self::$orderDesc[ $value ];
    }

    public function getMethodPayAttribute( $value )
    {
        $this->append( 'method_pay_desc' );
        return $value;
    }

    public function getMethodPayDescAttribute()
    {
        $value = $this->getOriginal( 'method_pay' );
        $pay = PayClass::PAYTYPE;
        return $pay[ $value ];
    }

    public function getPayStatusAttribute( $value )
    {
        $this->append( 'pay_status_desc' );
        return $value;
    }

    public function getPayStatusDescAttribute()
    {
        $value = $this->getOriginal( 'pay_status_desc' );
        return self::$payDesc[ $value ];

    }

    /**
     * Eloquent start
     */


    public function hasManyDetail()
    {
        return $this->hasMany( GoodsOrderDetail::class, 'order_id' );
    }


    public function belongsToUser()
    {
        return $this->belongsTo( User::class, 'user_id' )->withTrashed();
    }

    public function hasOneSubsidiary()
    {
        return $this->hasOne( GoodsOrderSubsidiary::class, 'order_id' );
    }

    public function hasOneAfterSales()
    {
        return $this->hasOne( GoodsAfterSales::class, 'order_id' )->latest();
    }
}
