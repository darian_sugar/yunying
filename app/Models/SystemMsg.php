<?php

namespace App\Models;

use App\Traits\Singleton;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;
use App\Helpers\Push;

class SystemMsg extends Model
{
    use Singleton, SoftDeletes;

    protected $fillable = [
        'title',
        'content',
        'to_id',
        'type',
        'source',
        'from_id',
        'msg_type',
    ];

    const ALL = 0;
    const USER = 1;
    const DOCTOR = 2;

    public function getStatusAttribute( $value )
    {
        $id = Auth::user()->id;
        if ( $this->to_id == 0 ) {
            $data = $this->hasMany( SystemMsgStatus::class, 'msg_id' )->where( 'to_id', $id )->first();
            return $data ? 1 : 0;
        } else {
            return $value;
        }
    }

    public function hasManyStatus()
    {
        $this->hasMany( SystemMsgStatus::class, 'msg_id' );
    }

    /**
     * 系统发送消息
     *
     * @param     $content
     * @param     $to_id
     * @param int $type 0 全部1 患者id 2 医生id
     *
     * @return $this
     */
    public function sendSysMsg( $content, $to_id, $type = 0, $title = '', $msg_type = 1 )
    {
        $this->content = $content;
        $this->to_id = $to_id;
        $this->type = $type;
        $this->source = 1;
        $this->title = $title;
        $this->msg_type = $msg_type;
        $this->save();
        return $this;
    }

    /**
     * @param     $title
     * @param     $content
     * @param     $to_id   接收者ID
     * @param     $type 1 患者 2 医生
     * @param int $from_id 发送者ID
     * @param int $source 1系统 2 患者 3 医生'
     *
     * @return $this
     */
    public function sendTo( $title, $content, $to_id, $type, $from_id = 0, $source = 1 )
    {
        $this->content = $content;
        $this->to_id = $to_id;
        $this->from_id = $from_id;
        $this->type = $type;
        $this->source = $source;
        $this->title = $title;
        $this->save();
        if ( $type == 1 )
            $push = new Push();
        else {
            $push = new Push( 2 );
        }
        $push->sendMsg( $title, $content, [ $to_id ] );
        return $this;
    }


    /**
     * 发送多条消息
     *
     * @param     $title
     * @param     $content
     * @param     $to_id   接收者ID
     * @param     $type 1 患者 2 医生
     * @param int $from_id 发送者ID
     * @param int $source 1系统 2 患者 3 医生'
     *
     * @return $this
     */
    public function sendToMore( $title, $content, $to_id, $type, $from_id = 0, $source = 1 )
    {
        $data = [];
        foreach ( $to_id as $k => $v ) {
            $data[ $k ]['content'] = $content;
            $data[ $k ]['to_id'] = $v;
            $data[ $k ]['from_id'] = $from_id;
            $data[ $k ]['type'] = $type;
            $data[ $k ]['source'] = $source;
            $data[ $k ]['title'] = $title;
        }
        self::insert( $data );
        if ( $type == 1 )
            $push = new Push();
        else {
            $push = new Push( 2 );
        }
        $push->sendMsg( $title, $content, $to_id );
        return $this;
    }

}
