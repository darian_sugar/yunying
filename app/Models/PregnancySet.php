<?php

namespace App\Models;

use App\Traits\Singleton;
use Illuminate\Database\Eloquent\Model;

class PregnancySet extends Model{

    use Singleton;


    public function addOrUpdate($data)
    {
        $model = self::getInstance()->where('user_id', $data['user_id'])->first();
        if (empty($model)) $model = new self();
        $model->user_id = $data['user_id'];
        $model->due_date = $data['due_date'];
        $model->predictive_due_date = $data['predictive_due_date'];
        $model->last_period = $data['last_period'];
        return $model->save();
    }
}
