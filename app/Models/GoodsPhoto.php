<?php

namespace App\Models;

use App\Helpers\Upload;
use App\Traits\Singleton;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class GoodsPhoto extends Model
{
    //
    use Singleton, SoftDeletes;

    protected $fillable = [
        'image',
        'is_default',
    ];
    protected $hidden = [ 'created_at', 'deleted_at' ];

    public function getImageAttribute( $value )
    {
        $this->append( 'original_image' );
        return Upload::getInstance()->getPicName( $value, Upload::IMAGE );
    }

    public function getOriginalImageAttribute()
    {
        return $this->getOriginal( 'image' );
    }
}
