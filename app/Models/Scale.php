<?php

namespace App\Models;

use App\Traits\Singleton;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Scale extends Model{

    use Singleton, SoftDeletes;

    protected $fillable = [
      'scale_name',
      'type',
      'introduce',
      'is_enable',
      'is_enable',
      'is_recommended',
    ];

    protected $hidden = [
      'created_at',
      'updated_at',
      'deleted_at',
    ];

    // 量表名称
    public function getTypeAttribute( $value )
    {
        $this->append('type_name');
        return $value;
    }

    // 量表名称
    public function getTypeNameAttribute()
    {
        $value = ScaleType::getInstance()->where('id', $this->type)->first(['type_name']);
        return $value->type_name ?? '';
    }


    // 量表 -- 量表问题
    public function hasManyQuestion()
    {
        return $this->hasMany( ScaleQuestion::class, 'scale_id' , 'id')
            ->orderByDesc( 'id' );
    }


    // 量表 -- 量表测量记录人数
    public function hasManyScaleRecord()
    {
        return $this->hasOne( ScaleRecord::class, 'scale_id' , 'id')
            ->select('user_id')->distinct();

    }
}
