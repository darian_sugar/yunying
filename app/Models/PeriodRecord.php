<?php

namespace App\Models;

use App\Traits\Singleton;
use Illuminate\Database\Eloquent\Model;

class PeriodRecord extends Model{

    use Singleton;

    public function addOrUpdate($data = [], $nextData = [])
    {
        if (!empty($data)) {
            $model = self::getInstance()->where(['user_id' => $data['user_id'], 'month' => $data['month']])
                ->orderByDesc('id')->first();
            if (empty($model)) $model = new self();
            $model->period_reality_date = $data['period_reality_date'];
            $model->period_reality_end_date = $data['period_reality_end_date'];
            $model->month = $data['month'];
            $model->user_id = $data['user_id'];
            $model->save();
        }

        if (!empty($nextData)) {
            $model = self::getInstance()->where(['user_id' => $nextData['user_id'], 'month' => $nextData['month']])
                ->orderByDesc('id')->first();
            if (empty($model)) $model = new self();
            $model->period_predictive_date = $nextData['period_predictive_date'];
            $model->period_predictive_end_date = $nextData['period_predictive_end_date'];
            $model->month = $nextData['month'];
            $model->user_id = $nextData['user_id'];
            return $model->save();
        }
    }
}
