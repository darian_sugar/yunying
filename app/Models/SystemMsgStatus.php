<?php

namespace App\Models;

use App\Traits\Singleton;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SystemMsgStatus extends Model
{
    //
    use Singleton, SoftDeletes;
}
