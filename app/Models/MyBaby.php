<?php

namespace App\Models;

use App\Helpers\Upload;
use App\Services\Common\CommonService;
use App\Traits\Singleton;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

class MyBaby extends BaseModel
{

    use Singleton, SoftDeletes;

    protected $table = 'my_baby';

    protected $hidden = [
        'created_at', 'updated_at', 'deleted_at'
    ];

    public function getBbBirthdayAttribute($value)
    {
        $this->append( 'bb_constellation' );
        return $value;
    }

    public function getBbConstellationAttribute()
    {
        $birthday = $this->getOriginal( 'bb_birthday' );
        return CommonService::getConstellation($birthday);
    }


    public function setAdd($data)
    {
        $default            = self::getInstance()->where(['user_id' => $data['user_id'], 'bb_default' => 1])->first();
        $model              = new self();
        $model->user_id     = $data['user_id'];
        $model->bb_sex      = $data['bb_sex'];
        $model->bb_nickname = '宝宝';
        $model->bb_birthday = $data['bb_birthday'];
        if (!empty($default)) $model->bb_default = 2;
        return $model->save();
    }


    public function getBbPicAttribute( $value )
    {
        $this->append( 'bb_original_head_pic' );
        return $value ? Upload::getInstance()->getPicName( $value, Upload::HEADIMG ) : '';
    }

    public function getBbOriginalHeadPicAttribute()
    {
        return $this->getOriginal( 'bb_pic' );
    }


    public function addAndUpdate($data)
    {
        $default =  self::getInstance()->where(['bb_default' => 1, 'user_id' => $data['user_id']])->first();
        $bb_id = $data['bb_id'] ?? '';
        if (!empty($bb_id)) {
            unset($data['bb_id']);
            $model = self::getInstance()->where('id', $bb_id)->first();
        } else {
            $model = new self();
        }
//        $data = array_filter($data, function ($v) {
//            if (is_null($v)) {
//                return false;
//            } else {
//                return true;
//            }
//        });
        $data = collect($data)->filter(function($value, $key) {
            if (is_null($value)) {
                return false;
            } else {
                return true;
            }
        });
        if (!empty($default)) $model->bb_default = 2;
        foreach ($data as $key => $val) {
            $model->$key = $val;
        }
//        $model->bb_pic = $data['bb_pic'];
//        $model->user_id = $data['user_id'];
//        $model->bb_sex = $data['bb_sex'];
//        $model->bb_birthday = $data['bb_birthday'];
//        $model->bb_nickname = $data['bb_nickname'];
//        $model->relation = $data['relation'];
//        $model->bb_height = $data['bb_height'];
//        $model->bb_weight = $data['bb_weight'];
        return $model->save();
    }
}
