<?php

namespace App\Models;

use App\Traits\Singleton;
use Illuminate\Database\Eloquent\Model;

class PeriodSet extends Model{

    use Singleton;

    protected $fillable = [
      'length',
      'cycle',
      'user_id',
      'month',
      'menstruation_date',
      'is_predictive',
    ];

    public function addOrUpdate($data)
    {
        $model = self::getInstance()->where('user_id', $data['user_id'])->first();
        if (empty($model)) $model = new self();
        $model->user_id = $data['user_id'];
        $model->month = date('Y-m', strtotime($data['menstruation_date']));
        $model->length = $data['length'];
        $model->cycle = $data['cycle'];
        $model->menstruation_date = $data['menstruation_date'];
        $model->is_predictive = $data['is_predictive'];
        return $model->save();
    }
}
