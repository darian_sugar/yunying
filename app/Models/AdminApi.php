<?php

namespace App\Models;

use App\Traits\Singleton;
use Illuminate\Database\Eloquent\Model;

class AdminApi extends Model
{
    //
    use Singleton;

    const COMMENT_API = [
        [ 'url' => '/api/c3/home/access', 'method' => 'get' ],//获取权限
        [ 'url' => '/api/c3/home/index', 'method' => 'get' ],//获取资料
        [ 'url' => '/api/c3/home/index', 'method' => 'post' ],//更新资料
        [ 'url' => '/api/c3/home/updatePwd', 'method' => 'post' ],//更新密码
        [ 'url' => '/api/c3/user/loginOut', 'method' => 'post' ],//退出
        [ 'url' => '/api/c3/common/uploadFile', 'method' => 'post' ],//上传
        [ 'url' => '/api/c3/hospital/province_list', 'method' => 'get' ],//省市区
    ];

}
