<?php

namespace App\Models;

use App\Traits\Singleton;
use Illuminate\Database\Eloquent\Model;

class ScaleQuestion extends Model{

    use Singleton;

    protected $fillable = [
        'question',
        'sort',
        'scale_id',
        'prompt',
        'risk_level',
        'risk_warning',
        'second_type',
        'q_type',
    ];

    /**
     * 问题--答案
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function hasManyAnswer()
    {
        return $this->hasMany( ScaleAnswer::class, 'q_id' );
    }
}
