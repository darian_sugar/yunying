<?php

namespace App\Models;

use App\Helpers\Upload;
use App\Traits\Singleton;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Suggest extends Model
{
    //
    use Singleton, SoftDeletes;

    protected $fillable = [
        'id',
        'user_id',
        'content',
        'image',
        'flag',
        'type',
    ];


    public $typeDesc = [
        1 => '产品操作意见',
        2 => '产品改善意见',
        3 => '建议新增领域'
    ];
    public $typeArrDesc = [
        [ 'id' => 1, 'title' => '产品操作意见' ],
        [ 'id' => 2, 'title' => '产品改善意见' ],
        [ 'id' => 3, 'title' => '建议新增领域' ],
    ];

    /**
     * Attribute start
     */


    public function setImageAttribute( $value )
    {
        $this->attributes['image'] = implode( ',', $value );
    }

    public function getImageAttribute( $value )
    {
        $imgs = $value ? explode( ',', $value ) : [];
        array_walk( $imgs, function ( &$value ) {
            $value = Upload::getInstance()->getPicName( $value, Upload::IMAGE );
        } );
        return $imgs;
    }

    public function getTypeAttribute( $value )
    {
        $this->append( 'type_desc' );
        return $value;
    }

    public function getTypeDescAttribute()
    {
        $value = $this->getOriginal( 'type' );
        return $this->typeDesc[ $value ] ?? '未知';
    }

    public function belongsToOwner()
    {
        $source = $this->getOriginal( 'source' );
        if ( $source == 2 ) {
            return $this->belongsToDoctor();
        } elseif ( $source == 1 ) {
            return $this->belongsToUser();
        }
    }

    public function belongsToDoctor()
    {
        return $this->belongsTo( Doctor::class, 'user_id' );
    }

    public function belongsToUser()
    {
        return $this->belongsTo( User::class, 'user_id' );
    }
}
