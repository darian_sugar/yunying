<?php

namespace App\Models;

use App\Traits\Singleton;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Appointment extends Model
{
    //
    use Singleton, SoftDeletes;

    protected $fillable = [
        'user_id',
        'doctor_id',
        'schedule_id',
        'order_number',
        'total_amount',
        'order_status',
        'method_pay',
        'cate_id',
        'cate_desc',
        'date',
        'start_time',
        'end_time',
        //        'check_status',
        //        'check_uid',
        'reply',
    ];
    protected $hidden = [ 'created_at', 'deleted_at' ];


    public $checkDesc = [
        0 => '待审核',
        1 => '已预约',
        1 => '已完成',
    ];


    public function setTotalAmountAttribute( $value )
    {
        $this->attributes['total_amount'] = bcmul( $value, 100 );
    }

    public function getTotalAmountAttribute( $value )
    {
        return $value ? floatval( bcdiv( $value, 100, 2 ) ) : 0;
    }

    public function getCheckStatusAttribute( $value )
    {
        $this->append( 'check_status_desc' );
        return $value;
    }

    public function getCheckStatusDescAttribute()
    {
        $value = $this->getOriginal( 'check_status' );
        return $this->checkDesc[ $value ];
    }

    public function getDateAttribute( $value )
    {
        $this->append( 'can_cancel' );
        return $value;
    }

    public function getCanCancelAttribute()
    {
        $date = $this->getAttribute( 'date' );
        $prevDay = Carbon::parse( $date )->subDay()->toDateString();
        $today = date( 'Y-m-d' );
        if ( $prevDay > $today || ( $prevDay == $today && date( 'H' ) <= 16 ) )
            return true;
        else
            return false;

    }

    /**
     * Eloquent start
     */
    public function belongsToSchedule()
    {
        return $this->belongsTo( DoctorSchedule::class, 'schedule_id' )->withTrashed();
    }

    public function belongsToDoctor()
    {
        return $this->belongsTo( Doctor::class, 'doctor_id' )->withTrashed();
    }

    public function belongsToUser()
    {
        return $this->belongsTo( User::class, 'user_id' )->withTrashed();
    }


}
