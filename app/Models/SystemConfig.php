<?php

namespace App\Models;

use App\Traits\Singleton;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SystemConfig extends Model
{
    //
    use Singleton, SoftDeletes;

    protected $fillable = [
        'admin_id',
        'name',
        'desc',
        'value',
    ];

    static public function getValue( $name, $default = 15 )
    {
        return self::where('name', $name)->value('value') ?? $default;
    }
}
