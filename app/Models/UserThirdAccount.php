<?php

namespace App\Models;

use App\Traits\Singleton;
use App\Helpers\Upload;
use function foo\func;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Hash;

class UserThirdAccount extends Model
{
    use Singleton; use SoftDeletes;
}
