<?php

namespace App\Models;

use App\Traits\Singleton;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class RolePriRelevance extends Model
{
    //
    use SoftDeletes, Singleton;

    protected $fillable = [
        'r_id',
        'type',
        'p_id',
        'is_api',
    ];

    public function setPIdAttribute($value)
    {
        $this->attributes['p_id'] = implode(',', $value);
    }

    public function getPIdAttribute($value)
    {
        return explode(',', $value);
    }
}
