<?php

namespace App\Models;

use App\Traits\Singleton;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class GoodsOrderSubsidiary extends Model
{
    use Singleton, SoftDeletes;

    protected $fillable = [
        'order_id',// 订单ID,
        'name',// 收货人姓名,
        'province_id',// 省ID,
        'area_id',// 区ID,
        'city_id',// 城市ID,
        'addr',
        'mobile',
    ];

    protected $hidden = [ 'created_at', 'deleted_at' ];


    /**
     * Eloquent start
     */
    public function belongsToArea()
    {
        return $this->belongsTo( Area::class, 'area_id' );
    }

    public function belongsToCity()
    {
        return $this->belongsTo( Area::class, 'city_id' );
    }

    public function belongsToProvince()
    {
        return $this->belongsTo( Area::class, 'province_id' );
    }
}
