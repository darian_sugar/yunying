<?php

namespace App\Models;

use App\Helpers\Upload;
use App\Traits\Singleton;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class GoodsCate extends Model
{
    //
    use Singleton, SoftDeletes;

    protected $fillable = [
        'name',
        'p_id',
        'level',
        'status',
        'icon',
        'desc',
        'parnets',
    ];
    protected $hidden = [ 'created_at', 'deleted_at' ];

    public function getIconAttribute( $value )
    {
        $this->append( 'original_icon' );
        return $value;
    }

    public function getOriginalIconAttribute()
    {
        $value = $this->getOriginal( 'icon' );
        return Upload::getInstance()->getPicName( $value, Upload::IMAGE );

    }


    public function hasManyGoods()
    {
        return $this->hasMany( Goods::class, 'cate_id' );
    }
}
