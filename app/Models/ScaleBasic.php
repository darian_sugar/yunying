<?php

namespace App\Models;

use App\Traits\Singleton;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ScaleBasic extends Model{

    use Singleton;

    protected $fillable = [
        'user_id'   ,
        'baby_name'   ,
        'baby_month'  ,
        'baby_sex'    ,
        'native_place',
        'province_id' ,
        'city_id'     ,
        'area_id'     ,
        'blood_type'  ,
        'mother_age'  ,
    ];

}
