<?php

namespace App\Models;

use App\Traits\Singleton;
use Illuminate\Database\Eloquent\Model;

class ScaleQuestionType extends Model
{
    //
    use Singleton;

    protected $hidden = [
      'created_at',
      'updated_at',
      'deleted_at',
    ];
}
