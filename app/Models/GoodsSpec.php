<?php

namespace App\Models;

use App\Helpers\Upload;
use App\Traits\Singleton;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class GoodsSpec extends Model
{
    //
    use Singleton, SoftDeletes;

    protected $fillable = [
        'goods_id',// 商品ID
        'num',// 商品数量,
        'packing_unit',// 包装单位,
        'weight_unit',// 计量单位,
        'price',// 价格,
        'stock',// 库存,
        'stock_warn',// 库存预警,
        'weight_num',
        'code',
    ];
    protected $hidden = [ 'created_at', 'deleted_at' ];

    /**
     * Attribute start
     */
    public function setCodeAttribute( $value )
    {
        if ( !$value )
            $this->attributes['code'] = uuid();
        else
            $this->attributes['code'] = $value;

    }

    public function getSpecAttribute()
    {
        return $this->getOriginal( 'weight_num' ) . '  / ' . $this->getOriginal( 'weight_unit' ) . '*' . $this->getOriginal( 'num' ) . '/' . $this->getOriginal( 'packing_unit' );
    }

    public function getNumAttribute( $value )
    {
        $this->append( [ 'spec' ] );
        return $value;
    }

    public function getPriceAttribute( $value )
    {
        return $value ? floatval( bcdiv( $value, 100, 2 ) ) : 0;
    }

    public function setPriceAttribute( $value )
    {
        $this->attributes['price'] = bcmul( $value, 100 );
    }


}
