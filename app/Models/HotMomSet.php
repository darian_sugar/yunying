<?php

namespace App\Models;

use App\Traits\Singleton;
use Illuminate\Database\Eloquent\SoftDeletes;

class HotMomSet extends BaseModel {

    use Singleton , SoftDeletes;

    public function addAndUpdate($data)
    {
        $model = self::getInstance()->where('user_id', $data['user_id'])->first();
        if (empty($model)) $model = new self();
        $model->user_id = $data['user_id'];
        $model->bb_sex = $data['bb_sex'];
        $model->bb_birthday = $data['bb_birthday'];
        $model->length = $data['length'];
        $model->cycle = $data['cycle'];
        $model->menstruation_date = $data['menstruation_date'];
        return $model->save();
    }
}
