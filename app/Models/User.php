<?php

namespace App\Models;

use App\Helpers\Upload;
use App\Traits\Singleton;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;

class User extends Model
{

    use Singleton, SoftDeletes;

    //必填 数据 过滤
    protected $fillable = [
        'nickname',
        'email',
        'head_pic',
        'sex',
        'accid ',
        'birthday',
        'api_token',
    ];
    protected $hidden = [ 'password', 'api_token', 'deleted_at' ];

    public $identityDesc = [
        0 => '未知', 1 => '备孕', 2 => '怀孕', 3 => '宝妈'
    ];
    public $identityArrDesc = [
        [ 'id' => 1, 'title' => '备孕' ],
        [ 'id' => 2, 'title' => '怀孕' ],
        [ 'id' => 3, 'title' => '宝妈' ],
    ];
    public $sexDesc = [
        1 => '男', 2 => '女', 0 => '未知'
    ];

    public function UserHasMany( $model )
    {
        return $this->hasMany( $model, 'user_id' );
    }

    public function UserHasOne( $model )
    {
        return $this->hasOne( $model, 'user_id' );
    }

    public function getHeadPicAttribute( $value )
    {
        $this->append( 'original_head_pic' );
        return $value ? Upload::getInstance()->getPicName( $value, Upload::HEADIMG ) : '';
    }

    public function getOriginalHeadPicAttribute()
    {
        return $this->getOriginal( 'head_pic' );
    }


    public function getByMobile( $mobile )
    {
        return $this->where( 'mobile', $mobile )->first();
    }


    public function saveToken()
    {
        $token = create_token( 1 );
        $this->api_token = $token;
        return $this->save();
    }


    /**
     * 更新手机号
     * @param $id
     * @param $mobile
     * @return bool
     */
    public function updateMobile( $id, $mobile )
    {
        $user = self::withTrashed()->find( $id );
        $user->mobile = $mobile;
        $res = $user->save();
        return $res;
    }


    public function getUserInfo( $id )
    {
        $data = $this
            ->find( $id );
        return $data;
    }

    public function hasManyThirdAccount()
    {
        return $this->hasMany( UserThirdAccount::class, 'user_id' );
    }


    /**
     * 用户 -- 收藏
     * @return HasMany
     */
    public function hasManyCollect()
    {
        return $this->hasMany( UserCollect::class, 'u_id' )->where( 'from', 1 );
    }


    /**
     * 用户 -- 问诊订单
     * @return HasMany
     */
    public function hasManyInquiry()
    {
        return $this->hasMany( InquiryOrder::class, 'user_id' )->where( [ 'pay_status' => 1, 'order_status' => 1, ] );
    }

    public function latestInquiry()
    {
        return $this->hasOne( InquiryOrder::class, 'user_id' )->latest( 'id' )->where( [ 'pay_status' => 1, 'order_status' => 1, ] );
    }

    public function latestAppoint()
    {
        return $this->hasOne( Appointment::class, 'user_id' )->latest( 'id' )->where( [ 'pay_status' => 1 ] )->whereIn( 'order_status', [ 1, 3 ] );
    }

    /**
     * 用户 ---- 线下预约
     * @return HasMany
     */
    public function hasManyAppoint()
    {
        return $this->UserHasMany( Appointment::class );
    }

    /**
     * 用户 医生评论
     * @return HasMany
     */
    public function hasManyComment()
    {
        return $this->UserHasMany( DoctorComment::class );
    }

    public function getIdentityAttribute( $value )
    {
        $this->append( 'identity_desc' );
        return $value;
    }

    public function getIdentityDescAttribute()
    {
        $value = $this->getOriginal( 'identity' );
        return $this->identityDesc[ $value ];
    }

    public function getSexAttribute( $value )
    {
        $this->append( 'sex_desc' );
        return $value;
    }

    public function getSexDescAttribute()
    {
        $value = $this->getOriginal( 'sex' );
        return $this->sexDesc[ $value ];
    }

    public function hasManyCartGoods()
    {
        return $this->hasMany( UserCart::class )->where( 'status', 1 );
    }

    public function getByAccid( $accid )
    {
        return self::where( 'accid', $accid )->first();
    }


}
