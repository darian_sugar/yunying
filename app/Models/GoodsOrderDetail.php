<?php

namespace App\Models;

use App\Helpers\Upload;
use App\Traits\Singleton;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class GoodsOrderDetail extends Model
{
    //
    use Singleton, SoftDeletes;

    protected $fillable = [
        //'order_id',// 订单ID,
        'cart_id',// 购物车ID,
        'goods_id',// 商品ID,
        'goods_name',// 商品名称,
        'spec_id',// 规格ID,
        'spec_name',// 规格名称,
        'price',// 单价,
        'num',// 数量,
        'total_price',// 总价,
        'image',// 主图,
        'goods_code',//商品货号
    ];

    /**
     * Attribute start
     */


    public function setTotalPriceAttribute( $value )
    {
        $this->attributes['total_price'] = bcmul( $value, 100 );
    }

    public function getTotalPriceAttribute( $value )
    {
        return $value ? floatval( bcdiv( $value, 100, 2 ) ) : 0;
    }

    public function getPriceAttribute( $value )
    {
        return $value ? floatval( bcdiv( $value, 100, 2 ) ) : 0;
    }

    public function setPriceAttribute( $value )
    {
        $this->attributes['price'] = bcmul( $value, 100 );
    }


    public function getImageAttribute( $value )
    {
        $this->append( 'original_image' );
        return Upload::getInstance()->getPicName( $value, Upload::IMAGE );
    }

    public function getOriginalImageAttribute()
    {
        return $this->getOriginal( 'image' );
    }


    /**
     * Eloquent start
     */

    public function belongsToGoods()
    {
        return $this->belongsTo( Goods::class, 'goods_id' )->withTrashed();
    }

    public function belongsToGoodsSpec()
    {
        return $this->belongsTo( GoodsSpec::class, 'spec_id' )->withTrashed();
    }
}
