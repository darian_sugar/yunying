<?php

namespace App\Models;

use App\Helpers\Upload;
use App\Traits\Singleton;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Message extends Model
{
    //

    use Singleton, SoftDeletes;

    protected $fillable = [
        'record_id',
        'content',
        'user_id',
        'to_id',
        'type',
        'class',
    ];

    protected $hidden = [ 'deleted_at' ];

    const TEXT = 1;//图文问诊

    public function getContentAttribute( $value )
    {
        if( !$value ) return '';
        $type = $this->getOriginal('type');
        if( $type == 1 )
            return $value;
        elseif( $type == 2 )
            return Upload::getInstance()->getPicName($value, Upload::IMAGE);
        else
            return '';
    }


    public function getTextRecord( $doctorId, $pageSize )
    {
        $data = [];
        $dataOne = self::withTrashed()
            ->where([ 'user_id' => $doctorId, 'class' => config('code.by_doctor') ])
            ->get([ 'to_id as patient_id', 'id' ])
            ->toArray();

        $userOne = array_unique(array_column($dataOne, 'patient_id'));

        $dataTwo = self::withTrashed()
            ->where([ 'to_id' => $doctorId, 'class' => config('code.by_patient') ])
            ->get('user_id as patient_id', 'id')
            ->toArray();

        $userTwo = array_unique(array_column($dataTwo, 'patient_id'));

        //此医生下的所有沟通用户ID
        $userAll = array_unique(array_merge($userOne, $userTwo));

        $class = '';
        foreach( $userAll as $k => $v ){
            $byPatientId = self::withTrashed()
                ->where([ 'user_id' => $v, 'to_id' => $doctorId, 'class' => config('code.by_patient') ])
                ->orderBy('id', 'DESC')
                ->first([ 'id', 'class' ]);
            //            var_dump($byPatientId);
            $byDoctorId = self::withTrashed()
                ->where([ 'user_id' => $doctorId, 'to_id' => $v, 'class' => config('code.by_doctor') ])
                ->orderBy('id', 'DESC')
                ->first([ 'id', 'class' ]);
            //获取最后一条聊天记录ID和class类型
            if( empty($byPatientId) ){
                $id = $byDoctorId->id;
                $class = $byDoctorId->class;
            }else{
                if( empty($doctorId) ){
                    $id = $byPatientId->id;
                    $class = $byPatientId->class;
                }else{
                    if( $byDoctorId->id >= $byPatientId->id ){
                        $id = $byDoctorId->id;
                        $class = $byDoctorId->class;
                    }else{
                        $id = $byPatientId->id;
                        $class = $byPatientId->class;
                    }
                }
            }

            $ids[$k] = $id;
        }

        //根据ID和类型查询用户信息和聊天内容
        if( $class == config('code.by_doctor') ){
            $data = self::withTrashed()
                ->select([ 'm.id', 'm.content', 'm.class', 'm.type', 'm.created_at', 'u.name', 'u.head_pic' ])
                ->from('messages as m')
                ->join('users as u', 'u.id', '=', 'm.to_id', 'left')
                ->whereIn('m.id', $ids)
                ->orderBy('created_at', 'DESC')
                ->paginate($pageSize);
        }else{
            $data = self::withTrashed()
                ->select([ 'm.id', 'm.content', 'm.class', 'm.type', 'm.created_at', 'u.name', 'u.head_pic' ])
                ->from('messages as m')
                ->join('users as u', 'u.id', '=', 'm.user_id', 'left')
                ->whereIn('m.id', $ids)
                ->orderBy('created_at', 'DESC')
                ->paginate($pageSize);
        }

        return $data;
    }


    /**
     * 消息列表
     *
     * @param $where
     * @param $pageSize
     *
     * @return mixed
     */
    public function getMsgList( $where, $pageSize )
    {
        return $this->where($where)->orderByDesc('id')->paginate($pageSize);
    }

    public function belongsToSend()
    {
        return $this->belongsTo( Doctor::class, 'user_id' );
    }

}
