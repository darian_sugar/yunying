<?php

namespace App\Models;

use App\Traits\Singleton;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class GoodsAfterSalesDetail extends Model
{
    //
    use Singleton, SoftDeletes;

    protected $fillable = [
        'after_sales_id',// 售后单ID,
        'goods_id',// 商品id,
        'num',// 退货数量,
        'price',// 退货金额,
    ];
}
