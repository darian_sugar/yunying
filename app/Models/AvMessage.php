<?php

namespace App\Models;

use App\Helpers\Upload;
use App\Traits\Singleton;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AvMessage extends Model
{
    //

    use Singleton, SoftDeletes;

    protected $fillable = [
        'record_id',
        'content',
        'user_id',
        'to_id',
        'type',
        'start_time',
        'end_time',
        'duration',
        'to_duration',
    ];

    protected $appends = [];

    public function getVoiceOrVideoRecord( $doctorId, $type )
    {
        $db = self::withTrashed();
        $data = $db->from( 'av_messages as m' )->join( 'users as u1', 'u1.id', '=', 'm.user_id', 'left' )
            ->where( 'm.type', $type )
            ->where( 'm.user_id', $doctorId )
            ->where( 'm.to_id', $doctorId, 'or' )
            ->paginate();
        return $data;
    }

    public function hasOneFile()
    {
        return $this->hasOne( AvMessageFile::class, 'channelId', 'channelId' )->orderByDesc( 'caller' );
    }


}
