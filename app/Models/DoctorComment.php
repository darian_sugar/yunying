<?php

namespace App\Models;

use App\Traits\Singleton;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DoctorComment extends Model
{
    //
    use SoftDeletes, Singleton;

    protected $fillable = [
        'content',
        'doctor_id',
        'user_id',
        'score',
    ];
    protected $hidden   = [ 'created_at', 'deleted_at' ];

    public function belongsToUser()
    {
        return $this->belongsTo( User::class, 'user_id' );
    }
}
