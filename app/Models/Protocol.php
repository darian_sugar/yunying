<?php

namespace App\Models;

use App\Traits\Singleton;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Protocol extends Model
{
    //
    use Singleton, SoftDeletes;
    protected $fillable = [
        'title',
        'content',
        'type',
        'admin_id',
        'status',
    ];

    /**
     * 操作人
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function belongsToOperator()
    {
        return $this->belongsTo( Admin::class, 'admin_id' )->withTrashed();
    }

    /**
     * 审核人
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function belongsToCheckInfo()
    {
        return $this->belongsTo( Admin::class, 'check_uid' )->withTrashed();
    }

    /**
     * 协议用户已读
     */
    public function hasOneRead()
    {
        return $this->hasOne( ProtocolRead::class, 'protocol_id' );
    }

    /**
     * @param int $type 1 注册 2 隐私
     * @return mixed
     */
    public function getLatest( $type = 2 )
    {
        $protocol = Protocol::getInstance()->where([ 'status' => 1, 'type' => $type, 'check_status' => 1 ])->latest()->first();
        return $protocol;
    }
}
