<?php

namespace App\Models;

use App\Traits\Singleton;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class KnowCate extends Model
{
    //
    use SoftDeletes, Singleton;

    protected $fillable = [
        'name',
        'admin_id',
        'status',
        'p_id',
        'channel_id',
        'type',
    ];
    protected $hidden = [
        'created_at',
        'deleted_at'
    ];

    const BASE_CATE = [
        2 => '备孕',
        1 => '怀孕',
        3 => '产后',
        4 => '育儿',
    ];
    static public $base_cate_arr = [
        [ 'id' => 2, 'name' => '备孕' ],
        [ 'id' => 1, 'name' => '怀孕' ],
        [ 'id' => 3, 'name' => '产后' ],
        [ 'id' => 4, 'name' => '育儿' ],
    ];

    public function getPNameAttribute()
    {
        $value = $this->getAttribute( 'p_id' );
        return self::BASE_CATE[ $value ];
    }

    public function getPIdAttribute( $value )
    {
        $this->append( 'p_name' );
        return $value;
    }

    public function belongsToChannel()
    {
        return $this->belongsTo( KnowCate::class, 'channel_id' );
    }

    public function belongsToDoctor()
    {
        return $this->belongsTo( Doctor::class, 'u_id' );
    }

    public function hasManyChildren()
    {
        return $this->hasMany( KnowCate::class, 'channel_id', '' );
    }


}
