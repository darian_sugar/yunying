<?php

namespace App\Models;

use App\Traits\Singleton;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AdminRoleRelevance extends Model
{
    //
    use Singleton, SoftDeletes;

    protected $fillable = [
        'a_id',
        'r_id',
    ];

    public function belongsToRole()
    {
        return $this->belongsTo( Role::class, 'r_id' )->withTrashed();
    }
}
