<?php

namespace App\Models;

use App\Traits\Singleton;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Role extends Model
{
    //
    use  Singleton, SoftDeletes;

    protected $fillable = [
        'r_name',
        'r_type',
        'store_id',
        'memo',
    ];

    static public $admin = 1;
    static public $drug = 2;

    public function hasManyAdmin()
    {
        return $this->hasMany( AdminRoleRelevance::class, 'r_id' );
    }


    public function hasManyPri()
    {
        return $this->hasMany(RolePriRelevance::class, 'r_id')->where('type', 1);
    }
}
