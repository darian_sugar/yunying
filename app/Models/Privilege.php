<?php

namespace App\Models;

use App\Traits\Singleton;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Privilege extends Model
{
    //
    use Singleton, SoftDeletes;

    protected $fillable = [
        'icon',
        'title',
        'p_name',
        'status',
        'pid',
        'level',
        'order_sort',
        'p_type',
    ];

    public function hasManyChildren()
    {
        return $this->hasMany( Privilege::class, 'pid' )->where( 'status', 1 );
    }

    public function hasManyApis()
    {
        return $this->hasMany( AdminApi::class, 'pri_id' )->where( 'status', 1 );
    }
}
