<?php


namespace App\Models;

use App\Traits\Singleton;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class QuestionComment extends Model
{
    use Singleton, SoftDeletes;

    protected $fillable = [
        'q_id',
        'user_id',
        'comment',
    ];
    protected $hidden   = [ 'created_at', 'deleted_at' ];

    public function belongsToUser()
    {
        return $this->belongsTo( User::class, 'user_id' );
    }

    public function belongsToQuestion()
    {
        return $this->belongsTo( Question::class, 'q_id' );
    }
}
