<?php

namespace App\Models;

use App\Traits\Singleton;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AvMessageFile extends Model
{
    //
    use Singleton, SoftDeletes;

    protected $fillable = [
        'channelId',
        'filename',
        'size',
        'type',
        'url',
        'user_id',
        'vid',
        'caller',
    ];
}
