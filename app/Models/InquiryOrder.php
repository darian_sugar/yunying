<?php

namespace App\Models;

use App\Helpers\PayClass;
use App\Helpers\Upload;
use App\Traits\Singleton;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class InquiryOrder extends Model
{
    //
    use Singleton, SoftDeletes;

    protected $fillable = [
        'user_name',
        'sex',
        'age',
        'illness',
        'image',
        'type',
        'user_id',
        'doctor_id',
        'record_id',
        'method_pay',
        'total_amount',
        'pay_amount',
        'order_number',
    ];

    //protected $casts = [
    //    'updated_at' => 'date:i:s',
    //];
    public $typeDesc = [
        1 => '图文',
        2 => '语音',
        3 => '视频',
    ];

    protected $hidden = [ 'deleted_at' ];

    public function setTotalAmountAttribute( $value )
    {
        $this->attributes['total_amount'] = bcmul( $value, 100 );
    }

    public function getTotalAmountAttribute( $value )
    {
        return $value ? floatval( bcdiv( $value, 100, 2 ) ) : 0;

    }

    public function setPayAmountAttribute( $value )
    {
        $this->attributes['pay_amount'] = bcmul( $value, 100 );
    }

    public function getPayAmountAttribute( $value )
    {
        return $value ? floatval( bcdiv( $value, 100, 2 ) ) : 0;
    }

    public function getTypeAttribute( $value )
    {
        $this->append( [ 'type_desc' ] );
        return $value;
    }

    public function getTypeDescAttribute()
    {
        $value = $this->getOriginal( 'type' );
        return $this->typeDesc[ $value ];
    }

    public function setImageAttribute( $value )
    {
        $this->attributes['image'] = implode( ',', $value );
    }

    public function getImageAttribute( $value )
    {
        $imgs = $value ? explode( ',', $value ) : [];
        array_walk( $imgs, function ( &$value ) {
            $value = Upload::getInstance()->getPicName( $value, Upload::IMAGE );
        } );
        return $imgs;
    }


    public function getMethodPayAttribute( $value )
    {
        $this->append( 'method_pay_desc' );
        return $value;
    }

    public function getMethodPayDescAttribute()
    {
        $value = $this->getOriginal( 'method_pay' );
        $pay = PayClass::PAYTYPE;
        return $pay[ $value ];
    }

    /**
     * Eloquent start
     */

    public function belongsToUser()
    {
        return $this->belongsTo( User::class, 'user_id' );
    }

    public function belongsToDoctor()
    {
        return $this->belongsTo( Doctor::class, 'doctor_id' );
    }

    /**
     * 音视频聊天记录
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function hasOneAvMsg()
    {
        return $this->hasOne( AvMessage::class, 'order_id' );
    }

    /**
     * 医生 问诊记录
     *
     * @param      $related
     * @param null $foreignKey
     * @param null $localKey
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function doctorRecord()
    {
        return $this->hasOne( UserDoctorRecord::class, 'doctor_id', 'doctor_id' );
    }

    /**
     * 患者 问诊记录 过期时间
     *
     * @param      $related
     * @param null $foreignKey
     * @param null $localKey
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function userRecord()
    {
        return $this->hasOne( UserDoctorRecord::class, 'user_id', 'user_id' );
    }


    /**
     * 用户-好友申请列表
     */
    public function hasManyThroughDepartment()
    {
        return $this->hasOneThrough( Department::class, Doctor::class, 'doctor_id', 'd_id' );
    }


}
