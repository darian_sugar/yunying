<?php

namespace App\Models;

use App\Traits\Singleton;
use App\Helpers\Upload;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Adv extends Model
{
    use Singleton, SoftDeletes;

    protected $fillable = [
        'image',// '图片URL路径',
        'adv_name',// '广告名称',
        'link_url',// '链接地址',
        'memo',// '广告备注',
        'start_time',// '开始时间',
        'end_time',// '结束时间',
        'status',// '1正常 2 下线',
        'admin_id',// '添加人ID',
    ];
    protected $hidden = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    public function getImageAttribute( $value )
    {
        $this->append( 'original_image' );
        return Upload::getInstance()->getPicName( $value, Upload::IMAGE );
    }

    public function getOriginalImageAttribute()
    {
        return $this->getOriginal( 'image' );
    }

}
