<?php

namespace App\Models;

use App\Traits\Singleton;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;

class OperationLog extends Model
{
    //
    use Singleton, SoftDeletes;

    protected $table = 'operation_log';


    protected $hidden = [ 'updated_at', 'deleted_at' ];


    public static function addLog( $score, $operatingType, $content, $userId = '', $doctorId = '' )
    {
        $ip = request()->getClientIp();
        $logCoding = date('YmdHis') . uniqid();
        $operatingTime = date('Y-m-d H:i:s');
        $accessTime = $operatingTime;
        $log = new OperationLog();
        $log->user_id = $userId;
        $log->doctor_id = $doctorId;
        $log->log_coding = $logCoding;
        $log->operating_time = $operatingTime;
        $log->access_time = $accessTime;
        $log->operating_type = $operatingType;
        $log->content = $content;
        $log->ip = $ip;
        $log->score = $score;
        $log->save();
    }

    public $symptomsSituation = [ '0', '用户注册', '用户登录', '用户退出登录', '线上咨询', '快速问诊',
                                  '慢病咨询', '住院缴费', '门诊缴费', '挂号缴费', '医生登录',
                                  '医生退出登录', '药店后台登录', '药店后台登出', '修改药店登录密码', '绑定亲属',
                                  '修改药店信息', '退号退费' ];


    public function getOperatingTypeAttribute( $value )
    {
        return $this->symptomsSituation[$value];
    }

    public function getScoreAttribute( $value )
    {
        $this->append('name');
        return $value;
    }

    public function getNameAttribute()
    {
        $score = $this->getOriginal('score');
        $userId = $this->getOriginal('user_id');
        $doctorId = $this->getOriginal('doctor_id');
        if( $score == 1 ){
            $name = User::getInstance()->where([ 'id' => $userId ])->select([ 'id', 'name' ])->first();
            $name = $name['name'];
        }else{
            $name = Doctor::getInstance()->where([ 'id' => $doctorId ])->select([ 'id', 'name' ])->first();
            $name = $name['name'];
        }
        return $name;
    }


}
