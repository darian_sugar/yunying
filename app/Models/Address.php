<?php

namespace App\Models;

use App\Traits\Singleton;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Address extends Model
{
    //
    use  Singleton, SoftDeletes;

    protected $fillable = [
        'user_id',
        'address',
        'province_id',
        'city_id',
        'area_id',
        'mobile',
        //        'is_default',
        'name',
        'from_id',
    ];

    protected $hidden = [ 'created_at', 'deleted_at' ];


    /**
     * Eloquent start
     */
    public function belongsToArea()
    {
        return $this->belongsTo( Area::class, 'area_id' );
    }

    public function belongsToCity()
    {
        return $this->belongsTo( Area::class, 'city_id' );
    }

    public function belongsToProvince()
    {
        return $this->belongsTo( Area::class, 'province_id' );
    }


}
