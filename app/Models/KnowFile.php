<?php

namespace App\Models;

use App\Helpers\Upload;
use App\Traits\Singleton;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class KnowFile extends Model
{
    //
    use Singleton, SoftDeletes;

    protected $fillable = [
        'cate_id',// '分类ID',
        'title',// '标题',
        'content',// '内容',
        'image',// '图片',
        'author_id',// '作者ID',
        'admin_id',// '上传者ID',
        'read',// '阅读数',
        'order_sort',// '排序',
        'price',// '价格',
        'from',// '1 admin 2 医生 ',
        'is_top',// '1 普通文章 2 热门文章',
        //        'status',// '1 启用 2 禁用',
        //        'check_status',// '0 待审核 1 审核通过 2 审核拒绝',
        'type',// '1百科 2 学堂',
        //        'check_uid',// '审核人ID',
        //        'check_time',// '审核时间',
    ];
    protected $hidden = [
        'deleted_at',
    ];
    public $checkStatusDesc = [
        0 => "待审核",
        1 => "已拒绝",
        2 => "已同意",
    ];

    public function getCateIdAttribute( $value )
    {
        if ( $this->getOriginal( 'type' ) == 2 )
            $this->append( 'cate_desc' );
        return $value;
    }

    public function getCateDescAttribute()
    {
        $value = $this->getOriginal( 'cate_id' );
        return KnowCate::BASE_CATE[ $value ];
    }

    public function getCheckStatusAttribute( $value )
    {
        $this->append( 'check_status_desc' );
        return $value;
    }

    public function getCheckStatusDescAttribute()
    {
        $value = $this->getOriginal( 'check_status' );
        return $this->checkStatusDesc[ $value ] ?? '未知';
    }

    public function getImageAttribute( $value )
    {
        $this->append( 'original_image' );
        return Upload::getInstance()->getPicName( $value, Upload::IMAGE );
    }

    public function getOriginalImageAttribute()
    {
        return $this->getOriginal( 'image' );
    }

    public function getContentAttribute( $value )
    {
        if ( $this->from == 2 )
            return Upload::getInstance()->getPicName( $value, Upload::IMAGE );
        return $value;
    }


    public function setPriceAttribute( $value )
    {
        $this->attributes['price'] = bcmul( $value, 100 );
    }

    public function getPriceAttribute( $value )
    {
        return $value ? floatval( bcdiv( $value, 100, 2 ) ) : 0;
    }

    public function belongsToAuthor()
    {
        return $this->belongsTo( Doctor::class, 'author_id' )->withTrashed();
    }

    public function belongsToCate()
    {
        return $this->belongsTo( KnowCate::class, 'cate_id' )->withTrashed();
    }

    public function hasOneCollect()
    {
        return $this->hasOne( UserCollect::class, 'c_id' )->where( 'type', 1 );
    }

    public function mergeSearch( $key, $type = 1 )
    {
        $user = Auth::user();
        if ( $type == 1 ) {
            $model = $this->where( [ 'status' => 1, 'check_status' => 1 ] );
        } elseif ( $type == 2 ) {
            $model = KnowCourse::getInstance()->where( [ 'status' => 1, 'check_status' => 1 ] )->select( 'content', 'duration', 'read', 'price' )
                ->withCount(
                    [
                        'hasOneCollect as is_collect' => function ( $query ) use ( $user ) {
                            $query->where( 'u_id', $user->id );
                        },
                        'hasManyDown as is_down'      => function ( $query ) use ( $user ) {
                            $query->where( [ 'u_id' => $user->id, 'from' => 2, 'pay_status' => 1 ] );
                        }
                    ] );

        } else {
            $model = KnowLive::getInstance()->where( [ 'status' => 1, 'check_status' => 1 ] );
            //$file = $this->selectRaw('id,title,1 as search_type,cate_id,image')->where([ 'status' => 1, 'check_status' => 1 ]);
            //$course = KnowCourse::getInstance()->selectRaw('id,title,2 as search_type ,cate_id,image')->where([ 'status' => 1, 'check_status' => 1 ]);
            //
            //$result = $file->unionAll($course);
            //$table = $result->toSql();
            //$model = DB::table(DB::raw("($table) as t"))
            //    ->selectRaw("t.*");
            //$model->setBindings(array_merge($result->getBindings(), $model->getBindings()));
        }

        if ( $key ) {
            $cateModel = KnowCate::getInstance()->where( [ 'status' => 1 ] );
            foreach ( $key as $v ) {
                $where[] = [ 'title', 'like', '%' . $v . '%' ];
                $cateModel->where( 'name', 'like', '%' . $v . '%' );
            }
            $cateId = $cateModel->pluck( 'id' )->toArray();

            $model->where( function ( $query ) use ( $where, $cateId ) {
                $query->where( $where );
                if ( $cateId )
                    $query->orWhereIn( 'cate_id', $cateId );

            } );


        }
        return $model->selectRaw( 'id,title,image,author_id,cate_id,created_at,`from`' )
            ->with( [
                'belongsToCate:id,name',
                'belongsToAuthor:id,name,head_pic,t_id',
            ] );
    }

    public function hasManyDown()
    {
        return $this->hasMany( KnowDown::class, 'course_id' )->where( 'type', 1 );
    }
}
