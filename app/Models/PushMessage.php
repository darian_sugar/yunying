<?php

namespace App\Models;

use App\Traits\Singleton;
use Illuminate\Database\Eloquent\Model;

class PushMessage extends Model
{
    use Singleton;

    protected $fillable = [
        'type',
        'title',
        'msg',
        'receive_type',
        'user_id',
        'status',
    ];

    protected $hidden = [
      'updated_at',
    ];
}
