<?php

namespace App\Models;

use App\Traits\Singleton;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProtocolRead extends Model
{
    //
    use Singleton,SoftDeletes;
    protected $fillable = [
        'u_id',
        'protocol_id',
    ];
}
