<?php

namespace App\Models;

use App\Traits\Singleton;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ScaleRecord extends Model{
    use Singleton;

    protected $fillable = [
      'user_id',
      'basic_id',
      'scale_id',
      'is_stop',
      'stop_id',
      'type',
      'scale_name',
    ];


    // 量表记录 -- 量表
    public function hasOneScale()
    {
        return $this->hasOne(Scale::class, 'id', 'scale_id');
    }


    // 量表记录 -- 基础信息
    public function hasOneBasic()
    {
        return $this->hasOne(ScaleBasic::class, 'id', 'basic_id');
    }


    // 量表记录 -- 用户信息
    public function hasOneUser()
    {
        return $this->hasOne(User::class, 'id', 'user_id');
    }


    // 量表记录 -- 记录详情
    public function hasManyRecordDetail()
    {
        return $this->hasMany(ScaleRecordDetail::class, 'scale_record_id', 'id');
    }
}
