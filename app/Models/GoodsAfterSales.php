<?php

namespace App\Models;

use App\Helpers\Upload;
use App\Traits\Singleton;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class GoodsAfterSales extends Model
{
    use Singleton, SoftDeletes;

    protected $fillable = [
        'order_id',
        'reson_id',
        'image',
        'content',
    ];
    protected $hidden = [ 'deleted_at' ];


    public $checkStatusDesc = [
        0 => "待审核",
        1 => "已拒绝",
        2 => "已同意",
    ];

    public $resonDesc = [
        [ 'id' => 1, 'title' => '产品颜色等与描述不符' ],
        [ 'id' => 2, 'title' => '产品与商家推荐不符合' ],
        [ 'id' => 3, 'title' => '7天无理由退货' ],
        [ 'id' => 4, 'title' => '其他的理由' ],
        [ 'id' => 5, 'title' => '质量问题' ],
        [ 'id' => 6, 'title' => '包装/商品破损/污渍' ],
        [ 'id' => 7, 'title' => '卖家发错货' ],
    ];

    /**
     * Attribute start
     */


    public function setImageAttribute( $value )
    {
        $this->attributes['image'] = implode( ',', $value );
    }

    public function getImageAttribute( $value )
    {
        $imgs = $value ? explode( ',', $value ) : [];
        array_walk( $imgs, function ( &$value ) {
            $value = Upload::getInstance()->getPicName( $value, Upload::IMAGE );
        } );
        return $imgs;
    }


    public function getResonIdAttribute( $value )
    {
        $this->append( 'reson_desc' );
        return $value;
    }

    public function getResonDescAttribute()
    {
        $value = $this->getOriginal( 'reson_id' );
        $data = collect( $this->resonDesc )->pluck( 'title', 'id' )->toArray();
        return $data[ $value ] ?? '未知';
    }

    public function getCheckStatusAttribute( $value )
    {
        $this->append( 'check_status_desc' );
        return $value;
    }

    public function getCheckStatusDescAttribute()
    {
        $value = $this->getOriginal( 'check_status' );
        return $this->checkStatusDesc[ $value ] ?? '未知';
    }

    public function setRefundAmountAttribute( $value )
    {
        $this->attributes['refund_amount'] = bcmul( $value, 100 );
    }

    public function getRefundAmountAttribute( $value )
    {
        return $value ? floatval( bcdiv( $value, 100, 2 ) ) : 0;
    }

    /**
     * Eloquent start
     */

    public function hasManyGoods()
    {
        return $this->hasMany( GoodsAfterSalesDetail::class, 'after_sales_id' );
    }
}
