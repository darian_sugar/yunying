<?php

namespace App\Models;

use App\Helpers\Upload;
use App\Traits\Singleton;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Doctor extends Model
{
    use Singleton, SoftDeletes;

    //必填 数据 过滤
    protected $fillable = [
        'name',
        'head_pic',
        'intro',
        't_id',
        'text_price', //单未分
        'video_price',//单位分
        'audio_price',//单位 分
        //'text_param', //文字咨询条数
        //'video_param',//'视频咨询时长',
        //'audio_param',//语音咨询时长
        'ask_status',//语音咨询时长
        'account',
        'change_uid',
        'change_time',
        'type',
        'appoint_price',
    ];
    protected $hidden = [ 'password', 'api_token', 'created_at', 'deleted_at', 'balance' ];

    public $title = [
        '医士',
        '医师',
        '主治医师',
        '副主任医师',
        '主任医师',
    ];

    public $d_desc = [
        1 => '线上咨询',
        2 => '线下预约',
        3 => '线下预约和线上咨询',
    ];

    static public $config_arr = [];

    public function __construct( array $attributes = [] )
    {
        parent::__construct( $attributes );
        if ( !self::$config_arr )
            self::$config_arr = SystemConfig::getInstance()->pluck( 'value', 'name' );
    }

    public function getTypeAttribute( $value )
    {
        $this->append( 'type_desc' );
        return $value;
    }

    public function getTypeDescAttribute()
    {
        $value = $this->getOriginal( 'type' );
        return $this->d_desc[ $value ];
    }

    public function setTextPriceAttribute( $value )
    {
        $this->attributes['text_price'] = bcmul( $value, 100 );
    }

    public function setVideoPriceAttribute( $value )
    {
        $this->attributes['video_price'] = bcmul( $value, 100 );
    }

    public function setAudioPriceAttribute( $value )
    {
        $this->attributes['audio_price'] = bcmul( $value, 100 );
    }

    public function setAppointPriceAttribute( $value )
    {
        $this->attributes['appoint_price'] = bcmul( $value, 100 );
    }


    public function setBalanceAttribute( $value )
    {
        $this->attributes['balance'] = bcmul( $value, 100 );
    }

    public function getBalanceAttribute( $value )
    {
        return $value ? floatval( bcdiv( $value, 100, 2 ) ) : 0;
    }

    public function getAppointPriceAttribute( $value )
    {
        return $value ? floatval( bcdiv( $value, 100, 2 ) ) : 0;
    }

//    public function getTextParamAttribute()
//    {
//        return self::$config_arr ['doctor_text'] ?? 0;
//    }
//
//    public function getAudioParamAttribute()
//    {
//        return self::$config_arr ['doctor_audio'] ?? 0;
//    }
//
//    public function getVideoParamAttribute()
//    {
//        return self::$config_arr ['doctor_video'] ?? 0;
//    }

    /**
     * 获取最小价格
     *
     * @param $value
     *
     * @return int|string|null
     */
    public function getMinPriceAttribute()
    {
        $arr = [
            $this->getAttribute( 'text_price' ),
            $this->getAttribute( 'video_price' ),
            $this->getAttribute( 'audio_price' ),
        ];
        return min( $arr );
    }


    public function getTextPriceAttribute( $value )
    {
        $this->append( [ 'min_price' ] );
        return $value ? floatval( bcdiv( $value, 100, 2 ) ) : 0;
    }


    public function getTIdAttribute( $value )
    {
        $this->append( [ 'job_title' ] );
        return $value;
    }

    public function getJobTitleAttribute()
    {
        return $this->title[ $this->t_id ];
    }

    public function getAudioPriceAttribute( $value )
    {
        return $value ? floatval( bcdiv( $value, 100, 2 ) ) : 0;
    }

    public function getVideoPriceAttribute( $value )
    {
        return $value ? floatval( bcdiv( $value, 100, 2 ) ) : 0;
    }

    public function getHeadPicAttribute( $value )
    {
        $this->append( [ 'original_head_pic' ] );
        return $value ? Upload::getInstance()->getPicName( $value, Upload::HEADIMG ) : '';
    }

    public function getOriginalHeadPicAttribute()
    {
        return $this->getOriginal( 'head_pic' );
    }


    /**
     * Eloquent start
     */

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function belongsToAdmin()
    {
        return $this->belongsTo( Admin::class, 'change_uid' );
    }

    /**
     * 医生 - 用户 问诊有效
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function hasOneRecord()
    {
        return $this->hasOne( UserDoctorRecord::class, 'doctor_id' );
    }

    /**
     * 医生 - 用户 问诊有效
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function hasManyRecord()
    {
        return $this->hasMany( UserDoctorRecord::class, 'doctor_id' );
    }


    /**
     * 医生 -- 聊天记录
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function hasManyMsg( $source = 1 )
    {
        return $this->hasMany( Message::class, 'user_id' )->where( 'source', $source );
    }

    /**
     * 医生 -- 未读消息数
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function hasManyMsgStatus()
    {
        return $this->hasMany( Message::class, 'to_id' );
    }

    /**
     * 医生 -- 系统消息数
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function hasManySysMsg()
    {
        return $this->hasMany( SystemMsg::class, 'to_id' );
    }

    public function UserHasMany( $model )
    {
        return $this->hasMany( $model, 'doctor_id' );
    }

    public function UserHasOne( $model )
    {
        return $this->hasOne( $model, 'doctor_id' );
    }


    public function getByMobile( $mobile )
    {
        return $this->where( 'mobile', $mobile )->first();
    }

    public function getByAccount( $account )
    {
        return $this->where( 'account', $account )->first();
    }

    public function getByAccid( $accid )
    {
        return self::where( 'accid', $accid )->first();
    }

    public function saveToken()
    {
        $token = create_token( 2 );
        $this->api_token = $token;
        return $this->save();
    }


    public function hasManySystemMsgStatus()
    {
        return $this->hasMany( SystemMsgStatus::class, 'to_id' )->where( 'type', 2 );
    }

    public function hasManyInquiryOrder()
    {
        return $this->UserHasMany( InquiryOrder::class );
    }

    public function hasManyAppointment()
    {
        return $this->UserHasMany( Appointment::class );
    }


    public function hasManySchedule()
    {
        return $this->UserHasMany( DoctorSchedule::class );
    }

    /**
     * 医生评论
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function hasManyComment()
    {
        return $this->UserHasMany( DoctorComment::class );
    }
}
