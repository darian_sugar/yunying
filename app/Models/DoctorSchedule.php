<?php

namespace App\Models;

use App\Traits\Singleton;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DoctorSchedule extends Model
{
    //
    use Singleton, SoftDeletes;

    protected $fillable = [
        'doctor_id',
        'date',
        'start_time',
        'end_time',
        'total_num',
        'used_num',
    ];
    protected $hidden   = [ 'created_at', 'deleted_at' ];

    static public $time_list = [
        '8:00-9:00',
        '9:00-10:00',
        '10:00-11:00',
        '11:00-12:00',
        '12:00-13:00',
        '13:00-14:00',
        '14:00-15:00',
        '15:00-16:00',
        '16:00-17:00',
        '17:00-18:00',
    ];


    public function getUsedNumAttribute( $value )
    {
        $this->append('num');
        return $value;
    }

    public function getNumAttribute()
    {
        $total = $this->getAttribute('total_num');
        $used = $this->getAttribute('used_num');
        return $total - $used;
    }


}
