<?php

namespace App\Models;

use App\Traits\Singleton;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserCollect extends Model
{
    //
    use Singleton, SoftDeletes;

    protected $fillable = [
        'u_id',
        'c_id',
        'type',
        'from',
    ];
    const FILE = 1;
    public $belongs = null;

    public function getBelongsToCollectAttribute()
    {

        return $this->belongs;

    }

    public function belongsToKnowFile()
    {
        return $this->belongsTo( KnowFile::class, 'c_id' )->withTrashed();

    }

    public function belongsToKnowCourse()
    {
        return $this->belongsTo( KnowCourse::class, 'c_id' )->withTrashed();

    }

    public function belongsDoctor()
    {
        return $this->belongsTo( Doctor::class, 'u_id' )->withTrashed();

    }

    public function hasManyComment()
    {
        return $this->hasMany( KnowCourseComment::class, 'c_id' )->where( 'type', 2 );

    }
}
