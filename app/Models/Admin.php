<?php

namespace App\Models;

use App\Traits\Singleton;
use App\Helpers\Upload;
use function foo\func;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Hash;

class Admin extends Model
{
    //
    use Singleton, SoftDeletes;

    protected $fillable = [
        'name',
        'head_pic',
        'email',
        'mobile',
        'account',
        //        'is_cs',
        'yx_token',
        'accid',
        //        'cs_status',
    ];

    protected $hidden = ['password', 'api_token'];

    public function getHeadPicAttribute($value)
    {
        $this->setAppends(['original_head_pic']);
        return $value ? Upload::getInstance()->getPicName($value, Upload::HEADIMG) : '';
    }

    public function getOriginalHeadPicAttribute()
    {
        return $this->getOriginal('head_pic');
    }

    public function saveToken()
    {
        $token = create_token(3);
        $this->api_token = $token;
        $this->last_login_time = date('Y-m-d H:i:s');
        return $this->save();
    }

    public function getByMobile($mobile)
    {
        return $this->where('mobile', $mobile)->first();
    }

    public function getByAccount($account)
    {
        return $this->where('account', $account)->first();
    }

    public function UserHasMany($model)
    {
        return $this->hasMany( $model, 'admin_id' );
    }

    public function hasManyRole()
    {
        return $this->hasMany( AdminRoleRelevance::class, 'a_id' );
    }


    public function hasManyPrie()
    {
        return $this->hasMany( RolePriRelevance::class, 'r_id' )->where( 'type', 2 );
    }


}
