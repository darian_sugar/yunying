<?php

namespace App\Models;

use App\Traits\Singleton;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserDoctorRecord extends Model
{
    use SoftDeletes, Singleton;

    protected $fillable = [
        'user_id',
        'doctor_id',
        'type',
        'time',
        'has_pay',
        'chat_at',
    ];
    protected $hidden   = [
        'deleted_at'
    ];

    protected $appends = [

    ];

    const TEXT  = 1;
    const AUDIO = 2;
    const VIDEO = 3;

    public function getTimeAttribute( $value )
    {
        $this->append([ 'time_desc' ]);
        if( $this->getOriginal('type') == 1 ){
            $sec = $value - time();
            if( $sec > 0 ){
                $minutes = ceil($sec / 60);
            }else{
                $minutes = 0;
            }
            return $minutes;
        }else
            return $value;
    }

    public function getTimeDescAttribute()
    {
        $time = $this->time;
        return $time . '分钟';
    }

    /**
     * 问诊记录 -- 问诊医生
     */
    public function belongsToDoctor()
    {
        return $this->belongsTo( Doctor::class, 'doctor_id' );
    }

    /**
     * 问诊记录 -- 问诊病人
     */
    public function belongsToUser()
    {
        return $this->belongsTo( User::class, 'user_id' );
    }

    /**
     * 订单 -- 聊天记录
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function hasManyMessage()
    {
        return $this->hasMany( Message::class, 'record_id' )->latest( 'id' );
    }

    public function hasLatestMessage()
    {
        return $this->hasOne( Message::class, 'record_id' )->latest();
    }


    /**
     * 获取记录id
     *
     * @param $userId   患者id
     * @param $doctorId 医生id
     * @param $type     1 图文 2 语音 3  视频',
     *
     * @return mixed
     */
    public function getRecord( $userId, $doctorId, $type = 1 )
    {
        $info = $this->where([ 'user_id' => $userId, 'doctor_id' => $doctorId, 'type' => $type ])->first();
        return $info;
    }


    /**
     * 绑定订单
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function hasManyInquiryOrder()
    {
        return $this->hasMany( InquiryOrder::class, 'record_id' );
    }

}
