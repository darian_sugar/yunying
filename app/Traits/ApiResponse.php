<?php

namespace App\Traits;

use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;
use Symfony\Component\HttpFoundation\Response as FoundationResponse;
use Response;

trait ApiResponse
{
    /**
     * @var int
     */
    protected $statusCode = FoundationResponse::HTTP_OK;


    /**
     * @return mixed
     */
    public function getStatusCode()
    {
        return $this->statusCode;
    }

    /**
     * @param $statusCode
     * @return $this
     */
    public function setStatusCode( $statusCode )
    {

        $this->statusCode = $statusCode;
        return $this;
    }

    /**
     * @param       $data
     * @param array $header
     * @return mixed
     */
    public function respond( $data, $header = [] )
    {

        //        return response()->json($data, $this->getStatusCode(), $header);
        $request = \Illuminate\Support\Facades\Request::instance();
        Log::debug("*********request_id:{$request->header('request_id')}:" . $request->path(), $data);
        return response()->json($data, 200, $header,JSON_UNESCAPED_UNICODE);
    }

    /**
     * @param       $status
     * @param array $data
     * @param null  $code
     * @return mixed
     */
    public function status( $status, array $data, $code = null )
    {

        if( $code ){
            $this->setStatusCode($code);
        }

        $status = [
            'status' => $status,
            'code'   => $this->statusCode
        ];

        $data = array_merge($status, $data);
        return $this->respond($data);

    }

    /**
     * @param        $message
     * @param int    $code
     * @param string $status
     * @return mixed
     */
    public function failed( $message, $code = FoundationResponse::HTTP_BAD_REQUEST, $status = 'error' )
    {

        return $this->setStatusCode($code)->message($message, $status);
    }

    /**
     * @param        $message
     * @param string $status
     * @return mixed
     */
    public function message( $message = '操作成功', $status = "success" )
    {

        return $this->status($status, [
            'message' => $message
        ]);
    }

    /**
     * @param string $message
     * @return mixed
     */
    public function internalError( $message = "Internal Error!" )
    {

        return $this->failed($message, FoundationResponse::HTTP_INTERNAL_SERVER_ERROR);
    }

    /**
     * @param string $message
     * @return mixed
     */
    public function created( $message = "created" )
    {
        return $this->setStatusCode(FoundationResponse::HTTP_CREATED)
            ->message($message);

    }

    /**
     * @param        $data
     * @param string $status
     * @param null   $code
     * @return mixed
     */
    public function success( $data = [], $status = "success", $code = null )
    {

        return $this->status($status, compact('data'), $code);
    }


    /**
     * 可返回空json格式数据
     * @param array $data
     * @param string $status
     * @param null $code
     * @return mixed
     */
    public function successJson( $data = [], $status = "success", $code = null )
    {
        if (empty($data)) $data = new \stdClass;
        return $this->status($status, compact('data'), $code);
    }


    //获取权限树（无限极分类）
    public function getTree( $data, $prefix = '' )
    {
        $haystack = [
            'belongs_to',
            'has_one',
            'has_many',
        ];
        if( $data && is_array($data) ){
            //首先定义一个静态数组常量用来保存结果
            static $result_array = array();
            //对多维数组进行循环
            foreach( $data as $k => $value ){
                //判断是否是数组，如果是递归调用方法
                if( Str::contains($k, $haystack) ){
                    $this->getTree($value, $k);
                }
                $result_array[$prefix . '' . $k] = $value;
            }
            //返回结果（静态数组常量）
            return $result_array;
        }else{
            $result_array[] = $data;

        }
    }

    /**
     * @param string $message
     * @return mixed
     */
    public function notFond( $message = 'Not Fond!' )
    {
        return $this->failed($message, Foundationresponse::HTTP_NOT_FOUND);
    }

    /**
     * @param string $message
     * @return mixed
     */
    public function methodNotAllow( $message = 'Method Not Allowed' )
    {
        return $this->failed($message, Foundationresponse::HTTP_METHOD_NOT_ALLOWED);
    }

    public function badRequest( $message = 'Bad Request' )
    {
        return $this->failed($message, Foundationresponse::HTTP_BAD_REQUEST);
    }

    public function unAuthenticated( $message = 'Unauthorized' )
    {
        return $this->failed($message, Foundationresponse::HTTP_UNAUTHORIZED);

    }

    public function validation( $message = 'Unprocessable Entity' )
    {
        return $this->failed($message, Foundationresponse::HTTP_UNPROCESSABLE_ENTITY);

    }


    public function format( array $data )
    {

        $res = $this->getTree($data);
        return $res;
    }


}
