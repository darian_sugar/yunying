<?php

namespace App\Traits;

trait Singleton
{
    private static $instance;

    static function getInstance(...$args)
    {
        if(!(self::$instance instanceof self)){
            self::$instance = new static(...$args);
        }
        return self::$instance;
    }
}
