<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

/**
 * 验证小数
 * Class numeric2Rule
 * @package App\Rules
 */
class Numeric2Rule implements Rule
{
    protected $num, $attribute, $attrMsg;

    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct( $num = 2, $attrMsg = '' )
    {
        //
        $this->num = $num;
        $this->attrMsg = $attrMsg;

    }

    /**
     * Determine if the validation rule passes.
     *
     * @param string $attribute
     * @param mixed $value
     *
     * @return bool
     */
    public function passes( $attribute, $value )
    {
        //
        $this->attribute = $attribute;
        if ( preg_match( '/^(-)?(\d+){0,9}(.\d{0,' . $this->num . '})?$/', $value ) )
            return true;
        else
            return false;

    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return '参数 ' . ( $this->attrMsg ?: $this->attribute ) . ' 必须为整数，或者保留 ' . $this->num . ' 位小数';
    }
}
