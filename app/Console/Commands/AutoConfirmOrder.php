<?php

namespace App\Console\Commands;

use App\Models\GoodsOrder;
use App\Models\SystemConfig;
use Carbon\Carbon;
use Illuminate\Console\Command;

class AutoConfirmOrder extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'goodsorder:confirm';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'auto goods order confirm ';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //自动确认收货 15天
        $date = Carbon::now()->subDays( SystemConfig::getValue( 'auto_goods_order_comfirm_days' ) )->toDateTimeString();
        $orderList = GoodsOrder::getInstance()
            ->where( [ 'pay_status' => 1, 'order_status' => 1 ] )
            ->whereIn( 'trans_status', [ 2, 3 ] )//已发货 已签收
            ->where( 'pay_time', '<=', $date )
            ->pluck( 'trans_status', 'id' );
        logLog( 'data:' . json_encode( $orderList ), 'orderconfirm' );
        if ( $orderList ) {
            GoodsOrder::getInstance()
                ->where( [ 'pay_status' => 1, 'order_status' => 1 ] )
                ->whereIn( 'trans_status', [ 2, 3 ] )
                ->where( 'pay_time', '<=', $date )
                ->update( [ 'trans_status' => 4 ] );
        }

    }
}
