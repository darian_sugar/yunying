<?php

namespace App\Console\Commands;

use App\Helpers\DingDingRobotService;
use App\Models\PeriodRecord;
use App\Models\PeriodSet;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Yansongda\Pay\Events\RequestReceived;

class PeriodPredictive extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:periodPredictive';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * 经期预测脚本
     * @throws \GuzzleHttp\Exception\GuzzleException
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        file_put_contents(storage_path() . '/logs/test.log', date('Y-m-d H:i:s') . $this->description . PHP_EOL, FILE_APPEND);
        // 1 记录表中不管是否是下个月，都进行预测修改
        // 2 先查询记录表上个月中是否有实际月经时间，没有按预测月经时间计算下月
        try {
            $data       = PeriodSet::getInstance()->where(['is_predictive' => 1])
                ->get()->toArray();
            $recordData = PeriodRecord::getInstance()->select(DB::raw('*, max(id) as id'))
                ->where('month', date('Y-m'))
                ->groupBy('user_id')->orderBy('id', 'asc')->get()->toArray();
            $data       = array_column($data, null, 'user_id');
            $recordData = array_column($recordData, null, 'user_id');
            $month      = date('Y-m', strtotime('+1 month'));
            foreach ($data as $user_id => $v) {
                $length                     = $v['length'];
                $cycle                      = $v['cycle'];
                $recordDetail = $recordData[$user_id] ?? '';
                $period_predictive_end_date = $recordDetail['period_predictive_end_date'];
                $period_reality_end_date    = $recordDetail['period_reality_end_date'];
                $periodDate                 = $period_reality_end_date ?? $period_predictive_end_date;
                $period_predictive_date = date('Y-m-d', strtotime("+$cycle day $periodDate"));
                $period_predictive_end_date = date('Y-m-d', strtotime("+$length day $period_predictive_date"));
                $nextMonthData = [
                    'user_id' => $user_id,
                    'month' => $month,
                    'period_predictive_date' => $period_predictive_date,
                    'period_predictive_end_date' => $period_predictive_end_date,
                ];
                PeriodRecord::getInstance()->addOrUpdate([], $nextMonthData);
            }
        }catch (\Exception $exception) {
          DingDingRobotService::getInstance()->send($exception->getMessage(), 5);
        }
    }
}
