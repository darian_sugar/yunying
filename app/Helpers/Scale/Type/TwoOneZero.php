<?php

namespace App\Helpers\Scale\Type;

class TwoOneZero
{

    public $TwoOneZero = '
   [
    {
        "question": "孩子性别",
        "q_type": 210,
        "jump_id": -1,
        "option_list": [
            {
                "content": "男",
                "prompt": "",
                "direction": 0,
                "is_fill": 0,
                "text": "",
                "attr_option": [
                ]
            },
            {
                "content": "女",
                "prompt": "",
                "direction": 0,
                "is_fill": 0,
                "text": "",
                "attr_option": []
            }
        ]
    }
]
   ';

}
