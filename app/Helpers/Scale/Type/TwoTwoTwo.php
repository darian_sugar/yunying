<?php

namespace App\Helpers\Scale\Type;

class TwoTwoTwo{

    /**
     * direction 0不存在方向，1左填空，2右填空
     * is_fill 0不存在选项填空，1选项填空，2不是选项填空
     * text 选项填空内容
     * @var string
     */
    public $twoOneTwo = '
[
    {
        "question": "选择 + 选项填空",
        "q_type": 212,
        "option_list": [
            {
                "content": "是",
                "prompt": "",
                "direction": 0,
                "is_fill": 2,
                 "jump_id": -1,
                "attr_option": []
            },
            {
                "content": "否",
                "prompt": "",
                "direction": 2,
                 "jump_id": -1,
                "text": "填空内容",
                "attr_option": []
            }
        ]
    }
]
    ';
}
