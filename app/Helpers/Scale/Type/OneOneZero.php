<?php

namespace App\Helpers\Scale\Type;

class OneOneZero{

    public $oneOneZero = '
    [
    {
        "question": "请填写以下内容",
        "q_type": 110,
        "option_list": [
            {
                "content": "年",
                "prompt": "",
                "direction": 1,
                "is_fill": 0,
                 "jump_id": -1,
                "attr_option": []
            },
            {
                "content": "月",
                "prompt": "",
                "direction": 1,
                "is_fill": 0,
                 "jump_id": -1,
                "attr_option": []
            },
            {
                "content": "日",
                "prompt": "",
                "direction": 1,
                "is_fill": 0,
                 "jump_id": -1,
                "attr_option": []
            }
        ]
    }
]
    ';
}
