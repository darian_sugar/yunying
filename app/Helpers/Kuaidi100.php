<?php
namespace App\Helpers;

use App\Services\Common\MongodbService;
use App\Services\ServiceManager;
use App\Traits\Singleton;
use Illuminate\Support\Facades\Redis;

/**
 * 快递100查询类
 * @Copyright (C) 2019 汉潮 All rights reserved.
 * @License http://www.hanchao9999.com
 * @Author xiaogg <xiaogg@sina.cn>
 */
class Kuaidi100
{
    use Singleton;

    /**
     * API根路径
     * @var string
     */
    protected $ApiBase = 'https://m.kuaidi100.com/query';
    private $config;
    //state 0：在途1：揽件2：疑难3：签收4：退签5：派件6：退回
    public $stateType = [
        '在途',
        '揽件',
        '疑难',
        '签收',
        '退签',
        '派件',
        '退回',
    ];

    /**
     * 查询快递状态
     * @param array sn   快递订单号
     *              code 快递公司编码
     * @return array | boolean
     */
    public function search( $data )
    {
        if ( !$data['sn'] || !$data['code'] )
            throw new \JsonException( '参数错误' );
        $key = 'kuai_' . $data['sn'] . ':' . $data['code'];

        if ( Redis::exists( $key ) )
            return json_decode( Redis::get( $key ), true );
        $params = array(
            'postid'   => $data['sn'],
            'id'       => 1,
            'valicode' => '',
            'temp'     => $this->random(),
            'type'     => $data['code'],
            'phone'    => '',
            'token'    => '',
            'platform' => 'MWWW',
            'coname'   => 'indexall',
        );
        $header = array(
            'User-Agent: application/json, text/javascript, */*; q=0.01',
            'Content-Type: application/x-www-form-urlencoded; charset=UTF-8',
        );
        $ref_url = 'https://m.kuaidi100.com/app/query/?coname=indexall&nu=' . $data['sn'] . '&com=' . $data['code'];
        $cookie = $this->getcookie( $ref_url, $header );
        $opt = array( CURLOPT_REFERER => $ref_url, CURLOPT_COOKIE => $cookie );
        $data = curlget( $this->ApiBase, $params, 'POST', $header, false, false, $opt );
        $result = json_decode( $data, true );
        $result['state_desc'] = $this->stateType[ $result['state'] ];
        //已签收
        if ( $result['ischeck'] == 1 )
            Redis::set( $key, json_encode( $result ) );
        else
            Redis::setex( $key, 60 * 60 * 10, json_encode( $result ) );
        return $result;
    }

    private function getcookie( $url, $header = array() )
    {
        $key = 'kuaidi100';
        if ( Redis::exists( $key ) )
            return Redis::get( $key );
        $opt = array( CURLOPT_HEADER => true );
        $result = curlget( $url, '', 'GET', $header, false, false, $opt );
        preg_match_all( '/.*?\r\nSet-Cookie: (.*?);.*?/si', $result, $matches );
        if ( isset( $matches[1] ) ) {
            $cookie = implode( '; ', $matches[1] );
        }
        if ( $cookie )
            Redis::setex( $key, 60 * 60 * 20, $cookie );
        return $cookie;
    }

    private function random( $min = 0, $max = 1 )
    {
        return $min + mt_rand() / mt_getrandmax() * ( $max - $min );
    }

    /**
     *获取快递公司编号
     */
    public function com_url()
    {
        return "http://api.kuaidi100.com/manager/openapi/download/kdbm.do";
    }
}

?>
