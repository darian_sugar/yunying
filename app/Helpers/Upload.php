<?php

namespace App\Helpers;

use App\Traits\Singleton;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use JsonException;

class Upload
{
    //
    use Singleton;

    const HEADIMG = 1;         //头像
    const IMAGE = 2;
    const VIDEO = 3;
    const FILE = 4;

    private $file;         //PHP文件
    private $blobNum;      //第几个文件块
    private $totalBlobNum; //文件块总数
    private $fileName;     //文件名
    private $md5FileName;  //文件MD5
    private $type;         //文件类型
    private $folder;       //存储文件夹
    private $ext;          //

    public function getFileUrl( $type )
    {
        switch ( $type ) {
            case 1:
                $path = 'head_pic/';
                break;
            case 2:
                $path = 'images/';
                break;
            case 3:
                $path = 'video/';
                break;
            case 4:
                $path = 'file/';
                break;
            default:
                throw  new JsonException( '上传类型错误' );
        }

        return $path;
    }


    public function uploadFile( $file, $type = 1 )
    {
        if ( $file->isValid() ) {
            $ext = $file->getClientOriginalExtension();
            $name = $this->getUniqidName() . '.' . $ext;
            $size = $file->getClientSize();
            $mimetype = $file->getMimeType();
            if ( $type != self::VIDEO ) {
                if (
                    !preg_match( '/^image.*/', $mimetype )
                    && $mimetype != 'application/pdf'
                    && $mimetype != 'application/msword'
                    && strtolower( $mimetype ) != 'application/vnd.openxmlformats-officedocument.wordprocessingml.document'
                    && strtolower( $mimetype ) != 'application/zip'
                    && strtolower( $mimetype ) != 'application/octet-stream'
                    && strtolower( $mimetype ) != 'audio/x-wav'
                )
                    throw new JsonException( '上传文件类型错误' );
                if ( $size >= 10485760 )
                    throw new JsonException( '文件超出限制,最大为10MB,请使用大文件上传接口' );
            } else {
                if ( !preg_match( '/^video.*/', $mimetype ) )
                    throw new JsonException( '上传文件类型错误' );
            }
            $name = Auth::user()->id . $name;
            $folder = $this->getFileUrl( $type );
            if ( Storage::disk( 'public' )->putFileAs( $folder, $file, $name ) )
                return $name;
        }
        throw new JsonException( '上传文件无效' );


    }

    public function getUniqidName()
    {
        return md5( uniqid() . time() );
    }


    public function getPicName( $name, $type = 1 )
    {
        if ( $name )
            $name = $this->getFileUrl( $type ) . $name;
        else
            return '';
        return asset( 'storage/' . $name );
    }

    /**
     * 上传大文件到本地
     *
     * @param $file         文件
     * @param $blobNum      当前片
     * @param $totalBlobNum 总片数
     * @param $fileName     文件名称
     * @param $md5FileName
     *
     * @throws \JsonException
     */
    public function uploadBigFile( $file, $blobNum, $totalBlobNum, $md5FileName, $type )
    {
        if ( $file->isValid() ) {
            $this->file = $file;
            $this->type = $type;
            $this->folder = $this->getFileUrl( $type );
            $this->blobNum = $blobNum;
            $this->totalBlobNum = $totalBlobNum;
            $this->fileName = $md5FileName;
            $this->ext = $file->getClientOriginalExtension();
            $this->moveFile();
            $fullName = $this->fileMerge();
            if ( $this->blobNum == $this->totalBlobNum ) {
                return $fullName;
            } else {
                return false;
            }
        }
        throw new \JsonException( '上传文件无效' );

    }

    //判断是否是最后一块，如果是则进行文件合成并且删除文件块
    private function fileMerge()
    {
        if ( $this->blobNum == $this->totalBlobNum ) {
            $fullName = $this->getUniqidName() . '.' . $this->fileName;
            $file = Storage::disk( 'public' )->path( $this->folder . $fullName );
            for ( $i = 1; $i <= $this->totalBlobNum; $i++ ) {
                $blob = Storage::disk( 'tmp' )->get( $this->folder . $this->fileName . '__' . $i );
                if ( $i == 1 )
                    Storage::disk( 'public' )->append( $this->folder . $fullName, $blob );
                else
                    file_put_contents( $file, $blob, FILE_APPEND );
            }
            $this->deleteFileBlob();
            return $fullName;
        }
    }

    //删除文件块
    private function deleteFileBlob()
    {
        for ( $i = 1; $i <= $this->totalBlobNum; $i++ ) {
            $url = $this->folder . $this->fileName . '__' . $i;
            Storage::disk( 'tmp' )->delete( $url );
        }
    }


    //上传文件
    private function moveFile()
    {
        $file = $this->file;
        if ( $file->isValid() ) {
            $name = $this->fileName . '__' . $this->blobNum;
            $folder = $this->getFileUrl( $this->type );
            Storage::disk( 'tmp' )->delete( $this->folder . $name );
            if ( Storage::disk( 'tmp' )->putFileAs( $folder, $file, $name ) )
                return $name;
        }
        throw new \JsonException( '上传文件无效' );
    }
}
