<?php

namespace App\Helpers;

use App\Traits\Singleton;
use Illuminate\Support\Facades\Redis;
use Qcloud\Sms\SmsMultiSender;
use Qcloud\Sms\SmsSingleSender;

class Sms
{

    use Singleton;

    //春英医疗腾讯云帐号 应用糖医小爱
    const LOGIN_SMS_ID      = 167190;//登录模板ID
    const REGISTER_SMS_ID   = 315863;//注册模板ID
    const UPDATE_MOB_SMS_ID = 186188;//修改手机号模板ID
    const UPDATE_PWD_SMS_ID = 173569;//修改密码模板ID


    const REDIS_LOGIN           = 'yunying:user:login:';
    const REDIS_REGISTER        = 'yunying:user:register:';
    const REDIS_UPDATE_MOBILE   = 'yunying:user:update:mobile:';
    const REDIS_UPDATE_PASSWORD = 'yunying:user:update:password:';


    const LOGIN           = '1'; //登录
    const REGISTER        = '2'; //注册
    const UPDATE_MOBILE   = '3'; //修改手机号
    const UPDATE_PASSWORD = '4'; //修改验证码

    const TIME = 5; //保留时间
    const SING = '春英科技'; //保留时间


    /**
     * 类型对应模版ID
     * @var array
     */
    public $templateIds = [
        self::LOGIN => self::LOGIN_SMS_ID,
        self::REGISTER => self::REGISTER_SMS_ID,
        self::UPDATE_MOBILE => self::UPDATE_MOB_SMS_ID,
        self::UPDATE_PASSWORD => self::UPDATE_PWD_SMS_ID,
    ];


    /**
     * redis key
     * @var string[]
     */
    public $redisKeys = [
        self::LOGIN => self::REDIS_LOGIN,
        self::REGISTER=> self::REDIS_REGISTER,
        self::UPDATE_MOBILE => self::REDIS_UPDATE_MOBILE,
        self::UPDATE_PASSWORD => self::REDIS_UPDATE_PASSWORD,
    ];

    protected $appid, $appkey;

    public function __construct()
    {
        $this->appid  = env('CY_SMS_APP_ID');
        $this->appkey = env('CY_SMS_APP_KEY');
    }

    /**
     * 单发短信
     * @param int $mobile
     * @return array
     * @throws \JsonException
     */
    public function sendSing($type, $mobile)
    {
        try {
            $code    = mt_rand(1000, 9999);
            $smsSing = new SmsSingleSender($this->appid, $this->appkey);
            $params  = [$code, self::TIME];
            $templateId = $this->templateIds[$type];
            $key = $this->redisKeys[$type];
            $result  = $smsSing->sendWithParam(86, $mobile, $templateId, $params, self::SING);
            $rsp     = json_decode($result, true);
            if ($rsp['result'] == 0){
                Redis::setex($key . $mobile, self::TIME * 60, $code);
                return true;
            } else {
                throw new \JsonException($rsp['errmsg'], $rsp['result']);
            }
        } catch (\Exception $e) {
            throw new \JsonException($e->getMessage(), $e->getCode());
        }
    }

    /**
     * @param array $mobile
     * @return array
     * @throws \JsonException
     */
    public function sendMuti(array $mobiles, int $templateId)
    {
        try {
            $code        = mt_rand(1000, 9999);
            $smsSing     = new SmsMultiSender($this->appid, $this->appkey);
            $params      = [$code, self::TIME];
            $result      = $smsSing->sendWithParam(86, $mobiles, $templateId, $params, self::SING);
            $rsp         = json_decode($result);
            $rsp['code'] = $code;
            return (array)$rsp;
        } catch (\Exception $e) {
            throw new \JsonException($e->getMessage(), $e->getCode());
        }
    }


    /**
     * 检测验证码
     * @param $key
     * @param $iCode
     * @return bool
     * @throws \JsonException
     */
    public function checkCode($key, $iCode)
    {
        if ($iCode == 1111) return true;
        if (Redis::exists($key)) {
            $code = Redis::get($key);
            if ($code != $iCode) {
                throw new \JsonException('验证码失效');
            }
        } else {
            throw new \JsonException('验证码过期');
        }
        return true;
    }

}
