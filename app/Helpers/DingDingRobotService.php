<?php

namespace App\Helpers;


use App\Traits\Singleton;
use GuzzleHttp\Client;

class DingDingRobotService
{
    use Singleton;
    /**
     * @param string $content
     * @param string $accessToken
     * @param bool $isAtAll
     * @param array $atMobiles
     * @return mixed|\Psr\Http\Message\ResponseInterface
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function hisErrorSend(array $content, string $accessToken = '', bool $isAtAll = true, array $atMobiles = [])
    {

        if (empty($accessToken)) $accessToken = 'a1ac236da43752e189cc24031c51b79e33e424462b46ea5175b3a6e87f59c90e';
        $params = [
            'msgtype' => 'text',
            'text' => [
                'content' => "his报错\n" . json_encode($content, JSON_UNESCAPED_UNICODE),
            ],
            'at' => [
                'isAtAll' => $isAtAll
            ],
        ];
        if ($atMobiles) {
            $params['at']['atMobiles'] = $atMobiles;
        }

        $url = 'https://oapi.dingtalk.com/robot/send?access_token=' . $accessToken;
        $client = new Client();
        return $client->request('POST', $url, [
            'json'    => $params,
            'headers' => ["Content-Type" => "application/json"]
        ]);
    }


    /**
     * @param array $content
     * @param int $type
     * @param string $accessToken
     * @param bool $isAtAll
     * @param array $atMobiles
     * @return mixed|\Psr\Http\Message\ResponseInterface
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function send($content, int $type, string $accessToken = '', bool $isAtAll = true, array $atMobiles = [])
    {
        switch ($type) {
            case 1:
                $label_content = 'his报错';
                $accessToken = 'a1ac236da43752e189cc24031c51b79e33e424462b46ea5175b3a6e87f59c90e';
                break;
            case 2:
                $label_content = 'his内容';
                $accessToken = '985189fd400a7251189f748434b475018d2b356f9cb82240dd58f64c96fcb14d';
                break;
            case 3:
                $label_content = 'tsx测试';
                $accessToken = '5f6f15a2aa719175ebc123c205d4f5c5c21a3820bcdc4e905bfe1bec79323362';
                break;
            case 4:
                $label_content = '神州视翰';
                $accessToken = 'c46d2605aa05dd64ab5ad32c7caa6696740fc38aac0595d18b9f642bb3229c1d';
                break;
            case 5:
                $label_content = '孕婴风控';
                $accessToken = 'ba27aeed7886dffa0f209e41d8cc799dd74d2080729881a7be09dffe4f202158';
                break;
            default:
                $label_content = '';
                $accessToken = 'a1ac236da43752e189cc24031c51b79e33e424462b46ea5175b3a6e87f59c90e';
                break;
        }

        if (is_array($content)) $content = json_encode($content, JSON_UNESCAPED_UNICODE);
        $params = [
            'msgtype' => 'text',
            'text' => [
                'content' => $label_content . PHP_EOL . $content,
            ],
            'at' => [
                'isAtAll' => $isAtAll
            ],
        ];
        if ($atMobiles) {
            $params['at']['atMobiles'] = $atMobiles;
        }

        $url = 'https://oapi.dingtalk.com/robot/send?access_token=' . $accessToken;
        $client = new Client();
        return $client->request('POST', $url, [
            'json'    => $params,
            'headers' => ["Content-Type" => "application/json"]
        ]);
    }
}
