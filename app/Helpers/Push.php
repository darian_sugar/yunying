<?php

namespace App\Helpers;

use App\Traits\Singleton;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use JPush\Client as JPush;
use JPush\Exceptions\APIRequestException;
use JPush\Exceptions\JPushException;

class Push extends Model
{
    //
    use Singleton;

    protected $appends       = [ 'admin_name', 'to_user_desc' ];
    private   $app_key       = '';
    private   $master_secret = '';
    private   $client        = '';

    protected $code = [
        '10001',//系统消息
    ];

    public function __construct( $type = 1 )
    {
        if( $type == 1 ){
            $this->app_key = env('JPUSH_APP_KEY');
            $this->master_secret = env('JPUSH_MASTER_SECRET');
        }else{
            $this->app_key = getenv('JPUSH_APP_KEY_D');
            $this->master_secret = getenv('JPUSH_MASTER_SECRET_D');
        }
        $this->client = new JPush($this->app_key, $this->master_secret);

    }

    public function sendMsg( $title, $msg, $userId = [], $data = [], $is_all = false )
    {

        if( $is_all == true ){
            $this->sendAll($title, $msg, $data);
        }else{
            if( is_array($userId) && !$userId )
                return false;

            $title = (string)$title;
            $msg = (string)$msg;
            $collection = collect($userId)->unique();
            //别名推送最多一千人
            foreach( $collection->chunk(1000) as $v ){
                $userId = $userId = $v->toArray();
                $userId = array_map(function( $val ){
                    $val = 'alias_' . $val;
                    if( getenv('APP_ENV') != 'production' )
                        $val .= '_test';
                    return $val;
                }, $userId);
                $result = $this->send($userId, $title, $msg, $data);
            }
        }


    }

    public function sendAll( $title, $msg, $data = [] )
    {
        try{
            $push = $this->client->push()
                ->setPlatform('all')
                ->iosNotification(
                    array(
                        'title'    => $title,
                        'subtitle' => $msg
                    ),
                    array(
                        'sound'             => 'sound.caf',
                        'badge'             => '+1',
                        'content-available' => true,
                        'mutable-content'   => true,
                        'category'          => 'jiguang',
                        'extras'            => $data,
                    ))
                ->androidNotification($msg, array(
                    'title'  => $title,
                    'extras' => $data,
                ));
            if( getenv('APP_ENV') != 'production' ){
                $push->addTag('debug')
                    ->options([ 'apns_production' => false ]);//苹果生产环境

            }else{
                $push->addTag('release')
                    ->options([ 'apns_production' => true ]);//苹果正式环境
            }
            $response = $push->send();
            return $response;
        }catch( JPushException $e ){
            //try something else here
            //throw new JsonException($e->getMessage(), $e->getCode());

        }catch( APIRequestException $e ){
            //throw new JsonException($e->getMessage(), $e->getCode());
        }
    }


    public function send( $userId = [], $title, $msg, $data )
    {

        $userId = array_values($userId);
        try{
            $push = $this->client->push()
                ->setPlatform('all')
                ->iosNotification(
                    array(
                        'title'    => $title,
                        'subtitle' => $msg
                    ),
                    array(
                        'sound'             => 'sound.caf',
                        'badge'             => '+1',
                        'content-available' => true,
                        'mutable-content'   => true,
                        'category'          => 'jiguang',
                        'extras'            => $data,
                    ))
                ->androidNotification($msg, array(
                    'title'  => $title,
                    // 'builder_id' => 2,
                    'extras' => $data,
                ))
                ->addAlias($userId);
            if( getenv('APP_ENV') == 'production' )
                $push->options([ 'apns_production' => true ]);//苹果生产环境
            $response = $push->send();
            return $response;
        }catch( JPushException $e ){
            // try something else here
            				throw new \JsonException($e->getMessage(), $e->getCode());

        }catch( APIRequestException $e ){
            				throw new \JsonException($e->getMessage(), $e->getCode());
        }
    }
}
