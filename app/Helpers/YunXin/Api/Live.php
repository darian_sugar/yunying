<?php
/**
 * User: wenxue
 * Date: 18-12-12
 * Time: 上午11:12
 */

namespace App\Helpers\YunXin\Api;


use App\Helpers\YunXin\Exception\YunXinArgExcetption;
use App\Helpers\YunXin\Exception\YunXinBusinessException;
use App\Helpers\YunXin\Exception\YunXinInnerException;
use App\Helpers\YunXin\Exception\YunXinNetworkException;
use GuzzleHttp\Exception\GuzzleException;
use LogicException;

class Live extends Base
{

    const CHANNEL_NAME_LENGTH = 64;
    const CHANNEL_CID_LENGTH = 32;
    const CHANNEL_TYPE = [0, 1, 2];


    /**
     * 验证参数
     *
     * @param       $accid
     * @param       $name
     * @param array $props
     * @param       $icon
     * @param       $token
     * @param       $sign
     * @param       $email
     * @param       $birth
     * @param       $mobile
     * @param       $gender
     * @param       $ex
     *
     * @throws YunXinArgExcetption
     */
    private function verifyInfo($name, $type, $cid = '')
    {
        $type = intval($type);

        if (!$cid || !is_string($cid)) {
            throw new LogicException('cid 不合法！');
        }
        if (strlen($cid) > self::CHANNEL_CID_LENGTH) {
            throw new LogicException('cid长度为' . self::CHANNEL_CID_LENGTH . '字符！');
        }
        if (strlen($name) > self::CHANNEL_NAME_LENGTH) {
            throw new YunXinArgExcetption('直播室最大长度' . self::CHANNEL_NAME_LENGTH . '字符！');
        }
        if (!in_array($type, self::CHANNEL_TYPE)) {
            throw new YunXinArgExcetption('直播室类型不合法');
        }
    }


    /**
     * 创建直播室
     *
     * @param $name 直播室名称
     * @param $type 直播室类型  0 : rtmp, 1 : hls, 2 : http
     *
     * @return mixed
     *              cid    String    频道ID，32位字符串
     *              ctime    Long    创建频道的时间戳
     *              name    String    频道名称
     *              pushUrl    String    推流地址
     *              httpPullUrl    String    http拉流地址
     *              hlsPullUrl    String    hls拉流地址
     *              rtmpPullUrl    String    rtmp拉流地址
     * @throws YunXinArgExcetption
     * @throws YunXinBusinessException
     * @throws YunXinInnerException
     * @throws YunXinNetworkException
     * @throws GuzzleException
     */
    public function create($name, $accid, $type = 0)
    {
        $this->verifyInfo($name, $type, 'create');
        $res = $this->sendRequest('channel/create', array_filter([
            'name' => $name,
            'type' => $type,
        ]), 2);
        return $res['ret'];
    }


    /**
     * 创建IM房间
     *
     * @param     $name
     * @param     $accid
     * @param int $type
     *
     * @return mixed
     * @throws YunXinBusinessException
     * @throws YunXinInnerException
     * @throws YunXinNetworkException
     * @throws GuzzleException
     */
    public function createIm($name, $accid, $type = 0)
    {
        $res = $this->sendRequest('nrtc/createChannel.action', [
            'channelName' => $name,
            'type'        => $type,
            'accid'       => $accid,
        ]);
        return $res['cid'];
    }


    public function deleteIM()
    {
        https://roomserver-dev.netease.im/v1/api/rooms/{id}
    }


    /**
     * 更新直播室
     *
     * @param $name       直播室名称
     * @param $type       直播室类型  0 : rtmp, 1 : hls, 2 : http
     * @param $cid        直播室cid
     * @param $needRecord 1-开启录制； 0-关闭录制
     * @param $format     0-mp4, 1-flv, 2-mp3
     * @param $duration   录制切片时长(分钟)，5~120分钟
     *
     * @return mixed
     * @throws YunXinArgExcetption
     * @throws YunXinBusinessException
     * @throws YunXinInnerException
     * @throws YunXinNetworkException
     * @throws GuzzleException
     */
    public function update($cid, $name, $type, $needRecord = 1, $format = 0, $duration = 120, $filename = '')
    {
        $this->verifyInfo($name, $type, $cid);
        $res = $this->sendRequest('channel/update', array_filter([
            'cid'        => $cid,
            'name'       => $name,
            'type'       => $type,
            'needRecord' => $needRecord,
            'format'     => $format,
            'duration'   => $duration,
            'filename'   => $filename,
        ]), 2);
        return $res['ret'];
    }

    /**
     * 删除直播室
     *
     * @param $cid  直播室cid
     *
     * @return mixed
     * @throws YunXinArgExcetption
     * @throws YunXinBusinessException
     * @throws YunXinInnerException
     * @throws YunXinNetworkException
     * @throws GuzzleException
     */
    public function delete($cid)
    {
        $this->verifyInfo('', '', $cid);
        $res = $this->sendRequest('channel/delete', array_filter([
            'cid' => $cid,
        ]), 2);
        return $res['ret'];
    }

    /**
     * 直播室状态
     *
     * @param $cid  直播室cid
     *
     * @return mixed
     *              ctime    Long    创建频道的时间戳
     *              cid    String    频道ID，32位字符串
     *              name    String    频道名称
     *              status    int    频道状态（0：空闲； 1：直播； 2：禁用； 3：直播录制）
     *              type    int    频道类型 ( 0 : rtmp, 1 : hls, 2 : http)
     *              uid    Long    用户ID，是用户在网易云视频与通信业务的标识，用于与其他用户的业务进行区分。
     *              needRecord    int    1-开启录制； 0-关闭录制
     *              format    int    1-flv； 0-mp4
     *              duration    int    录制切片时长(分钟)，默认120分钟
     *              filename    String    录制后文件名
     * @throws YunXinArgExcetption
     * @throws YunXinBusinessException
     * @throws YunXinInnerException
     * @throws YunXinNetworkException
     * @throws GuzzleException
     */
    public function channelstats($cid)
    {
        $this->verifyInfo('', '', $cid);
        $res = $this->sendRequest('channelstats', array_filter([
            'cid' => $cid,
        ]), 2);
        return $res['ret'];
    }

    /**
     * 获取直播室推流地址
     *
     * @param $cid  直播室cid
     *
     * @return mixed
     *              pushUrl    String    推流地址
     *              httpPullUrl    String    http拉流地址
     *              hlsPullUrl    String    hls拉流地址
     *              rtmpPullUrl    String    rtmp拉流地址
     * @throws YunXinArgExcetption
     * @throws YunXinBusinessException
     * @throws YunXinInnerException
     * @throws YunXinNetworkException
     * @throws GuzzleException
     */
    public function address($cid)
    {
        $this->verifyInfo('', '', $cid);
        $res = $this->sendRequest('address', array_filter([
            'cid' => $cid,
        ]), 2);
        return $res['ret'];
    }

    /**
     * 禁用频道
     *
     * @param $cid  直播室cid
     *
     * @return mixed
     * @throws YunXinArgExcetption
     * @throws YunXinBusinessException
     * @throws YunXinInnerException
     * @throws YunXinNetworkException
     * @throws GuzzleException
     */
    public function pause($cid)
    {
        $this->verifyInfo('', '', $cid);
        $res = $this->sendRequest('channel/pause', array_filter([
            'cid' => $cid,
        ]), 2);
        return $res['ret'];
    }

    /**
     * 批量禁用频道
     *
     * @param $cid  直播室cid
     *
     * @return mixed
     * @throws YunXinArgExcetption
     * @throws YunXinBusinessException
     * @throws YunXinInnerException
     * @throws YunXinNetworkException
     * @throws GuzzleException
     */
    public function mutiPause(array $cid)
    {
        $this->verifyInfo('', '', $cid);
        $res = $this->sendRequest('channellist/pause', array_filter([
            'cid' => json_encode($cid),
        ]), 2);
        return $res['ret'];
    }

    /**
     * 恢复频道
     *
     * @param $cid  直播室cid
     *
     * @return mixed
     * @throws YunXinArgExcetption
     * @throws YunXinBusinessException
     * @throws YunXinInnerException
     * @throws YunXinNetworkException
     * @throws GuzzleException
     */
    public function resume($cid)
    {
        $this->verifyInfo('', '', $cid);
        $res = $this->sendRequest('channel/resume', array_filter([
            'cid' => $cid,
        ]), 2);
        return $res['ret'];
    }


    /**
     * 批量恢复频道
     *
     * @param array $cid 直播室cid
     *
     * @return mixed
     * @throws YunXinArgExcetption
     * @throws YunXinBusinessException
     * @throws YunXinInnerException
     * @throws YunXinNetworkException
     * @throws GuzzleException
     */
    public function mutiResume(array $cid)
    {
        $this->verifyInfo('', '', $cid);
        $res = $this->sendRequest('channellist/resume', array_filter([
            'cid' => json_encode($cid),
        ]), 2);
        return $res['ret'];
    }

    /**
     * 获取录制文件列表
     *
     * @param     $cid     直播室cid
     * @param int $records 单页记录数，默认值为10
     * @param int $pnum    要取第几页，默认值为
     *
     * @return mixed
     *                     videoList    JsonArray    录制视频列表
     *                     video_name    String    录制后文件名，
     *                     orig_video_key    String    视频文件在点播桶中的存储路径
     *                     uid    Long    用户ID，是用户在网易云视频与通信业务的标识，用于与其他用户的业务进行区分。
     *                     vid    Long    视频文件ID
     *                     pnum    Long    当前页
     *                     totalRecords    Long    总记录数
     *                     totalPnum    Long    总页数
     *                     records    Long    单页记录数
     * @throws YunXinArgExcetption
     * @throws YunXinBusinessException
     * @throws YunXinInnerException
     * @throws YunXinNetworkException
     * @throws GuzzleException
     */
    public function videolist($cid, $records = 10, $pnum = 1)
    {

        $this->verifyInfo('', '', $cid);
        $res = $this->sendRequest('videolist', array_filter([
            'cid'     => $cid,
            'records' => $records,
            'pnum'    => $pnum,
        ]), 2);
        return $res['ret'];
    }

    /**
     * 设置频道状态变化回调地址
     *
     * @return mixed
     * @throws YunXinBusinessException
     * @throws YunXinInnerException
     * @throws YunXinNetworkException
     * @throws GuzzleException
     */
    public function setcallback()
    {
        $res = $this->sendRequest('chstatus/setcallback', array_filter([
            'chStatusClk' => getenv('APP_URL') . '/api/live/index',
        ]), 2);
        return $res['ret'];
    }


}
