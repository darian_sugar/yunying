<?php
/**
 * User: wenxue
 * Date: 18-12-12
 * Time: 上午11:12
 */

namespace App\Helpers\YunXin\Api;


use App\Helpers\YunXin\Exception\YunXinArgExcetption;
use App\Helpers\YunXin\Exception\YunXinBusinessException;
use App\Helpers\YunXin\Exception\YunXinInnerException;
use App\Helpers\YunXin\Exception\YunXinNetworkException;
use GuzzleHttp\Exception\GuzzleException;
use LogicException;

class Friend extends Base
{

    const FRIEND_MSG_LIMIT = 256;
    const FRINED_ADD_TYPE = [1, 2, 3, 4];


    /**
     * 验证参数
     *
     * @param       $accid
     * @param       $name
     * @param array $props
     * @param       $icon
     * @param       $token
     * @param       $sign
     * @param       $email
     * @param       $birth
     * @param       $mobile
     * @param       $gender
     * @param       $ex
     *
     * @throws YunXinArgExcetption
     */
    private function verifyInfo($accid, $faccid, $type = 1, $msg = '', $serverex = '')
    {
        $type = intval($type);

        if (!$accid || !is_string($accid)) {
            throw new LogicException('accid 不合法！');
        }
        if (!$faccid || !is_string($faccid)) {
            throw new LogicException('faccid 不合法！');
        }

        if (!in_array($type, self::FRINED_ADD_TYPE)) {
            throw new YunXinArgExcetption('好友类型不合法！');
        }
        if (strlen($msg) > self::FRIEND_MSG_LIMIT) {
            throw new YunXinArgExcetption('请求消息最大长度' . self::FRIEND_MSG_LIMIT . '字符！');
        }
        if (strlen($serverex) > self::FRIEND_MSG_LIMIT) {
            throw new YunXinArgExcetption('扩展字段最大长度' . self::FRIEND_MSG_LIMIT . '字符！');
        }
    }


    /**
     * 添加好友
     *
     * @param        $accid    发起者accid
     * @param        $faccid   对方accid
     * @param int    $type     1直接加好友，2请求加好友，3同意加好友，4拒绝加好友
     * @param string $msg      加好友对应的请求消息，第三方组装，最长256字符
     * @param array  $serverex 服务器端扩展字段，限制长度256 此字段client端只读，server端读写
     *
     * @return mixed
     * @throws YunXinBusinessException
     * @throws YunXinInnerException
     * @throws YunXinNetworkException
     * @throws GuzzleException
     */
    public function add($accid, $faccid, $type = 1, $msg = '', $serverex = '')
    {
        $this->verifyInfo($accid, $faccid, $type, $msg, $serverex);
        $res = $this->sendRequest('friend/add.action', array_filter([
            'accid'    => $accid,
            'faccid'   => $faccid,
            'type'     => $type,
            'msg'      => $msg,
            'serverex' => $serverex,
        ]));
        return $res;
    }

    public function get($accid, $updatetime = 1540029502000)
    {
        $res = $this->sendRequest('friend/get.action', array_filter([
            'accid'      => $accid,
            'updatetime' => $updatetime,
        ]));
        return $res;
    }

}
