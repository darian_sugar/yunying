<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 2019/11/19
 * Time: 18:01
 */

namespace App\Helpers\YunXin\Api;


use App\Helpers\YunXin\Exception\YunXinArgExcetption;
use App\Helpers\YunXin\Exception\YunXinBusinessException;
use App\Helpers\YunXin\Exception\YunXinInnerException;
use App\Helpers\YunXin\Exception\YunXinNetworkException;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;

class AudioAndVideo extends Base
{

    const CHANNEL_NAME_LENGTH = 64;


    /**
     * 验证参数
     *
     * @param $name
     *
     * @throws YunXinArgExcetption
     */
    private function verifyInfo($name)
    {

        if (strlen($name) > self::CHANNEL_NAME_LENGTH) {
            throw new YunXinArgExcetption('房间最大长度' . self::CHANNEL_NAME_LENGTH . '字符！');
        }

    }


    /**
     * 创建IM房间
     *
     * @param     $name
     * @param     $accid
     * @param int $type
     *
     * @return mixed
     * @throws YunXinArgExcetption
     * @throws YunXinBusinessException
     * @throws YunXinInnerException
     * @throws YunXinNetworkException
     * @throws GuzzleException
     */
    public function createIm($name, $accid, $type = 0)
    {
        $this->verifyInfo($name);
        $res = $this->sendRequest('nrtc/createChannel.action', [
            'channelName' => $name,
            'type'        => $type,
            'accid'       => $accid,
        ]);
        return $res['cid'];
    }


    public function deleteIM($roomNum)
    {
        $res = $this->sendRequest('v1/api/rooms/' . $roomNum, [], 3, 'DELETE');
        return $res;
    }


}
