<?php
/**
 * User: salamander
 * Date: 18-12-12
 * Time: 上午11:12
 */

namespace App\Helpers\YunXin\Api;


use App\Helpers\YunXin\Exception\YunXinArgExcetption;
use App\Helpers\YunXin\Exception\YunXinBusinessException;
use App\Helpers\YunXin\Exception\YunXinInnerException;
use App\Helpers\YunXin\Exception\YunXinNetworkException;
use GuzzleHttp\Exception\GuzzleException;

class Team extends Base
{
    const ACCID_LEGAL_LENGTH = 32;
    const NAME_LEGAL_LENGTH = 64;
    const MSG_LEGAL_LENGTH = 150;
    const INTRO_LEGAL_LENGTH = 512;
    const ICON_LEGAL_LENGTH = 1024;

    /**
     * 创建群组 超大群
     *
     * @param        $creator
     * @param        $name
     * @param string $announcement
     * @param string $broadcasturl
     * @param string $ext
     * @param int    $queuelevel
     *
     * @return array
     * @throws YunXinArgExcetption
     * @throws GuzzleException
     * @throws YunXinBusinessException
     * @throws YunXinNetworkException
     */
    public function create($owner, $inviteAccids, $name, $intro = '', $icon = '')
    {

        if (strlen($owner) > self::ACCID_LEGAL_LENGTH) {
            throw new YunXinArgExcetption('Invalid Length');
        }
        if (!$inviteAccids || !is_array($inviteAccids)) {
            throw new YunXinArgExcetption('Invalid type');
        }

        if (strlen($intro) > self::INTRO_LEGAL_LENGTH) {
            throw new YunXinArgExcetption('Invalid Intro Length');
        }

        if (strlen($icon) > self::ICON_LEGAL_LENGTH) {
            throw new YunXinArgExcetption('Invalid Icon Length');
        }

        $res = $this->sendRequest('superteam/create.action', [
            'owner'        => $owner,
            'inviteAccids' => json_encode(array_values($inviteAccids)),
            'tname'        => $name,
            'intro'        => $intro,
            'icon'         => $icon,
        ]);
        return $res['tid'];
    }


    /**
     * 创建高级群
     *
     * @param        $owner
     * @param        $members
     * @param        $name
     * @param string $msg
     * @param int    $magree
     * @param int    $joinmode
     *
     * @return mixed
     * @throws YunXinArgExcetption
     * @throws YunXinBusinessException
     * @throws YunXinInnerException
     * @throws YunXinNetworkException
     * @throws GuzzleException
     */
    public function createSenior($owner, $members, $name, $msg = '邀请入群', $magree = 0, $joinmode = 0)
    {

        if (strlen($owner) > self::ACCID_LEGAL_LENGTH) {
            throw new YunXinArgExcetption('Invalid Length');
        }
        if (!$members || !is_array($members)) {
            throw new YunXinArgExcetption('Invalid type');
        }

        if (strlen($name) > self::NAME_LEGAL_LENGTH) {
            throw new YunXinArgExcetption('Invalid Length');
        }

        if (strlen($msg) > self::MSG_LEGAL_LENGTH) {
            throw new YunXinArgExcetption('Invalid Length');
        }

        $res = $this->sendRequest('team/create.action', [
            'owner'    => $owner,
            'members'  => json_encode(array_values($members)),
            'tname'    => $name,
            'msg'      => $msg,
            'magree'   => $magree,
            'joinmode' => $joinmode,
        ]);
        return $res['tid'];
    }


    /**
     * 邀请人入群
     *
     * @param       $tid          群id
     * @param       $owner        创建者id
     * @param array $inviteAccids 邀请人id
     *
     * @return mixed
     * @throws YunXinArgExcetption
     * @throws YunXinBusinessException
     * @throws YunXinInnerException
     * @throws YunXinNetworkException
     * @throws GuzzleException
     */
    public function invite($tid, $owner, array $inviteAccids)
    {
        if (!$tid) {
            throw new YunXinArgExcetption('Invalid GroupId！');
        }
        if (!$owner) {
            throw new YunXinArgExcetption('Invalid Owner！');
        }
        if (strlen($owner) > self::ACCID_LEGAL_LENGTH) {
            throw new YunXinArgExcetption('Invalid Length');
        }
        if (!$inviteAccids || !is_array($inviteAccids)) {
            throw new YunXinArgExcetption('Invalid type');
        }

        $res = $this->sendRequest('superteam/invite.action', [
            'tid'          => $tid,
            'owner'        => $owner,
            'inviteAccids' => json_encode(array_values($inviteAccids)),
        ]);
        return true;
    }


    /**
     * 踢人出群
     *
     * @param       $tid   群id
     * @param       $owner 群主或者管理员id
     * @param array $kickAccids
     *
     * @return mixed
     * @throws YunXinArgExcetption
     * @throws YunXinBusinessException
     * @throws YunXinInnerException
     * @throws YunXinNetworkException
     * @throws GuzzleException
     */
    public function kick($tid, $owner, array $kickAccids)
    {
        if (!$tid) {
            throw new YunXinArgExcetption('Invalid GroupId！');
        }
        if (!$owner) {
            throw new YunXinArgExcetption('Invalid Owner！');
        }
        if (strlen($owner) > self::ACCID_LEGAL_LENGTH) {
            throw new YunXinArgExcetption('Invalid Length');
        }
        if (!$kickAccids || !is_array($kickAccids)) {
            throw new YunXinArgExcetption('Invalid type');
        }

        $res = $this->sendRequest('superteam/kick.action', [
            'tid'        => $tid,
            'owner'      => $owner,
            'kickAccids' => json_encode(array_values($kickAccids)),
        ]);
        return true;
    }


    /**
     * 更新群信息
     *
     * @param        $tid
     * @param        $owner
     * @param        $name
     * @param string $intro
     * @param string $icon
     *
     * @return mixed
     * @throws YunXinArgExcetption
     * @throws YunXinBusinessException
     * @throws YunXinInnerException
     * @throws YunXinNetworkException
     * @throws GuzzleException
     */
    public function updateTinfo($tid, $owner, $name, $intro = '', $icon = '')
    {

        if (strlen($owner) > self::ACCID_LEGAL_LENGTH) {
            throw new YunXinArgExcetption('Invalid Length');
        }

        if (strlen($intro) > self::INTRO_LEGAL_LENGTH) {
            throw new YunXinArgExcetption('Invalid Intro Length');
        }

        if (strlen($icon) > self::ICON_LEGAL_LENGTH) {
            throw new YunXinArgExcetption('Invalid Icon Length');
        }

        $this->sendRequest('superteam/updateTinfo.action', [
            'tid'   => $tid,
            'owner' => $owner,
            'tname' => $name,
            'intro' => $intro,
            'icon'  => $icon,
        ]);
        return true;
    }


    /**
     * 退群
     *
     * @param $tid
     * @param $userIdaddGroup
     *
     * @return bool
     * @throws YunXinBusinessException
     * @throws YunXinInnerException
     * @throws YunXinNetworkException
     * @throws GuzzleException
     */
    public function leave($tid, $userId)
    {
        $this->sendRequest('superteam/leave.action', [
            'tid'   => $tid,
            'accid' => $userId,
        ]);
        return true;
    }


    /**
     * 解散高级群
     *
     * @param $tid
     * @param $owner
     *
     * @return bool
     * @throws YunXinBusinessException
     * @throws YunXinInnerException
     * @throws YunXinNetworkException
     * @throws GuzzleException
     */
    public function dismiss($tid, $owner)
    {
        $this->sendRequest('team/remove.action', [
            'tid'   => $tid,
            'owner' => $owner,
        ]);
        return true;
    }

}
