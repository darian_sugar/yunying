<?php
/**
 * User: salamander
 * Date: 18-12-12
 * Time: 上午11:12
 */

namespace App\Helpers\YunXin\Api;


use App\Helpers\YunXin\Exception\YunXinArgExcetption;
use App\Helpers\YunXin\Exception\YunXinBusinessException;
use App\Helpers\YunXin\Exception\YunXinInnerException;
use App\Helpers\YunXin\Exception\YunXinNetworkException;
use GuzzleHttp\Exception\GuzzleException;
use LogicException;

class User extends Base
{
    const GET_UINFOS_LIMIT = 200;

    const USER_NAME_LIMIT = 64;

    const USER_PROPS_LIMIT = 1024;

    const USER_ICON_LIMIT = 1024;

    const USER_TOKEN_LIMIT = 128;

    const USER_SIGN_LIMIT = 256;

    const USER_EMAIL_LIMIT = 64;

    const USER_BIRTH_LIMIT = 16;

    const USER_MOBILE_LIMIT = 32;

    const USER_GENDER_TYPES = [0, 1, 2];

    const USER_EX_LIMIT = 1024;


    /**
     * 验证参数
     *
     * @param       $accid
     * @param       $name
     * @param array $props
     * @param       $icon
     * @param       $token
     * @param       $sign
     * @param       $email
     * @param       $birth
     * @param       $mobile
     * @param       $gender
     * @param       $ex
     *
     * @throws YunXinArgExcetption
     */
    private function verifyUserInfo($accid, $name, array $props = [], $icon, $token, $sign,
                                    $email, $birth, $mobile, $gender, $ex)
    {
        $gender = intval($gender);
        $propsStr = json_encode($props);

        if (!$accid || !is_string($accid)) {
            throw new LogicException('accid 不合法！');
        }
        if (strlen($name) > self::USER_NAME_LIMIT) {
            throw new YunXinArgExcetption('用户昵称最大长度' . self::USER_NAME_LIMIT . '字符！');
        }
        if (strlen($propsStr) > self::USER_PROPS_LIMIT) {
            throw new YunXinArgExcetption('用户props最大长度' . self::USER_PROPS_LIMIT . '字符！');
        }
        if (strlen($icon) > self::USER_ICON_LIMIT) {
            throw new YunXinArgExcetption('用户头像URL最大长度' . self::USER_ICON_LIMIT . '字符！');
        }
        if (strlen($token) > self::USER_TOKEN_LIMIT) {
            throw new YunXinArgExcetption('用户token最大长度' . self::USER_TOKEN_LIMIT . '字符！');
        }
        if (strlen($sign) > self::USER_SIGN_LIMIT) {
            throw new YunXinArgExcetption('用户sign最大长度' . self::USER_SIGN_LIMIT . '字符！');
        }
        if (strlen($email) > self::USER_EMAIL_LIMIT) {
            throw new YunXinArgExcetption('用户邮箱最大长度' . self::USER_EMAIL_LIMIT . '字符！');
        }
        if (strlen($birth) > self::USER_BIRTH_LIMIT) {
            throw new YunXinArgExcetption('用户生日最大长度' . self::USER_BIRTH_LIMIT . '字符！');
        }
        if (strlen($mobile) > self::USER_MOBILE_LIMIT) {
            throw new YunXinArgExcetption('用户手机号最大长度' . self::USER_MOBILE_LIMIT . '字符！');
        }
        if (!in_array($gender, self::USER_GENDER_TYPES)) {
            throw new YunXinArgExcetption('用户性别不合法！');
        }
        if (strlen($ex) > self::USER_EX_LIMIT) {
            throw new YunXinArgExcetption('用户名片扩展最大长度' . self::USER_EX_LIMIT . '字符！');
        }
    }


    /**
     * 创建
     * accid    String    是APP内唯一（只允许字母、数字、半角下划线_ @、半角点以及半角-组成，不区分大小写，会统一小写处理，请注意以此接口返回结果中的accid为准）。
     * name    String    否    网易云通信ID昵称，最大长度64字符。
     * props    String    否    json属性，开发者可选填，最大长度1024字符
     * icon    String    否    网易云通信ID头像URL，开发者可选填，最大长度1024
     * token    String    否    网易云通信ID可以指定登录token值，最大长度128字符 并更新，如果未指定，会自动生成token，并在创建成功后返回
     * sign    String    否    用户签名，最大长度256字符
     * email    String    否    用户email，最大长度64字符
     * birth    String    否    用户生日，最大长度16字符
     * mobile    String    否    用户mobile，最大长度32字符，非中国大陆手机号码需要填写国家代码(如美国：+1-xxxxxxxxxx)或地区代码(如香港：+852-xxxxxxxx)
     * gender    int    否    用户性别，0表示未知，1表示男，2女表示女，其它会报参数错误
     * ex    String    否    用户名片扩展字段，最大长度1024字符，用户可自行扩展，建议封装成JSON字符串
     *
     * @return mixed
     * @throws YunXinArgExcetption
     * @throws YunXinBusinessException
     * @throws YunXinInnerException
     * @throws YunXinNetworkException
     * @throws GuzzleException
     *                                              accid    String    是    网易云通信ID，最大长度32字符，必须保证一个
     */
    public function create($accid, $name, $icon = '', $sign = '', array $props = [], $token = '', $email = '', $birth = '',
                           $mobile = '', $gender = 0, $ex = '')
    {
        $this->verifyUserInfo($accid, $name, $props, $icon, $token, $sign,
            $email, $birth, $mobile, $gender, $ex);
        $res = $this->sendRequest('user/create.action', array_filter([
            'accid'  => $accid,
            'name'   => $name,
            'props'  => json_encode($props),
            'icon'   => $icon,
            'token'  => $token,
            'sign'   => $sign,
            'email'  => $email,
            'birth'  => $birth,
            'mobile' => $mobile,
            'gender' => $gender,
            'ex'     => $ex,
        ]));
        return $res['info'];
    }

    /**
     * 网易云通信ID基本信息更新
     *
     * @param string $accid
     * @param array  $props
     * @param string $token
     *
     * @return array
     * @throws YunXinArgExcetption
     * @throws GuzzleException
     * @throws YunXinBusinessException
     * @throws YunXinNetworkException
     */
    public function update($accid, array $props = [], $token = '')
    {
        $this->verifyUserInfo($accid, '', $props, '', $token, '',
            '', '', '', 0, '');

        $res = $this->sendRequest('user/update.action', [
            'accid' => $accid,
            'props' => json_encode($props),
            'token' => $token,
        ]);
        return $res;
    }

    /**
     * 更新并获取新token
     *
     * @param string $accid
     *
     * @return mixed
     * @throws YunXinArgExcetption
     * @throws GuzzleException
     * @throws YunXinBusinessException
     * @throws YunXinNetworkException
     */
    public function refreshToken($accid)
    {
        $this->verifyUserInfo($accid, '', [], '', '', '',
            '', '', '', 0, '');

        $res = $this->sendRequest('user/refreshToken.action', [
            'accid' => $accid,
        ]);
        return $res['info'];
    }

    /**
     * 封禁网易云通信ID
     *
     * @param string $accid
     * @param bool   $kick
     *
     * @return array
     * @throws YunXinArgExcetption
     * @throws GuzzleException
     * @throws YunXinBusinessException
     * @throws YunXinNetworkException
     */
    public function block($accid, $kick = false)
    {
        $this->verifyUserInfo($accid, '', [], '', '', '',
            '', '', '', 0, '');

        $res = $this->sendRequest('user/block.action', [
            'accid'    => $accid,
            'needkick' => $kick,
        ]);
        return $res;
    }

    /**
     * 解禁网易云通信ID
     *
     * @param $accid
     *
     * @return array
     * @throws YunXinArgExcetption
     * @throws GuzzleException
     * @throws YunXinBusinessException
     * @throws YunXinNetworkException
     */
    public function unblock($accid)
    {
        $this->verifyUserInfo($accid, '', [], '', '', '',
            '', '', '', 0, '');

        $res = $this->sendRequest('user/unblock.action', [
            'accid' => $accid,
        ]);
        return $res;
    }

    /**
     * 更新用户名片
     *
     * @param $accid
     * @param $name
     * @param $icon
     * @param $sign
     * @param $email
     * @param $birth
     * @param $mobile
     * @param $gender
     * @param $ex
     *
     * @return array
     * @throws YunXinArgExcetption
     * @throws GuzzleException
     * @throws YunXinBusinessException
     * @throws YunXinNetworkException
     */
    public function updateUserInfo($accid, $name = '', $icon = '', $sign = '', $email = '',
                                   $birth = '', $mobile = '', $gender = '', $ex = '')
    {
        $this->verifyUserInfo($accid, $name, [], $icon, '', $sign,
            $email, $birth, $mobile, $gender, $ex);

        $res = $this->sendRequest('user/updateUinfo.action', array_filter([
            'accid'  => $accid,
            'name'   => $name,
            'icon'   => $icon,
            'sign'   => $sign,
            'email'  => $email,
            'birth'  => $birth,
            'mobile' => $mobile,
            'gender' => $gender,
            'ex'     => $ex,
        ]));
        return $res;
    }

    /**
     * 获取用户名片，可以批量
     *
     * @param array $accids
     *
     * @return mixed
     * @throws YunXinArgExcetption
     * @throws GuzzleException
     * @throws YunXinBusinessException
     * @throws YunXinNetworkException
     */
    public function getUserInfos(array $accids)
    {
        if (empty($accids)) {
            throw new YunXinArgExcetption('查询用户不能为空！');
        }
        if (count($accids) > self::GET_UINFOS_LIMIT) {
            throw new YunXinArgExcetption('查询用户数量超过限制！');
        }
        $res = $this->sendRequest('user/getUinfos.action', [
            'accids' => json_encode($accids)
        ]);
        return $res['uinfos'];
    }
}
