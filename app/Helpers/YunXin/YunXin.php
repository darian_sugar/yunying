<?php

namespace App\Helpers\YunXin;

use App\Traits\Singleton;
use Exception;
use Illuminate\Support\Str;

class YunXin
{
    use Singleton;

    public const ADMIN  = 'admin';
    public const USER   = 'user';
    public const DOCTOR = 'doctor';
    protected $entrance;

    /**
     * __call
     *
     * @param $method
     * @param $args
     *
     * @return mixed
     */
    public function __call( $method, $args )
    {
        return $this->make($method);
    }


    /**
     * __get
     *
     * @param $name
     *
     * @return mixed
     */
    public function __get( $name )
    {
        return $this->make($name);
    }

    /**
     * Make
     *
     * @param $resource
     *
     * @return mixed
     * @throws Exception
     */
    public function make( $resource )
    {
        $class = 'App\\Helpers\\YunXin\\Api\\' . Str::studly($resource);
        if( class_exists($class) ){
            return new $class(getenv('YXKEY'), getenv('YXSECRET'));
        }
        throw new Exception('Wrong method');
    }

}
