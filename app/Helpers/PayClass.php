<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 2019-03-04
 * Time: 10:52
 */

namespace App\Helpers;

use App\Traits\Singleton;
use Yansongda\Pay\Log;
use Yansongda\Pay\Pay;

class PayClass
{
    use Singleton;

    const ORDERTEXT = 10;   //文本
    const ORDERAUDICE = 20; //音频
    const ORDERVIDEO = 30;  //视频
    const ORDERAPPOINT = 40;//线下预约
    const ORDERGOODS = 50;  //商品

    const PAYTYPE = [
        0 => '未支付',
        1 => '支付宝',
        2 => '微信',
    ];
    public $ali_config = [
        'notify_url'     => '',
        'return_url'     => '',
        'app_id'         => '',
        'ali_public_key' => '',
        'private_key'    => '',
        'log'            => [
            // optional
            'file'     => '../storage/logs/alipay.log',
            'level'    => 'info', // 建议生产环境等级调整为 info，开发环境为 debug
            'type'     => 'single', // optional, 可选 daily.
            'max_file' => 30, // optional, 当 type 为 daily 时有效，默认 30 天
        ],
        'http'           => [
            // optional
            'timeout'         => 5.0,
            'connect_timeout' => 5.0,
            // 更多配置项请参考 [Guzzle](https://guzzle-cn.readthedocs.io/zh_CN/latest/request-options.html)
        ],
        //'mode'           => 'dev', // optional,设置此参数，将进入沙箱模式
    ];
    public $wx_config = [
        'appid'       => '', // APP APPID
        'app_id'      => '', // 公众号 APPID
        'miniapp_id'  => '', // 小程序 APPID
        'mch_id'      => '',
        'key'         => '',
        'notify_url'  => '',
        'cert_client' => './hospitalcert/apiclient_cert.pem', // optional，退款等情况时用到
        'cert_key'    => './hospitalcert/apiclient_key.pem',// optional，退款等情况时用到
        'log'         => [
            // optional
            'file'     => '../storage/logs/wechat.log',
            'level'    => 'info', // 建议生产环境等级调整为 info，开发环境为 debug
            'type'     => 'single', // optional, 可选 daily.
            'max_file' => 30, // optional, 当 type 为 daily 时有效，默认 30 天
        ],
        'http'        => [
            // optional
            'timeout'         => 5.0,
            'connect_timeout' => 5.0,
            // 更多配置项请参考 [Guzzle](https://guzzle-cn.readthedocs.io/zh_CN/latest/request-options.html)
        ],
        //'mode'        => 'dev', // optional, dev/hk;当为 `hk` 时，为香港 gateway。
    ];

    protected $app_id, $ali_public_key, $notify_url, $private_key, $appid, $mch_id, $key, $wx_notify_url, $cert_client, $cert_key;

    /**
     * PayClass constructor.
     *
     */
    public function __construct()
    {
        $this->app_id = env( 'ALI_APP_ID', '' );
        $this->ali_public_key = env( 'ALI_PUBLIC_KEY', '' );
        $this->private_key = trim( chunk_split( env( 'ALI_PRI_KEY', '' ), 64, "\n" ) );
        $this->appid = env( 'WX_APP_ID', '' );
        $this->mch_id = env( 'WX_MCH_ID', '' );
        $this->key = env( 'WX_KEY', '' );
        $this->notify_url = env( 'APP_URL', '' ) . '/api/pay/aliNotify';
        $this->wx_notify_url = env( 'APP_URL', '' ) . '/api/pay/wxNotify';
        $this->cert_client = env( 'WX_CERT' );
        $this->cert_key = env( 'WX_CERT_KEY' );

    }

    /**
     * @param int $type 1支付宝 2 微信
     *
     * @return array
     */
    public function getConfig( $type = 1 )
    {
        if ( $type == 1 ) {
            $config = $this->ali_config;
            $config['app_id'] = $this->app_id;
            $config['ali_public_key'] = $this->ali_public_key;
            $config['private_key'] = $this->private_key;
            $config['notify_url'] = $this->notify_url;
        } else {
            $config = $this->wx_config;
            $config['appid'] = $this->appid;
            $config['mch_id'] = $this->mch_id;
            $config['key'] = $this->key;
            $config['notify_url'] = $this->wx_notify_url;
            $config['cert_client'] = $this->cert_client;
            $config['cert_key'] = $this->cert_key;
        }
        return $config;
    }

    /**
     * @param int $type 1支付宝 2 微信
     *
     * @return array
     */
    public function getConfigs( $type = 1 )
    {
        if ( $type == 1 ) {
            $config = $this->ali_config;
            $config['app_id'] = env( 'ALI_APP_ID', '' );
            $config['ali_public_key'] = env( 'ALI_PUBLIC_KEY', '' );
            $config['notify_url'] = env( 'APP_URL', '' ) . '/api/pay/aliNotify';
            $config['private_key'] = trim( chunk_split( env( 'ALI_PRI_KEY', '' ), 64, "\n" ) );
        } else {
            $config = $this->wx_config;
            $config['appid'] = env( 'WX_APP_ID', '' );
            $config['mch_id'] = env( 'WX_MCH_ID', '' );
            $config['key'] = env( 'WX_KEY', '' );
            $config['notify_url'] = env( 'APP_URL', '' ) . '/api/pay/wxNotify';
        }
        return $config;
    }


    /**
     * 支付宝微信app支付，$type=1为支付宝，=2为微信
     *
     * @param $out_trade_no
     * @param $total          支付宝单位 元，微信单位分
     * @param $subject
     * @param $body
     * @param $type
     * @param $passbackParams 附加参数 默认空数组 %5B%5D
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function pay( $out_trade_no, $total, $subject, $body, $type = 1, $passbackParams = '%5B%5D' )
    {
        $config = $this->getConfig( $type );
        $total = 0.01;
        if ( $type == 1 ) {
            $order = [
                'out_trade_no'    => $out_trade_no,
                'total_amount'    => $total,
                'subject'         => $subject,
                'body'            => $body,
                'passback_params' => $passbackParams,
            ];
            $app_pay = Pay::alipay( $config )->app( $order );
        } else {
            $order = [
                'out_trade_no' => $out_trade_no,
                'body'         => $body,
                'total_fee'    => $total * 100,
                'attach'       => $passbackParams,
            ];
            $app_pay = Pay::wechat( $config )->app( $order );
        }
        return $app_pay->getContent();
    }

    public function refund( $out_trade_no, $total, $amount, $type )
    {
        if ( $type == 1 ) {
            $this->aliRefund( $out_trade_no, $amount );
        } else {
            $this->wxRefund( $out_trade_no, $total, $amount, '原支付退款' );;
        }
    }


    /**
     * 支付宝网页支付
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index()
    {
        $date = time();
        $order = [
            'out_trade_no' => $date,
            'total_amount' => '1',
            'subject'      => 'test subject - 测试',
        ];
        file_put_contents( storage_path( 'time.txt' ), $date, FILE_APPEND );
        $alipay = Pay::alipay( $this->wx_config )->web( $order );

        return $alipay;// laravel 框架中请直接 `return $alipay`
    }


    public function wxScan( $orderNumber, $totalFee, $userName, $body, $type, $attach )
    {
        $config = $this->getConfig( 2 );
        $order = [
            'out_trade_no' => $orderNumber,
            'body'         => 'subject-测试',
            'total_fee'    => '1',
            'attach'       => $attach,
        ];
        $alipay = Pay::wechat( $config )->scan( $order );
        return $alipay;// laravel 框架中请直接 `return $alipay`
    }

    /**
     * 支付宝退款
     * @return \Yansongda\Supports\Collection
     * @throws \Yansongda\Pay\Exceptions\GatewayException
     * @throws \Yansongda\Pay\Exceptions\InvalidConfigException
     * @throws \Yansongda\Pay\Exceptions\InvalidSignException
     */
    public function aliRefund( $out_trade_no, $rsfund_amount )
    {
        $order = [
            'out_trade_no'  => $out_trade_no,
            'refund_amount' => $rsfund_amount,
        ];
        $config = $this->getConfig( 1 );
        $refund = Pay::alipay( $config )->refund( $order );
        return $refund;// laravel 框架中请直接 `return $alipay`
    }

    public function aliNotifyRefund( $out_trade_no, $rsfund_amount )
    {
        $order = [
            'out_trade_no'  => $out_trade_no,
            'refund_amount' => $rsfund_amount,
        ];
        $config = $this->getConfig( 1 );
        Pay::alipay( $config )->refund( $order );
        //        return $refund;// laravel 框架中请直接 `return $alipay`
    }


    /**
     * 微信APP退款
     * 如果您需要退 APP/小程序 的订单，请传入参数：['type' => 'app']/['type' => 'miniapp']
     * @return \Yansongda\Supports\Collection
     * @throws \Yansongda\Pay\Exceptions\GatewayException
     * @throws \Yansongda\Pay\Exceptions\InvalidArgumentException
     * @throws \Yansongda\Pay\Exceptions\InvalidSignException
     */
    public function wxRefund( $out_trade_no, $total_fee, $refund_fee, $refund_desc )
    {
        $order = [
            'out_trade_no'  => $out_trade_no,
            'out_refund_no' => time(),
            'total_fee'     => $total_fee,
            'refund_fee'    => $refund_fee,
            'refund_desc'   => $refund_desc,
            'type'          => 'app',
        ];
        $config = $this->getConfig( 2 );
        $refund = Pay::wechat( $config )->refund( $order );
        return $refund;
    }


    /**
     * 支付宝同步通知
     * @throws \Yansongda\Pay\Exceptions\InvalidConfigException
     * @throws \Yansongda\Pay\Exceptions\InvalidSignException
     */
    public function ali_return()
    {
        $data = Pay::alipay( $this->ali_config )->verify(); // 是的，验签就这么简单！

        // 订单号：$data->out_trade_no
        // 支付宝交易号：$data->trade_no
        // 订单总金额：$data->total_amount
    }


    /**
     * 验证支付宝异步通知
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function ali_notify()
    {
        $alipay = Pay::alipay( $this->ali_config );

        try {
            $data = $alipay->verify(); // 是的，验签就这么简单！

            // 请自行对 trade_status 进行判断及其它逻辑进行判断，在支付宝的业务通知中，只有交易通知状态为 TRADE_SUCCESS 或 TRADE_FINISHED 时，支付宝才会认定为买家付款成功。
            // 1、商户需要验证该通知数据中的out_trade_no是否为商户系统中创建的订单号；
            // 2、判断total_amount是否确实为该订单的实际金额（即商户订单创建时的金额）；
            // 3、校验通知中的seller_id（或者seller_email) 是否为out_trade_no这笔单据的对应的操作方（有的时候，一个商户可能有多个seller_id/seller_email）；
            // 4、验证app_id是否为该商户本身。
            // 5、其它业务逻辑情况
            file_put_contents( storage_path() . 'time.txt', $data->get( 'trade_status' ) );
            Log::debug( 'Alipay notify', $data->all() );
        } catch ( \Exception $e ) {
            // $e->getMessage();
        }

        return $alipay->success();// laravel 框架中请直接 `return $alipay->success()`
    }


    /**
     * 验证微信异步通知
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \Yansongda\Pay\Exceptions\InvalidArgumentException
     */
    public function wx_notify()
    {
        $pay = Pay::wechat( $this->wx_config );

        try {
            $data = $pay->verify(); // 是的，验签就这么简单！

            file_put_contents( storage_path() . 'time.txt', $data->all() );
        } catch ( \Exception $e ) {
            // $e->getMessage();
        }

        return $pay->success();// laravel 框架中请直接 `return $pay->success()`
    }

    public function getOrderNumber( $orderType, $userId )
    {
        return $orderType . date( 'Ymd' ) . $userId . uniqid();
    }


    /**
     * 点对点打款
     *
     * @param int $type 1 支付宝 2  微信
     * @param     $out_trade_no
     * @param     $account
     *
     * @return \Yansongda\Supports\Collection
     */
    public function transfer( $type = 1, $out_trade_no, $account, $amount )
    {
        if ( $type == 1 ) {
            $order = [
                'out_biz_no'    => $out_trade_no,   //订单号
                'payee_type'    => 'ALIPAY_LOGONID',//
                'payee_account' => $account,        //支付宝账户
                'amount'        => $amount,          //金额 单位元
            ];

            $config = $this->getConfig( 1 );
            $refund = Pay::alipay( $config )->transfer( $order );
            return $refund;
        } else {
            $order = [
                'partner_trade_no' => $out_trade_no,              //商户订单号
                'openid'           => '',                        //收款人的openid
                'check_name'       => 'NO_CHECK',            //NO_CHECK：不校验真实姓名\FORCE_CHECK：强校验真实姓名
                // 're_user_name'=>'张三',              //check_name为 FORCE_CHECK 校验实名的时候必须提交
                'amount'           => $amount * 100,                       //企业付款金额，单位为分
                'desc'             => '帐户提现',                  //付款说明
            ];
            $config = $this->getConfig( 2 );

            $transfer = Pay::wechat( $config )->transfer( $order );
            return $transfer;

        }

    }


}
