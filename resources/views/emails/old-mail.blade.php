<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>用户测试报告</title>
    <link rel="stylesheet" href="/bootstrap/css/bootstrap.min.css">
</head>
<body>
    <h2>用户测试报告</h2>
    <hr>
    患者姓名：{{$patientName}}
    <br>
    患者测试结果<a href="{{$view}}">点击查看</a>
</body>
</html>