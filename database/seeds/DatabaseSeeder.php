<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        //factory(\App\Models\Goods::class, 20)->create();
        //factory(\App\Models\GoodsPhoto::class, 50)->create();
        //factory(\App\Models\GoodsSpec::class, 50)->create();
        //factory(\App\Models\GoodsBrand::class, 20)->create();
        factory( \App\Models\GoodsCate::class, 20 )->create();
    }
}
