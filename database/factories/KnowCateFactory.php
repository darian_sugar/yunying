<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use Illuminate\Support\Str;
use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(\App\Models\KnowCate::class, function( Faker $faker ){
    $data = [
        'name'       => $faker->name,
        'p_id'       => rand(0, 4),
        'channel_id' => rand(0, 50),

    ];
    if( $data['channel_id'] == 0 )
        $data['type'] = 1;

    return $data;
});
