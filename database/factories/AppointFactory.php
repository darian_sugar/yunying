<?php

/** @var Factory $factory */

use App\Models\InquiryOrder;
use Illuminate\Database\Eloquent\Factory;
use Illuminate\Support\Str;
use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(\App\Models\Appointment::class, function( Faker $faker ){
    return [
        'user_id'      => rand(1, 20),//int(11) NOT NULL DEFAULT '0' COMMENT '用户id',
        'doctor_id'    => rand(1, 20),//int(11) NOT NULL DEFAULT '0' COMMENT '医生id',
        'order_number' => str_random(20),//varchar(255) NOT NULL DEFAULT '' COMMENT '订单编号',
        'method_pay'   => rand(1, 2),//tinyint(1) NOT NULL DEFAULT '0' COMMENT '1 支付宝 2 微信',
        'total_amount' => rand(1, 1000),//int(11) NOT NULL DEFAULT '0' COMMENT '总金额单位分',
        'pay_amount'   => rand(1, 1000),//int(11) NOT NULL DEFAULT '0' COMMENT '实际支付金额单位分',
        'pay_status'   => 1,
    ];
});
