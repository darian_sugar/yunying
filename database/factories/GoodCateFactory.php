<?php

/** @var Factory $factory */

use App\Models\Goods;
use App\Models\InquiryOrder;
use Illuminate\Database\Eloquent\Factory;
use Illuminate\Support\Str;
use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define( \App\Models\GoodsCate::class, function ( Faker $faker ) {
    return [
        'p_id'    => rand( 1, 10 ),
        'name'    => $faker->word,
        'icon'    => $faker->word,
        'desc'    => $faker->sentence,
        'status'  => 1,
        'is_show' => rand( 1, 2 ),
    ];
} );
