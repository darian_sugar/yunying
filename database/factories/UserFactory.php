<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use Illuminate\Support\Str;
use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(\App\Models\User::class, function( Faker $faker ){
    return [
        'nickname' => $faker->word,
        'age'      => rand(10, 30),
        'sex'      => rand(0, 3),
        'password' => '$2y$10$ACzMwqhnspa2c0l5jTtk7.z/AAGDUJqchNW0jvRjEmFH9RQMKyhZy', // password
        'mobile'   => $faker->phoneNumber,
        'identity' => rand(0, 3),
        'birthday' => $faker->date(),
        'head_pic' => Str::random(),
    ];
});
