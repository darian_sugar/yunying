<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use Illuminate\Support\Str;
use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(\App\Models\KnowFile::class, function( Faker $faker ){
    $data = [
        'cate_id'   => 5,
        'title'     => $faker->word,
        'content'   => $faker->sentence,
        'image'     => uniqid() . '.jpg',
        'author_id' => 2,
        'admin_id'  => 1,
        'read'      => rand(10, 20),
    ];
    return $data;
});
