<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use Illuminate\Support\Str;
use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(\App\Models\KnowCourse::class, function( Faker $faker ){
    $data = [
        'cate_id'      => rand(4, 5),
        'image'        => Str::random(),
        'title'        => $faker->sentence,
        'content'      => $faker->paragraph,
        'author_id'    => rand(1, 20),
        'price'        => 0,
        'product_id'   => 0,
        'read'         => rand(10, 200),
        'status'       => 1,
        'check_status' => 1,
        'check_uid'    => 1,
        'check_time'   => $faker->dateTime,
        'is_top'       => rand(0, 1),
        'duration'     => rand(10, 500),
        'admin_id'     => rand(1, 10),
    ];
    return $data;
});
