<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use Illuminate\Support\Str;
use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(\App\Models\Doctor::class, function( Faker $faker ){
    return [
        'account'     => $faker->word,
        'password'    => '$2y$10$ACzMwqhnspa2c0l5jTtk7.z/AAGDUJqchNW0jvRjEmFH9RQMKyhZy', // password
        'head_pic'    => Str::random(),
        'name'        => $faker->name,
        'sex'         => rand(0, 2),
        'ask_status'  => 1,
        'type'        => rand(1, 2),
        'is_top'      => rand(0, 1),
        't_id'        => rand(0, 5),
        'text_price'  => 0.01,
        'text_param'  => 15,
        'video_price' => 0.01,
        'video_param' => 15,
        'audio_price' => 0.01,
        'audio_param' => 15,
        'intro'       => $faker->sentence,
    ];
});
