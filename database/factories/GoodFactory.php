<?php

/** @var Factory $factory */

use App\Models\Goods;
use App\Models\InquiryOrder;
use Illuminate\Database\Eloquent\Factory;
use Illuminate\Support\Str;
use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define( Goods::class, function ( Faker $faker ) {
    return [
        'cate_id'      => rand( 1, 20 ),
        'name'         => $faker->word,
        'code'         => uniqid(),
        'intro'        => $faker->sentence,
        'company'      => $faker->company,
        'medicine_no'  => $faker->creditCardNumber,
        '69_no'        => $faker->creditCardNumber,
        'price'        => rand( 10, 20 ),
        'brand_id'     => rand( 10, 20 ),
        'retail_price' => rand( 1, 9 ),
        'stock'        => rand( 121, 888 ),
    ];
} );
